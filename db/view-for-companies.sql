CREATE OR REPLACE VIEW merged.companies as
WITH last_version AS (
    SELECT
       row1.ver,
       row1.id,
       row1.created_at AS updated_at,
       row1.name,
       row1.notes
    FROM public."companies" row1
    LEFT JOIN public."companies" row2 ON row1.ver < row2.ver AND row1.id = row2.id
    WHERE row2.ver IS NULL AND row1.deleted IS NULL
), first_version AS (
    SELECT
        row1.id,
        row1.created_at
    FROM public."companies" row1
    LEFT JOIN public."companies" row2 ON row1.ver > row2.ver AND row1.id = row2.id
    WHERE row2.ver IS NULL
)
SELECT lv.id,
       lv.ver,
       lv.updated_at,
       lv.name,
       lv.notes,
       fv.created_at
FROM last_version lv
LEFT JOIN first_version fv ON fv.id = lv.id;
