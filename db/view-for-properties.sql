CREATE OR REPLACE VIEW merged.properties as
WITH last_version AS (
    SELECT
       row1.ver,
       row1.id,
       row1.created_at AS updated_at,
       row1.name,
       row1.notes,
       row1.building_administrator_id,
       row1.building_administrator_ver
    FROM public."properties" row1
    LEFT JOIN public."properties" row2 ON row1.ver < row2.ver AND row1.id = row2.id
    WHERE row2.ver IS NULL AND row1.deleted IS NULL
), first_version AS (
    SELECT
        row1.id,
        row1.created_at
    FROM public."properties" row1
    LEFT JOIN public."properties" row2 ON row1.ver > row2.ver AND row1.id = row2.id
    WHERE row2.ver IS NULL
)
SELECT lv.id,
       lv.ver,
       lv.updated_at,
       lv.name,
       lv.notes,
       fv.created_at,
       lv.building_administrator_id,
       lv.building_administrator_ver
FROM last_version lv
LEFT JOIN first_version fv ON fv.id = lv.id;
