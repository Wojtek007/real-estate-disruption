CREATE OR REPLACE VIEW public.edges_view as
WITH last_version AS (
    SELECT
       edge1.ver,
       edge1.id,
       edge1.created_at AS updated_at,
       edge1.name,
       edge1.from_state_id,
       edge1.from_state_ver,
       edge1.to_state_id,
       edge1.to_state_ver
    FROM public."Edges" edge1
    LEFT JOIN public."Edges" edge2 ON edge1.ver < edge2.ver AND edge1.id = edge2.id
    WHERE edge2.ver IS NULL AND edge1.deleted IS NULL
), first_version AS (
    SELECT
        edge1.id,
        edge1.created_at
    FROM public."Edges" edge1
    LEFT JOIN public."Edges" edge2 ON edge1.ver > edge2.ver AND edge1.id = edge2.id
    WHERE edge2.ver IS NULL
)
SELECT lv.id,
       lv.ver,
       lv.updated_at,
       lv.name,
       lv.from_state_id,
       lv.from_state_ver,
       lv.to_state_id,
       lv.to_state_ver,
       fv.created_at
FROM last_version lv
LEFT JOIN first_version fv ON fv.id = lv.id;
COMMENT ON VIEW example is 'an example view:)';