import CreateForm from "@modules/forms/create-form";
import { getPropertiesStaticPaths } from "@modules/property-details/service";
import { staticFetch } from "@lib/apolloClient";
import MenuShell from "@modules/app-shell/pages/menu-shell";

// // This function gets called at build time
// export async function getStaticPaths() {
//   return getPropertiesStaticPaths();
// }

// // This also gets called at build time
// export async function getStaticProps({ params: { propertyId } }) {
//   const { properties } = await staticFetch(GET_PROPERTY("query"), {
//     variables: { propertyId },
//   });

//   if (!properties[0]) {
//     return {
//       notFound: true,
//     };
//   }

//   return { props: { property: properties[0] }, revalidate: 60 };
// }

const Page = () => (
  <MenuShell>
    <CreateForm />
  </MenuShell>
);

export default Page;
