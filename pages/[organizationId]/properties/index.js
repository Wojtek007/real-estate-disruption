import PropertiesList from "@modules/properties-list/full-page";
import MenuShell from "@modules/app-shell/pages/menu-shell";

export default function Home() {
  return (
    <MenuShell>
      <PropertiesList />
    </MenuShell>
  );
}
