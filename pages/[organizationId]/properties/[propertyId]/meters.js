import MenuShell from "@modules/app-shell/pages/menu-shell";
import PropertyMeters, {
  SUBSCRIBE_PROPERTY,
  SUBSCRIBE_UTILITY_TYPES,
} from "@modules/property-details/pages/property-meters";
import { getPropertiesStaticPaths } from "@modules/property-details/service";
import { staticFetch } from "@lib/apolloClient";

// // This function gets called at build time
// export async function getStaticPaths() {
//   return getPropertiesStaticPaths();
// }

// // This also gets called at build time
// export async function getStaticProps({ params: { propertyId } }) {
//   const { properties } = await staticFetch(SUBSCRIBE_PROPERTY("query"), {
//     variables: { propertyId },
//   });
//   const { utilityTypes } = await staticFetch(SUBSCRIBE_UTILITY_TYPES("query"), {
//     variables: { propertyId },
//   });



//   return { props: { property: properties[0], utilityTypes }, revalidate: 60 };
// }
const Example = ({ property, utilityTypes }) => {
  return (
    <MenuShell>
      <PropertyMeters property={property} utilityTypes={utilityTypes} />
    </MenuShell>
  );
};

export default Example;
