import MenuShell from "@modules/app-shell/pages/menu-shell";
import PropertyContracts, {
  SUBSCRIBE_PROPERTY,
  GET_CONTRACT_CATEGORIES,
} from "@modules/property-details/pages/property-contracts";
import { getPropertiesStaticPaths } from "@modules/property-details/service";
import { staticFetch } from "@lib/apolloClient";

// // This function gets called at build time
// export async function getStaticPaths() {
//   return getPropertiesStaticPaths();
// }

// // This also gets called at build time
// export async function getStaticProps({ params: { propertyId } }) {
//   console.log("getStaticProps", propertyId);
//   const { properties } = await staticFetch(SUBSCRIBE_PROPERTY("query"), {
//     variables: { propertyId },
//   });
//   const { contractCategories } = await staticFetch(GET_CONTRACT_CATEGORIES);



//   return {
//     props: { property: properties[0], contractCategories },
//     revalidate: 60,
//   };
// }

const Example = ({ property, contractCategories }) => {
  return (
    <MenuShell>
      <PropertyContracts
        property={property}
        contractCategories={contractCategories}
      />
    </MenuShell>
  );
};

export default Example;
