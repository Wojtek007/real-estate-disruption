import { useRouter } from "next/router";
import MenuShell from "@modules/app-shell/pages/menu-shell";
import PropertyInfo, {
  GET_PROPERTY,
} from "@modules/property-details/pages/property-info";
import { getPropertiesStaticPaths } from "@modules/property-details/service";
import { staticFetch } from "@lib/apolloClient";

// // This function gets called at build time
// export async function getStaticPaths() {
//   return getPropertiesStaticPaths();
// }

// // This also gets called at build time
// export async function getStaticProps({ params: { propertyId } }) {
//   const { properties } = await staticFetch(GET_PROPERTY("query"), {
//     variables: { propertyId },
//   });



//   return { props: { property: properties[0] }, revalidate: 60 };
// }

const Example = ({ property }) => {
  return (
    <MenuShell>
      <PropertyInfo property={property} />
    </MenuShell>
  );
};

export default Example;
