import { useRouter } from "next/router";
import Link from "next/link";
import { useSubscription } from "@apollo/react-hooks";
import gql from "graphql-tag";
import FormBuilder from "@modules/form-builder/engine/form-builder";
import { propertyConfig } from "@modules/form-builder/configs/domain-config";
import { setPath } from "@modules/form-builder/redux/form-reducer.js";

const getObjectQuery = (() => {
  const config = propertyConfig();
  console.log("getObjectQuery 111", config);
  const { objectName, objectTable } = config.objectDef;

  const fieldIds = config.formSections
    .map((section) =>
      section.fields
        .map((fieldsRow) =>
          fieldsRow.map((field) =>
            field.type === "objectDropdown" ? `${field.id}Id` : field.id
          )
        )
        .flat()
    )
    .flat();
  console.log("getObjectQuery 22222", fieldIds);
  const desiredQueryObject = fieldIds.reduce((acc, fieldId) => {
    const queryPath = fieldId.split("/").filter((chunk) => chunk !== "data");

    return setPath(acc, queryPath, null);
  }, {});
  console.log("getObjectQuery 33333", desiredQueryObject);

  const transformToGqlFields = (objToTransform) =>
    "id\n" +
    Object.entries(objToTransform)
      .map(([key, value]) =>
        value ? `${key} {\n${transformToGqlFields(value)} \n}` : key
      )
      .join("\n");

  const GET_OBJECT_QUERY = gql`
    query MyQuery123412341234($${objectName}Id: uuid) {
    ${objectTable}(where: { id: { _eq: $${objectName}Id } }) {
        ${transformToGqlFields(desiredQueryObject)}         
      }
    }
  `;

  return GET_OBJECT_QUERY;
})();

function Page() {
  const router = useRouter();
  const { propertyId } = router.query;

  const { loading, error, data } = useSubscription(getObjectQuery, {
    variables: { propertyId },
  });

  const property = data?.properties[0] ?? {};
  console.log("property", property);

  if (loading) {
    return <span>Wczytuję...</span>;
  }
  if (error) {
    console.error(error);
    return <span>Błąd! {error?.message ?? JSON.stringify(error)}</span>;
  }

  if (!property) {
    return <span>Nieruchomość o tym identyfikatorze nie istnieje</span>;
  }

  const transformToMutableForm = (object) =>
    Object.fromEntries(
      Object.entries(object)
        .filter(([key]) => key !== "__typename")
        .map(([key, value]) =>
          value instanceof Object
            ? [key, { data: transformToMutableForm(value) }]
            : [key, value]
        )
    );

  console.log("property editigblae", transformToMutableForm(property));

  return (
    <FormBuilder
      config={propertyConfig()}
      initialObject={transformToMutableForm(property)}
    />
  );
}

export default Page;
