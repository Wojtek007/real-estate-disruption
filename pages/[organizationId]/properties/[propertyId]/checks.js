import MenuShell from "@modules/app-shell/pages/menu-shell";
import PropertyChecks, {
  SUBSCRIBE_PROPERTY,
} from "@modules/property-details/pages/property-checks";
import { getPropertiesStaticPaths } from "@modules/property-details/service";
import { staticFetch } from "@lib/apolloClient";

// // This function gets called at build time
// export async function getStaticPaths() {
//   return getPropertiesStaticPaths();
// }

// // This also gets called at build time
// export async function getStaticProps({ params: { propertyId } }) {
//   const { properties } = await staticFetch(SUBSCRIBE_PROPERTY("query"), {
//     variables: { propertyId },
//   });



//   return { props: { property: properties[0] }, revalidate: 60 };
// }
const Example = ({ property }) => {
  return (
    <MenuShell>
      <PropertyChecks property={property} />
    </MenuShell>
  );
};

export default Example;
