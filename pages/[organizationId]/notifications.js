import NotificationsCenter, {
  GET_NOTIFICATIONS_CONTRACTS,
  GET_NOTIFICATIONS_CHECKS,
  GET_NOTIFICATIONS_DEFECTS
} from "@modules/notifications-center";
import {
  PROPERTIES_LIST,
} from "@modules/notifications-center/components-smart/properties-list";
import MenuShell from "@modules/app-shell/pages/menu-shell";

import { getOrganizationsStaticPaths } from "@modules/property-details/service";
import { staticFetch } from "@lib/apolloClient";

// // This function gets called at build time
// export async function getStaticPaths() {
//   return getOrganizationsStaticPaths();
// }

// // This also gets called at build time
// export async function getStaticProps({ params: { organizationId } }) {
//   // console.log('QUERY', GET_NOTIFICATIONS_CONTRACTS("query", organizationId))
//   // console.log('QUERY', GET_NOTIFICATIONS_CONTRACTS("query", organizationId))
//   // var startTime = performance.now()
//   let [
//     { notifications: contractsNotification },
//     { notifications: checksNotification },
//     { notifications: defectsNotification },
//     { properties }
//   ] = await Promise.all([
//     staticFetch(GET_NOTIFICATIONS_CONTRACTS("query", organizationId)),
//     staticFetch(GET_NOTIFICATIONS_CHECKS("query", organizationId)),
//     staticFetch(GET_NOTIFICATIONS_DEFECTS("query", organizationId)),
//     staticFetch(PROPERTIES_LIST("query"))
//   ]);
//   // var endTime = performance.now()

//   // console.log(`Call to fetch static notifications took ${(endTime - startTime) / 1000} seconds`)

//   // if (!notifications) {
//   //   return {
//   //     notFound: true,
//   //   };
//   // }

//   return {
//     props: {
//       notifications: [...contractsNotification, ...checksNotification, ...defectsNotification],
//       properties
//     }, revalidate: 60
//   };
// }

export default function Home({ notifications, properties }) {
  console.log('static-notifications', notifications)
  return (
    <MenuShell>
      <NotificationsCenter notifications={notifications} properties={properties} />
    </MenuShell>
  );
}
