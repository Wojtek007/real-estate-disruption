import Contacts from "@modules/contacts";
import MenuShell from "@modules/app-shell/pages/menu-shell";

export default function Home() {
  return (
    <MenuShell>
      <Contacts />
    </MenuShell>
  );
}
