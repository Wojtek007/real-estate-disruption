import MeterCard from "../../components/details/meter-card";
import DetailsTemplate from "../../components/details/details-template";
import {
  FireIcon,
  BeakerIcon,
  LightningBoltIcon,
} from "@heroicons/react/outline";
import {
  CheckIcon,
  AdjustmentsIcon,
  CalendarIcon,
  ReceiptRefundIcon,
} from "@heroicons/react/solid";

const meters = [
  {
    id: 1,
    name: "Prąd",
    rentAdvance: 320.32,
    stat: -124.23,
    icon: LightningBoltIcon,
    // stan: "niedopłata",

    periods: [
      {
        fromDate: "1 stycznia 2020",
        toDate: "31 marca 2020",
        timeline: [
          {
            id: 2,
            content: "Początkowy stan liczników",
            target: "2156.23",
            href: "#",
            date: "22.05.2020",
            datetime: "2020-01-01",
            icon: AdjustmentsIcon,
            iconBackground: "bg-blue-500",
          },
          {
            id: 1,
            content: "Przewidywany rachunek",
            target: "230.58 zł",
            href: "#",
            date: "20.06.2021",
            datetime: "2020-09-20",
            icon: CalendarIcon,
            iconBackground: "bg-gray-400",
          },
          {
            id: 2,
            content: "Końcowy stan liczników",
            target: "2311.55",
            href: "#",
            date: "29.03.2020",
            datetime: "2020-09-22",
            icon: AdjustmentsIcon,
            iconBackground: "bg-blue-500",
          },
          {
            id: 5,
            content: "Otrzymano rachunek",
            target: "254.34 zł",
            href: "#",
            date: "28.06.2019",
            datetime: "2020-10-04",
            icon: ReceiptRefundIcon,
            iconBackground: "bg-yellow-500",
          },
          {
            id: 3,
            content: "Opłacony rachunek",
            target: "312.44 zł",
            href: "#",
            date: "28.06.2020",
            datetime: "2020-09-28",
            icon: CheckIcon,
            iconBackground: "bg-green-500",
          },
        ],
      },
    ],
  },
  {
    id: 2,
    name: "Woda zimna",
    rentAdvance: 110,
    stat: 2.16,
    icon: BeakerIcon,
    // stan: "nadpłata",
    change: "5.4",
    changeType: "increase",
  },
  {
    id: 3,
    name: "Woda ciepła",
    stat: 0,
    icon: BeakerIcon,
    change: "53.2",
    changeType: "decrease",
  },
  {
    id: 4,
    name: "Gaz",
    stat: -15,
    icon: FireIcon,
    change: "53.2",
    changeType: "decrease",
  },
  {
    id: 5,
    name: "Ogrzewanie",
    stat: 125,
    icon: FireIcon,
    change: "53.2",
    changeType: "decrease",
  },
];

export default function Example() {
  return (
    <DetailsTemplate>
      <div>
        {/* <h3 className="text-lg leading-6 font-medium text-gray-900">
        Last 30 days
      </h3> */}

        <dl className="grid grid-cols-1 gap-5 ">
          {/* <dl className="grid grid-cols-1 gap-5 sm:grid-cols-2 lg:grid-cols-3"> */}

          {meters.map((meter) => (
            <MeterCard meter={meter} />
          ))}
        </dl>
      </div>
    </DetailsTemplate>
  );
}
