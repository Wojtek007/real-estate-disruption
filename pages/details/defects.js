import DefectCard from "../../components/details/defect-card";
import DetailsTemplate from "../../components/details/details-template";

export default function Example() {
  return (
    <DetailsTemplate>
      <DefectCard />
    </DetailsTemplate>
  );
}
