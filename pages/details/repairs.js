import RepairCard from "../../components/details/repair-card";
import DetailsTemplate from "../../components/details/details-template";

const repairs = [
  {
    id: 1,
    name: "Malowanie ścian w salonie",
    state: "Wykonywane",
    reporter: "Bradley Cooper",
    cost: 150,
    startDate: "2020-01-07",
    startDateFull: "7 stycznia 2020",
    notificationDate: "2020-01-07",
    notificationDateFull: "7 stycznia 2020",
    tasks: [
      {
        name: "Kupić farbę",
        cost: 29,
        plannedDate: "2020-01-07",
        plannedDateFull: "7 stycznia 2020",
        executor: "Janek and Co.",
      },
      {
        name: "Zadzwonnić do Pana Janka i sprawdzić czy ma gzymsy od sufitu, żeby dobrze trzymały",
        cost: 33,
        plannedDate: "2020-01-07",
        plannedDateFull: "7 stycznia 2020",
        executor: "Janek and Co.",
      },
    ],
  },
  {
    id: 2,
    name: "Czyszczenie piekarnika",
    state: "Planowane",
    reporter: "Bradley Cooper",
    cost: 250,
    startDate: "2020-01-07",
    startDateFull: "7 stycznia 2020",
    notificationDate: "2020-01-07",
    notificationDateFull: "7 stycznia 2020",
    tasks: [
      {
        name: "Zadwonić po firmę ",
        cost: 0,
        plannedDate: "2020-01-07",
        plannedDateFull: "7 stycznia 2020",
        executor: "Janek and Co.",
      },
      {
        name: "Sprawdzić rezultat",
        cost: 0,
        plannedDate: "2020-01-07",
        plannedDateFull: "7 stycznia 2020",
        executor: "Janek and Co.",
      },
      {
        name: "Zapłacić",
        cost: 344,
        plannedDate: "2020-01-07",
        plannedDateFull: "7 stycznia 2020",
        executor: "Janek and Co.",
      },
    ],
  },
  {
    id: 3,
    name: "Zasadzenine lasu",
    state: "Zgłoszone",
    reporter: "Bradley Cooper",
    cost: 350,

    notificationDate: "2020-01-14",
    notificationDateFull: "January 14, 2020",
  },
];

export default function Example() {
  return (
    <DetailsTemplate>
      {repairs.map((repair) => (
        <RepairCard repair={repair} />
      ))}
    </DetailsTemplate>
  );
}
