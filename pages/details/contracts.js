import DetailsTemplate from "../../components/details/details-template";
import ContractsCard from "../../components/details/contracts-card";

export default function Example() {
  const rentContracts = [
    {
      id: 123,
      person: { name: "Jaylon Brown" },
      type: "Protokół przekazania",
      startDate: "7 Lutego 2021",
      status: "Podpisana",
    },
    {
      id: 123,
      person: { name: "Jaylon Brown" },
      type: "Umowa najmu",
      startDate: "7 Lutego 2021",
      endDate: "7 Lutego 2022",
      status: "Obowiązuje",
    },
    {
      id: 232,
      person: { name: "Ricardo Cooper" },
      type: "Protokół odbioru",
      startDate: "7 Stycznia 2021",
      status: "Podpisana",
    },
    {
      id: 232,
      person: { name: "Ricardo Cooper" },
      type: "Rozwiązanie",
      startDate: "7 Stycznia 2021",
      status: "Podpisana",
    },
    {
      id: 3434,
      person: { name: "Ricardo Cooper" },
      type: "Aneks",
      startDate: "7 Pażdziernika 2020",
      status: "Przerwana",
    },
    {
      id: 454,
      person: { name: "Ricardo Cooper" },
      type: "Umowa najmu",
      startDate: "22 Marca 2020",
      endDate: "22 Marca 2021",
      status: "Przerwana",
    },
    {
      id: 5656,
      person: { name: "Ricardo Cooper" },
      type: "Umowa najmu",
      startDate: "22 Marca 2019",
      endDate: "22 Marca 2020",
      status: "Zakończona",
    },
  ];
  const managementContracts = [
    {
      id: 123,
      person: { name: "Tyrone Mings" },
      type: "Umowa zarządzania",
      startDate: "7 Lutego 2021",
      endDate: "Nieokreślony",
      status: "Obowiązuje",
    },
    {
      id: 232,
      person: { name: "Tyrone Mings" },
      type: "Pełnomocnictwo",
      startDate: "7 Stycznia 2021",
      endDate: "Nieokreślony",
      status: "Obowiązuje",
    },
    {
      id: 3434,
      person: { name: "Tyrone Mings" },
      type: "Aneks",
      startDate: "7 Pażdziernika 2020",
      status: "Podpisana",
    },
  ];
  const insurances = [
    {
      id: 123,
      company: { name: "Lemonade" },
      type: "Zniszczenia najemcy",
      startDate: "1 Kwietnia 2020",
      endDate: "1 Kwietnia 2022",
      status: "Obowiązuje",
    },
    {
      id: 232,
      company: { name: "Lemonade" },
      type: "Podpalenie",
      startDate: "1 Kwietnia 2020",
      endDate: "1 Kwietnia 2022",
      status: "Obowiązuje",
    },
    {
      id: 123,
      company: { name: "Lemonade" },
      type: "Zniszczenia najemcy",
      startDate: "1 Kwietnia 2018",
      endDate: "1 Kwietnia 2020",
      status: "Zakończona",
    },
  ];

  return (
    <DetailsTemplate>
      <ContractsCard
        title="Umowy najmu mieszkania"
        contracts={rentContracts}
        showButton
      />
      <ContractsCard
        title="Umowy zarządzania mieszkaniem"
        contracts={managementContracts}
        showButton
      />
      <ContractsCard title="Ubezpieczenia" contracts={insurances} showButton />
    </DetailsTemplate>
  );
}
