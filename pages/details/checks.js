import CheckCard from "../../components/details/check-card";
import DetailsTemplate from "../../components/details/details-template";

const checks = [
  {
    id: 1,
    title: "Sprawdzenie piecyków",
    period: "co roczne",
    state: "notification",
    notifcationDate: "7 maja 2021",
  },

  {
    id: 2,
    title: "Sprawdzenie kominu",
    period: "co 2 lata",
    state: "planned",
    notifcationDate: "9 maja 2021",
    plannedDate: "12 maja 2021",
    responsiblePerson: { name: "Grzegorz Przeglądny" },
  },
  {
    id: 3,
    title: "Sprawdzenie prądu",
    period: "co pół roku",
    state: "done",
    notifcationDate: "1 maja 2021",
    plannedDate: "3 maja 2021",
    doneDate: "4 maja 2021",
    responsiblePerson: { name: "Tomasz Przeglądny" },
    issues: [],
  },
];

export default function Example() {
  return (
    <DetailsTemplate>
      <div className="">
        <ul className="grid grid-cols-1 gap-5">
          {/*divide-y divide-gray-200*/}
          {checks.map((check) => (
            <CheckCard check={check} />
          ))}
        </ul>
      </div>
      {/* <CheckCard title="Sprawdzenie liczników elektrycznnych" />
        <CheckCard title="Sprawdzenie liczników wody" /> */}
    </DetailsTemplate>
  );
}
