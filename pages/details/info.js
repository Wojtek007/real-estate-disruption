import InformationCard from "../../components/cards/information-card";
import DetailsTemplate from "../../components/details/details-template";

export default function Example() {
  const apartmentInfo = [
    { label: "Adres", value: "ul. Sienna 72a/203, 00-833 Warszawa" },
    { label: "Powierzchnia", value: "80 m2" },
    { label: "Księga wieczysta", value: "WAW-234/2321/2323" },
    { label: "Numer polisy", value: "2345-6676-575" },
    { label: "Kod wejścia", value: "23K232" },
    { label: "Miejsce garażowe", value: "-3/56" },
    {
      label: "Notatki",
      value:
        "Fugiat ipsum ipsum deserunt culpa aute sint do nostrud anim incididunt cillum culpa consequat. Excepteur qui ipsum aliquip consequat sint. Sit id mollit nulla mollit nostrud in ea officia proident. Irure nostrud pariatur mollit ad adipisicing reprehenderit deserunt qui eu.",
    },
  ];

  const administratorInfo = [
    { label: "Nazwa firmy", value: "SB Starówka", id: "company" },
    {
      label: "Adres",
      value: "ul Krogulska 2, 05-530 Górna wioska",
    },
    { label: "Email", value: "moc@gazeta.pl" },
    { label: "Telefon", value: "+48 777 888 999" },
    { label: "Hydraulik", desc: "Tomasz Zrury", value: "+48 777 888 999" },
    { label: "Nadzorca", desc: "Grzegorz Zorny", value: "+48 777 888 999" },
    {
      label: "Notatki",
      value:
        "Fugiat ipsum ipsum deserunt culpa aute sint do nostrud anim incididunt cillum culpa consequat. Excepteur qui ipsum aliquip consequat sint. Sit id mollit nulla mollit nostrud in ea officia proident. Irure nostrud pariatur mollit ad adipisicing reprehenderit deserunt qui eu.",
    },
  ];
  const tenantInfo = [
    { label: "Imie i nazwisko", value: "Margarett Tacher", id: "person" },
    {
      label: "Adres zameldowania",
      value: "ul Broniewskiego 2, 05-530 Góra Kalwaria",
    },
    { label: "Numer konta", value: "1231 2313 4544 1231 2313 4544" },
    { label: "Email", value: "moc@gazeta.pl" },
    { label: "Telefon", value: "+48 777 888 999" },
    { label: "Pesel", value: "89120810101" },
    {
      label: "Notatki",
      value:
        "Fugiat ipsum ipsum deserunt culpa aute sint do nostrud anim incididunt cillum culpa consequat. Excepteur qui ipsum aliquip consequat sint. Sit id mollit nulla mollit nostrud in ea officia proident. Irure nostrud pariatur mollit ad adipisicing reprehenderit deserunt qui eu.",
    },
  ];
  const ownerInfo = [
    { label: "Imie i nazwisko", value: "Józef Piłsudski", id: "person" },
    {
      label: "Adres zamieszkania",
      value: "ul Gończa 2, 05-530 Góra Kalwaria",
    },
    { label: "Numer konta", value: "1231 2313 4544 1231 2313 4544" },
    { label: "Email", value: "siła@gazeta.pl" },
    { label: "Telefon", value: "+48 777 888 999" },
    { label: "Pesel", value: "89120810101" },
    {
      label: "Notatki",
      value:
        "Fugiat ipsum ipsum deserunt culpa aute sint do nostrud anim incididunt cillum culpa consequat. Excepteur qui ipsum aliquip consequat sint. Sit id mollit nulla mollit nostrud in ea officia proident. Irure nostrud pariatur mollit ad adipisicing reprehenderit deserunt qui eu.",
    },
  ];
  return (
    <DetailsTemplate>
      <InformationCard title="Dane mieszkania" data={apartmentInfo} />
      <InformationCard title="Dane najemcy" data={tenantInfo} />
      <InformationCard title="Dane administracji" data={administratorInfo} />
      <InformationCard title="Dane właściciela" data={ownerInfo} />
    </DetailsTemplate>
  );
}
