import "tailwindcss/tailwind.css";
import { useEffect } from 'react';

import { useFetchUser } from "../lib/user";
import { withApollo } from "../lib/withApollo";

import SlideOver from "../modules/details-slide-overs/engine/slide-over";
import Login from "../components/Auth/Login";
import "react-datepicker/dist/react-datepicker.css";
import "../modules/form-builder/inputs/data-picker.css";
import * as moment from "moment";

function MyApp({ Component, pageProps }) {
  const { user, loading, ...rest } = useFetchUser({ required: true });
  console.log("AUTH: user, loading , rest", user, loading, rest);
  useEffect(() => {
    document.body.className = 'overflow-x-hidden';
  });

  if (loading) {
    return <div>Test bezpieczeństwa...</div>;
  }
  if (!loading && !user) {
    return <Login />;
  }

  moment.locale("pl");
  return (
    <>
      <Component {...pageProps} user={user} />
      <SlideOver />
    </>
  );
}

export default withApollo({ ssr: true })(MyApp);
