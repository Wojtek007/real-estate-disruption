SET check_function_bodies = false;
CREATE TABLE public.companies (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    name text NOT NULL,
    address_street text,
    address_street_number text,
    address_apartment_number text,
    postal_code text,
    city text,
    country text,
    region text,
    email text,
    phone text,
    notes text,
    nip text,
    organization_id uuid NOT NULL,
    ver integer NOT NULL,
    deleted boolean,
    created_at timestamp with time zone DEFAULT now()
);
CREATE TABLE public.properties (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    name text NOT NULL,
    address_street text,
    address_street_number text,
    address_apartment_number text,
    postal_code text,
    city text,
    flat_area text,
    land_register_number text,
    policy_number text,
    building_entrance_code text,
    garage_place text,
    notes text,
    country text,
    region text,
    estate_entrance_code text,
    owner_id uuid,
    tenant_id uuid,
    building_administrator_id uuid,
    orgainzation_id uuid NOT NULL,
    created_at date DEFAULT now() NOT NULL,
    ver integer NOT NULL,
    deleted boolean,
    building_administrator_ver integer
);
CREATE SEQUENCE public.companies_ver_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.companies_ver_seq OWNED BY public.companies.ver;
CREATE TABLE public.employees (
    person_id uuid NOT NULL,
    company_id uuid NOT NULL,
    role text,
    organization_id uuid NOT NULL
);
CREATE TABLE public.organizations (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    name text NOT NULL
);
CREATE TABLE public.people (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    firstname text,
    surname text NOT NULL,
    nickname text,
    bank_account_number text,
    email text,
    phone text,
    notes text,
    pesel text,
    organization_id uuid NOT NULL
);
CREATE SEQUENCE public.properties_ver_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.properties_ver_seq OWNED BY public.properties.ver;
CREATE VIEW public.properties_view AS
 WITH last_version AS (
         SELECT row1.ver,
            row1.id,
            row1.created_at AS updated_at,
            row1.name,
            row1.notes,
            row1.building_administrator_id,
            row1.building_administrator_ver
           FROM (public.properties row1
             LEFT JOIN public.properties row2 ON (((row1.ver < row2.ver) AND (row1.id = row2.id))))
          WHERE ((row2.ver IS NULL) AND (row1.deleted IS NULL))
        ), first_version AS (
         SELECT row1.id,
            row1.created_at
           FROM (public.properties row1
             LEFT JOIN public.properties row2 ON (((row1.ver > row2.ver) AND (row1.id = row2.id))))
          WHERE (row2.ver IS NULL)
        )
 SELECT lv.id,
    lv.ver,
    lv.updated_at,
    lv.name,
    lv.notes,
    fv.created_at,
    lv.building_administrator_id,
    lv.building_administrator_ver
   FROM (last_version lv
     LEFT JOIN first_version fv ON ((fv.id = lv.id)));
CREATE TABLE public.users (
    id text NOT NULL,
    name text NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    last_seen timestamp with time zone,
    organization_id uuid
);
ALTER TABLE ONLY public.companies ALTER COLUMN ver SET DEFAULT nextval('public.companies_ver_seq'::regclass);
ALTER TABLE ONLY public.properties ALTER COLUMN ver SET DEFAULT nextval('public.properties_ver_seq'::regclass);
ALTER TABLE ONLY public.properties
    ADD CONSTRAINT apartments_pkey PRIMARY KEY (id, ver);
ALTER TABLE ONLY public.companies
    ADD CONSTRAINT companies_pkey PRIMARY KEY (id, ver);
ALTER TABLE ONLY public.employees
    ADD CONSTRAINT employees_pkey PRIMARY KEY (person_id, company_id);
ALTER TABLE ONLY public.organizations
    ADD CONSTRAINT organizations_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.people
    ADD CONSTRAINT people_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.companies
    ADD CONSTRAINT companies_organization_id_fkey FOREIGN KEY (organization_id) REFERENCES public.organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.employees
    ADD CONSTRAINT employees_person_id_fkey FOREIGN KEY (person_id) REFERENCES public.people(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.people
    ADD CONSTRAINT people_organization_id_fkey FOREIGN KEY (organization_id) REFERENCES public.organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.properties
    ADD CONSTRAINT properties_building_administrator_ver_building_administrator FOREIGN KEY (building_administrator_ver, building_administrator_id) REFERENCES public.companies(ver, id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.properties
    ADD CONSTRAINT properties_orgainzation_id_fkey FOREIGN KEY (orgainzation_id) REFERENCES public.organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.properties
    ADD CONSTRAINT properties_owner_fkey FOREIGN KEY (owner_id) REFERENCES public.people(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.properties
    ADD CONSTRAINT properties_tenant_fkey FOREIGN KEY (tenant_id) REFERENCES public.people(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_organization_id_fkey FOREIGN KEY (organization_id) REFERENCES public.organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
