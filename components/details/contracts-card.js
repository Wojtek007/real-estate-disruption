import {
  ClipboardCheckIcon,
  ClipboardIcon,
  ClipboardCopyIcon,
  XCircleIcon,
} from "@heroicons/react/solid";
import Link from "next/link";
import { useRouter } from "next/router";

const applications = [
  {
    applicant: {
      name: "Ricardo Cooper",
      email: "ricardo.cooper@example.com",
      imageUrl:
        "https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80",
    },
    date: "2020-01-07",
    dateFull: "7 Stycznia 2020 - 7 stycznia 2021",
    stageIcon: (
      <XCircleIcon
        className="flex-shrink-0 mr-1.5 h-5 w-5 text-red-400"
        aria-hidden="true"
      />
    ),
    stage: "Rozwiązany",
    href: "#",
  },
  {
    applicant: {
      name: "Ricardo Cooper",
      email: "ricardo.cooper@example.com",
      imageUrl:
        "https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80",
    },
    date: "2020-01-07",
    dateFull: "7 Stycznia 2020 - 7 stycznia 2021",
    stageIcon: (
      <ClipboardIcon
        className="flex-shrink-0 mr-1.5 h-5 w-5 text-yellow-400"
        aria-hidden="true"
      />
    ),
    stage: "Wygenerowane",
    href: "#",
  },
  {
    applicant: {
      name: "Kristen Ramos",
      email: "kristen.ramos@example.com",
      imageUrl:
        "https://images.unsplash.com/photo-1550525811-e5869dd03032?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80",
    },
    date: "2020-01-07",
    dateFull: "7 Stycznia 2020 - 7 stycznia 2021",
    stageIcon: (
      <ClipboardCopyIcon
        className="flex-shrink-0 mr-1.5 h-5 w-5 text-yellow-400"
        aria-hidden="true"
      />
    ),
    stage: "Wysłane",
    href: "#",
  },
  {
    applicant: {
      name: "Ted Fox",
      email: "ted.fox@example.com",
      imageUrl:
        "https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80",
    },
    date: "2020-01-07",
    dateFull: "7 Stycznia 2020 - 7 stycznia 2021",
    stageIcon: (
      <ClipboardCheckIcon
        className="flex-shrink-0 mr-1.5 h-5 w-5 text-green-400"
        aria-hidden="true"
      />
    ),
    stage: "Podpisane",
    href: "#",
  },
];

const getStatusIcon = ({ type, status }) => {
  switch (status) {
    case "Obowiązuje":
      return (
        <ClipboardCheckIcon
          className="flex-shrink-0 mr-1.5 h-5 w-5 text-green-400"
          aria-hidden="true"
        />
      );
    case "Podpisana":
      return ["Rozwiązanie", "Protokół odbioru"].includes(type) ? (
        <ClipboardCheckIcon
          className="flex-shrink-0 mr-1.5 h-5 w-5 text-red-400"
          aria-hidden="true"
        />
      ) : (
        <ClipboardCheckIcon
          className="flex-shrink-0 mr-1.5 h-5 w-5 text-green-400"
          aria-hidden="true"
        />
      );
    case "Przerwana":
      return (
        <XCircleIcon
          className="flex-shrink-0 mr-1.5 h-5 w-5 text-red-400"
          aria-hidden="true"
        />
      );
    case "Przerwana":
      return (
        <XCircleIcon
          className="flex-shrink-0 mr-1.5 h-5 w-5 text-yellow-400"
          aria-hidden="true"
        />
      );
    case "Zakończona":
      return (
        <XCircleIcon
          className="flex-shrink-0 mr-1.5 h-5 w-5 text-yellow-400"
          aria-hidden="true"
        />
      );

    default:
      break;
  }
};

export default function Example({
  title = "Job Postings",
  desc,
  button = "Wrzuć dokument",
  contracts = [],
  showButton,
}) {
  const router = useRouter();
  return (
    <div className="bg-white shadow overflow-hidden sm:rounded-lg">
      <div className="px-4 py-5 bg-white border-b border-gray-200 sm:px-6">
        <div className="-ml-4 -mt-4 flex justify-between items-center flex-wrap sm:flex-nowrap">
          <div className="ml-4 mt-4">
            <h3 className="text-lg leading-6 font-medium text-gray-900">
              {title}
            </h3>
            {desc && <p className="mt-1 text-sm text-gray-500">{desc}</p>}
          </div>
          {showButton && (
            <div className="ml-4 mt-4 flex-shrink-0">
              <button
                type="button"
                className="relative inline-flex items-center px-4 py-2 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              >
                {button}
              </button>
            </div>
          )}
        </div>
      </div>
      <ul className="divide-y divide-gray-200">
        {contracts.map((contract) => (
          <li key={contract.id}>
            <Link
              href={{
                pathname: router.pathname,
                query: { panel: "contract" },
              }}
              scroll={false}
            >
              <a href={contract.href} className="block hover:bg-gray-50">
                <div className="flex items-center px-4 py-4 sm:px-6">
                  <div className="min-w-0 flex-1 flex items-center">
                    {/* <div className="flex-shrink-0">
                    <img
                      className="h-12 w-12 rounded-full"
                      src={application.applicant.imageUrl}
                      alt=""
                    />
                  </div> */}
                    <div className="min-w-0 flex-1 px-4 md:grid md:grid-cols-2 md:gap-4">
                      <div>
                        <p className="text-sm font-medium text-indigo-600 truncate">
                          <Link
                            href={{
                              pathname: router.pathname,
                              query: { panel: "person" },
                            }}
                            scroll={false}
                          >
                            {contract.person?.name ?? contract.company?.name}
                          </Link>
                        </p>

                        <p className="mt-2 flex items-center text-sm text-gray-500">
                          <ClipboardIcon
                            className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
                            aria-hidden="true"
                          />
                          <span className="truncate">{contract.type}</span>
                        </p>
                      </div>
                      <div className="hidden md:block">
                        <div>
                          <p className="text-sm text-gray-900">
                            <time dateTime={contract.startDate}>
                              {contract.startDate}
                            </time>
                            {contract.endDate && " - "}
                            {contract.endDate && (
                              <time dateTime={contract.endDate}>
                                {contract.endDate}
                              </time>
                            )}
                          </p>
                          <p className="mt-2 flex items-center text-sm text-gray-500">
                            {getStatusIcon(contract)}
                            {contract.status}
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="ml-4 flex-shrink-0">
                    <a
                      href="#"
                      className="font-medium text-indigo-600 hover:text-indigo-500"
                    >
                      Pobierz
                    </a>
                  </div>
                </div>
              </a>
            </Link>
          </li>
        ))}
      </ul>
      <div>
        <a
          href="#"
          className="block bg-gray-50 text-sm font-medium text-gray-500 text-center px-4 py-4 hover:text-gray-700 sm:rounded-b-lg"
        >
          Pokaż starsze dokumenty
        </a>
      </div>
    </div>
  );
}
