/* This example requires Tailwind CSS v2.0+ */
import { Fragment } from "react";
import { Disclosure, Menu, Transition } from "@headlessui/react";
import { BellIcon, MenuIcon, XIcon } from "@heroicons/react/outline";

import Navbar from "./top-navbar";
import Header from "./header";
import SideMenu from "./appartment-menu";

export default function Example({ children }) {
  return (
    <div className="min-h-screen bg-gray-100">
      <Navbar />
      <div className="py-10">
        <header>
          <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
            <Header
              breadcrumbs={["Mieszkania", "Zarządzane"]}
              title="Sienna 72a/203"
            />
          </div>
        </header>
        <main>
          <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
            {/* Replace with your content */}
            <div className="px-4 py-8 sm:px-0">
              <div className="lg:grid lg:grid-cols-12 lg:gap-x-5">
                <SideMenu />
                <div className="space-y-6 sm:px-6 lg:px-0 lg:col-span-9">
                  {children}
                </div>
              </div>
            </div>
            {/* /End replace */}
          </div>
        </main>
      </div>
    </div>
  );
}
