/* This example requires Tailwind CSS v2.0+ */
import { CalendarIcon,CashIcon,UsersIcon } from "@heroicons/react/solid";

export default function Example({ repair }) {
  return (
    <div>
      <div className="bg-white shadow overflow-hidden sm:rounded-md ">
        <ul className="divide-y divide-gray-200">
          <li key={repair.id}>
            <a href="#" className="block hover:bg-gray-50">
              <div className="px-4 py-4 sm:px-6">
                <div className="flex items-center justify-between">
                  <p className="text-md font-medium text-indigo-600 truncate">
                    {repair.name}
                  </p>
                  <div className="ml-2 flex-shrink-0 flex">
                    <p className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                      {repair.state}
                    </p>
                  </div>
                </div>
                <div className="mt-2 sm:flex sm:justify-between">
                  <div className="sm:flex">
                    <p className="flex items-center text-sm text-gray-500">
                      <UsersIcon
                        className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
                        aria-hidden="true"
                      />
                      {repair.reporter}
                    </p>
                    <p className="mt-2 flex items-center text-sm text-gray-500 sm:mt-0 sm:ml-6">
                      <CashIcon
                        className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
                        aria-hidden="true"
                      />
                      {repair.cost?.toFixed(2)} zł
                    </p>
                  </div>
                  <div className="mt-2 flex items-center text-sm text-gray-500 sm:mt-0">
                    <CalendarIcon
                      className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
                      aria-hidden="true"
                    />
                    {repair.startDate ? (
                      <p>
                        Rozpoczęcie{" "}
                        <time dateTime={repair.startDate}>
                          {repair.startDateFull}
                        </time>
                      </p>
                    ) : (
                      <p>
                        Przypomnienie{" "}
                        <time dateTime={repair.notificationDate}>
                          {repair.notificationDateFull}
                        </time>
                      </p>
                    )}
                  </div>
                </div>
              </div>
            </a>
          </li>
        </ul>
      </div>
      <div className="ml-6">
        {repair.tasks && <Tasks tasks={repair.tasks} />}
      </div>
    </div>
  );
}

const plans = [
  {
    name: "Startup",
    priceMonthly: 29,
    priceYearly: 290,
    limit: "Up to 5 active job postings",
  },
  {
    name: "Business",
    priceMonthly: 99,
    priceYearly: 990,
    limit: "Up to 25 active job postings",
  },
  {
    name: "Enterprise",
    priceMonthly: 249,
    priceYearly: 2490,
    limit: "Unlimited active job postings",
  },
];

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export function Tasks({ tasks }) {
  return (
    <div>
      <h3 className="sr-only">Pricing plans</h3>
      <div className="relative bg-white rounded-md -space-y-px">
        {tasks.map((task,taskIdx) => (
          <div
            key={task.name}
            value={task}
            className={classNames(
              taskIdx === 0 ? "rounded-tl-md rounded-tr-md" : "",
              taskIdx === tasks.length - 1 ? "rounded-bl-md rounded-br-md" : "",
              task.checked
                ? "bg-indigo-50 border-indigo-200 z-10"
                : "border-gray-200",
              "relative border p-4 flex flex-col cursor-pointer md:pl-4 md:pr-6 md:grid md:grid-cols-6 focus:outline-none"
            )}
          >
            <div className="flex items-center text-sm md:col-span-3">
              <span
                className={classNames(
                  task.checked
                    ? "bg-indigo-600 border-transparent"
                    : "bg-white border-gray-300",
                  task.active ? "ring-2 ring-offset-2 ring-indigo-500" : "",
                  "h-4 w-4 rounded border flex items-center justify-center"
                )}
                aria-hidden="true"
              >
                {/* <div className="flex items-center h-5">
                      <input
                        id="offers"
                        name="offers"
                        type="checkbox"
                        className="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded"
                      />
                    </div>
                    <div className="ml-3 text-sm">
                      <label htmlFor="offers" className="font-medium text-gray-700">
                        Offers
                      </label>
                      <p className="text-gray-500">Get notified when a candidate accepts or rejects an offer.</p>
                    </div> */}
                <span className="rounded bg-white w-1.5 h-1.5" />
              </span>
              <h4 as="span" className="ml-3 font-medium text-gray-700">
                {task.name}
              </h4>
            </div>
            <p className="ml-6 pl-1 text-sm md:ml-0 md:pl-0 md:text-center md:col-span-2">
              <span
                className={classNames(
                  task.checked ? "text-indigo-900" : "text-gray-700",
                  "font-medium"
                )}
              >
                {task.cost} zł
              </span>{" "}
              <span
                className={task.checked ? "text-indigo-700" : "text-gray-500"}
              >
                ({task.plannedDateFull})
              </span>
            </p>
            <p
              className={classNames(
                task.checked ? "text-indigo-700" : "text-gray-500",
                "ml-6 pl-1 text-sm md:ml-0 md:pl-0 md:text-right"
              )}
            >
              {task.executor}
            </p>
          </div>
        ))}
      </div>
    </div>
  );
}
