/* This example requires Tailwind CSS v2.0+ */
import { MailIcon, PhoneIcon } from "@heroicons/react/solid";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function Example({ guarantee }) {
  return (
    <li
      key={guarantee.email}
      className="col-span-1 flex flex-col text-center bg-white rounded-lg shadow divide-y divide-gray-200"
    >
      <div className="flex-1 flex flex-col p-8">
        <img
          className="w-32 h-32 flex-shrink-0 mx-auto bg-black rounded-lg"
          src={guarantee.imageUrl}
          alt=""
        />
        <h3 className="mt-6 text-gray-900 text-sm font-medium">
          {guarantee.name}
        </h3>
        <dl className="mt-1 flex-grow flex flex-col justify-between">
          <dt className="sr-only">Title</dt>
          <dd className="text-gray-500 text-sm">{guarantee.title}</dd>
          <dt className="sr-only">Role</dt>
          <dd className="mt-3">
            <span
              className={classNames(
                guarantee.isPositive
                  ? "bg-green-100 text-green-800"
                  : "bg-red-100 text-red-800",
                "px-2 py-1 text-xs font-medium rounded-full"
              )}
            >
              {guarantee.role}
            </span>
          </dd>
        </dl>
      </div>
      {/* <div>
        <div className="-mt-px flex divide-x divide-gray-200">
          <div className="w-0 flex-1 flex">
            <a
              href={`mailto:${guarantee.email}`}
              className="relative -mr-px w-0 flex-1 inline-flex items-center justify-center py-4 text-sm text-gray-700 font-medium border border-transparent rounded-bl-lg hover:text-gray-500"
            >
              <MailIcon className="w-5 h-5 text-gray-400" aria-hidden="true" />
              <span className="ml-3">Email</span>
            </a>
          </div>
          <div className="-ml-px w-0 flex-1 flex">
            <a
              href={`tel:${guarantee.telephone}`}
              className="relative w-0 flex-1 inline-flex items-center justify-center py-4 text-sm text-gray-700 font-medium border border-transparent rounded-br-lg hover:text-gray-500"
            >
              <PhoneIcon className="w-5 h-5 text-gray-400" aria-hidden="true" />
              <span className="ml-3">Call</span>
            </a>
          </div>
        </div> 
      </div>*/}
    </li>
  );
}
