import { useRouter } from "next/router";
import Link from "next/link";

import {
  InformationCircleIcon,
  AdjustmentsIcon,
  ClipboardCheckIcon,
  CurrencyDollarIcon,
  ExclamationCircleIcon,
  PencilAltIcon,
  ShieldCheckIcon,
  BriefcaseIcon,
} from "@heroicons/react/outline";

const navigation = (pathname) => [
  {
    name: "Informacje",
    href: "/details/info",
    icon: InformationCircleIcon,
    current: pathname.includes("/details/info"),
  },
  {
    name: "Umowy",
    href: "/details/contracts",
    icon: PencilAltIcon,
    current: pathname.includes("/details/contracts"),
  },
  {
    name: "Przeglądy",
    href: "/details/checks",
    icon: ClipboardCheckIcon,
    current: pathname.includes("/details/checks"),
  },
  {
    name: "Usterki",
    href: "/details/defects",
    icon: ExclamationCircleIcon,
    current: pathname.includes("/details/defects"),
  },
  {
    name: "Liczniki",
    href: "/details/meters",
    icon: AdjustmentsIcon,
    current: pathname.includes("/details/meters"),
  },
  {
    name: "Finanse",
    href: "/details/finance",
    icon: CurrencyDollarIcon,
    current: pathname.includes("/details/finance"),
  },
  {
    name: "Gwarancje",
    href: "/details/guarantees",
    icon: ShieldCheckIcon,
    current: pathname.includes("/details/guarantees"),
  },
  {
    name: "Remonty",
    href: "/details/repairs",
    icon: BriefcaseIcon,
    current: pathname.includes("/details/repairs"),
  },
];

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function Example() {
  const { pathname } = useRouter();

  return (
    <aside className="py-6 px-2 sm:px-6 lg:py-0 lg:px-0 lg:col-span-3">
      <nav className="space-y-1">
        {navigation(pathname).map((item) => (
          <Link href={item.href}>
            <a
              key={item.name}
              className={classNames(
                item.current
                  ? "bg-gray-50 text-indigo-700 hover:text-indigo-700 hover:bg-white"
                  : "text-gray-900 hover:text-gray-900 hover:bg-gray-50",
                "group rounded-md px-3 py-2 flex items-center text-sm font-medium"
              )}
              aria-current={item.current ? "page" : undefined}
            >
              <item.icon
                className={classNames(
                  item.current
                    ? "text-indigo-500 group-hover:text-indigo-500"
                    : "text-gray-400 group-hover:text-gray-500",
                  "flex-shrink-0 -ml-1 mr-3 h-6 w-6"
                )}
                aria-hidden="true"
              />
              <span className="truncate">{item.name}</span>
            </a>
          </Link>
        ))}
      </nav>
    </aside>
  );
}
