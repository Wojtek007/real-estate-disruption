/* This example requires Tailwind CSS v2.0+ */
import {
  MailIcon,
  PhoneIcon,
  DocumentAddIcon,
  DocumentDownloadIcon,
  FireIcon,
  CheckIcon,
} from "@heroicons/react/solid";
import Link from "next/link";
import { useRouter } from "next/router";
const people = [
  {
    date: "5 Stycznia 2021",
    name: "Zepsuty czajnik",
    person: "Jhon Eyreinhonsen",
    title: "W czajniku nie działa przycisk włączania",
    role: "Pilne",
    roleColor: "red",
    status: "Zgłoszona",
    statusIcon: (
      <DocumentDownloadIcon
        className="w-5 h-5 text-gray-400"
        aria-hidden="true"
      />
    ),
    email: "janecooper@example.com",
    telephone: "+1-202-555-0170",
    imageUrl:
      "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60",
  },
  {
    date: "5 Stycznia 2021",
    name: "Ściana pęka w łazience",
    person: "Jhon Eyre",
    title:
      "Tydzień temu w nocy usłyszałam dziwny dzwięk, jakby dom sie walił. Następnego dnia zobaczyłam w łazience, że pod sufitem Ļlytki są popękane i odstają mocno od ściany. Grozi to, że płytki spadną na głowę.",
    roleColor: "yellow",
    role: "Ważne",
    status: "Zaakceptowana",
    statusIcon: (
      <DocumentAddIcon className="w-5 h-5 text-yellow-400" aria-hidden="true" />
    ),
    email: "janecooper@example.com",
    telephone: "+1-202-555-0170",
    imageUrl:
      "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60",
  },
  {
    date: "5 Stycznia 2021",
    name: "Ściana pęka w łazience",
    person: "Jhon Eyre",
    title:
      "Tydzień temu w nocy usłyszałam dziwny dzwięk, jakby dom sie walił. Następnego dnia zobaczyłam w łazience, że pod sufitem Ļlytki są popękane i odstają mocno od ściany. Grozi to, że płytki spadną na głowę.",
    roleColor: "blue",
    role: "Zgłoszone",
    status: "Wykonywana",
    statusIcon: (
      <FireIcon className="w-5 h-5 text-red-400" aria-hidden="true" />
    ),
    email: "janecooper@example.com",
    telephone: "+1-202-555-0170",
    imageUrl:
      "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60",
  },
  {
    date: "5 Stycznia 2021",
    name: "Ściana pęka w łazience",
    person: "Jhon Eyre",
    title:
      "Tydzień temu w nocy usłyszałam dziwny dzwięk, jakby dom sie walił. Następnego dnia zobaczyłam w łazience, że pod sufitem Ļlytki są popękane i odstają mocno od ściany. Grozi to, że płytki spadną na głowę.",
    roleColor: "blue",
    role: "Zgłoszone",
    status: "Zakończona",
    statusIcon: (
      <CheckIcon className="w-5 h-5 text-green-400" aria-hidden="true" />
    ),
    email: "janecooper@example.com",
    telephone: "+1-202-555-0170",
    imageUrl:
      "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60",
  },

  // More people...
];

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function Example() {
  const router = useRouter();
  return (
    <ul className="grid grid-cols-1 gap-6 sm:grid-cols-2 lg:grid-cols-2">
      {people.map((person) => (
        <li
          key={person.email}
          className="col-span-1 bg-white rounded-lg shadow divide-y divide-gray-200"
        >
          <Link
            href={{
              pathname: router.pathname,
              query: { panel: 'defect' },
            }}
            scroll={false}
          >
            <a href="#" className="block hover:bg-gray-50">
              <div className="w-full flex items-center justify-between p-6 space-x-6">
                <div className="flex-1 truncate">
                  <div className="flex items-center space-x-3">
                    <h3 className="text-gray-900 text-sm font-medium truncate">
                      {person.name}
                    </h3>
                  </div>
                  <div className="mt-1 text-gray-500 text-sm truncate">
                    <span
                      className={classNames(
                        `text-${person.roleColor}-800`,
                        `bg-${person.roleColor}-100`,
                        "flex-shrink-0 inline-block px-2 py-0.5 text-xs font-medium rounded-full"
                      )}
                    >
                      {person.role}
                    </span>
                    {/* {person.title} */}
                  </div>
                </div>
                <div>
                  <div className="text-right text-sm whitespace-nowrap text-gray-500">
                    <time dateTime={person.datetime}>{person.date}</time>
                  </div>
                  <p className="mt-1 text-gray-500 text-sm truncate">
                    przez {person.person}
                  </p>
                </div>
                {/* <img
              className="w-10 h-10 bg-gray-300 rounded-full flex-shrink-0"
              src={person.imageUrl}
              alt=""
            /> */}
              </div>
            </a></Link>
          <div>
            <div className="-mt-px flex divide-x divide-gray-200">
              <div className="w-0 flex-1 flex">
                <a
                  href={`mailto:${person.email}`}
                  className="relative -mr-px w-0 flex-1 inline-flex items-center justify-center py-4 text-sm text-gray-700 font-medium border border-transparent rounded-bl-lg hover:text-gray-500"
                >
                  {person.statusIcon}
                  <span className="ml-3">{person.status}</span>
                </a>
              </div>
              <div className="-ml-px w-0 flex-1 flex">
                <a
                  href={`tel:${person.telephone}`}
                  className="relative w-0 flex-1 inline-flex items-center justify-center py-4 text-sm text-gray-700 font-medium border border-transparent rounded-br-lg hover:text-gray-500"
                >
                  <MailIcon
                    className="w-5 h-5 text-gray-400"
                    aria-hidden="true"
                  />
                  <span className="ml-3">Skontaktuj się</span>
                </a>
              </div>
            </div>
          </div>
        </li>
      ))}
    </ul>
  );
}
