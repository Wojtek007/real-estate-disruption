function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function Example({ meter }) {
  return (
    <div
      key={meter.id}
      className="relative bg-white pt-5 px-4 pb-12 sm:pt-6 sm:px-6 shadow rounded-lg overflow-hidden"
    >
      <dt>
        <div className="absolute bg-indigo-500 rounded-md p-3">
          <meter.icon className="h-6 w-6 text-white" aria-hidden="true" />
        </div>
        <p className="ml-16 text-sm font-medium text-gray-500 truncate">
          {meter.name}
        </p>
      </dt>
      <dd className="ml-16 pb-6 flex items-baseline sm:pb-7">
        <p
          className={classNames(
            meter.stat >= 0 ? "text-green-600" : "text-red-600",
            "text-2xl font-semibold "
          )}
        >
          {meter.stat} zł
        </p>
        {/* <p
                className={classNames(
                  meter.stan === "nadpłata" ? "text-green-600" : "text-red-600",
                  "ml-2 flex items-baseline text-sm font-semibold"
                )}
              >
                {meter.stan}
              </p> */}
      </dd>
      <div className="ml-2">
        <p className="text-sm font-semibold">{meter.rentAdvance} w czynszu</p>
      </div>
      {meter.periods?.map((period) => (
        <Bills period={period} />
      ))}
      <div className="absolute bottom-0 inset-x-0 bg-gray-50 px-4 py-4 sm:px-6">
        <div className="text-sm">
          <a
            href="#"
            className="font-medium text-indigo-600 hover:text-indigo-500"
          >
            View all<span className="sr-only"> {meter.name} stats</span>
          </a>
        </div>
      </div>
    </div>
  );
}

function Bills({ period }) {
  return (
    <div className="flow-root">
      <h3 className="text-sm leading-6 font-medium text-gray-500 text-center pb-3">
        {period.fromDate} - {period.toDate}
      </h3>
      <ul className="flex">
        {period.timeline.map((event, eventIdx) => (
          <li key={event.id} className="flex-grow">
            <div className="relative pb-8">
              {eventIdx !== period.timeline.length - 1 ? (
                <span
                  className="absolute top-4 left-1/2 w-full h-0.5 bg-gray-200 -ml-px"
                  aria-hidden="true"
                />
              ) : null}
              <div className="relative flex space-x-2 flex-col items-center">
                <div>
                  <span
                    className={classNames(
                      event.iconBackground,
                      "h-8 w-8 rounded-full flex items-center justify-center ring-8 ring-white"
                    )}
                  >
                    <event.icon
                      className="h-5 w-5 text-white"
                      aria-hidden="true"
                    />
                  </span>
                </div>
                <p className="text-sm text-gray-500 flex-1 pt-1.5 flex justify-between text-center">
                  {event.content}
                </p>
                <p className="font-medium text-gray-900 flex-1 pt-1.5 flex justify-between text-center">
                  {event.target}
                </p>

                <div className="text-center text-sm whitespace-nowrap text-gray-500">
                  <time dateTime={event.datetime}>{event.date}</time>
                </div>
              </div>
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
}
