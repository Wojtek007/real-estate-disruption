/* This example requires Tailwind CSS v2.0+ */
import {
  PaperClipIcon,
  BellIcon,
  UserIcon,
  XIcon,
  CheckIcon,
} from "@heroicons/react/solid";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function Example({ meter }) {
  return (
    <div
      key={meter.id}
      className="relative bg-white pt-5 px-4 pb-12 sm:pt-6 sm:px-6 shadow rounded-lg overflow-hidden"
    >
      <dt>
        <div className="absolute bg-indigo-500 rounded-md p-3">
          <meter.icon className="h-6 w-6 text-white" aria-hidden="true" />
        </div>
        <p className="ml-16 text-sm font-medium text-gray-500 truncate">
          {meter.name}
        </p>
      </dt>
      <dd className="ml-16 pb-6 flex items-baseline sm:pb-7">
        <p
          className={classNames(
            meter.stat >= 0 ? "text-green-600" : "text-red-600",
            "text-2xl font-semibold "
          )}
        >
          {meter.stat} zł
        </p>
        {/* <p
                className={classNames(
                  meter.stan === "nadpłata" ? "text-green-600" : "text-red-600",
                  "ml-2 flex items-baseline text-sm font-semibold"
                )}
              >
                {meter.stan}
              </p> */}
      </dd>
      {/* <div className="ml-2">
        <p className="text-sm font-semibold">{meter.rentAdvance} w czynszu</p>
      </div> */}
      {meter.periods?.map((period) => (
        <Bills period={period} />
      ))}
      <div className="absolute bottom-0 inset-x-0 bg-gray-50 px-4 py-4 sm:px-6">
        <div className="text-sm">
          <a
            href="#"
            className="font-medium text-indigo-600 hover:text-indigo-500"
          >
            View all<span className="sr-only"> {meter.name} stats</span>
          </a>
        </div>
      </div>
    </div>
  );
}

function Bills({ period }) {
  return (
    <div className="flow-root mb-3 -mt-16">
      <h3 className="text-sm leading-6 font-medium text-gray-500 text-center pb-3">
        Okres rozliczeniowy<br />{period.fromDate} - {period.toDate}
      </h3>
      <Table />
    </div>
  );
}


/* This example requires Tailwind CSS v2.0+ */
const people = [
  {
    name: 'Jane Cooper',
    title: 'Regional Paradigm Technician',
    department: 'Optimization',
    role: 'Admin',
    email: 'jane.cooper@example.com',
    image:
      'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60',
  },
  {
    name: 'Jane Cooper',
    title: 'Regional Paradigm Technician',
    department: 'Optimization',
    role: 'Admin',
    email: 'jane.cooper@example.com',
    image:
      'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60',
  },
  {
    name: 'Jane Cooper',
    title: 'Regional Paradigm Technician',
    department: 'Optimization',
    role: 'Admin',
    email: 'jane.cooper@example.com',
    image:
      'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60',
  },
  // More people...
]

function Table() {
  return (
    <div className="-mx-6 flex flex-col">
      <div className="overflow-x-auto ">
        <div className="align-middle inline-block min-w-full ">
          <div className="shadow overflow-hidden border-b border-gray-200 ">
            <table className="min-w-full divide-y divide-gray-200">
              <thead className="bg-gray-50">
                <tr>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Miesiąc
                  </th>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Kwota w czynszu
                  </th>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Liczniki
                  </th>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Przewidywany
                  </th>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Rzeczywista
                  </th>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Dopłata
                  </th>
                  {/* <th scope="col" className="relative px-6 py-3">
                    <span className="sr-only">Edit</span>
                  </th> */}
                </tr>
              </thead>
              <tbody className="bg-white divide-y divide-gray-200">
                {people.map((person) => (
                  <tr key={person.email} className="text-center">
                    <td className="px-6 py-4 whitespace-nowrap">
                      {/* <div className="flex items-center"> */}
                      {/* <div className="flex-shrink-0 h-10 w-10">
                          <img className="h-10 w-10 rounded-full" src={person.image} alt="" />
                        </div> */}
                      {/* <div className="ml-4"> */}
                      <div className="text-sm font-medium text-gray-900">05.2021</div>
                      <div className="text-xs text-gray-500">opłata do<br />10.06.2021</div>
                      {/* </div> */}
                      {/* </div> */}
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap">
                      <div className="text-sm font-medium text-gray-900">250.00 zł</div>
                      <div className="flex items-center justify-center">
                        <UserIcon
                          className="w-4 h-4 text-gray-400"
                          aria-hidden="true"
                        />
                        <div className="text-sm text-gray-500 ml-1">Kruszyński</div>
                      </div>
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap">
                      {/* <div className="text-sm text-gray-900">Start: 201923</div> */}
                      <div className="text-sm text-gray-500">Start: 201923</div>
                      <div className="text-sm text-gray-500">Koniec: 202223</div>
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap bg-green-500">
                      <div className="text-sm font-medium text-white">220.23 zł</div>
                      <div className="flex items-center justify-center">
                        <CheckIcon
                          className="w-4 h-4 text-white"
                          aria-hidden="true"
                        />
                        <div className="text-xs text-white ml-1">Opłacone</div>
                      </div>
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap bg-green-500">
                      <div className="text-sm font-medium text-white">261.81 zł</div>
                      <div className="flex items-center justify-center">
                        <CheckIcon
                          className="w-4 h-4 text-white"
                          aria-hidden="true"
                        />
                        <div className="text-xs text-white ml-1">Opłacone</div>
                      </div>
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap bg-red-500">
                      <div className="text-sm font-medium text-white">41.61 zł</div>
                      <div className="flex items-center justify-center">
                        <XIcon
                          className="w-4 h-4 text-white"
                          aria-hidden="true"
                        />
                        <div className="text-xs text-white ml-1">Czekam na<br />najemce</div>
                      </div>
                    </td>

                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  )
}
