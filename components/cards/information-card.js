import { PaperClipIcon } from "@heroicons/react/solid";
import Link from "next/link";
import { useRouter } from "next/router";

const exampleData = [
  { label: "Full name", value: "Margot Foster" },
  { label: "Application for", value: "Backend Developer" },
  { label: "Email address", value: "margotfoster@example.com" },
  { label: "Salary expectation", value: "$120,000" },
  {
    label: "About",
    value:
      "Fugiat ipsum ipsum deserunt culpa aute sint do nostrud anim incididunt cillum culpa consequat. Excepteur qui ipsum aliquip consequat sint. Sit id mollit nulla mollit nostrud in ea officia proident. Irure nostrud pariatur mollit ad adipisicing reprehenderit deserunt qui eu.",
  },
];

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function Example({
  title = "Applicant Information",
  desc,
  data = exampleData,
}) {
  const router = useRouter();
  return (
    <div className="bg-white shadow overflow-hidden sm:rounded-lg">
      <div className="px-4 py-5 sm:px-6">
        <h3 className="text-lg leading-6 font-medium text-gray-900">{title}</h3>
        {desc && <p className="mt-1 max-w-2xl text-sm text-gray-500">{desc}</p>}
      </div>
      <div className="border-t border-gray-200">
        <dl>
          {data.map((row, i) => (
            <div
              className={classNames(
                i % 2 === 0 ? "bg-gray-50" : "bg-white",
                " px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6"
              )}
            >
              <dt className="text-sm font-medium text-gray-500">{row.label}</dt>

              {(() => {
                const value = row.desc ? (
                  <>
                    <dd className="mt-1 text-sm text-gray-900 sm:mt-0">
                      {row.desc}
                    </dd>
                    <dd className="mt-1 text-sm text-gray-900 sm:mt-0">
                      {row.value}
                    </dd>
                  </>
                ) : (
                  <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                    {row.value}
                  </dd>
                );

                return row.id ? (
                  <Link
                    href={{
                      pathname: router.pathname,
                      query: { panel: row.id },
                    }}
                    scroll={false}
                  >
                    <a>{value}</a>
                  </Link>
                ) : (
                  value
                );
              })()}
            </div>
          ))}
          {/* <div className="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
            <dt className="text-sm font-medium text-gray-500">Attachments</dt>
            <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
              <ul className="border border-gray-200 rounded-md divide-y divide-gray-200">
                <li className="pl-3 pr-4 py-3 flex items-center justify-between text-sm">
                  <div className="w-0 flex-1 flex items-center">
                    <PaperClipIcon
                      className="flex-shrink-0 h-5 w-5 text-gray-400"
                      aria-hidden="true"
                    />
                    <span className="ml-2 flex-1 w-0 truncate">
                      resume_back_end_developer.pdf
                    </span>
                  </div>
                  <div className="ml-4 flex-shrink-0">
                    <a
                      href="#"
                      className="font-medium text-indigo-600 hover:text-indigo-500"
                    >
                      Download
                    </a>
                  </div>
                </li>
                <li className="pl-3 pr-4 py-3 flex items-center justify-between text-sm">
                  <div className="w-0 flex-1 flex items-center">
                    <PaperClipIcon
                      className="flex-shrink-0 h-5 w-5 text-gray-400"
                      aria-hidden="true"
                    />
                    <span className="ml-2 flex-1 w-0 truncate">
                      coverletter_back_end_developer.pdf
                    </span>
                  </div>
                  <div className="ml-4 flex-shrink-0">
                    <a
                      href="#"
                      className="font-medium text-indigo-600 hover:text-indigo-500"
                    >
                      Download
                    </a>
                  </div>
                </li>
              </ul>
            </dd>
          </div> */}
        </dl>
      </div>
    </div>
  );
}
