/* This example requires Tailwind CSS v2.0+ */
import {
  PaperClipIcon,
  BellIcon,
  CalendarIcon,
  UserIcon,
  CurrencyDollarIcon,
  CheckIcon,
} from "@heroicons/react/solid";

const tasks = [
  {
    checked: true,
    name: "Zadwonić po firmę ",
    plannedDateFull: "7 stycznia 2020",
    notificationFull: "1 stycznia 2020",
    executor: "Janek and Co.",
  },
  {
    name: "Spytać właściciela o zgodę",
    plannedDateFull: "7 stycznia 2020",
    notificationFull: "1 stycznia 2020",
    executor: "Janek and Co.",
  },
  {
    name: "Sprawdzić rezultat",
    plannedDateFull: "7 stycznia 2020",
    notificationFull: "1 stycznia 2020",
    executor: "Janek and Co.",
    attachment: 'ProForma'
  },
  {
    name: "Zapłacić",
    cost: 344,
    plannedDateFull: "7 stycznia 2020",
    notificationFull: "1 stycznia 2020",
    executor: "Janek and Co.",
    attachment: 'Rachunek'
  },
]

export default function Example() {
  return (
    <div className="flow-root px-4 py-3">
      <h2 className="max-w-6xl mx-auto mt-8 px-4 text-lg leading-6 font-medium text-gray-900 sm:px-6 lg:px-8">
        Naprawa
      </h2>
      <div className="sm:col-span-2 px-4 py-3">
        <dt className="text-sm font-medium text-gray-500">Notatka</dt>
        <dd className="mt-1 text-sm text-gray-900">
          Fugiat ipsum ipsum deserunt culpa aute sint do nostrud anim incididunt cillum culpa consequat. Excepteur
          qui ipsum aliquip consequat sint. Sit id mollit nulla mollit nostrud in ea officia proident. Irure nostrud
          pariatur mollit ad adipisicing reprehenderit deserunt qui eu.
        </dd>
      </div>
      <Tasks tasks={tasks} />
    </div>
  )
}


function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export function Tasks({ tasks }) {
  return (
    <div>
      <div className="relative bg-white rounded-md -space-y-px">
        {tasks.map((task,taskIdx) => (
          <div
            key={task.name}
            value={task}
            className={classNames(
              taskIdx === 0 ? "rounded-tl-md rounded-tr-md" : "",
              taskIdx === tasks.length - 1 ? "rounded-bl-md rounded-br-md" : "",
              task.checked
                ? "bg-indigo-50 border-indigo-200 z-10"
                : "border-gray-200",
              "relative border p-4  focus:outline-none"
            )}
          >
            <div>
              <div className="flex flex-col cursor-pointer md:pl-4 md:pr-6 md:grid md:grid-cols-6">
                <div className="flex items-center text-sm md:col-span-3">
                  <span
                    className={classNames(
                      task.checked
                        ? "bg-indigo-600 border-transparent"
                        : "bg-white border-gray-300",
                      task.active ? "ring-2 ring-offset-2 ring-indigo-500" : "",
                      "h-4 w-4 rounded border flex items-center justify-center"
                    )}
                    aria-hidden="true"
                  >
                    <span className="rounded bg-white w-1.5 h-1.5" />
                  </span>
                  <h4 as="span" className="ml-3 font-medium text-gray-700">
                    {task.name}
                  </h4>
                </div>

              </div>
              {/* <div className="flex flex-col cursor-pointer md:pl-4 md:pr-6 md:grid md:grid-cols-6"> */}
              <div className="mt-2 ml-6 text-sm md:pl-4 md:pr-6 grid md:grid-cols-3 grid-cols-2">

                {task.plannedDateFull && <div className="flex-1 inline-flex items-center  py-1 text-sm text-gray-700 font-medium border border-transparent rounded-br-lg hover:text-gray-500"    >
                  <CalendarIcon
                    className="w-5 h-5 text-gray-400"
                    aria-hidden="true"
                  />
                  <span className="ml-3">{task.plannedDateFull}</span>
                </div>}
                {task.notificationFull && <div className="flex-1 inline-flex items-center  py-1 text-sm text-gray-700 font-medium border border-transparent rounded-br-lg hover:text-gray-500"    >
                  <BellIcon
                    className="w-5 h-5 text-gray-400"
                    aria-hidden="true"
                  />
                  <span className="ml-3">{task.notificationFull}</span>
                </div>}
                {task.executor && <div className="flex-1 inline-flex items-center  py-1 text-sm text-gray-700 font-medium border border-transparent rounded-br-lg hover:text-gray-500"    >
                  <UserIcon
                    className="w-5 h-5 text-gray-400"
                    aria-hidden="true"
                  />
                  <span className="ml-3">{task.executor}</span>
                </div>}
                {task.cost && <div className="flex-1 inline-flex items-center  py-1 text-sm text-gray-700 font-medium border border-transparent rounded-br-lg hover:text-gray-500"    >
                  <CurrencyDollarIcon
                    className="w-5 h-5 text-gray-400"
                    aria-hidden="true"
                  />
                  <span className="ml-3">{task.cost}</span>
                </div>}
                {task.attachment && <div className="flex-1 inline-flex items-center  py-1 text-sm text-gray-700 font-medium border border-transparent rounded-br-lg hover:text-gray-500"    >
                  <PaperClipIcon
                    className="w-5 h-5 text-gray-400"
                    aria-hidden="true"
                  />
                  <span className="ml-3">{task.attachment}</span>
                </div>}
              </div>

              {/* </div> */}
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
