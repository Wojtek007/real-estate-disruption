import { Steps } from '../../details/check-card'

export default function Example() {
  return (
    <div className="flow-root px-4 py-3">
      <h2 className="max-w-6xl mx-auto mt-8 px-4 text-lg leading-6 font-medium text-gray-900 sm:px-6 lg:px-8">
        Najbliższy przegląd
      </h2>
      <div className="sm:col-span-2 px-4 py-3">
        <dt className="text-sm font-medium text-gray-500">Notatka</dt>
        <dd className="mt-1 text-sm text-gray-900">
          Fugiat ipsum ipsum deserunt culpa aute sint do nostrud anim incididunt cillum culpa consequat. Excepteur
          qui ipsum aliquip consequat sint. Sit id mollit nulla mollit nostrud in ea officia proident. Irure nostrud
          pariatur mollit ad adipisicing reprehenderit deserunt qui eu.
        </dd>
      </div>
      <Steps state="planned" isVertical />
    </div>
  )
}