/* This example requires Tailwind CSS v2.0+ */
import { Fragment,useState } from "react";
import { Dialog,Menu,Transition } from "@headlessui/react";
import { XIcon } from "@heroicons/react/outline";
import { DotsVerticalIcon } from "@heroicons/react/solid";
import Link from "next/link";
import { useRouter } from "next/router";
/* This example requires Tailwind CSS v2.0+ */
import {
  MailIcon,
  PhoneIcon,
  DocumentAddIcon,
  DocumentDownloadIcon,
  FireIcon,
  CheckIcon,
} from "@heroicons/react/solid";
import { Steps } from '../../details/check-card'

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}
const tenantInfo = [
  { label: "Imie i nazwisko",value: "Margarett Tacher",id: "person" },
  {
    label: "Adres zameldowania",
    value: "ul Broniewskiego 2, 05-530 Góra Kalwaria",
  },
  { label: "Numer konta",value: "1231 2313 4544 1231 2313 4544" },
  { label: "Email",value: "moc@gazeta.pl" },
  { label: "Telefon",value: "+48 777 888 999" },
  { label: "Pesel",value: "89120810101" },
  {
    label: "Notatki",
    value:
      "Fugiat ipsum ipsum deserunt culpa aute sint do nostrud anim incididunt cillum culpa consequat. Excepteur qui ipsum aliquip consequat sint. Sit id mollit nulla mollit nostrud in ea officia proident. Irure nostrud pariatur mollit ad adipisicing reprehenderit deserunt qui eu.",
  },
];
export default function Example() {
  const router = useRouter();

  const defect = {
    date: "5 Stycznia 2021",
    name: "Ściana pęka w łazience",
    person: "Jhon Eyre",
    title:
      "Tydzień temu w nocy usłyszałam dziwny dzwięk, jakby dom sie walił. Następnego dnia zobaczyłam w łazience, że pod sufitem Ļlytki są popękane i odstają mocno od ściany. Grozi to, że płytki spadną na głowę.",
    roleColor: "blue",
    role: "Zgłoszone",
    status: "Zakończona",
    statusIcon: (
      <CheckIcon className="w-5 h-5 text-green-400" aria-hidden="true" />
    ),
    email: "janecooper@example.com",
    telephone: "+1-202-555-0170",
    imageUrl:
      "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60",
  }

  return (
    <div className="divide-y divide-gray-200">
      <div className="pb-6">
        <div className=" flow-root px-4 sm:flex sm:items-end sm:px-6 ">
          {/* <div>
            <div className="-m-1 flex">
              <div className="inline-flex rounded-lg overflow-hidden border-4 border-white">
                <img
                  className="flex-shrink-0 h-24 w-24 sm:h-40 sm:w-40 lg:w-48 lg:h-48"
                  src="https://images.unsplash.com/photo-1501031170107-cfd33f0cbdcc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=256&h=256&q=80"
                  alt=""
                />
              </div>
            </div>
          </div> */}
          <div className="mt-6 sm:ml-6 sm:flex-1">
            <div>
              <div className="flex items-center">
                <h3 className="font-bold text-xl text-gray-900 sm:text-2xl">
                  Sprawdzenie piecyków
                </h3>
                {/* <span className="ml-2.5 bg-green-400 flex-shrink-0 inline-block h-2 w-2 rounded-full">
                  <span className="sr-only">Online</span>
                </span> */}
              </div>
              <p className="text-sm text-gray-500">co roczny przegląd</p>
            </div>
            {/* <div className="mt-5 flex flex-wrap space-y-3 sm:space-y-0 sm:space-x-3">
              <button
                type="button"
                className="flex-shrink-0 w-full inline-flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:flex-1"
              >
                Message
              </button>
              <button
                type="button"
                className="flex-1 w-full inline-flex items-center justify-center px-4 py-2 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              >
                Call
              </button>
              <span className="ml-3 inline-flex sm:ml-0">
                <Menu as="div" className="relative inline-block text-left">
                  {({ open }) => (
                    <>
                      <Menu.Button className="inline-flex items-center p-2 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-400 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        <span className="sr-only">Open options menu</span>
                        <DotsVerticalIcon
                          className="h-5 w-5"
                          aria-hidden="true"
                        />
                      </Menu.Button>
                      <Transition
                        show={open}
                        as={Fragment}
                        enter="transition ease-out duration-100"
                        enterFrom="transform opacity-0 scale-95"
                        enterTo="transform opacity-100 scale-100"
                        leave="transition ease-in duration-75"
                        leaveFrom="transform opacity-100 scale-100"
                        leaveTo="transform opacity-0 scale-95"
                      >
                        <Menu.Items
                          static
                          className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none"
                        >
                          <div className="py-1">
                            <Menu.Item>
                              {({ active }) => (
                                <a
                                  href="#"
                                  className={classNames(
                                    active
                                      ? "bg-gray-100 text-gray-900"
                                      : "text-gray-700",
                                    "block px-4 py-2 text-sm"
                                  )}
                                >
                                  View profile
                                </a>
                              )}
                            </Menu.Item>
                            <Menu.Item>
                              {({ active }) => (
                                <a
                                  href="#"
                                  className={classNames(
                                    active
                                      ? "bg-gray-100 text-gray-900"
                                      : "text-gray-700",
                                    "block px-4 py-2 text-sm"
                                  )}
                                >
                                  Copy profile link
                                </a>
                              )}
                            </Menu.Item>
                          </div>
                        </Menu.Items>
                      </Transition>
                    </>
                  )}
                </Menu>
              </span>
            </div> */}
          </div>
        </div>
      </div>
      <div className="px-4 py-5 sm:p-6">
        <h3 className="text-lg leading-6 font-medium text-gray-900">Status najbliższego przeglądu</h3>
        <div className="mt-5">
          <div className="rounded-md bg-gray-50 border-t border-b border-gray-200 ">
            <Steps state="planned" />
            {/* <div className="sm:flex sm:items-start">
              <div className="mt-3 sm:mt-0 sm:ml-4">
                <div className="text-sm font-medium text-gray-900">Umówić przegląd</div>
                <div className="mt-1 text-sm text-gray-600 sm:flex sm:items-center">
                  <div>Następne przypomnienie 27 września 2021</div>
                  <span className="hidden sm:mx-2 sm:inline" aria-hidden="true">
                    &middot;
                  </span>
                  <div className="mt-1 sm:mt-0">Przekładane 2 razy</div>
                </div>
              </div>
            </div>
            <div className="mt-4 sm:mt-0 sm:ml-6 sm:flex-shrink-0">
              <button
                type="button"
                className="inline-flex items-center px-4 py-2 border border-gray-300 shadow-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:text-sm"
              >
                Edytuj
              </button>
            </div> */}
          </div>
        </div>
      </div>
      <div className="px-4 py-5 sm:p-6">
        <h3 className="text-lg leading-6 font-medium text-gray-900">Następna notyfikacja</h3>
        <div className="mt-5">
          <div className="rounded-md bg-gray-50 px-6 py-5 sm:flex sm:items-start sm:justify-between">
            <div className="sm:flex sm:items-start">
              <div className="mt-3 sm:mt-0 sm:ml-4">
                <div className="text-sm font-medium text-gray-900">Sprawdzić wykonanie przeglądu</div>
                <div className="mt-1 text-sm text-gray-600 sm:flex sm:items-center">
                  <div>Następne przypomnienie 27 września 2021</div>
                  <span className="hidden sm:mx-2 sm:inline" aria-hidden="true">
                    &middot;
                  </span>
                  <div className="mt-1 sm:mt-0">Przekładane 2 razy</div>
                </div>
              </div>
            </div>
            <div className="mt-4 sm:mt-0 sm:ml-6 sm:flex-shrink-0">
              <button
                type="button"
                className="inline-flex items-center px-4 py-2 border border-gray-300 shadow-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:text-sm"
              >
                Edytuj
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className="px-4 py-5 sm:p-6">
        <h3 className="text-lg leading-6 font-medium text-gray-900">Powiązane usterki</h3>
        <div className="mt-5">
          <div className="rounded-md bg-gray-50  col-span-1 shadow divide-y divide-gray-200">
            {/* <li
          key={person.email}
          className="col-span-1 bg-white rounded-lg shadow divide-y divide-gray-200"
        > */}
            <div className="w-full flex items-center justify-between p-6 space-x-6">
              <div className="flex-1 truncate">
                <div className="flex items-center space-x-3">
                  <h3 className="text-gray-900 text-sm font-medium truncate">
                    {defect.name}
                  </h3>
                  <span
                    className={classNames(
                      `text-${defect.roleColor}-800`,
                      `bg-${defect.roleColor}-100`,
                      "flex-shrink-0 inline-block px-2 py-0.5 text-xs font-medium rounded-full"
                    )}
                  >
                    {defect.role}
                  </span>
                </div>
                <p className="mt-1 text-gray-500 text-sm truncate">
                  {defect.title}
                </p>
              </div>
              <div>
                <div className="text-right text-sm whitespace-nowrap text-gray-500">
                  <time dateTime={defect.datetime}>{defect.date}</time>
                </div>
                <p className="mt-1 text-gray-500 text-sm truncate">
                  przez {defect.person}
                </p>
              </div>
              {/* <img
              className="w-10 h-10 bg-gray-300 rounded-full flex-shrink-0"
              src={person.imageUrl}
              alt=""
            /> */}
            </div>
            <div>
              <div className="-mt-px flex divide-x divide-gray-200">
                <div className="w-0 flex-1 flex">
                  <a
                    href={`mailto:${defect.email}`}
                    className="relative -mr-px w-0 flex-1 inline-flex items-center justify-center py-4 text-sm text-gray-700 font-medium border border-transparent rounded-bl-lg hover:text-gray-500"
                  >
                    {defect.statusIcon}
                    <span className="ml-3">{defect.status}</span>
                  </a>
                </div>
                <div className="-ml-px w-0 flex-1 flex">
                  <a
                    href={`tel:${defect.telephone}`}
                    className="relative w-0 flex-1 inline-flex items-center justify-center py-4 text-sm text-gray-700 font-medium border border-transparent rounded-br-lg hover:text-gray-500"
                  >
                    <MailIcon
                      className="w-5 h-5 text-gray-400"
                      aria-hidden="true"
                    />
                    <span className="ml-3">Skontaktuj się</span>
                  </a>
                </div>
              </div>
            </div>
            {/* </li> */}
          </div>
        </div>
      </div>
      {/* <div className="px-4 py-5 sm:px-0 sm:py-0">
        <dl className="space-y-8 sm:divide-y sm:divide-gray-200 sm:space-y-0">
          {tenantInfo.map((row,i) => (
            <div
              className={classNames(
                i % 2 === 0 ? "bg-gray-50" : "bg-white",
                " px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6"
              )}
            >
              <dt className="text-sm font-medium text-gray-500">{row.label}</dt>

              {(() => {
                const value = row.desc ? (
                  <>
                    <dd className="mt-1 text-sm text-gray-900 sm:mt-0">
                      {row.desc}
                    </dd>
                    <dd className="mt-1 text-sm text-gray-900 sm:mt-0">
                      {row.value}
                    </dd>
                  </>
                ) : (
                  <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                    {row.value}
                  </dd>
                );

                return row.id ? (
                  <Link
                    href={{
                      pathname: router.pathname,
                      query: { panel: row.id },
                    }}
                    scroll={false}
                  >
                    <a>{value}</a>
                  </Link>
                ) : (
                  value
                );
              })()}
            </div>
          ))}
        </dl>
      </div> */}
    </div>
  );
}
