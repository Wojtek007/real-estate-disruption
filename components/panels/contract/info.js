/* This example requires Tailwind CSS v2.0+ */
import { Fragment, useState } from "react";
import { Dialog, Menu, Transition } from "@headlessui/react";
import { XIcon } from "@heroicons/react/outline";
import { DotsVerticalIcon } from "@heroicons/react/solid";
import Link from "next/link";
import { useRouter } from "next/router";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}
const tenantInfo = [
  { label: "Imie i nazwisko", value: "Margarett Tacher", id: "person" },
  {
    label: "Adres zameldowania",
    value: "ul Broniewskiego 2, 05-530 Góra Kalwaria",
  },
  { label: "Numer konta", value: "1231 2313 4544 1231 2313 4544" },
  { label: "Email", value: "moc@gazeta.pl" },
  { label: "Telefon", value: "+48 777 888 999" },
  { label: "Pesel", value: "89120810101" },
  {
    label: "Notatki",
    value:
      "Fugiat ipsum ipsum deserunt culpa aute sint do nostrud anim incididunt cillum culpa consequat. Excepteur qui ipsum aliquip consequat sint. Sit id mollit nulla mollit nostrud in ea officia proident. Irure nostrud pariatur mollit ad adipisicing reprehenderit deserunt qui eu.",
  },
];
export default function Example() {
  const router = useRouter();

  return (
    <div className="divide-y divide-gray-200">
      <div className="pb-6">
        <div className=" flow-root px-4 sm:flex sm:items-end sm:px-6 ">
          {/* <div>
            <div className="-m-1 flex">
              <div className="inline-flex rounded-lg overflow-hidden border-4 border-white">
                <img
                  className="flex-shrink-0 h-24 w-24 sm:h-40 sm:w-40 lg:w-48 lg:h-48"
                  src="https://images.unsplash.com/photo-1501031170107-cfd33f0cbdcc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=256&h=256&q=80"
                  alt=""
                />
              </div>
            </div>
          </div> */}
          <div className="mt-6 sm:ml-6 sm:flex-1">
            <div>
              <div className="flex items-center">
                <h3 className="font-bold text-xl text-gray-900 sm:text-2xl">
                  Ashley Porter
                </h3>
                <span className="ml-2.5 bg-green-400 flex-shrink-0 inline-block h-2 w-2 rounded-full">
                  <span className="sr-only">Online</span>
                </span>
              </div>
              <p className="text-sm text-gray-500">@ashleyporter</p>
            </div>
            <div className="mt-5 flex flex-wrap space-y-3 sm:space-y-0 sm:space-x-3">
              <button
                type="button"
                className="flex-shrink-0 w-full inline-flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:flex-1"
              >
                Message
              </button>
              <button
                type="button"
                className="flex-1 w-full inline-flex items-center justify-center px-4 py-2 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              >
                Call
              </button>
              <span className="ml-3 inline-flex sm:ml-0">
                <Menu as="div" className="relative inline-block text-left">
                  {({ open }) => (
                    <>
                      <Menu.Button className="inline-flex items-center p-2 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-400 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        <span className="sr-only">Open options menu</span>
                        <DotsVerticalIcon
                          className="h-5 w-5"
                          aria-hidden="true"
                        />
                      </Menu.Button>
                      <Transition
                        show={open}
                        as={Fragment}
                        enter="transition ease-out duration-100"
                        enterFrom="transform opacity-0 scale-95"
                        enterTo="transform opacity-100 scale-100"
                        leave="transition ease-in duration-75"
                        leaveFrom="transform opacity-100 scale-100"
                        leaveTo="transform opacity-0 scale-95"
                      >
                        <Menu.Items
                          static
                          className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none"
                        >
                          <div className="py-1">
                            <Menu.Item>
                              {({ active }) => (
                                <a
                                  href="#"
                                  className={classNames(
                                    active
                                      ? "bg-gray-100 text-gray-900"
                                      : "text-gray-700",
                                    "block px-4 py-2 text-sm"
                                  )}
                                >
                                  View profile
                                </a>
                              )}
                            </Menu.Item>
                            <Menu.Item>
                              {({ active }) => (
                                <a
                                  href="#"
                                  className={classNames(
                                    active
                                      ? "bg-gray-100 text-gray-900"
                                      : "text-gray-700",
                                    "block px-4 py-2 text-sm"
                                  )}
                                >
                                  Copy profile link
                                </a>
                              )}
                            </Menu.Item>
                          </div>
                        </Menu.Items>
                      </Transition>
                    </>
                  )}
                </Menu>
              </span>
            </div>
          </div>
        </div>
      </div>
      <div className="px-4 py-5 sm:px-0 sm:py-0">
        <dl className="space-y-8 sm:divide-y sm:divide-gray-200 sm:space-y-0">
          {tenantInfo.map((row, i) => (
            <div
              className={classNames(
                i % 2 === 0 ? "bg-gray-50" : "bg-white",
                " px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6"
              )}
            >
              <dt className="text-sm font-medium text-gray-500">{row.label}</dt>

              {(() => {
                const value = row.desc ? (
                  <>
                    <dd className="mt-1 text-sm text-gray-900 sm:mt-0">
                      {row.desc}
                    </dd>
                    <dd className="mt-1 text-sm text-gray-900 sm:mt-0">
                      {row.value}
                    </dd>
                  </>
                ) : (
                  <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                    {row.value}
                  </dd>
                );

                return row.id ? (
                  <Link
                    href={{
                      pathname: router.pathname,
                      query: { panel: row.id },
                    }}
                    scroll={false}
                  >
                    <a>{value}</a>
                  </Link>
                ) : (
                  value
                );
              })()}
            </div>
          ))}
        </dl>
      </div>
    </div>
  );
}
