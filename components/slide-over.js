/* This example requires Tailwind CSS v2.0+ */
import { useRouter } from "next/router";
import Router from "next/router";

import SlideOverTemplate from "./slide-over-template";
import PersonInfoPanel from "./panels/person/info";
import PersonPaymentsPanel from "./panels/person/payments";
import PersonMessagesPanel from "./panels/person/messages";
import ContractsCard from "./details/contracts-card";

import ContractInfoPanel from "./panels/contract/info";
import ContractHistoryPanel from "./panels/contract/history";

import CheckInfoPanel from "./panels/check/info";
import CheckPlannerPanel from "./panels/check/planner";

import DefectInfoPanel from "./panels/defect/info";
import DefectPlannerPanel from "./panels/defect/planner";

const rentContracts = [
  {
    id: 123,
    person: { name: "Jaylon Brown" },
    type: "Protokół przekazania",
    startDate: "7 Lutego 2021",
    status: "Podpisana",
  },
  {
    id: 123,
    person: { name: "Jaylon Brown" },
    type: "Umowa najmu",
    startDate: "7 Lutego 2021",
    endDate: "7 Lutego 2022",
    status: "Obowiązuje",
  },
  {
    id: 232,
    person: { name: "Ricardo Cooper" },
    type: "Protokół odbioru",
    startDate: "7 Stycznia 2021",
    status: "Podpisana",
  },
  {
    id: 232,
    person: { name: "Ricardo Cooper" },
    type: "Rozwiązanie",
    startDate: "7 Stycznia 2021",
    status: "Podpisana",
  },
  {
    id: 3434,
    person: { name: "Ricardo Cooper" },
    type: "Aneks",
    startDate: "7 Pażdziernika 2020",
    status: "Przerwana",
  },
  {
    id: 454,
    person: { name: "Ricardo Cooper" },
    type: "Umowa najmu",
    startDate: "22 Marca 2020",
    endDate: "22 Marca 2021",
    status: "Przerwana",
  },
  {
    id: 5656,
    person: { name: "Ricardo Cooper" },
    type: "Umowa najmu",
    startDate: "22 Marca 2019",
    endDate: "22 Marca 2020",
    status: "Zakończona",
  },
];

export default function Example({ isStartOpen }) {
  const router = useRouter();
  console.log("router.query", router.query.panel);

  const isOpen = isStartOpen || !!router.query.panel;

  const closePanel = () => {
    Router.push({ pathname: router.pathname }, undefined, { scroll: false });
  };

  const buildLinkHref = (tabId) => ({
    pathname: router.pathname,
    query: { panel: router.query.panel, tab: tabId },
  });

  const currentTabId = router.query.tab;

  switch (router.query.panel) {
    case "company":
    case "person":
      const personTabs = [
        {
          name: "Info",
          id: "info",
          current: !currentTabId || currentTabId === "info",
        },
        {
          name: "Płatności",
          id: "payments",
          current: currentTabId === "payments",
        },
        {
          name: "Umowy",
          id: "contracts",
          current: currentTabId === "contracts",
        },
        {
          name: "Wiadomości",
          id: "messages",
          current: currentTabId === "messages",
        },
      ];
      return (
        <SlideOverTemplate
          closePanel={closePanel}
          isOpen={isOpen}
          title="Osoba"
          tabs={personTabs}
          buildLinkHref={buildLinkHref}
        >
          {(() => {
            switch (currentTabId) {
              case "payments":
                return <PersonPaymentsPanel />;
              case "contracts":
                return (
                  <ContractsCard
                    title="Umowy najmu mieszkania Sienna 72/203"
                    contracts={rentContracts}
                  />
                );
              case "messages":
                return <PersonMessagesPanel />;

              case "info":
              default:
                return <PersonInfoPanel />;
            }
          })()}
        </SlideOverTemplate>
      );

    case "contract":
      const contractTabs = [
        {
          name: "Info",
          id: "info",
          current: !currentTabId || currentTabId === "info",
        },
        {
          name: "Historia",
          id: "history",
          current: currentTabId === "history",
        },
      ];
      return (
        <SlideOverTemplate
          closePanel={closePanel}
          isOpen={isOpen}
          title="Umowa"
          tabs={contractTabs}
          buildLinkHref={buildLinkHref}
        >
          {(() => {
            switch (currentTabId) {
              case "history":
                return <ContractHistoryPanel />;

              case "info":
              default:
                return <ContractInfoPanel />;
            }
          })()}
        </SlideOverTemplate>
      );
    case "check":
      const checkTabs = [
        {
          name: "Podsumowanie",
          id: "general",
          current: !currentTabId || currentTabId === "general",
        },
        {
          name: "Najbliższy",
          id: "closest",
          current: currentTabId === "closest",
        },
        {
          name: "Przeszłe",
          id: "history",
          current: currentTabId === "history",
        },
      ];
      return (
        <SlideOverTemplate
          closePanel={closePanel}
          isOpen={isOpen}
          title="Przegląd"
          tabs={checkTabs}
          buildLinkHref={buildLinkHref}
        >
          {(() => {
            switch (currentTabId) {
              case "closest":
                return <CheckPlannerPanel />;
              case "history":
                return <CheckPlannerPanel />;

              case "general":
              default:
                return <CheckInfoPanel />;
            }
          })()}
        </SlideOverTemplate>
      );
    case "defect":
      const defectTabs = [
        {
          name: "Info",
          id: "general",
          current: !currentTabId || currentTabId === "general",
        },
        {
          name: "Naprawa",
          id: "fix",
          current: currentTabId === "fix",
        },
        {
          name: "Historia",
          id: "history",
          current: currentTabId === "history",
        },
      ];
      return (
        <SlideOverTemplate
          closePanel={closePanel}
          isOpen={isOpen}
          title="Usterka"
          tabs={defectTabs}
          buildLinkHref={buildLinkHref}
        >
          {(() => {
            switch (currentTabId) {
              case "fix":
                return <DefectPlannerPanel />;
              case "history":
                return <DefectPlannerPanel />;

              case "general":
              default:
                return <DefectInfoPanel />;
            }
          })()}
        </SlideOverTemplate>
      );

    default:
      return (
        <SlideOverTemplate
          closePanel={closePanel}
          isOpen={isOpen}
          title="Nie dobrze..."
        >
          <div>Zły typ panelu.</div>
        </SlideOverTemplate>
      );
  }
}
