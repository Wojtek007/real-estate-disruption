const createMachine = (stateMachineDefinition) => {
  const machine = {
    value: stateMachineDefinition.initialState,
    transitions: (currentState) =>
      stateMachineDefinition[
        currentState ?? stateMachineDefinition.initialState
      ].transitions,
    transition(currentState, event) {
      const currentStateDefinition =
        stateMachineDefinition[
          currentState ?? stateMachineDefinition.initialState
        ];
      const destinationTransition = currentStateDefinition.transitions[event];
      if (!destinationTransition) {
        console.error(
          "defect state machine destinationTransition Not FOUND!",
          currentState,
          event
        );
        return currentState;
      }
      const destinationState = destinationTransition.target;
      const destinationStateDefinition =
        stateMachineDefinition[destinationState];
      machine.value = destinationState;

      return destinationState;
    },
  };

  return machine;
};

const states = {
  REPORTED: { name: "Zgłoszone", color: "gray", icon: "", key: "REPORTED" },
  TO_FIX_NOW: {
    name: "Do naprawy teraz",
    color: "green",
    icon: "Check",
    key: "TO_FIX_NOW",
  },
  TO_FIX_IN_FUTURE: {
    name: "Do naprawy w przyszłości",
    color: "yellow",
    icon: "Calendar",
    key: "TO_FIX_IN_FUTURE",
  },
  FIX_REFUSED: {
    name: "Odrzucona naprawa",
    color: "red",
    icon: "X",
    key: "FIX_REFUSED",
  },
  FIX_CONTRACTOR_VALUATION: {
    name: "Oczekuje wyceny",
    color: "green",
    icon: "Check",
    key: "FIX_CONTRACTOR_VALUATION",
  },
  SELF_FIX: {
    name: "Do samodzielnej naprawy",
    color: "green",
    icon: "Check",
    key: "SELF_FIX",
  },
  FIX_AGREED_WITH_OWNER: {
    name: "Naprawa zaakceptowana",
    color: "green",
    icon: "Check",
    key: "FIX_AGREED_WITH_OWNER",
  },
  FIX_REFUSED_BY_OWNER: {
    name: "Naprawa odrzucona",
    color: "red",
    icon: "Check",
    key: "FIX_REFUSED_BY_OWNER",
  },
  CONTRACTOR_APPOINTMENT_AGREED: {
    name: "Naprawa umówiona",
    color: "green",
    icon: "Check",
    key: "CONTRACTOR_APPOINTMENT_AGREED",
  },
  MATERIALS_BOUGHT: {
    name: "Materiały zakupione",
    color: "green",
    icon: "Check",
    key: "MATERIALS_BOUGHT",
  },
  FIX_DONE: {
    name: "Naprawa wykonana",
    color: "green",
    icon: "Check",
    key: "FIX_DONE",
  },
  PAYMENT_DONE: {
    name: "Opłacona naprawa",
    color: "green",
    icon: "Check",
    key: "PAYMENT_DONE",
  },
  COSTS_RETURNED_BY_OWNER: {
    name: "Rozliczono naprawę",
    color: "green",
    icon: "Check",
    key: "COSTS_RETURNED_BY_OWNER",
  },
  //   REPORTED:'REPORTED',
};

const events = {
  FIX_NOW: "FIX_NOW",
  FIX_IN_FUTURE: "FIX_IN_FUTURE",
  REFUSE_FIX: "REFUSE_FIX",
  ADD_VALUATION: "ADD_VALUATION",
  FIX_YOURSELF: "FIX_YOURSELF",
  OWNER_AGREEE: "OWNER_AGREEE",
  OWNER_REFUSED: "OWNER_REFUSED",
  APOINTMENT_AGREED: "APOINTMENT_AGREED",
  MATERIALS_BOUGHT: "MATERIALS_BOUGHT",
  FIX_DONE: "FIX_DONE",
  PAYMENT_DONE: "PAYMENT_DONE",
  COSTS_RETURNED_BY_OWNER: "COSTS_RETURNED_BY_OWNER",
};

const machine = createMachine({
  initialState: states.REPORTED.key,
  [states.REPORTED.key]: {
    transitions: {
      [events.FIX_NOW]: {
        target: states.TO_FIX_NOW.key,
        name: "Do naprawy teraz",
      },
      [events.FIX_IN_FUTURE]: {
        target: states.TO_FIX_IN_FUTURE.key,
        name: "Do naprawy w przyszłości",
      },
      [events.REFUSE_FIX]: {
        target: states.FIX_REFUSED.key,
        name: "Odrzuć",
      },
    },
  },
  [states.FIX_REFUSED.key]: {
    transitions: {},
  },
  [states.TO_FIX_NOW.key]: {
    transitions: {
      [events.ADD_VALUATION]: {
        target: states.FIX_CONTRACTOR_VALUATION.key,
        name: "Dodaj wycenę",
      },
      [events.FIX_YOURSELF]: {
        target: states.SELF_FIX.key,
        name: "Wybierz naprawę samemu",
      },
    },
  },
  [states.TO_FIX_IN_FUTURE.key]: {
    transitions: {
      [events.ADD_VALUATION]: {
        target: states.FIX_CONTRACTOR_VALUATION.key,
        name: "Dodaj wycenę",
      },
      [events.FIX_YOURSELF]: {
        target: states.SELF_FIX.key,
        name: "Wybierz naprawę samemu",
      },
    },
  },
  [states.FIX_CONTRACTOR_VALUATION.key]: {
    transitions: {
      [events.OWNER_AGREEE]: {
        target: states.FIX_AGREED_WITH_OWNER.key,
        name: "OWNER_AGREEE",
      },
      [events.OWNER_REFUSED]: {
        target: states.FIX_REFUSED_BY_OWNER.key,
        name: "OWNER_REFUSED",
      },
    },
  },
  [states.SELF_FIX.key]: {
    transitions: {
      [events.OWNER_AGREEE]: {
        target: states.FIX_AGREED_WITH_OWNER.key,
        name: "OWNER_AGREEE",
      },
      [events.OWNER_REFUSED]: {
        target: states.FIX_REFUSED_BY_OWNER.key,
        name: "OWNER_REFUSED",
      },
    },
  },
  [states.FIX_REFUSED_BY_OWNER.key]: {
    transitions: {},
  },
  [states.FIX_AGREED_WITH_OWNER.key]: {
    transitions: {
      [events.APOINTMENT_AGREED]: {
        target: states.CONTRACTOR_APPOINTMENT_AGREED.key,
        name: "APOINTMENT_AGREED",
      },
    },
  },
  [states.CONTRACTOR_APPOINTMENT_AGREED.key]: {
    transitions: {
      [events.MATERIALS_BOUGHT]: {
        target: states.MATERIALS_BOUGHT.key,
        name: "MATERIALS_BOUGHT",
      },
    },
  },
  [states.MATERIALS_BOUGHT.key]: {
    transitions: {
      [events.FIX_DONE]: {
        target: states.FIX_DONE.key,
        name: "FIX_DONE",
      },
    },
  },
  [states.FIX_DONE.key]: {
    transitions: {
      [events.PAYMENT_DONE]: {
        target: states.PAYMENT_DONE.key,
        name: "PAYMENT_DONE",
      },
    },
  },
  [states.PAYMENT_DONE.key]: {
    transitions: {
      [events.COSTS_RETURNED_BY_OWNER]: {
        target: states.COSTS_RETURNED_BY_OWNER.key,
        name: "COSTS_RETURNED_BY_OWNER",
      },
    },
  },
  [states.COSTS_RETURNED_BY_OWNER.key]: {
    transitions: {},
  },
});
export const createDefect = (defect) => {
  // console.log("STATEFUL - 11", destinationStateDefinition);
  return { ...defect, machine: defect.machine ?? machine };
};

export const getStatus = (defect) => {
  // console.log("STATEFUL - 11", destinationStateDefinition);
  return states[defect.machine.value];
};

export const buildDefectActions = (defect, status, setStatus) => {
  return Object.entries(defect.machine.transitions(status)).map(
    ([key, value]) => {
      console.log("STATEFUL - 11", key, value);
      const color = states[value.target].color;
      const icon = states[value.target].icon;
      const name = value.name;
      const onClick = () => setStatus(defect.machine.transition(status, key));
      return { color, icon, name, onClick };
    }
  );
};
