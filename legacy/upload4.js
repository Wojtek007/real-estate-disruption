import React, { useState } from "react";
import AWS from "aws-sdk";
import axios from "axios";

const S3_BUCKET = "hrigo-qa1";
const REGION = "eu-central-1";

AWS.config.update({
  accessKeyId: "AKIAVX6E3SEBBLNFNCZS",
  secretAccessKey: "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",
});

const myBucket = new AWS.S3({
  params: { Bucket: S3_BUCKET },
  region: REGION,
});

const UploadImageToS3WithNativeSdk = () => {
  const [progress, setProgress] = useState(0);
  const [selectedFile, setSelectedFile] = useState(null);
  const [uploadState, setUploadState] = useState({});

  const handleFileInput = (e) => {
    setSelectedFile(e.target.files[0]);
  };

  const uploadFile = (file) => {
    const params = {
      ACL: "public-read",
      Body: file,
      Bucket: S3_BUCKET,
      Key: file.name,
    };

    myBucket
      .putObject(params)
      .on("httpUploadProgress", (evt) => {
        setProgress(Math.round((evt.loaded / evt.total) * 100));
      })
      .send((err) => {
        if (err) console.log(err);
      });
  };

  const handleUpload = (file) => {
    // let file = uploadInput.current.files[0];
    // Split the filename to get the name and type
    let fileParts = file.name.split(".");
    let fileName = fileParts[0];
    let fileType = fileParts[1];
    axios
      .post("/api/file-upload", {
        fileName: fileName,
        fileType: fileType,
      })
      .then((res) => {
        const signedRequest = res.data.signedRequest;
        const url = res.data.url;
        setUploadState({
          ...uploadState,
          url,
        });

        var options = {
          headers: {
            "Content-Type": fileType,
          },
          onUploadProgress: (evt) => {
            setProgress(Math.round((evt.loaded / evt.total) * 99));
          },
        };
        axios
          .put(signedRequest, file, options)
          .then((_) => {
            setProgress(100);

            setUploadState({ ...uploadState, success: true });
            // mutate();
          })
          .catch((error) => {
            console.log("error", "We could not upload your image", error);
          });
      })
      .catch((error) => {
        console.log("error", "We could not upload your image", error);
      });
  };
  console.log("uploadState", uploadState);
  return (
    <div>
      <div>Native SDK File Upload Progress is {progress}%</div>
      <input type="file" onChange={handleFileInput} />
      <button onClick={() => handleUpload(selectedFile)}> Upload to S3</button>
    </div>
  );
};

export default UploadImageToS3WithNativeSdk;
