import fetch from "isomorphic-unfetch";
import { ApolloClient } from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import { HttpLink } from "apollo-link-http";
import { setContext } from "apollo-link-context";
import { onError } from "apollo-link-error";
import { WebSocketLink } from "apollo-link-ws";
import { SubscriptionClient } from "subscriptions-transport-ws";
import { parseCookies } from "nookies";
import auth0 from "./auth0";

let accessToken = null;

const requestAccessToken = async () => {
  if (accessToken) return;
  console.log("requestAccessToken", process.env.APP_HOST, window.location.href, window.location);

  const res = await fetch(`/api/session`);
  if (res.ok) {
    const json = await res.json();
    accessToken = json.accessToken;
    console.log("accessToken", accessToken);
  } else {
    accessToken = "public";
  }
};

// remove cached token on 401 from the server
const resetTokenLink = onError(({ networkError }) => {
  if (
    networkError &&
    networkError.name === "ServerError" &&
    networkError.statusCode === 401
  ) {
    accessToken = null;
  }
});

const createHttpLink = (headers) => {
  const httpLink = new HttpLink({
    uri: `https://${process.env.HASURA_URL}/v1/graphql`,
    credentials: "include",
    headers, // auth token is fetched on the server side
    fetch,
  });
  return httpLink;
};

const createWSLink = () => {
  console.log("createWSLink", process.env.HASURA_URL, process.env);
  return new WebSocketLink(
    new SubscriptionClient(`wss://${process.env.HASURA_URL}/v1/graphql`, {
      lazy: true,
      reconnect: true,
      connectionParams: async () => {
        await requestAccessToken(); // happens on the client
        return {
          headers: {
            authorization: accessToken ? `Bearer ${accessToken}` : "",
          },
        };
      },
    })
  );
};

export default function createApolloClient(initialState, headers) {
  const ssrMode = typeof window === "undefined";
  let link;
  if (ssrMode) {
    link = createHttpLink(headers);
  } else {
    link = createWSLink();
  }
  return new ApolloClient({
    ssrMode,
    link,
    cache: new InMemoryCache().restore(initialState),
  });
}

export async function staticFetch(query, { variables, preview } = {}) {
  // console.log("variables", variables, query);
  const res = await fetch(`https://${process.env.HASURA_URL}/v1/graphql`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      // Authorization: `Bearer ${accessToken}`,
      "x-hasura-admin-secret": process.env.HASURA_ADMIN_SECRET,
    },
    body: JSON.stringify({
      query,
      variables,
    }),
  });

  const json = await res.json();
  if (json.errors) {
    console.error(json.errors);
    throw new Error("Failed to fetch API", json.errors);
  }
  return json.data;
}
