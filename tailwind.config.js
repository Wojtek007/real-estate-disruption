module.exports = {
  purge: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
    "./modules/**/*.{js,ts,jsx,tsx}",
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        indigo: {
          // 'copper-rust': {
          DEFAULT: "#995B4D",
          50: "#EEE0DD",
          100: "#E5D1CC",
          200: "#D4B2AA",
          300: "#C39388",
          400: "#B37566",
          500: "#995B4D",
          600: "#77473C",
          700: "#55332B",
          800: "#331E1A",
          900: "#110A09",
        },
        "santa-fe": {
          DEFAULT: "#B07163",
          50: "#FDFCFC",
          100: "#F5EDEB",
          200: "#E4CEC9",
          300: "#D3AFA7",
          400: "#C19085",
          500: "#B07163",
          600: "#95594B",
          700: "#73453A",
          800: "#513029",
          900: "#2F1C18",
        },
        "spicy-mix": {
          DEFAULT: "#885144",
          50: "#E5D1CC",
          100: "#DDC1BB",
          200: "#CCA399",
          300: "#BB8477",
          400: "#AA6555",
          500: "#885144",
          600: "#663D33",
          700: "#442822",
          800: "#221411",
          900: "#000000",
        },
        ochre: {
          DEFAULT: "#c39100",
          50: "#fefce8",
          100: "#fef9c3",
          200: "#fef08a",
          300: "#fde047",
          400: "#e1b600",
          500: "#c39100",
          600: "#ba7c00",
          700: "#a16207",
          800: "#854d0e",
          900: "#713f12",
        },
        gold: {
          DEFAULT: "#B27B15",
          50: "#F6E0B6",
          100: "#F4D69F",
          200: "#EEC372",
          300: "#E9AF44",
          400: "#E09B1A",
          500: "#B27B15",
          600: "#845B10",
          700: "#573C0A",
          800: "#291C05",
          900: "#000000",
        },
        copper: {
          DEFAULT: "#AE8537",
          50: "#F3E9D8",
          100: "#EDDFC5",
          200: "#E0C99E",
          300: "#D4B477",
          400: "#C89E51",
          500: "#AE8537",
          600: "#88672B",
          700: "#614A1F",
          800: "#3A2C12",
          900: "#130F06",
        },
      },
    },
  },
  variants: {
    extend: {
      opacity: ["disabled"],
      textColor: ["disabled"],
      backgroundColor: ["disabled"],
    },
  },
  plugins: [require("@tailwindcss/forms")],
};
