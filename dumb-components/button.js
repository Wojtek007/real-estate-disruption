import Link from "next/link";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

const Button = ({
  pathname,
  query,
  onClick,
  children,
  isSecondary = false,
  className,
}) => {
  const primaryClassName =
    "relative inline-flex items-center px-4 py-2 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500";
  const secondaryClassName =
    "relative inline-flex items-center px-4 py-2 border-2 border-indigo-600 shadow-sm text-sm font-medium rounded-md text-indigo-600 bg-white hover:bg-gray-100 hover:text-indigo-800 hover:border-indigo-800 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500";
  return (
    <div className={classNames(className, "flex-shrink-0")}>
      {onClick
        ? <button
          type="button"
          onClick={onClick}
          className={isSecondary ? secondaryClassName : primaryClassName}
        >
          {children}
        </button>
        : <Link
          href={{
            pathname,
            query,
          }}
        >
          <a
            className={isSecondary ? secondaryClassName : primaryClassName}
          >
            {children}
          </a>
        </Link>}
    </div>
  );
};

export default Button;
