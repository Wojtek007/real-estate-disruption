import { useRouter } from "next/router";
import Link from "next/link";

import TabNav from "../../components/tab-nav";

import InfoPanel from "./info";
import Planner from "./planner";
// import ContractsPanel from "./contracts";
// import PaymentsPanel from "../../../../components/panels/person/payments";
// import MessagesPanel from "../../../../components/panels/person/messages";
import DefectPlannerPanel from "../../../../components/panels/defect/planner";

import CreateFormBuilder from "@modules/forms/create-form-slider.js";
import EditFormBuilder from "@modules/forms/edit-form-slider.js";

const tabs = [
  {
    name: "Detale",
    id: "details",
  },

  // {
  //   name: "Naprawa",
  //   id: "fix",
  // },
  // {
  //   name: "Historia",
  //   id: "history",
  // },
];

export default function Example({ buildLinkHref }) {
  const router = useRouter();
  const {
    tab = "details",
    notificationId,
    notificationVer,
    hidden,
    objectId,
    defectId,
    defectVer,
    id
  } = router.query;

  if (tab === "new-appointment") {
    return (
      <CreateFormBuilder
        objectName={"appointment"}
        object={{ notificationId, notificationVer }}
        hiddenFields={hidden}
      // onSuccess={({ id, ver }) => {
      //   onChange(`${objectId}Id`, id);
      //   onChange(`${objectId}Ver`, ver);
      //   setIsSlideOverOpen(false);
      // }}
      // onCancel={() => setIsSlideOverOpen(false)}
      />
    );
  }
  if (tab === "edit-appointment") {
    return (
      <EditFormBuilder
        objectName={"appointment"}
        // object={{ notificationId, notificationVer }}
        hiddenFields={hidden}
        objectId={objectId}
      // onSuccess={({ id, ver }) => {
      //   onChange(`${objectId}Id`, id);
      //   onChange(`${objectId}Ver`, ver);
      //   setIsSlideOverOpen(false);
      // }}
      // onCancel={() => setIsSlideOverOpen(false)}
      />
    );
  }
  if (tab === "add-fix-result") {
    return (
      <CreateFormBuilder
        objectName={"fixResult"}
        object={{
          defectId,
          defectVer
        }}
        hiddenFields={hidden}
      // onSuccess={({ id, ver }) => {
      //   onChange(`${objectId}Id`, id);
      //   onChange(`${objectId}Ver`, ver);
      //   setIsSlideOverOpen(false);
      // }}
      // onCancel={() => setIsSlideOverOpen(false)}
      />
    );
  }

  if (tab === "edit-defect") {
    return (
      <EditFormBuilder
        objectName={"defect"}
        // object={{ notificationId, notificationVer }}
        hiddenFields={hidden}
        objectId={id}
      // onSuccess={({ id, ver }) => {
      //   onChange(`${objectId}Id`, id);
      //   onChange(`${objectId}Ver`, ver);
      //   setIsSlideOverOpen(false);
      // }}
      // onCancel={() => setIsSlideOverOpen(false)}
      />
    );
  }
  return (
    <>
      <TabNav tabs={tabs} buildLinkHref={buildLinkHref} current={tab} />
      {(() => {
        switch (tab) {
          // case "payments":
          //   return <PaymentsPanel />;
          // case "contracts":
          //   return <ContractsPanel />;
          case "fix":
            return <Planner />;
          case "details":
            return <InfoPanel buildLinkHref={buildLinkHref} />;
        }
      })()}
    </>
  );
}
