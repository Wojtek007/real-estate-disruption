/* This example requires Tailwind CSS v2.0+ */
import { Fragment, useState } from "react";
import { Dialog, Menu, Transition } from "@headlessui/react";
import { XIcon } from "@heroicons/react/outline";
import { DotsVerticalIcon } from "@heroicons/react/solid";
import Link from "next/link";
import { useRouter } from "next/router";
import { ChevronDownIcon, FireIcon } from "@heroicons/react/solid";

import ProfileInfo from "../../components/profile-info";
import ContractObligations from "../../components/contract-obligations";
import Notification from "../../components/notification";
import ContractItem from "../../../property-details/components/contract-item";

import { useSubscription } from "@apollo/react-hooks";
import gql from "graphql-tag";
import * as moment from "moment";

// TODO add notes fields

// sourceCheck {
//   id
//   name
// }
// status {
//   color
//   icon
//   name
//   id
// }
// tag {
//   color
//   id
//   name
// }
// mediaAttachedToDefects {
//   mediaFile {
//     extension
//     id
//     name
//   }
// }

// reportingMessage {
//   author {
//     id
//     firstname
//     surname
//   }
//   content
//   createdAt
//   subject
//   id
// }
const GET_DEFECT = gql`
  subscription MySubscription12312333($defectId: uuid) {
    defects(where: { id: { _eq: $defectId } }) {
      id
      estimatedCost
      isForContractor
      isOwnerAcceptanceNecessary
      name
      notes
      reportingMessage
      property {
        id
        name
        ver
      }
      reportingMessage
      urgency {
        color
        id
        icon
        key
        name
        ver
      }
      ver
    }
  }
`;

const renderDefect = (defect) => {
  if (!defect) return [];

  const {
    createdAt,
    notes,
    property,
    sourceCheck,
    status,
    tag,
    mediaAttachedToDefects,
    reporter,
    reportingMessage,
    isForContractor,
    estimatedCost,
    isOwnerAcceptanceNecessary
  } = defect;
  return [
    // {
    //   label: "Osoba zgłaszająca",
    //   value: `${reporter.firstname ?? ""} ${reporter.surname ?? ""}`,
    // },
    {
      label: "Nieruchomość",
      value: property.name,
    },
    {
      label: "Data zgłoszenia",
      value: moment(createdAt).format("L"),
    },
    {
      label: "Notatki",
      value: notes,
    },
    {
      label: "Szacowany koszt",
      value: estimatedCost,
    },
    {
      label: "Czy zgoda właściciela wymagana",
      value: isOwnerAcceptanceNecessary ? 'Tak' : 'Nie',
    },
    {
      label: "Wymaga zewnętrznego specjalisty",
      value: isForContractor ? 'Tak' : 'Nie',
    },
  ];
};
export default function Example({ buildLinkHref }) {
  const router = useRouter();
  const { id: defectId } = router.query;
  const { loading, error, data } = useSubscription(GET_DEFECT, {
    variables: { defectId },
  });

  const defect = data?.defects[0];
  console.log("Info slide over - defect", defect);

  if (loading) {
    return <span>Wczytuję...</span>;
  }
  if (error) {
    console.error(error);
    return <span>Błąd! {error?.message ?? JSON.stringify(error)}</span>;
  }

  if (!defect) {
    return <span>Usterka o tym identyfikatorze nie istnieje</span>;
  }

  const tag = (
    <span
      className={classNames(
        `text-${defect.urgency.color}-800`,
        `bg-${defect.urgency.color}-100`,
        "flex-shrink-0 inline-block px-2 py-0.5 text-xs font-medium rounded-full"
      )}
    >
      {defect.urgency.name}
    </span>
  );

  const editBtn = (
    <Link href={buildLinkHref("edit-defect", {
      redirect: router.asPath,
      hidden: [
        "subjectProperty",
        "notification/data/precedingPeriod",
        "source_check"
      ],
    })} scroll={false}>
      <a>
        <button
          type="button"
          className="flex-shrink-0 inline-flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        >
          Edytuj
        </button>
      </a>
    </Link>
  );

  return (
    <div className="divide-y divide-gray-200">
      <ProfileInfo
        title={defect.name}
        subtitle={tag}
        primaryBtn={editBtn}
        // primaryBtn={<DropdownButton />}
        data={renderDefect(defect)}
      />

      {defect.reportingMessage && (
        <div className="px-4 py-5 sm:p-6">
          <h3 className="text-lg leading-6 font-medium text-gray-900 mb-5">
            Zgłoszenie
          </h3>
          <div
            key={defect.reportingMessage.id}
            className="bg-white  py-6 shadow sm:rounded-lg sm:px-6"
          >
            {/* <div className="sm:flex sm:justify-between sm:items-baseline">
              <h3 className="text-base font-medium">
                <span className="text-gray-900">
                  {defect.reportingMessage.author}
                </span>
                <span className="text-gray-600">napisał</span>
              </h3>
              <p className="mt-1 text-sm text-gray-600 whitespace-nowrap sm:mt-0 sm:ml-3">
                <time dateTime={defect.reportingMessage.createAt}>
                  {moment(defect.reportingMessage.createAt).format("L")}
                </time>
              </p>
            </div> */}
            <div
              className="mt-4 space-y-6 text-sm text-gray-800"
              dangerouslySetInnerHTML={{
                __html: defect.reportingMessage,
              }}
            />
          </div>
        </div>
      )}
      {/* <div className="px-4 py-5 sm:p-6">
        <h3 className="text-lg leading-6 font-medium text-gray-900 mb-5">
          Załączniki
        </h3>
        <Photos />
      </div> */}
    </div>
  );
}

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}
const tenantInfo = [
  { label: "Zgłoszone przez", value: "Margarett Tacher", id: "person" },
  {
    label: "Mieszkanie",
    value: "Sienna 72a/203, Kraków",
  },
  {
    label: "Notatka",
    value:
      "Fugiat ipsum ipsum deserunt culpa aute sint do nostrud anim incididunt cillum culpa consequat. Excepteur qui ipsum aliquip consequat sint. Sit id mollit nulla mollit nostrud in ea officia proident. Irure nostrud pariatur mollit ad adipisicing reprehenderit deserunt qui eu.",
  },
];
const message = {
  subject: "Re: New pricing for existing customers",
  sender: "joearmstrong@example.com",
  status: "Open",
  items: [
    {
      id: 1,
      author: "Joe Armstrong",
      date: "Yesterday at 7:24am",
      datetime: "2021-01-28T19:24",
      body: "<p>Straszenie się ściana popsuła.</p>",
    },
  ],
};

const items = [
  { name: "Save and schedule", href: "#" },
  { name: "Save and publish", href: "#" },
  { name: "Export PDF", href: "#" },
];

function DropdownButton() {
  return (
    <span className="relative z-0 inline-flex shadow-sm rounded-md">
      <button
        type="button"
        className="relative inline-flex items-center px-4 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:z-10 focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500"
      >
        <FireIcon className="w-5 h-5 text-red-400" aria-hidden="true" />
        <span className="ml-3">Wykonywana</span>
      </button>
      <Menu as="span" className="-ml-px relative block">
        {({ open }) => (
          <>
            <Menu.Button className="relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50 focus:z-10 focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500">
              <span className="sr-only">Open options</span>
              <ChevronDownIcon className="h-5 w-5" aria-hidden="true" />
            </Menu.Button>
            <Transition
              show={open}
              as={Fragment}
              enter="transition ease-out duration-100"
              enterFrom="transform opacity-0 scale-95"
              enterTo="transform opacity-100 scale-100"
              leave="transition ease-in duration-75"
              leaveFrom="transform opacity-100 scale-100"
              leaveTo="transform opacity-0 scale-95"
            >
              <Menu.Items
                static
                className="origin-top-right absolute right-0 mt-2 -mr-1 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none"
              >
                <div className="py-1">
                  {items.map((item) => (
                    <Menu.Item key={item.name}>
                      {({ active }) => (
                        <a
                          href={item.href}
                          className={classNames(
                            active
                              ? "bg-gray-100 text-gray-900"
                              : "text-gray-700",
                            "block px-4 py-2 text-sm"
                          )}
                        >
                          {item.name}
                        </a>
                      )}
                    </Menu.Item>
                  ))}
                </div>
              </Menu.Items>
            </Transition>
          </>
        )}
      </Menu>
    </span>
  );
}

const files = [
  {
    title: "IMG_4985.HEIC",
    size: "3.9 MB",
    source:
      "https://images.unsplash.com/photo-1582053433976-25c00369fc93?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=512&q=80",
  },
  {
    title: "IMG_4985.HEIC",
    size: "3.9 MB",
    source:
      "https://images.unsplash.com/photo-1582053433976-25c00369fc93?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=512&q=80",
  },
  {
    title: "IMG_4985.HEIC",
    size: "3.9 MB",
    source:
      "https://images.unsplash.com/photo-1582053433976-25c00369fc93?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=512&q=80",
  },
  {
    title: "IMG_4985.HEIC",
    size: "3.9 MB",
    source:
      "https://images.unsplash.com/photo-1582053433976-25c00369fc93?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=512&q=80",
  },
  {
    title: "IMG_4985.HEIC",
    size: "3.9 MB",
    source:
      "https://images.unsplash.com/photo-1582053433976-25c00369fc93?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=512&q=80",
  },
  {
    title: "IMG_4985.HEIC",
    size: "3.9 MB",
    source:
      "https://images.unsplash.com/photo-1582053433976-25c00369fc93?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=512&q=80",
  },
  {
    title: "IMG_4985.HEIC",
    size: "3.9 MB",
    source:
      "https://images.unsplash.com/photo-1582053433976-25c00369fc93?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=512&q=80",
  },
  {
    title: "IMG_4985.HEIC",
    size: "3.9 MB",
    source:
      "https://images.unsplash.com/photo-1582053433976-25c00369fc93?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=512&q=80",
  },
  // More files...
];

function Photos() {
  return (
    <ul
      role="list"
      className="grid grid-cols-2 gap-x-4 gap-y-8 sm:grid-cols-3 sm:gap-x-6 lg:grid-cols-4 xl:gap-x-8"
    >
      {files.map((file) => (
        <li key={file.source} className="relative">
          <div className="group block w-full aspect-w-10 aspect-h-7 rounded-lg bg-gray-100 focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-offset-gray-100 focus-within:ring-indigo-500 overflow-hidden">
            <img
              src={file.source}
              alt=""
              className="object-cover pointer-events-none group-hover:opacity-75"
            />
            <button
              type="button"
              className="absolute inset-0 focus:outline-none"
            >
              <span className="sr-only">View details for {file.title}</span>
            </button>
          </div>
          <p className="mt-2 block text-sm font-medium text-gray-900 truncate pointer-events-none">
            {file.title}
          </p>
          <p className="block text-sm font-medium text-gray-500 pointer-events-none">
            Pobierz
          </p>
        </li>
      ))}
    </ul>
  );
}
