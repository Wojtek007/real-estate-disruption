/* This example requires Tailwind CSS v2.0+ */
import { Fragment, useState } from "react";
import { Dialog, Menu, Transition } from "@headlessui/react";
import { XIcon } from "@heroicons/react/outline";
import { DotsVerticalIcon } from "@heroicons/react/solid";
import Link from "next/link";
import { useRouter } from "next/router";

import ProfileInfo from "../../components/profile-info";
import ContractsCard from "../../../property-details/components/contracts-card";

import { useSubscription } from "@apollo/react-hooks";
import gql from "graphql-tag";

const GET_CONTRACTS = gql`
  subscription MySubscription34342($personId: uuid) {
    contractCategories {
      id
      key
      label
      contracts(
        where: { signerId: { _eq: $personId } }
        order_by: { endDate: asc, subjectPropertyId: asc }
      ) {
        documentLink {
          name
          extension
          hasUploadFinished
          awsLink
          createdAt
        }
        startDate
        endDate
        id
        ver
        signer {
          surname
          firstname
          id
          ver
        }
        type {
          name
          id
          ver
        }
        subjectProperty {
          id
          name
        }
      }
    }
  }
`;

export default function Example() {
  const router = useRouter();
  const { id: personId } = router.query;
  const { loading, error, data } = useSubscription(GET_CONTRACTS, {
    variables: { personId },
  });

  const contractCategories = data?.contractCategories;
  console.log("contractCategories", contractCategories);

  if (loading) {
    return <span>Wczytuję...</span>;
  }
  if (error) {
    console.error(error);
    return <span>Błąd! {error?.message ?? JSON.stringify(error)}</span>;
  }

  if (!contractCategories) {
    return <span>Osoba o tym identyfikatorze nie istnieje</span>;
  }

  return contractCategories
    ?.filter((category) => category.contracts.length)
    .map((category) => {
      const groupedContracts = category.contracts.reduce(
        (grouper, contract) => {
          const thisContractGroup = grouper[contract.subjectProperty.id];
          if (thisContractGroup) {
            return {
              ...grouper,
              [contract.subjectProperty.id]: [...thisContractGroup, contract],
            };
          }

          return {
            ...grouper,
            [contract.subjectProperty.id]: [contract],
          };
        },
        {}
      );

      return Object.values(groupedContracts).map((contracts) => (
        <ContractsCard
          title={`${category.label} - ${contracts[0].subjectProperty.name}`}
          contracts={category.contracts}
          category={category}
          property={contracts[0].subjectProperty}
        />
      ));
    });
}
