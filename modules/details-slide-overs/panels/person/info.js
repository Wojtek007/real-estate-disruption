/* This example requires Tailwind CSS v2.0+ */
import { Fragment, useState } from "react";
import { Dialog, Menu, Transition } from "@headlessui/react";
import { XIcon } from "@heroicons/react/outline";
import { DotsVerticalIcon } from "@heroicons/react/solid";
import Link from "next/link";
import { useRouter } from "next/router";

import ProfileInfo from "../../components/profile-info";
import * as moment from "moment";
import { useSubscription } from "@apollo/react-hooks";
import gql from "graphql-tag";

const GET_PERSON = gql`
  subscription MySubscription4545452($personId: uuid) {
    people(where: { id: { _eq: $personId } }) {
      email
      firstname
      id
      nickname
      notes
      pesel
      phone
      surname
      ver
      employees {
        role
        company {
          name
          id
        }
      }
      ownedProperties {
        id
        name
      }
      contracts(
        where: {
          _and: {
            category: {key: {_eq: "rent"}}, 
            type: {effect: {_eq: "aggreament"}}, 
            endDate: {_gte: "${moment().format()}"}
          }
        }) {
        startDate
        endDate
        id
        ver
        subjectProperty {
          id
          name
        }
        
      }
    }
  }
`;

const renderPerson = (person) => {
  if (!person) return [];

  const {
    id,
    email,
    firstname,
    nickname,
    notes,
    pesel,
    phone,
    surname,
    address,
  } = person;
  return [
    {
      label: "Imie i nazwisko",
      value: `${firstname ?? ""} ${surname ?? ""} ${nickname ? '"' + nickname + '"' : ""
        }`,
      panel: "person",
      id,
    },
    { label: "Numer konta", value: "1231 2313 4544 1231 2313 4544" },
    { label: "Email", value: email },
    { label: "Telefon", value: phone },
    { label: "Pesel", value: pesel },
    {
      label: "Notatki",
      value: notes,
    },
  ];
};

export default function Example({ buildLinkHref }) {
  const router = useRouter();
  const { id: personId } = router.query;
  const { loading, error, data } = useSubscription(GET_PERSON, {
    variables: { personId },
  });

  const person = data?.people[0];
  console.log("person", person);

  if (loading) {
    return <span>Wczytuję...</span>;
  }
  if (error) {
    console.error(error);
    return <span>Błąd! {error?.message ?? JSON.stringify(error)}</span>;
  }

  if (!person) {
    return <span>Osoba o tym identyfikatorze nie istnieje</span>;
  }
  const title = `${person.firstname ?? ""} ${person.surname ?? ""}`;
  const subtitle = person.employees.length
    ? `Pracuje jako ${person.employees[0].role} w ${person.employees[0].company.name}`
    : person.ownedProperties.length > 0
      ? `Posiada ${person.ownedProperties.length} nieruchomości`
      : person.contracts.length > 0
        ? `Wynajmuje nieruchomość ${person.contracts[0].subjectProperty.name}`
        : "";

  const editBtn = (
    <Link href={buildLinkHref("edit-person")} scroll={false}>
      <a>
        <button
          type="button"
          className="flex-shrink-0 inline-flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        >
          Edytuj
        </button>
      </a>
    </Link>
  );

  return (
    <ProfileInfo
      title={title}
      subtitle={subtitle}
      data={renderPerson(person)}
      primaryBtn={editBtn}
    />
  );
}
