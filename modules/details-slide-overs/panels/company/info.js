/* This example requires Tailwind CSS v2.0+ */
import { Fragment, useState } from "react";
import { Dialog, Menu, Transition } from "@headlessui/react";
import { XIcon } from "@heroicons/react/outline";
import { DotsVerticalIcon } from "@heroicons/react/solid";
import Link from "next/link";
import { useRouter } from "next/router";

import ProfileInfo from "../../components/profile-info";

import { useSubscription } from "@apollo/react-hooks";
import gql from "graphql-tag";

const GET_PERSON = gql`
  subscription GetCompany($companyId: uuid) {
    companies(where: { id: { _eq: $companyId } }) {
      email
      name
      id
      notes
      phone
      taxIdentificationNumber
      address {
        apartmentNumber
        city
        country
        postalCode
        id
        region
        street
        streetNumber
      }
      bankAccounts {
        accountNumber
        id
      }
      employees {
        id
        role
        person {
          id
          email
          firstname
          phone
          surname
        }
      }
      properties {
        id
        name
      }
    }
  }
`;
const renderAddress = (address) => {
  if (!address) return;
  const {
    apartmentNumber,
    city,
    country,
    postalCode,
    region,
    street,
    streetNumber,
  } = address;
  return `${street ? "ul. " + street : ""} ${streetNumber ?? ""}${
    apartmentNumber ? "/" + apartmentNumber : ""
  }${street ? ", " : ""}${postalCode ?? ""} ${city ?? ""} ${region ?? ""} ${
    country ?? ""
  }`;
};

const renderCompany = (company) => {
  if (!company) return [];

  const {
    id,
    email,
    name,
    notes,
    phone,
    taxIdentificationNumber,
    address,
    employees = [],
  } = company;
  return [
    { label: "Nazwa firmy", value: name, panel: "company", id },
    {
      label: "Adres",
      value: renderAddress(address),
    },
    { label: "Email", value: email },
    { label: "Telefon", value: phone },
    { label: "NIP", value: taxIdentificationNumber },
    ...employees.map((employee) => ({
      label: employee.role,
      desc: `${employee.person.firstname} ${employee.person.surname}`,
      value: employee.person.phone,
    })),
    {
      label: "Notatki",
      value: notes,
    },
  ];
};

export default function Example({ buildLinkHref }) {
  const router = useRouter();
  const { id: companyId } = router.query;
  const { loading, error, data } = useSubscription(GET_PERSON, {
    variables: { companyId },
  });

  const company = data?.companies[0];
  console.log("company", company);

  if (loading) {
    return <span>Wczytuję...</span>;
  }
  if (error) {
    console.error(error);
    return <span>Błąd! {error?.message ?? JSON.stringify(error)}</span>;
  }

  if (!company) {
    return <span>Osoba o tym identyfikatorze nie istnieje</span>;
  }
  const subtitle =
    company.properties.length > 1
      ? `Administruje ${
          company.properties.length
        } nieruchomości: ${company.properties.map((p) => p.name).join(", ")}`
      : company.properties.length === 1
      ? `Administruje  nieruchomość ${company.properties[0].name}`
      : "";

  const editBtn = (
    <Link href={buildLinkHref("edit-company")} scroll={false}>
      <a>
        <button
          type="button"
          className="flex-shrink-0 inline-flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        >
          Edytuj
        </button>
      </a>
    </Link>
  );

  return (
    <ProfileInfo
      title={company.name}
      subtitle={subtitle}
      data={renderCompany(company)}
      primaryBtn={editBtn}
    />
  );
}
