import { useRouter } from "next/router";
import Link from "next/link";

import TabNav from "../../components/tab-nav";
import EditFormBuilder from "@modules/forms/edit-form-slider.js";

import InfoPanel from "./info";
// import ContractsPanel from "./contracts";
// import PaymentsPanel from "../../../../components/panels/person/payments";
// import MessagesPanel from "../../../../components/panels/person/messages";

const tabs = [
  {
    name: "Detale",
    id: "details",
  },
  // {
  //   name: "Płatności",
  //   id: "payments",
  // },
  // {
  //   name: "Umowy",
  //   id: "contracts",
  // },
  // {
  //   name: "Wiadomości",
  //   id: "messages",
  // },
];

export default function Example({ buildLinkHref }) {
  const router = useRouter();
  const { tab = "details", id } = router.query;

  if (tab === "edit-company") {
    return (
      <EditFormBuilder
        objectName={"company"}
        // object={{ notificationId, notificationVer }}
        // hiddenFields={hidden}
        objectId={id}
        // onSuccess={({ id, ver }) => {
        //   onChange(`${objectId}Id`, id);
        //   onChange(`${objectId}Ver`, ver);
        //   setIsSlideOverOpen(false);
        // }}
        // onCancel={() => setIsSlideOverOpen(false)}
      />
    );
  }

  return (
    <>
      <TabNav tabs={tabs} buildLinkHref={buildLinkHref} current={tab} />
      {(() => {
        switch (tab) {
          // case "payments":
          //   return <PaymentsPanel />;
          // case "contracts":
          //   return <ContractsPanel />;
          // case "messages":
          //   return <MessagesPanel />;
          case "details":
            return <InfoPanel buildLinkHref={buildLinkHref} />;
        }
      })()}
    </>
  );
}
