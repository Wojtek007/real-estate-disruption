import { useRouter } from "next/router";
import Link from "next/link";

import TabNav from "../../components/tab-nav";

import InfoPanel from "./info";
// import ContractsPanel from "./contracts";
// import PaymentsPanel from "../../../../components/panels/person/payments";
// import MessagesPanel from "../../../../components/panels/person/messages";

import CreateFormBuilder from "@modules/forms/create-form-slider.js";
import EditFormBuilder from "@modules/forms/edit-form-slider.js";

const tabs = [
  {
    name: "Detale",
    id: "details",
  },
  // {
  //   name: "Płatności",
  //   id: "payments",
  // },
  // {
  //   name: "Umowy",
  //   id: "contracts",
  // },
  // {
  //   name: "Wiadomości",
  //   id: "messages",
  // },
];

export default function Example({ buildLinkHref }) {
  const router = useRouter();
  const {
    tab = "details",
    id,
    notificationId,
    notificationVer,
    hidden,
    objectId,
    signerId,
    signerVer,
    categoryId,
    categoryVer,
    subjectPropertyId,
    subjectPropertyVer,
    typeId,
    typeVer,
    refersToContractId,
    refersToContractVer,
    notification_data_precedingPeriodId,
    notification_data_precedingPeriodVer
  } = router.query;

  if (tab === "new-appointment") {
    return (
      <CreateFormBuilder
        objectName={"appointment"}
        object={{ notificationId, notificationVer }}
        hiddenFields={hidden}
        overrideTitle={'Dodaj spotkanie'}
      // onSuccess={({ id, ver }) => {
      //   onChange(`${objectId}Id`, id);
      //   onChange(`${objectId}Ver`, ver);
      //   setIsSlideOverOpen(false);
      // }}
      // onCancel={() => setIsSlideOverOpen(false)}
      />
    );
  }
  if (tab === "edit-appointment") {
    return (
      <EditFormBuilder
        objectName={"appointment"}
        // object={{ notificationId, notificationVer }}
        hiddenFields={hidden}
        objectId={objectId}
        overrideTitle={'Edytuj spotkanie'}
      // onSuccess={({ id, ver }) => {
      //   onChange(`${objectId}Id`, id);
      //   onChange(`${objectId}Ver`, ver);
      //   setIsSlideOverOpen(false);
      // }}
      // onCancel={() => setIsSlideOverOpen(false)}
      />
    );
  }
  if (tab === "add-contract") {
    return (
      <CreateFormBuilder
        objectName={"contract"}
        object={{
          signerId,
          signerVer,
          categoryId,
          categoryVer,
          subjectPropertyId,
          subjectPropertyVer,
          typeId,
          typeVer,
          refersToContractId,
          refersToContractVer,
          notification_data_precedingPeriodId,
          notification_data_precedingPeriodVer
        }}
        hiddenFields={hidden}
      // onSuccess={({ id, ver }) => {
      //   onChange(`${objectId}Id`, id);
      //   onChange(`${objectId}Ver`, ver);
      //   setIsSlideOverOpen(false);
      // }}
      // onCancel={() => setIsSlideOverOpen(false)}
      />
    );
  }
  if (tab === "edit-contract") {
    return (
      <EditFormBuilder
        objectName={"contract"}
        // object={{ notificationId, notificationVer }}
        hiddenFields={hidden}
        objectId={id}
      // onSuccess={({ id, ver }) => {
      //   onChange(`${objectId}Id`, id);
      //   onChange(`${objectId}Ver`, ver);
      //   setIsSlideOverOpen(false);
      // }}
      // onCancel={() => setIsSlideOverOpen(false)}
      />
    );
  }
  return (
    <>
      <TabNav tabs={tabs} buildLinkHref={buildLinkHref} current={tab} />
      {(() => {
        switch (tab) {
          // case "payments":
          //   return <PaymentsPanel />;
          // case "contracts":
          //   return <ContractsPanel />;
          // case "messages":
          //   return <MessagesPanel />;
          case "details":
            return <InfoPanel buildLinkHref={buildLinkHref} />;
        }
      })()}
    </>
  );
}
