/* This example requires Tailwind CSS v2.0+ */
import { Fragment, useState } from "react";
import { Dialog, Menu, Transition } from "@headlessui/react";
import { XIcon } from "@heroicons/react/outline";
import { DotsVerticalIcon } from "@heroicons/react/solid";
import Link from "next/link";
import { useRouter } from "next/router";

import ProfileInfo from "../../components/profile-info";
import ContractObligations from "../../components/contract-obligations";
import Notification from "../../components/notification";
import ContractItem from "../../../property-details/components/contract-item";

import { useSubscription } from "@apollo/react-hooks";
import gql from "graphql-tag";
import * as moment from "moment";

const GET_CONTRACT = gql`
  subscription MySubscriptionDEtailsContract($contractId: uuid) {
    contracts(where: { id: { _eq: $contractId } }) {
      ...contractsFields

      contractObligations {
        financialObligation
        paidUnits
        ver
        updatedAt
        id
        obligationType {
          id
          key
          label
          relatedUtilityType {
            measurementUnit
            label
            key
            id
            icon
          }
        }
      }
      category {
        id
        key
        label
      }
      notification {
        createdAt
        id
        reminderDate
        precedingPeriod {
          id
          ver
          monthsBefore
          name
          weeksBefore
        }
        tasks {
          cost
          executionDate
          id
          name
          reponsiblePerson {
            id
            firstname
            surname
          }
        }

        contracts {
          id
        }
      }

      subjectProperty {
        id
        name
      }

      refersToContract {
        ...contractsFields
      }
    }
  }

  fragment contractsFields on contracts {
    documentLink {
      id
      name
      extension
      hasUploadFinished
      awsLink
      createdAt
    }
    startDate
    endDate
    id
    ver
    signer {
      surname
      firstname
      id
      ver
    }
    type {
      name
      id
      ver
    }
  }
`;

const renderContract = (contract) => {
  if (!contract) return [];

  const {
    id,
    ver,
    createdAt,
    documentLink,
    endDate,
    startDate,
    contractObligations,
    category,
    notification,
    signer: { firstname, surname },
    subjectProperty,
    type,
  } = contract;
  return [
    {
      label: "Sygnatariusz",
      value: `${firstname ?? ""} ${surname ?? ""}`,
    },
    {
      label: "Nieruchomość",
      value: subjectProperty.name,
    },
    {
      label: "Okres obowiązywania",

      value: endDate ? `${moment(startDate).format("L")} - ${moment(endDate).format("L")}` : moment(startDate).format("L"),
    },
    {
      label: "Rodzaj umowy",
      value: category.label,
    },
    {
      label: "Typ umowy",
      value: type.name,
    },
  ];
};

const renderTitle = (contract) => {
  if (!contract) return [];

  const {
    category,
    notification,
    signer: { firstname, surname },
    subjectProperty,
    type,
  } = contract;
  return `${type.name} ${subjectProperty.name} z ${firstname} ${surname}`;
};

export default function Example({ buildLinkHref }) {
  const router = useRouter();
  const { id: contractId } = router.query;
  const { loading, error, data } = useSubscription(GET_CONTRACT, {
    variables: { contractId },
  });

  const contract = data?.contracts[0];
  console.log("Info slide over - contract", contract);

  if (loading) {
    return <span>Wczytuję...</span>;
  }
  if (error) {
    console.error(error);
    return <span>Błąd! {error?.message ?? JSON.stringify(error)}</span>;
  }

  if (!contract) {
    return <span>Umowa o tym identyfikatorze nie istnieje</span>;
  }
  const editBtn = (
    <Link href={buildLinkHref("edit-contract", {
      redirect: router.asPath,
      hidden: [
        "category",
        "subjectProperty",
        "refersToContract",
        "type",
        "notification/data/reminderDate",
      ],
    })} scroll={false}>
      <a>
        <button
          type="button"
          className="flex-shrink-0 inline-flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        >
          Edytuj
        </button>
      </a>
    </Link>
  );
  return (
    <div className="divide-y divide-gray-200">
      <ProfileInfo
        title={renderTitle(contract)}
        subtitle={""}
        data={renderContract(contract)}
        primaryBtn={editBtn}

      />
      {contract.contractObligations.length > 0 ? (
        <ContractObligations
          contractObligations={contract.contractObligations}
        />
      ) : null}
      {/* <Notification notification={contract.notification} /> */}
      {contract.refersToContract && (
        <div className="px-4 py-5 sm:p-6">
          <h3 className="text-lg leading-6 font-medium text-gray-900 mb-5">
            Odnosi się do umowy
          </h3>
          <ContractItem
            contract={contract.refersToContract}
            property={contract.subjectProperty}
          />
        </div>
      )}
    </div>
  );
}
