/* This example requires Tailwind CSS v2.0+ */
import { CheckIcon, XIcon } from "@heroicons/react/solid";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

const exampleActionButton = (
  <a
    href="#"
    className="bg-indigo-600 text-white hover:bg-indigo-700 mt-6 w-full inline-block py-2 px-8 border border-transparent rounded-md shadow-sm text-center text-sm font-medium sm:mt-0 sm:w-auto lg:mt-6 lg:w-full"
  >
    Nieopłacone
  </a>
);

const exampleExplenation = [
  { main: "2", desc: "miesiące", subDesc: "21.06.2021 - 26.07.2021" },
  { main: "x" },
  { main: "123 zł", desc: "Zaliczka na prąd" },
];

export default function Example({
  title = "Przewidywany rachunek",
  finalAmount = "____ zł",
  actionButton, //= exampleActionButton,
  explenation// = exampleExplenation,
}) {
  return (
    <div className="bg-gray-50">
      <div className="relative ">
        <div className="relative mt-8 max-w-2xl mx-auto px-4 pb-8 sm:mt-12 sm:px-6 lg:max-w-7xl lg:px-8 lg:pb-0">
          <div
            aria-hidden="true"
            className="hidden absolute top-4 bottom-6 left-8 right-8 inset-0 bg-indigo-700 rounded-lg lg:block"
          />

          <div className="relative space-y-6 lg:space-y-0 lg:grid lg:grid-cols-2">
            <div className="bg-white ring-2 ring-indigo-700 shadow-md pt-6 px-6 pb-3 rounded-lg lg:px-8 lg:pt-12">
              <div>
                <h3 className="text-indigo-600 text-sm font-semibold uppercase tracking-wide">
                  {title}
                </h3>
                <div className="flex flex-col items-start sm:flex-row sm:items-center sm:justify-between lg:flex-col lg:items-start">
                  <div className="mt-3 flex items-center">
                    <p className="text-indigo-600 text-4xl font-extrabold tracking-tight">
                      {finalAmount}
                    </p>
                  </div>
                  {actionButton}
                </div>
              </div>
            </div>
            <div className="bg-indigo-700 lg:bg-transparent py-6 px-6 pb-3 rounded-lg lg:px-8 lg:py-12">
              <div className="items-start sm:flex-row sm:items-center sm:justify-between grid grid-cols-2">
                {explenation.map((ingridient, i) => (
                  <>
                    <p className="text-white text-xl font-extrabold tracking-tight">
                      {ingridient.main}
                    </p>
                    <div className="ml-4">
                      <p className="text-white text-sm">{ingridient.desc}</p>
                      <p className="text-indigo-200 text-sm">
                        {ingridient.subDesc}
                      </p>
                    </div>
                  </>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
