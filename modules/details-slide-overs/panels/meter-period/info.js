/* This example requires Tailwind CSS v2.0+ */
import { Fragment, useState } from "react";
import { Dialog, Menu, Transition } from "@headlessui/react";
import { XIcon } from "@heroicons/react/outline";
import { DotsVerticalIcon } from "@heroicons/react/solid";
import Link from "next/link";
import { useRouter } from "next/router";

import ProfileInfo from "../../components/profile-info";
import ContractObligations from "../../components/contract-obligations";
import Notification from "../../components/notification";
import ContractItem from "../../../property-details/components/contract-item";
import Cards from "./cards";
import Button from "@components/button";
import {
  getValidContractObligation,
  printMeterPeriodDate,
  printMeterPeriodPrepaidAmount,
  printMeterPeriodOwner,
  printMeterPeriodPrice,
  printMeterPeriodMeasurment,
  printMeterPeriodExpectedPrice,
  getPrepaidPeriods,
  printMeterPeriodRealPrice,
  printMeterPeriodPriceDifference,
  getMonthDifference,
  printPrepaidObligation
} from '@modules/property-details/meters/domain/meter-period'

import { useSubscription } from "@apollo/react-hooks";
import gql from "graphql-tag";
import * as moment from "moment";

const GET_METER_PERIOD = gql`
  subscription MyQueryB($meterPeriodId: uuid, $propertyId: uuid) {
    meterPeriods(where: { id: { _eq: $meterPeriodId } }) {
      id
      measurementDate
      measurementValue
      pricePerUnit
      finalPrice
      createdAt
      updatedAt
      meterPeriodPriceIngredients {
        bruttoPricePerUnit
        id
        name
        nettoPricePerUnit
        price
        quantity
      }
      paymentGroup {
        id
        ver
      }
      utilityType {
        icon
        id
        key
        label
        measurementUnit
        ver
        contractObligationTypes {
          label
          id
          isUnitBased
          key
          ver
          contractObligations(
            where: { contract: { subjectPropertyId: { _eq: $propertyId } } }
            order_by: { contract: { createdAt: asc } }
          ) {
            id
            financialObligation
            paidUnits
            obligationType {
              id
              key
              isUnitBased
              label
              relatedUtilityType {
                measurementUnit
                label
                key
                id
                icon
              }
            }
            contract {
              id
              signer {
                id
                firstname
                surname
              }
              startDate
              endDate
              type {
                name
                id
                effect
                ver
              }
              refersToContract {
                id
                startDate
                endDate
                type {
                  id
                  effect
                }
                contracts {
                  id
                  startDate
                  endDate
                  type {
                    id
                    effect
                  }
                }
              }
              contracts {
                id
                startDate
                endDate
                type {
                  id
                  effect
                }
              }
            }
          }
        }
      }
    }
  }
`;

const GET_PREVIOUS_METER_PERIOD = gql`
subscription MyQueryA( $propertyId: uuid, $currentPeriodDate: timestamptz,$utilityTypeId:uuid) {
  meterPeriods(where: {measurementDate: {_lt: $currentPeriodDate}, utilityTypeId: {_eq: $utilityTypeId},propertyId: {_eq: $propertyId}}, order_by: {measurementDate: desc}, limit: 1) {
    id
    measurementDate
    measurementValue
    pricePerUnit
    finalPrice
    createdAt
    updatedAt
    utilityType {
      icon
      id
      key
      label
      measurementUnit
      ver
      contractObligationTypes {
        label
        id
        isUnitBased
        key
        ver
        contractObligations(where: {contract: {subjectPropertyId: {_eq: $propertyId}}}, order_by: {contract: {createdAt: asc}}) {
          id
          financialObligation
          paidUnits
          contract {
            id
            signer {
              id
              firstname
              surname
            }
            startDate
            endDate
            type {
              name
              id
              effect
              ver
            }
            refersToContract {
              id
              startDate
              endDate
              type {
                id
                effect
              }
              contracts {
                id
                startDate
                endDate
                type {
                  id
                  effect
                }
              }
            }
            contracts {
              id
              startDate
              endDate
              type {
                id
                effect
              }
            }
          }
        }
      }
    }
  }
}

`;

const renderMeterPeriod = (period, utilityType) => {
  if (!period) return [];

  return [
    {
      label: "Data pomiaru",
      value: printMeterPeriodDate({ period }),
    },
    {
      label: "Wartość pomiaru",
      value: printMeterPeriodMeasurment({ period, utilityType }),
    },
    ...(period.finalPrice ? [
      {
        label: "Cena całkowita",
        value: `${period.finalPrice} zł`,
      },
      ...period.meterPeriodPriceIngredients.map(ingridient => ({
        label: ingridient.name,
        value: `${ingridient.bruttoPricePerUnit ?? 0} zł * ${ingridient.quantity ?? 0} = ${ingridient.price ?? 0} zł`
      }))
    ] : [])
  ];
};

const renderTitle = (meterPeriod) => {
  if (!meterPeriod) return [];

  const { measurementDate, utilityType } = meterPeriod;
  return `Rozliczenie za ${utilityType.label} z dnia ${moment(
    measurementDate
  ).format("L")}`;
};

export default function Example({ buildLinkHref }) {
  const router = useRouter();
  const { id: meterPeriodId, propertyId, organizationId } = router.query;
  const { loading, error, data } = useSubscription(GET_METER_PERIOD, {
    variables: { meterPeriodId, propertyId },
  });

  const meterPeriod = data?.meterPeriods[0];
  const utilityType = meterPeriod?.utilityType;
  const { loading: loadingPreviousPeriod, error: errorPreviousPeriod, data: dataPreviousPeriod } = useSubscription(GET_PREVIOUS_METER_PERIOD, {
    variables: { currentPeriodDate: meterPeriod?.measurementDate, propertyId, utilityTypeId: utilityType?.id },
    skip: !meterPeriod
  });
  const previousMeterPeriod = dataPreviousPeriod?.meterPeriods[0];
  console.log("Info slide over - meterPeriod", meterPeriod, meterPeriod?.measurementDate);
  console.log("Info slide over - previousMeterPeriod", !meterPeriod, loadingPreviousPeriod, errorPreviousPeriod, previousMeterPeriod);

  if (loading) {
    return <span>Wczytuję...</span>;
  }
  if (error) {
    console.error(error);
    return <span>Błąd! {error?.message ?? JSON.stringify(error)}</span>;
  }

  if (!meterPeriod) {
    return <span>Pomiar licznika o tym identyfikatorze nie istnieje</span>;
  }

  //COPY FORM meter-card
  const period = meterPeriod
  const previousPeriod = previousMeterPeriod

  const contractObligations = (utilityType.contractObligationTypes[0] && period && previousPeriod && getValidContractObligation(period, previousPeriod, utilityType)) ?? [];
  console.log('contractObligationsXXX', contractObligations, utilityType)



  // const getContractObligation = (utilityType) => {
  //   console.log("11111", utilityType);
  //   return utilityType.contractObligationTypes[0]?.contractObligations[0];
  // };
  // const getIsContractObligationUnitBased = (utilityType) => {
  //   console.log("11111", utilityType);
  //   return utilityType.contractObligationTypes[0]?.isUnitBased;
  // };

  // const getContractFinancialObligation = (utilityType) =>
  //   getContractObligation(utilityType)?.financialObligation ?? 0;
  // const getContractPaidUnitsIncluded = (utilityType) =>
  //   getContractObligation(utilityType)?.paidUnits ?? 0;
  // const getContractSigner = (utilityType) =>
  //   getContractObligation(utilityType)?.contract.signer;


  // console.log(
  //   "periodperiodpreviousPeriod",
  //   meterPeriod.measurementValue,
  //   previousMeterPeriod?.measurementValue
  // );
  // console.log("periodperiodutilityType", utilityType);
  // console.log("getIsContractObligationUnitBased(utilityType)", utilityType, getIsContractObligationUnitBased(utilityType));

  // const durationFromPreviousMeasurment = moment.duration(
  //   moment(meterPeriod.measurementDate).diff(moment(previousMeterPeriod?.measurementDate))
  // );
  // const monthDifference = Math.round(durationFromPreviousMeasurment.asMonths());
  // console.log("periodperiod monthDifference", monthDifference);
  // const predictedObligation = getIsContractObligationUnitBased(utilityType)
  //   ? getContractPaidUnitsIncluded(utilityType) * period.pricePerUnit * monthDifference
  //   : getContractFinancialObligation(utilityType) * monthDifference;
  // const realUsage = (meterPeriod.measurementValue - previousMeterPeriod?.measurementValue).toFixed(2);
  // const realPrice = period.finalPrice ?? (realUsage * meterPeriod.pricePerUnit).toFixed(2);
  // const priceDifference = (realPrice - predictedObligation).toFixed(2);

  // END of COPY FORM meter-card

  console.log('previousMeterPeriodXX', previousMeterPeriod)

  const editBtn = !meterPeriod.paymentGroup && previousMeterPeriod && (
    <Button
      pathname={`/${organizationId}/meter-period/${meterPeriod.id}/edit`}
      query={{
        removed: [
          'previousMeasurementValue'
        ],
        hidden: [
          "utilityType",
          "property",
          ...(previousMeterPeriod ? [] : ["meterPeriodPriceIngredients/data", "finalPrice", "pricePerUnit"]),
        ],
        previousMeasurementValue: previousMeterPeriod?.measurementValue,
        removed: ['previousMeasurementValue'],
        redirect: router.asPath,
      }}

    >
      Edytuj
    </Button>
  );

  const meterPeriodData = { period, utilityType, previousPeriod }


  return (
    <div className="divide-y divide-gray-200">
      <ProfileInfo
        title={renderTitle(meterPeriod)}
        subtitle={""}
        data={renderMeterPeriod(meterPeriod, utilityType)}
        primaryBtn={editBtn}
      />

      {contractObligations.length > 0 && <div className="px-4 py-5 sm:p-6">
        <h3 className="text-lg leading-6 font-medium text-gray-900 mb-5">
          Obliczane na podstawie umowy
        </h3>
        {contractObligations.map(contractObligation => (
          <ContractItem
            contract={contractObligation.contract}
            property={{}}
          />
        ))}
      </div>}
      {contractObligations.length > 0 ? (
        <ContractObligations contractObligations={contractObligations} />
      ) : null}
      {previousMeterPeriod && <>
        <div className="px-4 py-5 sm:p-6">
          <h3 className="text-lg leading-6 font-medium text-gray-900 mb-5">
            Obliczane rozliczenia okresu
          </h3>
        </div>
        <Cards
          title="Przewidywany rachunek"
          finalAmount={printMeterPeriodExpectedPrice(meterPeriodData)}
          explenation={
            getPrepaidPeriods(meterPeriodData).map((prepaidPeriod, i) => {
              console.log('P{PPPPPPP', prepaidPeriod)
              return (
                [
                  i !== 0 && {
                    main: "+"
                  },
                  {
                    main: `${getMonthDifference(prepaidPeriod)} miesiące x ${printPrepaidObligation({ prepaidPeriod })}`,
                    desc: 'Okres:',
                    subDesc: `${moment(prepaidPeriod.startDate).format("L")} - ${moment(prepaidPeriod.endDate).format("L")}`,
                  },
                  // { main: "x" },
                  // {
                  //   main: getIsContractObligationUnitBased(utilityType)
                  //     ? getContractPaidUnitsIncluded(utilityType) + " " + utilityType.measurementUnit
                  //     : `${getContractFinancialObligation(utilityType)} zł`,
                  //   desc: getContractObligation(utilityType)?.obligationType.label
                  // },
                ]
              )
            }).flat()



            //   [
            //   {
            //     main: `${monthDifference} miesiące`,
            //     desc: `${moment(previousPeriod?.measurementDate).format("L")} - ${moment(period.measurementDate).format("L")}`,
            //   },
            //   { main: "x" },
            //   {
            //     main: getIsContractObligationUnitBased(utilityType)
            //       ? getContractPaidUnitsIncluded(utilityType) + " " + utilityType.measurementUnit
            //       : `${getContractFinancialObligation(utilityType)} zł`,
            //     desc: getContractObligation(utilityType)?.obligationType.label
            //   },
            // ]
          }
        />
        <Cards
          title="Rzeczywisty rachunek"
          finalAmount={printMeterPeriodRealPrice(meterPeriodData)}
          explenation={[
            {
              main: printMeterPeriodMeasurment({ period, utilityType }),
              desc: "Stan licznika",
              subDesc: moment(period.measurementDate).format("L")
            },
            { main: "-" },
            {
              main: printMeterPeriodMeasurment({ period: previousPeriod, utilityType }),
              desc: "Poprzedni stan licznika",
              subDesc: moment(previousPeriod.measurementDate).format("L")
            },
            ...(period.pricePerUnit ? [
              { main: "x" },
              { main: printMeterPeriodPrice(meterPeriodData), desc: "Cena za jednostkę" }
            ] : [])
          ]}
        />
        <Cards
          title="Dopłata za okres"
          finalAmount={printMeterPeriodPriceDifference(meterPeriodData)}
          explenation={[
            { main: printMeterPeriodRealPrice(meterPeriodData), desc: "Rzeczywisty rachunek" },
            { main: "-" },
            { main: printMeterPeriodExpectedPrice(meterPeriodData), desc: "Przewidywany rachunek" },
          ]}
        />
      </>}
      {/* {contract.contractObligations.length > 0 ? (
        <ContractObligations
          contractObligations={contract.contractObligations}
        />
      ) : null}
      <Notification notification={contract.notification} />
      {contract.refersToContract && (
        <div className="px-4 py-5 sm:p-6">
          <h3 className="text-lg leading-6 font-medium text-gray-900 mb-5">
            Odnosi się do kontraktu
          </h3>
          <ContractItem
            contract={contract.refersToContract}
            property={contract.subjectProperty}
          />
        </div>
      )} */}
    </div>
  );
}
