import { useRouter } from "next/router";
import Link from "next/link";

import TabNav from "../../components/tab-nav";

import SummaryPanel from "./summary";
// import ContractsPanel from "./contracts";
// import PaymentsPanel from "../../../../components/panels/person/payments";
// import MessagesPanel from "../../../../components/panels/person/messages";

import CreateFormBuilder from "@modules/forms/create-form-slider.js";
import EditFormBuilder from "@modules/forms/edit-form-slider.js";

const tabs = [
  {
    name: "Detale",
    id: "details",
  },
  // {
  //   name: "Najbliższy",
  //   id: "closest",
  // },
  // {
  //   name: "Przeszłe",
  //   id: "history",
  // },
];
// http://localhost:3000/5a42f659-7087-439d-a1f9-27d5febffaf6/notifications
// ?panel=check&notificationId=9913b698-44dd-4d3c-8f83-bfaf67000955
// &notificationVer=1
// &id=278eedd8-8c85-4644-b807-eded644b9891&tab=new-appointment
export default function Example({ buildLinkHref }) {
  const router = useRouter();
  const {
    id,
    tab = "details",
    notificationId,
    notificationVer,
    parentNotificationId,
    parentNotificationVer,
    checkId,
    checkVer,
    date,
    cost,
    hidden,
    objectId,
    // defects_data_propertyId,
    // defects_data_propertyVer,
    nextCheckNotification_data_precedingPeriodId,
    nextCheckNotification_data_precedingPeriodVer,
    ...rest
  } = router.query;
  // console.log("initialStateinitialState- rest", rest);
  if (tab === "new-appointment") {
    return (
      <CreateFormBuilder
        objectName={"appointment"}
        object={{ notificationId, notificationVer }}
        hiddenFields={hidden}
      // onSuccess={({ id, ver }) => {
      //   onChange(`${objectId}Id`, id);
      //   onChange(`${objectId}Ver`, ver);
      //   setIsSlideOverOpen(false);
      // }}
      // onCancel={() => setIsSlideOverOpen(false)}
      />
    );
  }
  if (tab === "edit-appointment") {
    return (
      <EditFormBuilder
        objectName={"appointment"}
        // object={{ notificationId, notificationVer }}
        hiddenFields={hidden}
        objectId={objectId}
      // onSuccess={({ id, ver }) => {
      //   onChange(`${objectId}Id`, id);
      //   onChange(`${objectId}Ver`, ver);
      //   setIsSlideOverOpen(false);
      // }}
      // onCancel={() => setIsSlideOverOpen(false)}
      />
    );
  }
  if (tab === "add-check-result") {
    return (
      <CreateFormBuilder
        objectName={"checkResult"}
        object={{
          parentNotificationId,
          parentNotificationVer,
          checkId,
          checkVer,
          date,
          cost,
          // defects_data_propertyId,
          // defects_data_propertyVer,
          nextCheckNotification_data_precedingPeriodId,
          nextCheckNotification_data_precedingPeriodVer,
        }}
        hiddenFields={hidden}
      // onSuccess={({ id, ver }) => {
      //   onChange(`${objectId}Id`, id);
      //   onChange(`${objectId}Ver`, ver);
      //   setIsSlideOverOpen(false);
      // }}
      // onCancel={() => setIsSlideOverOpen(false)}
      />
    );
  }
  if (tab === "edit-check") {
    return (
      <EditFormBuilder
        objectName={"check"}
        // object={{ notificationId, notificationVer }}
        hiddenFields={hidden}
        objectId={id}
      // onSuccess={({ id, ver }) => {
      //   onChange(`${objectId}Id`, id);
      //   onChange(`${objectId}Ver`, ver);
      //   setIsSlideOverOpen(false);
      // }}
      // onCancel={() => setIsSlideOverOpen(false)}
      />
    );
  }
  return (
    <>
      <TabNav tabs={tabs} buildLinkHref={buildLinkHref} current={tab} />
      {(() => {
        switch (tab) {
          // case "payments":
          //   return <PaymentsPanel />;
          // case "contracts":
          //   return <ContractsPanel />;
          // case "messages":
          //   return <MessagesPanel />;
          case "details":
            return <SummaryPanel buildLinkHref={buildLinkHref} />;
          // case "new-appointment":
          //   return <div>a</div>;
        }
      })()}
    </>
  );
}
