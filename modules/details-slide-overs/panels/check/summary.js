/* This example requires Tailwind CSS v2.0+ */
import { Fragment, useState } from "react";
import { Dialog, Menu, Transition } from "@headlessui/react";
import { XIcon } from "@heroicons/react/outline";
import { DotsVerticalIcon } from "@heroicons/react/solid";
import Link from "next/link";
import { useRouter } from "next/router";

import ProfileInfo from "../../components/profile-info";
import ContractObligations from "../../components/contract-obligations";
import Notification from "../../components/notification";
import { CheckSteps } from "@modules/property-details/components/check-card";
import ContractItem from "../../../property-details/components/contract-item";
import DefectCard from "../../../property-details/components/defect-card";

import { useSubscription } from "@apollo/react-hooks";
import gql from "graphql-tag";
import * as moment from "moment";

/*
defects {
        id
        name
        status {
          icon
          name
          id
          color
        }
        tag {
          color
          id
          name
        }
      }
*/

const GET_CHECK = gql`
  subscription MySubscriptionSummmary($checkId: uuid) {
    checks(where: { id: { _eq: $checkId } }) {
      id
      ver
      name
      recurrenceInterval {
        id
        ver
        name
      }
      checkResults(limit: 1, order_by: {date: desc}) {
        ...checkResultsFields
      }
      initialCheckResult {
        ...checkResultsFields
      }
    }
  }

  fragment checkResultsFields on checkResults {
    cost
    date
    id
    nextCheckNotification {
      appointments {
        attendeeCompany {
          id
          name
        }
        attendeePerson {
          id
          firstname
          surname
        }
        date
        id
        notes
        plannedCost
      }
      id
      ver
      tasks {
        config
        cost
        decision
        id
        isDone
        name
        reponsiblePerson {
          firstname
          id
          surname
        }
      }
      precedingPeriod {
        id
        monthsBefore
        name
        weeksBefore
      }
      reminderDate
    }
  }
`;

const renderContract = (contract) => {
  if (!contract) return [];

  const {
    id,
    ver,
    createdAt,
    documentLink,
    endDate,
    startDate,
    contractObligations,
    category,
    notification,
    signer: { firstname, surname },
    subjectProperty,
    type,
  } = contract;
  return [
    {
      label: "Sygnatariusz",
      value: `${firstname ?? ""} ${surname ?? ""}`,
    },
    {
      label: "Nieruchomość",
      value: subjectProperty.name,
    },
    {
      label: "Okres obowiązywania",

      value: `${moment(startDate).format("L")} - ${moment(endDate).format(
        "L"
      )}`,
    },
    {
      label: "Rodzaj umowy",
      value: category.label,
    },
    {
      label: "Typ umowy",
      value: type.name,
    },
  ];
};
const renderAppointment = (appointment) => {
  if (!appointment) return [];

  const {
    attendeeCompany,
    attendeePerson,
    date: appointmentDate,
    createdAt,
    id,
    notes,
    plannedCost,
  } = appointment;
  return [
    ...(attendeePerson?.id
      ? [
        {
          label: "Osoba wykonująca przegląd",
          value: `${attendeePerson.firstname ?? ""} ${attendeePerson.surname ?? ""
            }`,
          id: attendeePerson.id,
        },
      ]
      : []),
    ...(attendeeCompany?.id
      ? [
        {
          label: "Firma wykonująca przegląd",
          value: attendeeCompany.name,
          id: attendeeCompany.id,
        },
      ]
      : []),
    {
      label: "Planowany koszt",
      value: plannedCost,
    },
    {
      label: "Data przeglądu",

      value: moment(appointmentDate).format("L"),
    },
    {
      label: "Notatki",
      value: notes,
    },
  ];
};

const renderTitle = (contract) => {
  if (!contract) return [];

  const {
    category,
    notification,
    signer: { firstname, surname },
    subjectProperty,
    type,
  } = contract;
  return `${type.name} ${subjectProperty.name} z ${firstname} ${surname}`;
};

export default function Example({ buildLinkHref }) {
  const router = useRouter();
  const { id: checkId } = router.query;
  const { loading, error, data } = useSubscription(GET_CHECK, {
    variables: { checkId },
  });
  const [isHovered, setIsHovered] = useState(false);

  const check = data?.checks[0];
  console.log("Info slide over - check", check);

  if (loading) {
    return <span>Wczytuję...</span>;
  }
  if (error) {
    console.error(error);
    return <span>Błąd! {error?.message ?? JSON.stringify(error)}</span>;
  }

  if (!check) {
    return <span>Przegląd o tym identyfikatorze nie istnieje</span>;
  }
  // const isAlreadyDoneOnce = !!check?.checkResults.length;
  const checkResult = check?.checkResults.length
    ? check.checkResults[0]
    : check?.initialCheckResult;
  // const { monthsBefore, weeksBefore } = checkResult.nextCheckNotification.precedingPeriod ?? {};
  // const { date } = checkResult
  // const notificationDate = moment(date).add(1, "years")
  //   .subtract(monthsBefore, "months")
  //   .subtract(weeksBefore, "weeks")

  console.log("Info slide over - checkResult", checkResult);

  const notification = checkResult.nextCheckNotification;
  const appointment = notification?.appointments[0];

  const editBtn = (
    <Link href={buildLinkHref("edit-check", {
      redirect: router.asPath,
      hidden: [
        "property",
        "check",
        "initialCheckResult/data/invoiceScan",
        "initialCheckResult/data/report",
        "initialCheckResult/data/cost",
        "initialCheckResult/data/date",
        "initialCheckResult/data/nextCheckNotification/data/reminderDate",
        "initialCheckResult/data/nextCheckNotification/data/precedingPeriod",
        "initialCheckResult/data/parentNotification",
      ],
    })} scroll={false}>
      <a>
        <button
          type="button"
          className="flex-shrink-0 inline-flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        >
          Edytuj przegląd
        </button>
      </a>
    </Link>
  );

  return (
    <div className="divide-y divide-gray-200">
      <ProfileInfo
        title={check.name}
        subtitle={`${check.recurrenceInterval.name} przegląd`}
        primaryBtn={editBtn}
      />
      <div className="px-4 py-5 sm:p-6">
        <h3 className="text-lg leading-6 font-medium text-gray-900">
          Status najbliższego przeglądu
        </h3>
        {/* <div className="mt-5">
          <div
            className="rounded-md bg-gray-50 border-t border-b border-gray-200 "
            onMouseEnter={() => setIsHovered(true)}
            onMouseLeave={() => setIsHovered(false)}
          >
            <CheckSteps check={check} isHovered={isHovered} />
          </div>
        </div> */}
      </div>
      {appointment && <ProfileInfo title={`Szczegóły umówionego przeglądu`} subtitle="" data={renderAppointment(appointment)} />}
      {/* <Notification notification={notification} /> */}
      {check.defects?.length > 0 ? (
        <div className="px-4 py-5 sm:p-6">
          <h3 className="text-lg leading-6 font-medium text-gray-900">
            Powiązane usterki
          </h3>
          <ul className="mt-5 grid grid-cols-1 gap-6 ">
            {checkResult.defects?.map((defect) => (
              <DefectCard defect={defect} />
            ))}
          </ul>
        </div>
      ) : null}
    </div>
  );

  return (
    <>
      <ProfileInfo
        title={renderTitle(contract)}
        subtitle={""}
        data={renderContract(contract)}
      />
      {contract.contractObligations.length > 0 ? (
        <ContractObligations
          contractObligations={contract.contractObligations}
        />
      ) : null}
      <Notification notification={contract.notification} />
      {contract.refersToContract && (
        <div className="px-4 py-5 sm:p-6">
          <h3 className="text-lg leading-6 font-medium text-gray-900 mb-5">
            Odnosi się do umowy
          </h3>
          <ContractItem
            contract={contract.refersToContract}
            property={contract.subjectProperty}
          />
        </div>
      )}
    </>
  );
}
