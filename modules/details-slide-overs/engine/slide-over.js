/* This example requires Tailwind CSS v2.0+ */
import { useRouter } from "next/router";
import Router from "next/router";

import SlideOverTemplate from "./slide-over-template";
import PersonInfoPanel from "../../../components/panels/person/info";
import PersonPaymentsPanel from "../../../components/panels/person/payments";
import PersonMessagesPanel from "../../../components/panels/person/messages";
import ContractsCard from "../../../components/details/contracts-card";

import ContractInfoPanel from "../../../components/panels/contract/info";
import ContractHistoryPanel from "../../../components/panels/contract/history";

import CheckInfoPanel from "../../../components/panels/check/info";
import CheckPlannerPanel from "../../../components/panels/check/planner";

import DefectInfoPanel from "../../../components/panels/defect/info";
import DefectPlannerPanel from "../../../components/panels/defect/planner";

import PersonDetails from "../panels/person";
import CompanyDetails from "../panels/company";
import ContractDetails from "../panels/contract";
import CheckDetails from "../panels/check";
import DefectDetails from "../panels/defect";
import MeterPeriodDetails from "../panels/meter-period";

export default function Example({ isStartOpen }) {
  const router = useRouter();
  const { panel, id, propertyId, tab, organizationId } = router.query;

  console.log("router.query panel + id", panel, id);

  const isOpen = isStartOpen || !!panel; /* && !!id*/

  const closePanel = () => {
    Router.push(
      {
        pathname: router.pathname,
        query: { propertyId, organizationId },
      },
      undefined,
      { scroll: false }
    );
  };

  const buildLinkHref = (tabId, otherQueryParams = {}) => ({
    pathname: router.pathname,
    query: { panel, propertyId, organizationId, id, tab: tabId, ...otherQueryParams },
  });

  const currentTabId = tab;

  switch (panel) {
    case "person":
      return (
        <SlideOverTemplate
          closePanel={closePanel}
          isOpen={isOpen}
          title="Osoba"
        // tabs={personTabs}
        >
          <PersonDetails buildLinkHref={buildLinkHref} />
        </SlideOverTemplate>
      );

    case "company":
      return (
        <SlideOverTemplate
          closePanel={closePanel}
          isOpen={isOpen}
          title="Firma"
        // tabs={personTabs}
        >
          <CompanyDetails buildLinkHref={buildLinkHref} />
        </SlideOverTemplate>
      );
    case "contract":
      return (
        <SlideOverTemplate
          closePanel={closePanel}
          isOpen={isOpen}
          title="Umowa"
        // tabs={personTabs}
        >
          <ContractDetails buildLinkHref={buildLinkHref} />
        </SlideOverTemplate>
      );
    case "check":
      return (
        <SlideOverTemplate
          closePanel={closePanel}
          isOpen={isOpen}
          title="Przegląd"
        // tabs={personTabs}
        >
          <CheckDetails buildLinkHref={buildLinkHref} />
        </SlideOverTemplate>
      );

    // case "check":
    //   const checkTabs = [
    //     {
    //       name: "Podsumowanie",
    //       id: "general",
    //       current: !currentTabId || currentTabId === "general",
    //     },
    //     {
    //       name: "Najbliższy",
    //       id: "closest",
    //       current: currentTabId === "closest",
    //     },
    //     {
    //       name: "Przeszłe",
    //       id: "history",
    //       current: currentTabId === "history",
    //     },
    //   ];
    //   return (
    //     <SlideOverTemplate
    //       closePanel={closePanel}
    //       isOpen={isOpen}
    //       title="Przegląd"
    //       tabs={checkTabs}
    //       buildLinkHref={buildLinkHref}
    //     >
    //       {(() => {
    //         switch (currentTabId) {
    //           case "closest":
    //             return <CheckPlannerPanel />;
    //           case "history":
    //             return <CheckPlannerPanel />;

    //           case "general":
    //           default:
    //             return <CheckInfoPanel />;
    //         }
    //       })()}
    //     </SlideOverTemplate>
    //   );
    case "defect":
      return (
        <SlideOverTemplate
          closePanel={closePanel}
          isOpen={isOpen}
          title="Usterka"
        // tabs={personTabs}
        >
          <DefectDetails buildLinkHref={buildLinkHref} />
        </SlideOverTemplate>
      );
    // case "defect":
    //   const defectTabs = [
    //     {
    //       name: "Info",
    //       id: "general",
    //       current: !currentTabId || currentTabId === "general",
    //     },
    //     {
    //       name: "Naprawa",
    //       id: "fix",
    //     current: currentTabId === "fix",
    //   },
    //   {
    //     name: "Historia",
    //     id: "history",
    //     current: currentTabId === "history",
    //   },
    // ];
    // return (
    //   <SlideOverTemplate
    //   closePanel={closePanel}
    //   isOpen={isOpen}
    //   title="Usterka"
    //   tabs={defectTabs}
    //   buildLinkHref={buildLinkHref}
    //   >
    //     {(() => {
    //       switch (currentTabId) {
    //         case "fix":
    //           return <DefectPlannerPanel />;
    //           case "history":
    //             return <DefectPlannerPanel />;

    //             case "general":
    //               default:
    //                 return <DefectInfoPanel />;
    //               }
    //             })()}
    //   </SlideOverTemplate>
    // );
    case "meter-period":
      return (
        <SlideOverTemplate
          closePanel={closePanel}
          isOpen={isOpen}
          title="Pomiar licznika"
        // tabs={personTabs}
        >
          <MeterPeriodDetails buildLinkHref={buildLinkHref} />
        </SlideOverTemplate>
      );

    default:
      return (
        <SlideOverTemplate
          closePanel={closePanel}
          isOpen={isOpen}
          title="Nie dobrze..."
        >
          <div>Zły typ panelu.</div>
        </SlideOverTemplate>
      );
  }
}
