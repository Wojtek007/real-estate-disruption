import { CalendarIcon, CheckIcon } from "@heroicons/react/solid";
import Link from "next/link";
import { useRouter } from "next/router";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function Steps({ state, isVertical }) {
  const steps = [
    {
      id: "01",
      name: "Umówione",
      description: "Umówiono Clean Inc. na 25.05.2021 - Planowany koszt 250 zł",
      href: "#",
      status: ["planned", "done"].includes(state) ? "complete" : "current",
    },
    {
      id: "02",
      name: "Wykonane",
      description: "Planowane na 30 Maja 2021",
      href: "#",
      status:
        state === "planned"
          ? "current"
          : state === "done"
          ? "complete"
          : "not started",
    },
  ];
  return (
    // <div className="lg:border-t lg:border-b lg:border-gray-200">
    //   <nav
    //     className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8"
    //     aria-label="Progress"
    //   >
    <ol
      className={`rounded-md overflow-hidden ${
        isVertical ? "" : "lg:flex"
      } lg:border-l lg:border-r lg:border-gray-200 lg:rounded-none`}
    >
      {steps.map((step, stepIdx) => (
        <li key={step.id} className="relative overflow-hidden lg:flex-1">
          <div
            className={classNames(
              stepIdx === 0 ? "border-b-0 rounded-t-md" : "",
              stepIdx === steps.length - 1 ? "border-t-0 rounded-b-md" : "",
              "border border-gray-200 overflow-hidden lg:border-0"
            )}
          >
            {step.status === "complete" ? (
              <a href={step.href} className="group">
                <span
                  className="absolute top-0 left-0 w-1 h-full bg-transparent group-hover:bg-gray-200 lg:w-full lg:h-1 lg:bottom-0 lg:top-auto"
                  aria-hidden="true"
                />
                <span className="px-3 py-3 flex items-center flex-col text-sm font-medium  text-center">
                  <span className="flex-shrink-0 py-2">
                    <span className="w-10 h-10 flex items-center justify-center bg-indigo-600 rounded-full">
                      <CheckIcon
                        className="w-6 h-6 text-white"
                        aria-hidden="true"
                      />
                    </span>
                  </span>
                  {isVertical ? (
                    <span className="mt-0.5 ml-4 min-w-0 flex flex-col">
                      <span className="text-xs font-semibold tracking-wide uppercase">
                        {step.name}
                      </span>
                      <span className="text-sm font-medium text-gray-500">
                        {step.description}
                      </span>
                    </span>
                  ) : (
                    <span className="text-xs font-semibold tracking-wide uppercase">
                      {step.name}
                    </span>
                  )}
                </span>
              </a>
            ) : step.status === "current" ? (
              <a href={step.href} aria-current="step">
                <span
                  className="absolute top-0 left-0 w-1 h-full bg-indigo-600 lg:w-full lg:h-1 lg:bottom-0 lg:top-auto"
                  aria-hidden="true"
                />
                <span className="px-3 py-3 flex items-center flex-col text-sm font-medium  text-center">
                  <span className="flex-shrink-0 py-2">
                    <span className="w-10 h-10 flex items-center justify-center border-2 border-indigo-600 rounded-full">
                      <span className="text-indigo-600">{step.id}</span>
                    </span>
                  </span>
                  {/* <span className="mt-0.5 ml-4 min-w-0 flex flex-col"> */}
                  {isVertical ? (
                    <span className="relative z-0 inline-flex shadow-sm rounded-md">
                      <button
                        type="button"
                        className="relative inline-flex items-center px-4 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:z-10 focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500"
                      >
                        Wrzuć rachunek
                      </button>
                      <button
                        type="button"
                        className="-ml-px relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:z-10 focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500"
                      >
                        Wrzuć dokument
                      </button>
                      <button
                        type="button"
                        className="-ml-px relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:z-10 focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500"
                      >
                        Dodaj usterkę
                      </button>
                      <button
                        type="button"
                        className="-ml-px relative inline-flex items-center px-4 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-green-700 hover:bg-green-50 focus:z-10 focus:outline-none focus:ring-1 focus:ring-green-500 focus:border-green-500"
                      >
                        Zakończ przegląd
                      </button>
                    </span>
                  ) : (
                    <span className="text-xs font-semibold text-indigo-600 tracking-wide uppercase">
                      {step.name}
                    </span>
                  )}
                  {/* <span className="text-sm font-medium text-gray-500">
                      {step.description}
                    </span> */}
                  {/* </span> */}
                </span>
              </a>
            ) : (
              <a href={step.href} className="group">
                <span
                  className="absolute top-0 left-0 w-1 h-full bg-transparent group-hover:bg-gray-200 lg:w-full lg:h-1 lg:bottom-0 lg:top-auto"
                  aria-hidden="true"
                />
                <span className="px-3 py-3 flex items-center flex-col text-sm font-medium  text-center">
                  <span className="flex-shrink-0 py-2">
                    <span className="w-10 h-10 flex items-center justify-center border-2 border-gray-300 rounded-full">
                      <span className="text-gray-500">{step.id}</span>
                    </span>
                  </span>
                  {/* <span className="mt-0.5 ml-4 min-w-0 flex flex-col"> */}
                  <span className="text-xs font-semibold text-gray-500 tracking-wide uppercase">
                    {step.name}
                  </span>
                  {/* <span className="text-sm font-medium text-gray-500">
                      {step.description}
                    </span> */}
                  {/* </span> */}
                </span>
              </a>
            )}

            {stepIdx !== 0 && !isVertical ? (
              <>
                {/* Separator */}
                <div
                  className="hidden absolute top-0 left-0 w-3 inset-0 lg:block"
                  aria-hidden="true"
                >
                  <svg
                    className="h-full w-full text-gray-300"
                    viewBox="0 0 12 82"
                    fill="none"
                    preserveAspectRatio="none"
                  >
                    <path
                      d="M0.5 0V31L10.5 41L0.5 51V82"
                      stroke="currentcolor"
                      vectorEffect="non-scaling-stroke"
                    />
                  </svg>
                </div>
              </>
            ) : null}
          </div>
        </li>
      ))}
    </ol>
    //   </nav>
    // </div>
  );
}
