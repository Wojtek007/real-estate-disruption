import * as moment from "moment";

const generateAbout = (notification) => {
  const { checks, contracts, repairs } = notification;

  if (checks?.length) {
    return "Zaplanować wykonanie przeglądu";
  }
  if (contracts?.length) {
    return "Sprawdzić możliwość przedłużenia";
  }
  if (repairs?.length) {
    return "Rozpocząć remont";
  }
};

export default function Example({ notification }) {
  return (
    <div className="px-4 py-5 sm:p-6">
      <h3 className="text-lg leading-6 font-medium text-gray-900">
        Następna notyfikacja
      </h3>
      <div className="mt-5">
        <div className="rounded-md bg-gray-50 px-6 py-5 sm:flex sm:items-start sm:justify-between">
          <div className="sm:flex sm:items-start">
            <div className="mt-3 sm:mt-0 sm:ml-4">
              <div className="text-sm font-medium text-gray-900">
                {generateAbout(notification)}
              </div>
              <div className="mt-1 text-sm text-gray-600 sm:flex sm:items-center">
                <div>
                  Następne przypomnienie{" "}
                  {moment(notification.reminderDate).format("L")}
                </div>
                {/* <span className="hidden sm:mx-2 sm:inline" aria-hidden="true">
                  &middot;
                </span>
                 <div className="mt-1 sm:mt-0">Przekładane 2 razy</div> */}
              </div>
            </div>
          </div>
          <div className="mt-4 sm:mt-0 sm:ml-6 sm:flex-shrink-0">
            <button
              type="button"
              className="inline-flex items-center px-4 py-2 border border-gray-300 shadow-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:text-sm"
            >
              Edytuj
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
