import Link from "next/link";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function Example({ tabs = [], buildLinkHref, current }) {
  return (
    <div className="border-b border-gray-200">
      <div className="px-6">
        <nav className="-mb-px flex space-x-6" x-descriptions="Tab component">
          {tabs.map((tab) => (
            <Link href={buildLinkHref(tab.id)} scroll={false}>
              <a
                key={tab.id}
                className={classNames(
                  tab.id === current
                    ? "border-indigo-500 text-indigo-600"
                    : "border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300",
                  "whitespace-nowrap pb-4 px-1 border-b-2 font-medium text-sm"
                )}
              >
                {tab.name}
              </a>
            </Link>
          ))}
        </nav>
      </div>
    </div>
  );
}
