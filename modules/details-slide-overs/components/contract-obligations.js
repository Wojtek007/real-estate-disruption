/* This example requires Tailwind CSS v2.0+ */
import { ArrowSmDownIcon, ArrowSmUpIcon } from "@heroicons/react/solid";
import {
  CursorClickIcon,
  MailOpenIcon,
  UsersIcon,
} from "@heroicons/react/outline";
import {
  FireIcon,
  BeakerIcon,
  LightningBoltIcon,
  CashIcon
} from "@heroicons/react/outline";

const getIcon = (iconName) => {
  switch (iconName) {
    case "LightningBolt":
      return (
        <LightningBoltIcon className="h-6 w-6 text-white" aria-hidden="true" />
      );
    case "Beaker":
      return <BeakerIcon className="h-6 w-6 text-white" aria-hidden="true" />;
    case "Fire":
      return <FireIcon className="h-6 w-6 text-white" aria-hidden="true" />;
    default:
      return <CashIcon className="h-6 w-6 text-white" aria-hidden="true" />;
  }
};

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function Example({ contractObligations }) {
  console.log('XXXXXXX', contractObligations)
  return (
    <div className="px-4 py-5 sm:px-6">
      <h3 className="text-lg leading-6 font-medium text-gray-900">
        Należności umowne
      </h3>

      <dl className="mt-5 grid grid-cols-1 gap-5 sm:grid-cols-2 ">
        {contractObligations.map((obligation) => (
          <div
            key={obligation.id}
            className="relative bg-white pt-4 px-3 pb-6 sm:pt-5 sm:px-5 shadow rounded-lg overflow-hidden"
          >
            <p className="mb-2 text-sm font-medium text-gray-500 truncate">
              {obligation.obligationType.label}
            </p>
            <dt>
              <div className="absolute bg-indigo-500 rounded-md p-3">
                {getIcon(obligation.obligationType.relatedUtilityType?.icon)}
              </div>
            </dt>
            <dd className="ml-16  flex flex-col items-baseline ">
              <p className="text-2xl font-semibold text-gray-900">
                {obligation.financialObligation
                  ? `${parseFloat(obligation.financialObligation)?.toFixed(2)} zł`
                  : `${obligation.paidUnits} ${obligation.obligationType.relatedUtilityType?.measurementUnit}`
                }
              </p>
              {obligation.financialObligation && obligation.paidUnits && <p className="ml-2 flex items-baseline text-sm font-semibold">
                {obligation.paidUnits} {obligation.obligationType.relatedUtilityType?.measurementUnit}
              </p>}
              {/* <div className="absolute bottom-0 inset-x-0 bg-gray-50 px-4 py-4 sm:px-6">
                <div className="text-sm">
                  <a
                    href="#"
                    className="font-medium text-indigo-600 hover:text-indigo-500"
                  >
                    {" "}
                    View all<span className="sr-only"> {item.name} stats</span>
                  </a>
                </div>
              </div> */}
            </dd>
          </div>
        ))}
      </dl>
    </div>
  );
}
