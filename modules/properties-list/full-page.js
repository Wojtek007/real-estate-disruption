/* This example requires Tailwind CSS v2.0+ */
const people = [
  {
    name: "Siennna 72a",
    propertyStatus: "Wynajety Joannie Grzyb/Poszukuje najemcy",
    rent: "1500 PLN",
    contractEnd: "Do 25.06.2021",
    contractStatus: "Obowiązuje",
    contractColor: "green",
    checksToSetColor: "red",
    checksToSet: "1 do umówienia",
    nextCheckToSet: "",
    pendingChecksColor: "yellow",
    pendingChecks: "2 oczekują wykonania",
    nextPendingChecks: "najbliższy 12.06.2021",
    meterStatus: "2 przedawnione powyżej kwartału",
    meterColor: "yellow",
    urgetRepairsStatus: "1 pilna naprawa",
    urgetRepairsColor: "red",
    futureRepairsStatus: "2 przyszłe remonty",
    image:
      "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60",
  },
  // More people...
];

export default function Example() {
  return (
    <div className="px-4 py-5 sm:px-6">
      <div className="flex flex-col">
        <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
            <h2 className="my-3 text-2xl font-bold leading-7 text-gray-900 sm:text-3xl sm:truncate">
              Statyczna testowa strona
            </h2>
            <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
              <table className="min-w-full divide-y divide-gray-200">
                <thead className="bg-gray-50">
                  <tr>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Nieruchomość
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Czynsz
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Umowa
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Przeglądy
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Liczniki
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Naprawy
                    </th>
                    <th scope="col" className="relative px-6 py-3">
                      <span className="sr-only">Detale</span>
                    </th>
                  </tr>
                </thead>
                <tbody className="bg-white divide-y divide-gray-200">
                  {people.map((person) => (
                    <tr key={person.email}>
                      <td className="px-6 py-4 whitespace-nowrap">
                        <div className="flex items-center">
                          {/* <div className="flex-shrink-0 h-10 w-10">
                            <img
                              className="h-10 w-10 rounded-full"
                              src={person.image}
                              alt=""
                            />
                          </div> */}
                          <div className="ml-4">
                            <div className="text-sm font-medium text-gray-900">
                              {person.name}
                            </div>
                            <div className="text-sm text-gray-500">
                              {person.propertyStatus}
                            </div>
                          </div>
                        </div>
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                        {person.rent}
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap">
                        <span
                          className={`px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-${person.contractColor}-100 text-${person.contractColor}-800`}
                        >
                          {person.contractStatus}
                        </span>
                        <div className="text-sm text-gray-900">
                          {person.contractEnd}
                        </div>
                        {/* <div className="text-sm text-gray-500">
                          {person.contractStatus}
                        </div> */}
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap">
                        <span
                          className={`px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-${person.checksToSetColor}-100 text-${person.checksToSetColor}-800`}
                        >
                          {person.checksToSet}
                        </span>
                        <div className="text-sm text-gray-500">
                          {person.nextCheckToSet}
                        </div>
                        <span
                          className={`px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-${person.pendingChecksColor}-100 text-${person.pendingChecksColor}-800`}
                        >
                          {person.pendingChecks}
                        </span>
                        <div className="text-sm text-gray-900">
                          {person.nextPendingChecks}
                        </div>
                      </td>

                      <td className="px-6 py-4 whitespace-nowrap">
                        <span
                          className={`px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-${person.meterColor}-100 text-${person.meterColor}-800`}
                        >
                          {person.meterStatus}
                        </span>
                        {/* <div className="text-sm text-gray-500">
                          {person.contractStatus}
                        </div> */}
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap">
                        <span
                          className={`px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-${person.urgetRepairsColor}-100 text-${person.urgetRepairsColor}-800`}
                        >
                          {person.urgetRepairsStatus}
                        </span>
                        <div className="text-sm text-gray-900">
                          {person.futureRepairsStatus}
                        </div>
                        {/* <div className="text-sm text-gray-500">
                          {person.contractStatus}
                        </div> */}
                      </td>
                      {/* <td className="px-6 py-4 whitespace-nowrap">
                        <span className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                          Active
                        </span>
                      </td> */}

                      <td className="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                        <a
                          href="#"
                          className="text-indigo-600 hover:text-indigo-900"
                        >
                          Detale
                        </a>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
