export default function Example() {

    return (
        <>
            <label htmlFor="country" className="block text-sm font-medium text-gray-700">
                Country / Region
            </label>
            <select
                id="country"
                name="country"
                autoComplete="country"
                className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
            >
                <option>United States</option>
                <option>Canada</option>
                <option>Mexico</option>
            </select>
        </>
    );
}
