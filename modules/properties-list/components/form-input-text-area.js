export default function Example({
  label,
  key,
  placeholder,
  onChange,
  value,
  desc,
}) {
  return (
    <div>
      <label htmlFor={key} className="block text-sm font-medium text-gray-700">
        {label}
      </label>
      <div className="mt-1">
        <textarea
          id={key}
          name={key}
          rows={3}
          className="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 mt-1 block w-full sm:text-sm border border-gray-300 rounded-md"
          placeholder={placeholder}
          onChange={(e) => onChange(e.target.value)}
          value={value}
        />
      </div>
      {desc && <p className="mt-2 text-sm text-gray-500">{desc}</p>}
    </div>
  );
}
