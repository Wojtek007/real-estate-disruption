export default function Example({
  label,
  key,
  onChange,
  value,
  autoComplete,
  desc,
}) {
  return (
    <div>
      <label htmlFor={key} className="block text-sm font-medium text-gray-700">
        {label}
      </label>
      <input
        type="text"
        name={key}
        id={key}
        autoComplete={autoComplete}
        onChange={(e) => onChange(e.target.value)}
        value={value}
        className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
      />
      {desc && <p className="mt-2 text-sm text-gray-500">{desc}</p>}
    </div>
  );
}
