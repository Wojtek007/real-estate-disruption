import Link from "next/link";
import { useSubscription } from "@apollo/react-hooks";
import gql from "graphql-tag";

import { DotsVerticalIcon } from "@heroicons/react/solid";

const projects = [
  {
    name: "Graph API",
    initials: "GA",
    href: "#",
    members: 16,
    bgColor: "bg-pink-600",
  },
  {
    name: "Component Design",
    initials: "CD",
    href: "#",
    members: 12,
    bgColor: "bg-purple-600",
  },
  {
    name: "Templates",
    initials: "T",
    href: "#",
    members: 16,
    bgColor: "bg-yellow-500",
  },
  {
    name: "React Components",
    initials: "RC",
    href: "#",
    members: 8,
    bgColor: "bg-green-500",
  },
];
const PROPERTIES_LIST = gql`
  query MyQuery12313289 {
    properties(order_by: { name: asc }) {
      name
      id
    }
  }
`;
function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}
const getBgColors = (i) => {
  const bgColors = [
    "bg-pink-600",
    "bg-purple-600",
    "bg-yellow-500",
    "bg-green-500",
  ];

  return bgColors[i % bgColors.length];
};

export default function Example({ user }) {
  const { loading, error, data } = useSubscription(PROPERTIES_LIST);

  const { properties } = data ?? {};

  if (loading) {
    return <span>Wczytuję...</span>;
  }
  if (error) {
    console.error(error);
    return <span>Błąd! {error?.message ?? JSON.stringify(error)}</span>;
  }

  //   const properties = data.properties.map((property) => (
  //     <div key={property.id}>{property.label}</div>
  //   ));

  return (
    <div>
      <h2 className="text-gray-500 text-xs font-medium uppercase tracking-wide">
        Zarządzane nieruchomości
      </h2>
      <ul className="mt-3 grid grid-cols-1 gap-5 ">
        {properties.map((property, i) => (
          <li
            key={property.id}
            className="col-span-1 flex shadow-sm rounded-md"
          >
            <div
              className={classNames(
                getBgColors(i),
                "flex-shrink-0 flex items-center justify-center w-16 text-white text-sm font-medium rounded-l-md"
              )}
            >
              IHI
            </div>
            <div className="flex-1 flex items-center justify-between border-t border-r border-b border-gray-200 bg-white rounded-r-md truncate">
              <div className="flex-1 px-4 py-2 text-sm truncate">
                <Link href={`/properties/${property.id}/info`}>
                  <a className="text-gray-900 font-medium hover:text-gray-600">
                    {property.name}
                  </a>
                </Link>
                {/* <p className="text-gray-500">22 powiadomień/miesiąc</p> */}
              </div>
              {/* <div className="flex-shrink-0 pr-2">
                <button className="w-8 h-8 bg-white inline-flex items-center justify-center text-gray-400 rounded-full bg-transparent hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                  <span className="sr-only">Open options</span>
                  <DotsVerticalIcon className="w-5 h-5" aria-hidden="true" />
                </button>
              </div> */}
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
}
