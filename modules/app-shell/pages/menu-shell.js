import { useRouter } from "next/router";

import Menu from "../components/menu.js";

export default function Example({ user, children, aside }) {
  const router = useRouter();

  return (
    <div className="min-h-screen bg-gray-100">
      <Menu />
      {children}
    </div>
  );
}
