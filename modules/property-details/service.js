import { staticFetch } from "@lib/apolloClient";

export const getPropertiesStaticPaths = async () => {
  const ORGS_QUERY = `
    query MyQueryt2y3123678162 {
      properties {
        id
        organization_id
      }
    }
  `;
  // Call an external API endpoint to get posts
  const { properties } = await staticFetch(ORGS_QUERY);
  console.log("RETURNED", properties);

  // Get the paths we want to pre-render based on posts
  const paths = properties.map((property) => ({
    params: {
      propertyId: property.id,
      organizationId: property.organization_id,
    },
  }));

  // We'll pre-render only these paths at build time.
  // { fallback: false } means other routes should 404.
  return { paths, fallback: true };
};

export const getOrganizationsStaticPaths = async () => {
  const ORGS_QUERY = `
  query MyQuery1283789123 {
    versioned_organizations {
      id
    }
  }
  `;
  // Call an external API endpoint to get posts
  const { versioned_organizations } = await staticFetch(ORGS_QUERY);
  console.log("RETURNED", versioned_organizations);

  // Get the paths we want to pre-render based on posts
  const paths = versioned_organizations.map((org) => ({
    params: {
      organizationId: org.id,
    },
  }));

  // We'll pre-render only these paths at build time.
  // { fallback: false } means other routes should 404.
  return { paths, fallback: true };
};
