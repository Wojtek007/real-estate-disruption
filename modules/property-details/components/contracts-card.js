import {
  ClipboardCheckIcon,
  ClipboardIcon,
  ClipboardCopyIcon,
  XCircleIcon,
} from "@heroicons/react/solid";
import { PlusIcon } from "@heroicons/react/solid";

import Link from "next/link";
import { useRouter } from "next/router";
import * as moment from "moment";
import ContractItem from "./contract-item";
import NoContractsState from "./no-contracts-state";
import Button from "@components/button";

const applications = [
  {
    applicant: {
      name: "Ricardo Cooper",
      email: "ricardo.cooper@example.com",
      imageUrl:
        "https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80",
    },
    date: "2020-01-07",
    dateFull: "7 Stycznia 2020 - 7 stycznia 2021",
    stageIcon: (
      <XCircleIcon
        className="flex-shrink-0 mr-1.5 h-5 w-5 text-red-400"
        aria-hidden="true"
      />
    ),
    stage: "Rozwiązany",
    href: "#",
  },
  {
    applicant: {
      name: "Ricardo Cooper",
      email: "ricardo.cooper@example.com",
      imageUrl:
        "https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80",
    },
    date: "2020-01-07",
    dateFull: "7 Stycznia 2020 - 7 stycznia 2021",
    stageIcon: (
      <ClipboardIcon
        className="flex-shrink-0 mr-1.5 h-5 w-5 text-yellow-400"
        aria-hidden="true"
      />
    ),
    stage: "Wygenerowane",
    href: "#",
  },
  {
    applicant: {
      name: "Kristen Ramos",
      email: "kristen.ramos@example.com",
      imageUrl:
        "https://images.unsplash.com/photo-1550525811-e5869dd03032?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80",
    },
    date: "2020-01-07",
    dateFull: "7 Stycznia 2020 - 7 stycznia 2021",
    stageIcon: (
      <ClipboardCopyIcon
        className="flex-shrink-0 mr-1.5 h-5 w-5 text-yellow-400"
        aria-hidden="true"
      />
    ),
    stage: "Wysłane",
    href: "#",
  },
  {
    applicant: {
      name: "Ted Fox",
      email: "ted.fox@example.com",
      imageUrl:
        "https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80",
    },
    date: "2020-01-07",
    dateFull: "7 Stycznia 2020 - 7 stycznia 2021",
    stageIcon: (
      <ClipboardCheckIcon
        className="flex-shrink-0 mr-1.5 h-5 w-5 text-green-400"
        aria-hidden="true"
      />
    ),
    stage: "Podpisane",
    href: "#",
  },
];

const getStatusIcon = ({ type, status }) => {
  switch (status) {
    case "Obowiązuje":
      return (
        <ClipboardCheckIcon
          className="flex-shrink-0 mr-1.5 h-5 w-5 text-green-400"
          aria-hidden="true"
        />
      );
    case "Podpisana":
      return ["Rozwiązanie", "Protokół odbioru"].includes(type) ? (
        <ClipboardCheckIcon
          className="flex-shrink-0 mr-1.5 h-5 w-5 text-red-400"
          aria-hidden="true"
        />
      ) : (
        <ClipboardCheckIcon
          className="flex-shrink-0 mr-1.5 h-5 w-5 text-green-400"
          aria-hidden="true"
        />
      );
    case "Przerwana":
      return (
        <XCircleIcon
          className="flex-shrink-0 mr-1.5 h-5 w-5 text-red-400"
          aria-hidden="true"
        />
      );
    case "Przerwana":
      return (
        <XCircleIcon
          className="flex-shrink-0 mr-1.5 h-5 w-5 text-yellow-400"
          aria-hidden="true"
        />
      );
    case "Zakończona":
      return (
        <XCircleIcon
          className="flex-shrink-0 mr-1.5 h-5 w-5 text-yellow-400"
          aria-hidden="true"
        />
      );

    default:
      break;
  }
};

export default function Example({
  title = "Job Postings",
  desc,
  contracts = [],
  showButton,
  category,
  property,
}) {
  const router = useRouter();
  const { organizationId } = router.query;
  console.log(
    "category, property,contracts-card",
    category,
    property,
    contracts
  );

  const groupContracts = contracts => Object.values(contracts
    .reduce((acc, contract) => {
      console.log('contractssssss1111', acc, contract)
      const isAgreement = contract.type.effect === 'aggreament';
      // const isAnnex = contract.type.effect === 'annex';
      // const isTermination = contract.type.effect === 'termination';

      const groupContractId = isAgreement ? contract.id : contract.refersToContract.id
      return {
        ...acc,
        [groupContractId]: acc[groupContractId]
          ? [...acc[groupContractId], contract]
          : [contract]
      };
    }, {}))
    .map(group => {
      const aggreament = group.find(c => c.type.effect === 'aggreament')
      const termination = group.find(c => c.type.effect === 'termination')
      return [
        aggreament,
        ...(termination ? [termination] : []),
        ...group.filter(c => ![aggreament.id, termination?.id].includes(c.id)),
      ]

    })
    .sort((a, b) => moment(b[0].startDate).diff(moment(a[0].startDate), 'days'))
    .flat();

  const sortedContracts = groupContracts(contracts)
  console.log('contractssss1999', sortedContracts)

  const isRentOrMngmnt = ["rent", "management"].includes(category.key)

  const isLastContractTerminated = sortedContracts.length && sortedContracts[0].contracts.some(contract => contract.type.effect === "termination")
  const hasExpired = sortedContracts.length && moment(sortedContracts[0].endDate).isBefore(moment(), "day");

  console.log('isRentOrMngmnt', isRentOrMngmnt, 'isLastContractTerminated', isLastContractTerminated, 'hasExpired', hasExpired, sortedContracts)
  const canAddContract = isRentOrMngmnt ? (isLastContractTerminated || hasExpired) : true;

  const addButton = (isSecondary) => (
    <Button
      isSecondary={isSecondary}
      pathname={`/${organizationId}/contract/new`}
      query={{
        // ...(isRentOrMngmnt
        //   ? {
        //     signerId: sortedContracts[0].signer?.id,
        //     signerVer: sortedContracts[0].signer?.ver,
        //     startDate: sortedContracts[0].endDate,
        //     refersToContractId: sortedContracts[0].id,
        //     refersToContractVer: sortedContracts[0].ver,
        //   }
        //   : {}),
        redirect: router.asPath,
        categoryId: category.id,
        categoryVer: category.ver,
        subjectPropertyId: property.id,
        subjectPropertyVer: property.ver,
        typeId: "0e02eb25-fd98-473a-ad2b-2e884bbd3615",
        typeVer: 1,




        hidden: [
          "category",
          "subjectProperty",
          "refersToContract",
          "type",
          "notification/data/reminderDate",
        ],
      }}
    >
      <PlusIcon className="-ml-1 mr-2 h-5 w-5" aria-hidden="true" />{/*isRentOrMngmnt ? 'Przedłuż umowę' : */'Dodaj umowę'}
    </Button>
  );
  if (!sortedContracts.length) {
    return <NoContractsState title={title} button={addButton(false)} />;
  }
  return (
    <div className="bg-white shadow sm:rounded-lg">
      <div className="px-4 py-5 bg-white border-b border-gray-200 sm:px-6">
        <div className="-ml-4 -mt-4 flex justify-between items-center flex-wrap sm:flex-nowrap">
          <div className="ml-4 mt-4">
            <h3 className="text-lg leading-6 font-medium text-gray-900">
              {title}
            </h3>
            {desc && <p className="mt-1 text-sm text-gray-500">{desc}</p>}
          </div>
          {showButton && canAddContract && (
            <div className="ml-4 mt-4 flex-shrink-0">{addButton(true)}</div>
          )}
        </div>
      </div>
      <ul className="divide-y divide-gray-200">
        {sortedContracts.map((contract) => (
          <li key={contract.id} >
            <ContractItem
              contract={contract}
              property={property}
              category={category}
            />
          </li>
        ))}
      </ul>
      {/* <div>
        <a
          href="#"
          className="block bg-gray-50 text-sm font-medium text-gray-500 text-center px-4 py-4 hover:text-gray-700 sm:rounded-b-lg"
        >
          Pokaż starsze dokumenty
        </a>
      </div> */}
    </div>
  );
}
