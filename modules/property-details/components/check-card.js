import { useState } from "react";
import { CalendarIcon, CheckIcon } from "@heroicons/react/solid";
import Link from "next/link";
import { useRouter } from "next/router";
import * as moment from "moment";
import Button from "@components/button";

export default function Example({ check, property }) {
  const [isHovered, setIsHovered] = useState(false);
  const {
    pathname,
    query: { organizationId },
  } = useRouter();
  const checkResult = check?.checkResults.length
    ? check.checkResults[0]
    : check?.initialCheckResult;
  const { monthsBefore, weeksBefore } = checkResult.nextCheckNotification.precedingPeriod ?? {};
  const appointment = checkResult.nextCheckNotification.appointments[0];
  console.log('details - check', check)

  const { date } = checkResult

  const notificationDate = moment(date).add(1, "years")
    .subtract(monthsBefore, "months")
    .subtract(weeksBefore, "weeks")
  return (
    <div
      key={check.id}
      className="bg-white shadow overflow-hidden sm:rounded-md"
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    >
      <Link
        href={{
          pathname: pathname,
          query: {
            panel: "check",
            organizationId,
            propertyId: property.id,
            id: check.id,
          },
        }}
        scroll={false}
      >
        <a className="block hover:bg-gray-50 ">
          <div className=" flex items-center">
            <div className="sm:grid grid-cols-2 gap-0 sm:items-center sm:justify-between w-full">
              <div className="col-span-1 px-4 py-4 sm:px-6">
                <div className="flex text-sm">
                  <p className="text-indigo-600 font-medium hover:underline hover:text-indigo-500">
                    {check.name}
                  </p>
                  <p className="ml-1 flex-shrink-0 font-normal text-gray-500">
                    {check.recurrenceInterval?.name}
                  </p>
                </div>
                <div className="mt-2 flex">
                  <div className="flex items-center text-sm text-gray-500">
                    <CalendarIcon
                      className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
                      aria-hidden="true"
                    />

                    {appointment ? (
                      <p>
                        Zaplanowane na{" "}
                        <time dateTime={appointment.date}>
                          {moment(appointment.date).format("L")}
                        </time>
                      </p>
                    ) : (
                      <p>
                        Przypomnienie{" "}
                        <time dateTime={notificationDate}>
                          {moment(notificationDate).format("L")}
                        </time>
                      </p>
                    )}
                  </div>
                </div>
              </div>
              {/* <div className="mt-4 flex-shrink-0 sm:mt-0 sm:ml-5">
              <div className="flex overflow-hidden -space-x-1">
                {check.applicants.map((applicant) => (
                  <img
                    key={applicant.email}
                    className="inline-block h-6 w-6 rounded-full ring-2 ring-white"
                    src={applicant.imageUrl}
                    alt={applicant.name}
                  />
                ))}
              </div>
            </div> */}
              {/* <CheckSteps
                check={check}
                property={property}
                isHovered={isHovered}
              /> */}
            </div>
            {/* <div className="ml-5 flex-shrink-0">
            <ChevronRightIcon
              className="h-5 w-5 text-gray-400"
              aria-hidden="true"
            />
          </div> */}
          </div>
        </a>
      </Link>
    </div>
  );
}

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export function CheckSteps({ check, property, isVertical, isHovered }) {
  const {
    asPath,
    query: { organizationId },
  } = useRouter();

  console.log('RRRRR', check)
  const checkResult = check?.checkResults.length
    ? check.checkResults[0]
    : check?.initialCheckResult;
  const notification = checkResult.nextCheckNotification;
  const appointment = notification?.appointments[0];
  console.log("Check card - notification", notification);

  const isFinished = !!notification.resultingCheckResult;
  console.log(
    "isFinished",
    notification,
    appointment,
    appointment?.checkResults?.length,
    isFinished);
  const steps = [
    {
      id: "01",
      currentName: "Do umówienia",
      completeName: "Umówione",
      description: "Umówiono Clean Inc. na 25.05.2021 - Planowany koszt 250 zł",
      pathname: `/${organizationId}/appointment/new`,
      query: {
        notificationId: notification.id,
        notificationVer: notification.ver,
        hidden: ["check", "notification"],
        redirect: asPath,
      },

      status: appointment && !isFinished ? "complete" : "current",
      btnLabel: "Umów",
    },
    {
      id: "02",
      currentName: "Do wykonania",
      completeName: "Wykonane",
      description: "Planowane na 30 Maja 2021",
      pathname: `/${organizationId}/check-result/new`,
      query: {
        checkId: check.id,
        checkVer: check.ver,
        date: notification.appointments[0]?.date,
        cost: notification.appointments[0]?.plannedCost,
        parentNotificationId: notification?.id,
        parentNotificationVer: notification?.ver,
        // defects_data_propertyId: property?.id,
        // defects_data_propertyVer: property?.ver,
        nextCheckNotification_data_precedingPeriodId:
          notification?.precedingPeriod.id,
        nextCheckNotification_data_precedingPeriodVer:
          notification?.precedingPeriod.ver,
        hidden: [
          "check",
          "parentNotification",
          "defects/data/notification/data/precedingPeriod",
          "defects/data/property",
          "defects/data/reportingMessage",
          "nextCheckNotification/data/precedingPeriod",
          "nextCheckNotification/data/reminderDate",
        ],
        redirect: asPath,
      },

      status: appointment && !isFinished ? "current" : "not started",
      btnLabel: "Zakończ",
      // state === "planned"
      //   ? "current"
      //   : state === "done"
      //   ? "complete"
      //   : "not started",
    },
  ];
  return (
    // <div className="lg:border-t lg:border-b lg:border-gray-200">
    //   <nav
    //     className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8"
    //     aria-label="Progress"
    //   >
    <ol
      className={`rounded-md overflow-hidden ${isVertical ? "" : "lg:flex"
        } lg:border-l lg:border-r lg:border-gray-200 lg:rounded-none`}
    >
      {steps.map((step, stepIdx) => (
        <li key={step.id} className="relative overflow-hidden lg:flex-1">
          <div
            className={classNames(
              stepIdx === 0 ? "border-b-0 rounded-t-md" : "",
              stepIdx === steps.length - 1 ? "border-t-0 rounded-b-md" : "",
              "border border-gray-200 overflow-hidden lg:border-0"
            )}
          >
            {step.status === "complete" ? (
              <a href={step.href} className="group">
                <span
                  className="absolute top-0 left-0 w-1 h-full bg-transparent group-hover:bg-gray-200 lg:w-full lg:h-1 lg:bottom-0 lg:top-auto"
                  aria-hidden="true"
                />
                <span className="px-3 py-3 flex items-center flex-col text-sm font-medium  text-center">
                  {isHovered ? (
                    <Button
                      isSecondary
                      className="my-4"
                      pathname={`/${organizationId}/appointment/${appointment?.id}/edit`}
                      query={{
                        hidden: ["check", "notification"],
                        redirect: asPath,
                      }}
                    >
                      Zmień wizytę
                    </Button>
                  ) : (
                    <>
                      <span className="flex-shrink-0 py-2">
                        <span className="w-10 h-10 flex items-center justify-center bg-indigo-600 rounded-full">
                          <CheckIcon
                            className="w-6 h-6 text-white"
                            aria-hidden="true"
                          />
                        </span>
                      </span>
                      {isVertical ? (
                        <span className="mt-0.5 ml-4 min-w-0 flex flex-col">
                          <span className="text-xs font-semibold tracking-wide uppercase">
                            {step.name}
                          </span>
                          <span className="text-sm font-medium text-gray-500">
                            {step.description}
                          </span>
                        </span>
                      ) : (
                        <span className="text-xs font-semibold tracking-wide uppercase">
                          {step.completeName}
                        </span>
                      )}
                    </>
                  )}
                </span>
              </a>
            ) : step.status === "current" ? (
              <a aria-current="step">
                <span
                  className="absolute top-0 left-0 w-1 h-full bg-indigo-600 lg:w-full lg:h-1 lg:bottom-0 lg:top-auto"
                  aria-hidden="true"
                />
                <span className="px-3 py-3 flex items-center flex-col text-sm font-medium text-center">
                  {isHovered ? (
                    <Button
                      pathname={step.pathname}
                      query={step.query}
                      className="my-4"
                    >
                      {step.btnLabel}
                    </Button>
                  ) : (
                    <>
                      <span className="flex-shrink-0 py-2">
                        <span className="w-10 h-10 flex items-center justify-center border-2 border-indigo-600 rounded-full">
                          <span className="text-indigo-600">{step.id}</span>
                        </span>
                      </span>
                      {/* <span className="mt-0.5 ml-4 min-w-0 flex flex-col"> */}
                      {isVertical ? (
                        <span className="relative z-0 inline-flex shadow-sm rounded-md">
                          <button
                            type="button"
                            className="relative inline-flex items-center px-4 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:z-10 focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500"
                          >
                            Wrzuć rachunek
                          </button>
                          <button
                            type="button"
                            className="-ml-px relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:z-10 focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500"
                          >
                            Wrzuć dokument
                          </button>
                          <button
                            type="button"
                            className="-ml-px relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:z-10 focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500"
                          >
                            Dodaj usterkę
                          </button>
                          <button
                            type="button"
                            className="-ml-px relative inline-flex items-center px-4 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-green-700 hover:bg-green-50 focus:z-10 focus:outline-none focus:ring-1 focus:ring-green-500 focus:border-green-500"
                          >
                            Zakończ przegląd
                          </button>
                        </span>
                      ) : (
                        <span className="text-xs font-semibold text-indigo-600 tracking-wide uppercase">
                          {step.currentName}
                        </span>
                      )}
                    </>
                  )}
                  {/* <span className="text-sm font-medium text-gray-500">
                      {step.description}
                    </span> */}
                  {/* </span> */}
                </span>
              </a>
            ) : (
              <a href={step.href} className="group">
                <span
                  className="absolute top-0 left-0 w-1 h-full bg-transparent group-hover:bg-gray-200 lg:w-full lg:h-1 lg:bottom-0 lg:top-auto"
                  aria-hidden="true"
                />
                <span className="px-3 py-3 flex items-center flex-col text-sm font-medium  text-center">
                  <span className="flex-shrink-0 py-2">
                    <span className="w-10 h-10 flex items-center justify-center border-2 border-gray-300 rounded-full">
                      <span className="text-gray-500">{step.id}</span>
                    </span>
                  </span>
                  {/* <span className="mt-0.5 ml-4 min-w-0 flex flex-col"> */}
                  <span className="text-xs font-semibold text-gray-500 tracking-wide uppercase">
                    {step.currentName}
                  </span>

                  {/* <span className="text-sm font-medium text-gray-500">
                      {step.description}
                    </span> */}
                  {/* </span> */}
                </span>
              </a>
            )}

            {stepIdx !== 0 && !isVertical ? (
              <>
                {/* Separator */}
                <div
                  className="hidden absolute top-0 left-0 w-3 inset-0 lg:block"
                  aria-hidden="true"
                >
                  <svg
                    className="h-full w-full text-gray-300"
                    viewBox="0 0 12 82"
                    fill="none"
                    preserveAspectRatio="none"
                  >
                    <path
                      d="M0.5 0V31L10.5 41L0.5 51V82"
                      stroke="currentcolor"
                      vectorEffect="non-scaling-stroke"
                    />
                  </svg>
                </div>
              </>
            ) : null}
          </div>
        </li>
      ))}
    </ol>
    //   </nav>
    // </div>
  );
}
