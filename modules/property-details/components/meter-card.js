import {
  PaperClipIcon,
  BellIcon,
  UserIcon,
  XIcon,
  CheckIcon,
} from "@heroicons/react/solid";
import {
  FireIcon,
  BeakerIcon,
  LightningBoltIcon,
} from "@heroicons/react/outline";
import {
  AdjustmentsIcon,
  CalendarIcon,
  ReceiptRefundIcon,
} from "@heroicons/react/solid";
import Link from "next/link";
import * as moment from "moment";
import { useRouter } from "next/router";
import Button from "@components/button";
import NoMeterPeriodsState from '../meters/components/no-meter-periods-state'
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";



const getContractObligation = (utilityType) => {
  return utilityType.contractObligationTypes[0]?.contractObligations[0];
};
const getIsContractObligationUnitBased = (utilityType) => {
  return utilityType.contractObligationTypes[0]?.isUnitBased;
};

const getContractFinancialObligation = (utilityType) =>
  getContractObligation(utilityType)?.financialObligation ?? 0;
const getContractPaidUnitsIncluded = (utilityType) =>
  getContractObligation(utilityType)?.paidUnits ?? 0;
const getContractSigner = (utilityType) =>
  getContractObligation(utilityType)?.contract.signer;


const getDurationFromPreviousMeasurment = (period, previousPeriod) => moment.duration(
  moment(period.measurementDate).diff(moment(previousPeriod?.measurementDate))
);
const getMonthDifference = (period, previousPeriod) => Math.round(getDurationFromPreviousMeasurment(period, previousPeriod).asMonths());

const getPredictedObligation = (period, previousPeriod, utilityType) =>
  getIsContractObligationUnitBased((utilityType))
    ? getContractPaidUnitsIncluded(utilityType) * period.pricePerUnit * getMonthDifference(period, previousPeriod)
    : getContractFinancialObligation(utilityType) * getMonthDifference(period, previousPeriod);

const getRealUsage = (period, previousPeriod) => period.measurementValue - previousPeriod?.measurementValue;
const getRealPrice = (period, previousPeriod) => period.finalPrice ?? getRealUsage(period, previousPeriod) * period.pricePerUnit;
const getPriceDifference = (period, previousPeriod, utilityType) => getRealPrice(period, previousPeriod) - getPredictedObligation(period, previousPeriod, utilityType);




