import { PaperClipIcon, } from "@heroicons/react/solid";
import { DocumentDuplicateIcon } from "@heroicons/react/outline";
import Link from "next/link";
import { useRouter } from "next/router";

const exampleData = [
  { label: "Full name", value: "Margot Foster" },
  { label: "Application for", value: "Backend Developer" },
  { label: "Email address", value: "margotfoster@example.com" },
  { label: "Salary expectation", value: "$120,000" },
  {
    label: "About",
    value:
      "Fugiat ipsum ipsum deserunt culpa aute sint do nostrud anim incididunt cillum culpa consequat. Excepteur qui ipsum aliquip consequat sint. Sit id mollit nulla mollit nostrud in ea officia proident. Irure nostrud pariatur mollit ad adipisicing reprehenderit deserunt qui eu.",
  },
];

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function Example({
  title = "Applicant Information",
  desc,
  data = exampleData,
  button,
}) {
  const router = useRouter();
  console.log("router", router);
  return (
    <div className="bg-white shadow overflow-hidden sm:rounded-lg">
      <div className="px-4 py-5 sm:px-6">
        <div className="flex justify-between items-center flex-wrap sm:flex-nowrap">
          <h3 className="text-lg leading-6 font-medium text-gray-900">
            {title}
          </h3>
          {button}
        </div>
        {desc && <p className="mt-1 max-w-2xl text-sm text-gray-500">{desc}</p>}
      </div>
      <div className="border-t border-gray-200">
        <dl>
          {data
            .filter((row) => row.value?.trim())
            .map((row, i) => (
              <div
                className={classNames(
                  !row.label && "font-bold",
                  i % 2 === 0 ? "bg-gray-50" : "bg-white",
                  " px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6"
                )}
              >
                <dt className="text-sm font-medium text-gray-500">
                  {row.label}
                </dt>

                {(() => {
                  const value = row.desc ? (
                    <>
                      <dd className="mt-1 text-sm  sm:mt-0">{row.desc}</dd>
                      <dd className="mt-1 text-sm  sm:mt-0">{row.value}</dd>
                    </>
                  ) : (
                    <dd className="mt-1 text-sm  sm:mt-0 sm:col-span-2 inline-flex items-center">
                      {row.value}
                      {row.copy && <button
                        type="button"
                        className="ml-2 inline-flex items-center px-1 py-0 border border-transparent text-xs font-medium rounded text-indigo-300 bg-white hover:text-indigo-500 hover:bg-indigo-100 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                        onClick={() => { navigator.clipboard.writeText(row.value) }}
                      >
                        <DocumentDuplicateIcon className="h-5 w-5" aria-hidden="true" />
                      </button>}
                    </dd>
                  );

                  return row.panel ? (
                    <Link
                      href={{
                        pathname: router.asPath,
                        query: { panel: row.panel, id: row.id },
                      }}
                      scroll={false}
                    >
                      <a className="text-indigo-600 font-medium hover:underline hover:text-indigo-500">
                        {value}
                      </a>
                    </Link>
                  ) : (
                    value
                  );
                })()}
              </div>
            ))}
        </dl>
      </div>
    </div>
  );
}
