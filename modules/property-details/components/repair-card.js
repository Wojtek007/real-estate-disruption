import { useState } from "react";
import { CalendarIcon, CashIcon, UsersIcon } from "@heroicons/react/solid";
import * as moment from "moment";
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";

const ADD_TASK = gql`
  mutation MyMutation(
    $task: versionedTasks_insert_input = {
      cost: 10
      executionDate: ""
      name: ""
      notificationId: ""
      notificationVer: 10
      reponsiblePersonId: ""
      reponsiblePersonVer: 10
    }
  ) {
    insertTask(object: $task) {
      id
    }
  }
`;

export default function Example({ repair }) {
  const [isAddingTask, setIsAddingTask] = useState(false);
  const [task, setTask] = useState({
    notificationId: repair.notification.id,
    notificationVer: repair.notification.ver,
  });
  const [addTask, { error, loading, data, called }] = useMutation(ADD_TASK);
  console.log("yyyyyy repair", repair);
  console.log("yyyyyy task", task);

  const addTaskHandler = async () => {
    const newTask = await addTask({
      variables: { task },
    });
    setIsAddingTask(false);
    setTask({
      notificationId: repair.notification.id,
      notificationVer: repair.notification.ver,
    });
  };

  return (
    <div key={repair.id}>
      <div className="bg-white shadow overflow-hidden sm:rounded-md ">
        <ul className="divide-y divide-gray-200">
          <li key={repair.id}>
            <a className="block hover:bg-gray-50">
              <div className="px-4 py-4 sm:px-6">
                <div className="flex items-center justify-between">
                  <p className="text-md font-medium text-indigo-600 truncate">
                    {repair.name}
                  </p>
                  {repair.startDate ? (
                    <div className="ml-2 flex-shrink-0 flex">
                      <p className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                        Wykonywane
                      </p>
                    </div>
                  ) : (
                    <div className="ml-2 flex-shrink-0 flex">
                      <p className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-yellow-100 text-yellow-800">
                        Planowane
                      </p>
                    </div>
                  )}
                </div>
                <div className="mt-2 sm:flex sm:justify-between">
                  {/* <div className="sm:flex">
                    <p className="flex items-center text-sm text-gray-500">
                      <UsersIcon
                        className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
                        aria-hidden="true"
                      />
                      {repair.reporter}
                    </p>
                    <p className="mt-2 flex items-center text-sm text-gray-500 sm:mt-0 sm:ml-6">
                      <CashIcon
                        className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
                        aria-hidden="true"
                      />
                      {repair.cost?.toFixed(2)} zł
                    </p>
                  </div> */}
                  <div className="mt-2 flex items-center text-sm text-gray-500 sm:mt-0">
                    <CalendarIcon
                      className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
                      aria-hidden="true"
                    />
                    {repair.startDate ? (
                      <p>
                        Rozpoczęcie{" "}
                        <time dateTime={repair.startDate}>
                          {moment(repair.startDate).format("L")}
                        </time>
                      </p>
                    ) : (
                      <p>
                        Przypomnienie{" "}
                        <time dateTime={repair.notification?.reminderDate}>
                          {moment(repair.notification?.reminderDate).format(
                            "L"
                          )}
                        </time>
                      </p>
                    )}
                  </div>
                  {isAddingTask ? (
                    <div>
                      <button
                        type="button"
                        onClick={addTaskHandler}
                        className="mr-2 inline-flex items-center px-3 py-1.5 border border-transparent text-xs font-medium rounded-full shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                      >
                        Zapisz
                      </button>
                      <button
                        type="button"
                        onClick={() => setIsAddingTask(false)}
                        className="inline-flex items-center px-2.5 py-1.5 border border-gray-300 shadow-sm text-xs rounded-full  font-medium rounded text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                      >
                        Anuluj
                      </button>
                    </div>
                  ) : (
                    <button
                      type="button"
                      onClick={() => setIsAddingTask(true)}
                      className="inline-flex items-center px-2.5 py-1.5 border border-gray-300 shadow-sm text-xs rounded-full  font-medium rounded text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                    >
                      Dodaj zadanie
                    </button>
                  )}
                </div>
              </div>
            </a>
          </li>
        </ul>
      </div>
      <div className="ml-6">
        {repair.notification?.tasks && (
          <Tasks tasks={repair.notification.tasks} />
        )}
        {isAddingTask && <NewTaskForm task={task} setTask={setTask} />}
      </div>
    </div>
  );
}

export function NewTaskForm({ task, setTask }) {
  return (
    <div
      key={task.id}
      className="border-gray-200 rounded relative bg-white rounded-md mt-2 border p-4 flex flex-col cursor-pointer focus:outline-none"
    >
      <div className="ml-3 relative border border-gray-300 rounded-md px-3 py-2 shadow-sm focus-within:ring-1 focus-within:ring-indigo-600 focus-within:border-indigo-600">
        <label
          htmlFor="name"
          className="absolute -top-2 left-2 -mt-px inline-block px-1 bg-white text-xs font-medium text-gray-900"
        >
          Zadanie do wykonania
        </label>
        <input
          type="text"
          onChange={(e) => setTask({ ...task, name: e.target.value })}
          value={task.name}
          name="name"
          id="name"
          className="block w-full border-0 p-0 text-gray-900 placeholder-gray-500 focus:ring-0 sm:text-sm"
        />
      </div>
      <div className="flex items-center text-sm md:grid md:grid-cols-3 mt-5">
        <div className="ml-3 relative border border-gray-300 rounded-md px-3 py-2 shadow-sm focus-within:ring-1 focus-within:ring-indigo-600 focus-within:border-indigo-600">
          <label
            htmlFor="cost"
            className="absolute -top-2 left-2 -mt-px inline-block px-1 bg-white text-xs font-medium text-gray-900"
          >
            Planowany koszt
          </label>
          <input
            type="text"
            onChange={(e) => setTask({ ...task, cost: e.target.value })}
            value={task.cost}
            name="cost"
            id="cost"
            className="block w-full border-0 p-0 text-gray-900 placeholder-gray-500 focus:ring-0 sm:text-sm"
          />
        </div>
        <div className="ml-3 relative border border-gray-300 rounded-md px-3 py-2 shadow-sm focus-within:ring-1 focus-within:ring-indigo-600 focus-within:border-indigo-600">
          <label
            htmlFor="execution_date"
            className="absolute -top-2 left-2 -mt-px inline-block px-1 bg-white text-xs font-medium text-gray-900"
          >
            Data wykonania
          </label>
          <input
            type="text"
            onChange={(e) =>
              setTask({ ...task, executionDate: e.target.value })
            }
            value={task.executionDate}
            name="execution_date"
            id="execution_date"
            className="block w-full border-0 p-0 text-gray-900 placeholder-gray-500 focus:ring-0 sm:text-sm"
          />
        </div>

        <div className="ml-3 relative border border-gray-300 rounded-md px-3 py-2 shadow-sm focus-within:ring-1 focus-within:ring-indigo-600 focus-within:border-indigo-600">
          <label
            htmlFor="responsible_person"
            className="absolute -top-2 left-2 -mt-px inline-block px-1 bg-white text-xs font-medium text-gray-900"
          >
            Odpowiedzialna osoba
          </label>
          <input
            type="text"
            onChange={(e) =>
              setTask({ ...task, responsiblePerson: e.target.value })
            }
            value={task.responsiblePerson}
            name="responsible_person"
            id="responsible_person"
            className="block w-full border-0 p-0 text-gray-900 placeholder-gray-500 focus:ring-0 sm:text-sm"
          />
        </div>
      </div>
    </div>
  );
}
const plans = [
  {
    name: "Startup",
    priceMonthly: 29,
    priceYearly: 290,
    limit: "Up to 5 active job postings",
  },
  {
    name: "Business",
    priceMonthly: 99,
    priceYearly: 990,
    limit: "Up to 25 active job postings",
  },
  {
    name: "Enterprise",
    priceMonthly: 249,
    priceYearly: 2490,
    limit: "Unlimited active job postings",
  },
];

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export function Tasks({ tasks }) {
  return (
    <div>
      <div className="relative bg-white rounded-md -space-y-px">
        {tasks.map((task, taskIdx) => (
          <div
            key={task.name}
            value={task}
            className={classNames(
              taskIdx === 0 ? "rounded-tl-md rounded-tr-md" : "",
              taskIdx === tasks.length - 1 ? "rounded-bl-md rounded-br-md" : "",
              task.checked
                ? "bg-indigo-50 border-indigo-200 z-10"
                : "border-gray-200",
              "relative border p-4 flex flex-col cursor-pointer md:pl-4 md:pr-6 md:grid md:grid-cols-6 focus:outline-none"
            )}
          >
            <div className="flex items-center text-sm md:col-span-3">
              <span
                className={classNames(
                  task.checked
                    ? "bg-indigo-600 border-transparent"
                    : "bg-white border-gray-300",
                  task.active ? "ring-2 ring-offset-2 ring-indigo-500" : "",
                  "h-4 w-4 rounded border flex items-center justify-center"
                )}
                aria-hidden="true"
              >
                {/* <div className="flex items-center h-5">
                      <input
                        id="offers"
                        name="offers"
                        type="checkbox"
                        className="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded"
                      />
                    </div>
                    <div className="ml-3 text-sm">
                      <label htmlFor="offers" className="font-medium text-gray-700">
                        Offers
                      </label>
                      <p className="text-gray-500">Get notified when a candidate accepts or rejects an offer.</p>
                    </div> */}
                <span className="rounded bg-white w-1.5 h-1.5" />
              </span>
              <h4 as="span" className="ml-3 font-medium text-gray-700">
                {task.name}
              </h4>
            </div>
            <p className="ml-6 pl-1 text-sm md:ml-0 md:pl-0 md:text-center md:col-span-2">
              <span
                className={classNames(
                  task.checked ? "text-indigo-900" : "text-gray-700",
                  "font-medium"
                )}
              >
                {task.cost && task.cost + " zł"}
              </span>
              <span
                className={task.checked ? "text-indigo-700" : "text-gray-500"}
              >
                {task.plannedDateFull}
              </span>
            </p>
            <p
              className={classNames(
                task.checked ? "text-indigo-700" : "text-gray-500",
                "ml-6 pl-1 text-sm md:ml-0 md:pl-0 md:text-right"
              )}
            >
              {task.executor}
            </p>
          </div>
        ))}
      </div>
    </div>
  );
}
