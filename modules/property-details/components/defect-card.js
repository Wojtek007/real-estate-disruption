import { useState } from "react";
import {
  XIcon,
  CalendarIcon,
  MailIcon,
  PhoneIcon,
  DocumentAddIcon,
  DocumentDownloadIcon,
  FireIcon,
  CheckIcon,
} from "@heroicons/react/solid";
import Link from "next/link";
import { useRouter } from "next/router";
import * as moment from "moment";
import { createDefect, buildDefectActions, getStatus } from "@domain/defect";
import Task from "@modules/notifications-center/todos/task";

import { getTodo } from "@modules/notifications-center/domain";

const getIcon = (iconName, color = "gray") => {
  switch (iconName) {
    case "DocumentDownload":
      return (
        <DocumentDownloadIcon
          className={`w-5 h-5 text-${color}-400`}
          aria-hidden="true"
        />
      );
    case "DocumentAdd":
      return (
        <DocumentAddIcon
          className={`w-5 h-5 text-${color}-400`}
          aria-hidden="true"
        />
      );
    case "Fire":
      return (
        <FireIcon className={`w-5 h-5 text-${color}-400`} aria-hidden="true" />
      );
    case "Check":
      return (
        <CheckIcon className={`w-5 h-5 text-${color}-400`} aria-hidden="true" />
      );
    case "X":
      return (
        <XIcon className={`w-5 h-5 text-${color}-400`} aria-hidden="true" />
      );
    case "Calendar":
      return (
        <CalendarIcon
          className={`w-5 h-5 text-${color}-400`}
          aria-hidden="true"
        />
      );

    default:
      return "No-icon";
  }
};

export default function Example({ defect, property }) {
  const {
    pathname,
    query: { organizationId, propertyId },
  } = useRouter();

  const ownerDecision = defect?.notification?.tasks?.find(task => task.config.key === "FIX_APPROVAL")?.decision
  const isFixDone = !!defect?.fixResults.length
  const fixDoneDate = isFixDone && moment(defect?.fixResults.date).format("L")
  const isAppointmentMade = !!defect?.notification?.appointments.length
  const appointmentDate = isAppointmentMade && moment(defect?.notification?.appointments[0].date).format("L")
  console.log("isFixDone", isFixDone);
  console.log("ownerDecision", ownerDecision);
  console.log("STATEFUL-final", defect);
  return (
    <li
      key={defect.id}
      className="col-span-1 bg-white rounded-lg shadow divide-y divide-gray-200"
    >
      <Link
        href={{
          pathname: pathname,
          query: { panel: "defect", propertyId, organizationId, id: defect.id },
        }}
        scroll={false}
      >
        <a href="#" className="block hover:bg-gray-50 p-6">
          <div className="w-full flex items-center justify-between space-x-6">
            <div className="flex-1 truncate">
              <div className="flex items-center space-x-3">
                <h3 className="text-gray-900 text-m font-medium truncate">
                  {defect.name}
                </h3>
              </div>
              <div className="mt-1 text-gray-500 text-sm truncate">
                <span
                  className={classNames(
                    `text-${defect.urgency.color}-800`,
                    `bg-${defect.urgency.color}-100`,
                    "flex-shrink-0 inline-block px-2 py-0.5 text-xs font-medium rounded-full"
                  )}
                >
                  {defect.urgency.name}
                </span>
              </div>
            </div>
            <div>
              <div className="text-right text-sm whitespace-nowrap text-gray-500">
                Zgłoszone{" "}
                <time dateTime={defect.createdAt}>
                  {moment(defect.createdAt).format("L")}
                </time>
              </div>
              {defect.reporter && (
                <p className="mt-1 text-gray-500 text-sm truncate">
                  przez {defect.reporter.firstname} {defect.reporter.surname}
                </p>
              )}
            </div>

            {/* <img
              className="w-10 h-10 bg-gray-300 rounded-full flex-shrink-0"
              src={defect.imageUrl}
              alt=""
            /> */}
          </div>
          {ownerDecision && <p className="text-sm text-gray-500">
            {ownerDecision === "NEGATIVE" ? 'Odrzucone przez właściciela' : 'Uzyskano zgodę właściciela'}
          </p>}
          {isFixDone && <div className="mt-1 text-gray-500 text-sm truncate">
            <span
              className={classNames(
                `text-yellow-800`,
                `bg-yellow-100`,
                "flex-shrink-0 inline-block px-2 py-0.5 text-xs font-medium rounded-full"
              )}
            >
              Umówiona naprawa na {appointmentDate}
            </span>
          </div>}
          {isAppointmentMade && <div className="mt-1 text-gray-500 text-sm truncate">
            <span
              className={classNames(
                `text-green-800`,
                `bg-green-100`,
                "flex-shrink-0 inline-block px-2 py-0.5 text-xs font-medium rounded-full"
              )}
            >
              Naprawa wykonana {fixDoneDate}
            </span>
          </div>}
        </a>
      </Link>
      {/* <div>
        <div className="-mt-px flex divide-x divide-gray-200">
          {defectActions.map((action) => (
            <div className="w-0 flex-1 flex">
              <a
                onClick={action.onClick}
                className="relative -mr-px w-0 flex-1 inline-flex items-center justify-center py-4 text-sm text-gray-700 font-medium border border-transparent rounded-bl-lg hover:text-gray-500"
              >
                {getIcon(action.icon, action.color)}
                <span className="ml-3">{action.name}</span>
              </a>
            </div>
          ))}
           <div className="-ml-px w-0 flex-1 flex">
            <a
              href={`tel:${defect.telephone}`}
              className="relative w-0 flex-1 inline-flex items-center justify-center py-4 text-sm text-gray-700 font-medium border border-transparent rounded-br-lg hover:text-gray-500"
            >
              <MailIcon className="w-5 h-5 text-gray-400" aria-hidden="true" />
              <span className="ml-3">Skontaktuj się</span>
            </a>
          </div> 
        </div>
      </div> */}
      {/* <div className="mt-1 pl-7">
        {getTodo(defect.notification)
          .tasks?.sort((a, b) => a.config.order - b.config.order)
          .map((task) => (
            <Task task={task} />
          ))}
      </div> */}
    </li>
  );
}

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}
