import { useRouter } from "next/router";
import Link from "next/link";

import {
  InformationCircleIcon,
  AdjustmentsIcon,
  ClipboardCheckIcon,
  CurrencyDollarIcon,
  ExclamationCircleIcon,
  PencilAltIcon,
  ShieldCheckIcon,
  BriefcaseIcon,
} from "@heroicons/react/outline";

const navigation = (pathname, propertyId, organizationId) => [
  {
    name: "Informacje",
    href: `/${organizationId}/properties/${propertyId}/info`,
    icon: InformationCircleIcon,
    current: pathname.split("/")[4].includes(`info`),
  },
  {
    name: "Umowy",
    href: `/${organizationId}/properties/${propertyId}/contracts`,
    icon: PencilAltIcon,
    current: pathname.split("/")[4].includes(`contracts`),
  },
  {
    name: "Przeglądy",
    href: `/${organizationId}/properties/${propertyId}/checks`,
    icon: ClipboardCheckIcon,
    current: pathname.split("/")[4].includes(`checks`),
  },
  {
    name: "Liczniki",
    href: `/${organizationId}/properties/${propertyId}/meters`,
    icon: AdjustmentsIcon,
    current: pathname.split("/")[4].includes(`meters`),
  },
  {
    name: "Usterki & Remonty",
    href: `/${organizationId}/properties/${propertyId}/defects`,
    icon: BriefcaseIcon, //ExclamationCircleIcon,
    current: pathname.split("/")[4].includes(`defects`),
  },
  // {
  //   name: "Finanse",
  //   href: `/${organizationId}/properties/${propertyId}/finance`,
  //   icon: CurrencyDollarIcon,
  //   current: pathname.split("/")[4].includes(`finance`),
  // },
  // {
  //   name: "Gwarancje",
  //   href: `/${organizationId}/properties/${propertyId}/guarantees`,
  //   icon: ShieldCheckIcon,
  //   current: pathname.split("/")[4].includes(`guarantees`),
  // },
  // {
  //   name: "Remonty",
  //   href: `/${organizationId}/properties/${propertyId}/repairs`,
  //   icon: BriefcaseIcon,
  //   current: pathname.split("/")[4].includes(`repairs`),
  // },
];

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function Example() {
  const { pathname, query } = useRouter();
  const { propertyId, organizationId } = query;
  console.log("details menu pathname", pathname.split("/"));
  return (
    <aside className="py-6 px-2 sm:px-6 lg:py-0 lg:px-0 lg:col-span-3">
      <nav className="space-y-1">
        {navigation(pathname, propertyId, organizationId).map((item) => (
          <Link href={item.href}>
            <a
              key={item.name}
              className={classNames(
                item.current
                  ? "bg-gray-50 text-indigo-700 hover:text-indigo-700 hover:bg-white"
                  : "text-gray-900 hover:text-gray-900 hover:bg-gray-50",
                "group rounded-md px-3 py-2 flex items-center text-sm font-medium"
              )}
              aria-current={item.current ? "page" : undefined}
            >
              <item.icon
                className={classNames(
                  item.current
                    ? "text-indigo-500 group-hover:text-indigo-500"
                    : "text-gray-400 group-hover:text-gray-500",
                  "flex-shrink-0 -ml-1 mr-3 h-6 w-6"
                )}
                aria-hidden="true"
              />
              <span className="truncate">{item.name}</span>
            </a>
          </Link>
        ))}
      </nav>
    </aside>
  );
}
