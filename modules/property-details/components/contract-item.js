import {
  ClipboardCheckIcon,
  ClipboardIcon,
  ClipboardCopyIcon,
  XCircleIcon,
} from "@heroicons/react/solid";
import Link from "next/link";
import { useRouter } from "next/router";
import * as moment from "moment";
import ThreeDotMenu from "./three-dot-menu";

const applications = [
  {
    applicant: {
      name: "Ricardo Cooper",
      email: "ricardo.cooper@example.com",
      imageUrl:
        "https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80",
    },
    date: "2020-01-07",
    dateFull: "7 Stycznia 2020 - 7 stycznia 2021",
    stageIcon: (
      <XCircleIcon
        className="flex-shrink-0 mr-1.5 h-5 w-5 text-red-400"
        aria-hidden="true"
      />
    ),
    stage: "Rozwiązany",
    href: "#",
  },
  {
    applicant: {
      name: "Ricardo Cooper",
      email: "ricardo.cooper@example.com",
      imageUrl:
        "https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80",
    },
    date: "2020-01-07",
    dateFull: "7 Stycznia 2020 - 7 stycznia 2021",
    stageIcon: (
      <ClipboardIcon
        className="flex-shrink-0 mr-1.5 h-5 w-5 text-yellow-400"
        aria-hidden="true"
      />
    ),
    stage: "Wygenerowane",
    href: "#",
  },
  {
    applicant: {
      name: "Kristen Ramos",
      email: "kristen.ramos@example.com",
      imageUrl:
        "https://images.unsplash.com/photo-1550525811-e5869dd03032?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80",
    },
    date: "2020-01-07",
    dateFull: "7 Stycznia 2020 - 7 stycznia 2021",
    stageIcon: (
      <ClipboardCopyIcon
        className="flex-shrink-0 mr-1.5 h-5 w-5 text-yellow-400"
        aria-hidden="true"
      />
    ),
    stage: "Wysłane",
    href: "#",
  },
  {
    applicant: {
      name: "Ted Fox",
      email: "ted.fox@example.com",
      imageUrl:
        "https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80",
    },
    date: "2020-01-07",
    dateFull: "7 Stycznia 2020 - 7 stycznia 2021",
    stageIcon: (
      <ClipboardCheckIcon
        className="flex-shrink-0 mr-1.5 h-5 w-5 text-green-400"
        aria-hidden="true"
      />
    ),
    stage: "Podpisane",
    href: "#",
  },
];

const getStatusIcon = ({ type, status }) => {
  switch (status) {
    case "Obowiązuje":
      return (
        <ClipboardCheckIcon
          className="flex-shrink-0 mr-1.5 h-5 w-5 text-green-600"
          aria-hidden="true"
        />
      );
    case "Nie rozpoczęta":
      return (
        <ClipboardCheckIcon
          className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-600"
          aria-hidden="true"
        />
      );
    case "Podpisana":
      return ["termination"].includes(type.effect) ? (
        <ClipboardCheckIcon
          className="flex-shrink-0 mr-1.5 h-5 w-5 text-red-600"
          aria-hidden="true"
        />
      ) : (
        <ClipboardCheckIcon
          className="flex-shrink-0 mr-1.5 h-5 w-5 text-green-600"
          aria-hidden="true"
        />
      );
    case "Przerwana":
      return (
        <XCircleIcon
          className="flex-shrink-0 mr-1.5 h-5 w-5 text-red-600"
          aria-hidden="true"
        />
      );
    case "Przerwana":
      return (
        <XCircleIcon
          className="flex-shrink-0 mr-1.5 h-5 w-5 text-yellow-600"
          aria-hidden="true"
        />
      );
    case "Zakończona":
      return (
        <XCircleIcon
          className="flex-shrink-0 mr-1.5 h-5 w-5 text-yellow-600"
          aria-hidden="true"
        />
      );

    default:
      break;
  }
};

const getContractStatus = (contract) => {
  if (!contract) {
    return null;
  }
  console.log("CCCCONTA start", contract.type.effect);
  if (contract.type.effect === "aggreament") {
    const hasTermination = contract.contracts
      .map((c) => c.type.effect)
      .includes("termination");

    if (hasTermination) {
      return "Przerwana";
    }

    const hasNotYetStarted = moment(contract.startDate).isAfter(moment(), "day");
    if (hasNotYetStarted) {
      return "Nie rozpoczęta";
    }
    const hasExpired = moment(contract.endDate).isBefore(moment(), "day");

    if (hasExpired) {
      return "Zakończona";
    }

    return "Obowiązuje";
  }
  if (contract.type.effect === "annex") {
    const isReferingTerminatedContract =
      getContractStatus(contract.refersToContract) === "Przerwana";
    if (isReferingTerminatedContract) {
      return "Przerwana";
    }

    return "Podpisana";
  }
  if (contract.type.effect === "termination") {
    return "Podpisana";
  }
};
function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function Example({ contract, property, category }) {
  const {
    asPath,
    pathname,
    query: { organizationId },
  } = useRouter();
  const contractStatus = getContractStatus(contract);
  console.log("CCCCONTA result", contractStatus);
  console.log("LLLLLL222 contract", contract);
  const isReferingTerminatedContract =
    getContractStatus(contract) === "Przerwana";

  const menuOptions = category && [
    contract.documentLink?.hasUploadFinished ? (
      <a
        href={contract.documentLink.awsLink}
        className="text-gray-700 block px-4 py-2 text-sm hover:text-gray-900 hover:bg-gray-100"
        download={`${contract.documentLink.name}.${contract.documentLink.extension}`}
      >
        Pobierz plik
      </a>
    ) : (
      <a className="text-gray-200 block px-4 py-2 text-sm">Brak pliku</a>
    ),
    <Link
      href={{
        pathname: `/${organizationId}/contract/${contract.id}/edit`,
        query: {
          redirect: asPath,
          hidden: [
            "category",
            "subjectProperty",
            "refersToContract",
            "type",
            "notification/data/reminderDate",
          ],
        },
      }}
    >
      <a className="text-gray-700 block px-4 py-2 text-sm hover:text-gray-900 hover:bg-gray-100">
        Edytuj
      </a>
    </Link>,
    ["aggreament", "termination"].includes(contract.type.effect) &&
    <Link
      href={{
        pathname: `/${organizationId}/contract/new`,
        query: {
          ...(["rent", "management"].includes(category.key)
            ? {
              signerId: contract.signer?.id,
              signerVer: contract.signer?.ver,
            }
            : {}),
          redirect: asPath,
          categoryId: category.id,
          categoryVer: category.ver,
          subjectPropertyId: property.id,
          subjectPropertyVer: property.ver,
          refersToContractId: contract.id,
          refersToContractVer: contract.ver,
          typeId: "ee33c218-90f9-4a7c-942c-2f3d7692edbd",
          typeVer: 2,
          hidden: [
            "category",
            "subjectProperty",
            "refersToContract",
            "type",
            "notification/data/reminderDate",
          ],
        },
      }}
    >
      <a className="text-gray-700 block px-4 py-2 text-sm hover:text-gray-900 hover:bg-gray-100">
        Aneksuj
      </a>
    </Link>,
    contract.type.effect === "aggreament" && !isReferingTerminatedContract && <Link
      href={{
        pathname: `/${organizationId}/contract/new`,
        query: {
          ...(["rent", "management"].includes(category.key)
            ? {
              signerId: contract.signer?.id,
              signerVer: contract.signer?.ver,
            }
            : {}),
          redirect: asPath,
          categoryId: category.id,
          categoryVer: category.ver,
          subjectPropertyId: property.id,
          subjectPropertyVer: property.ver,
          refersToContractId: contract.id,
          refersToContractVer: contract.ver,
          typeId: "54d6733d-d6ff-4ce7-8762-d41f00d8e312",
          typeVer: 3,
          hidden: [
            "category",
            "subjectProperty",
            "refersToContract",
            "type",
            "notification/data/reminderDate",
          ],
        },
      }}
    >
      <a className="text-gray-700 block px-4 py-2 text-sm hover:text-gray-900 hover:bg-gray-100">
        Rozwiąż
      </a>
    </Link>,


  ].filter(Boolean);
  return (
    <div className={classNames(contract.refersToContract && contract.type.effect !== "aggreament" && "ml-8", "flex items-center px-4 py-4 sm:px-6")}>
      <div className="min-w-0 flex-1 flex items-center">
        {/* <div className="flex-shrink-0">
                      <img
                        className="h-12 w-12 rounded-full"
                        src={application.applicant.imageUrl}
                        alt=""
                      />
                    </div> */}
        <div className="min-w-0 flex-1 px-4 md:grid md:grid-cols-2 md:gap-4">
          <div>
            <Link
              href={{
                pathname: pathname,
                query: {
                  panel: "person",
                  id: contract.signer?.id,
                  propertyId: property.id,
                  organizationId,
                },
              }}
              scroll={false}
            >
              <a className="text-sm text-indigo-600 font-medium hover:underline hover:text-indigo-500">
                {contract.signer?.firstname} {contract.signer?.surname}
              </a>
            </Link>

            <p className="mt-2 flex items-center text-sm text-gray-500">
              <ClipboardIcon
                className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
                aria-hidden="true"
              />
              <span className="truncate">{contract.type?.name}</span>
            </p>
          </div>
          <div className="hidden md:block">
            <div>
              <p className="text-sm text-gray-900">
                {contract.startDate && <time dateTime={contract.startDate}>
                  {moment(contract.startDate).format("L")}
                </time>}
                {contract.endDate && contract.startDate && " - "}
                {contract.endDate && (
                  <time dateTime={contract.endDate}>
                    {moment(contract.endDate).format("L")}
                  </time>
                )}
              </p>
              <p className="mt-2 flex items-center text-sm text-gray-500">
                {getStatusIcon({
                  type: contract.type,
                  status: contractStatus,
                })}
                {contractStatus}
              </p>
            </div>
          </div>
        </div>
      </div>
      {category && <div className="ml-4 flex-shrink-0">
        <Link
          href={{
            pathname: pathname,
            query: {
              panel: "contract",
              id: contract?.id,
              propertyId: property.id,
              organizationId,
            },
          }}
          scroll={false}
        >
          <a className="text-indigo-600 font-medium hover:underline hover:text-indigo-500">
            Detale
          </a>
        </Link>
      </div>}
      <div className="ml-4 flex-shrink-0"></div>
      {menuOptions && <ThreeDotMenu options={menuOptions} />}
    </div>
  );
}
