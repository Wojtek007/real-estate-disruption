import {
  PaperClipIcon,
  BellIcon,
  UserIcon,
  XIcon,
  CheckIcon,
} from "@heroicons/react/solid";
import {
  FireIcon,
  BeakerIcon,
  LightningBoltIcon,
} from "@heroicons/react/outline";
import {
  AdjustmentsIcon,
  CalendarIcon,
  ReceiptRefundIcon,
} from "@heroicons/react/solid";
import Link from "next/link";
import * as moment from "moment";
import { useRouter } from "next/router";
import Button from "@components/button";
import NoMeterPeriodsState from '../meters/components/no-meter-periods-state'
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";

const GROUP_METER_PERIODS = gql`
mutation MyMutation($paymentGroup: versionedPaymentGroupMeterPeriods_insert_input = {}) {
  insertPaymentGroupMeterPeriod(object: $paymentGroup) {
    id
  }
}
`;

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

const getIcon = (iconName) => {
  switch (iconName) {
    case "LightningBolt":
      return (
        LightningBoltIcon
      );
    case "Beaker":
      return BeakerIcon
    case "Fire":
      return FireIcon
  }
};


const getContractObligation = (utilityType) => {
  console.log("getContractObligation-START", utilityType.contractObligationTypes[0]);
  return utilityType.contractObligationTypes[0]?.contractObligations[0];
};
const getIsContractObligationUnitBased = (utilityType) => {
  console.log("11111", utilityType);
  return utilityType.contractObligationTypes[0]?.isUnitBased;
};

const getContractFinancialObligation = (utilityType) =>
  getContractObligation(utilityType)?.financialObligation ?? 0;
const getContractPaidUnitsIncluded = (utilityType) =>
  getContractObligation(utilityType)?.paidUnits ?? 0;
const getContractSigner = (utilityType) =>
  getContractObligation(utilityType)?.contract.signer;


const getDurationFromPreviousMeasurment = (period, previousPeriod) => moment.duration(
  moment(period.measurementDate).diff(moment(previousPeriod?.measurementDate))
);
const getMonthDifference = (period, previousPeriod) => Math.round(getDurationFromPreviousMeasurment(period, previousPeriod).asMonths());
const getPredictedObligation = (period, previousPeriod, utilityType) => getIsContractObligationUnitBased((utilityType))
  ? getContractPaidUnitsIncluded(utilityType) * period.pricePerUnit * getMonthDifference(period, previousPeriod)
  : getContractFinancialObligation(utilityType) * getMonthDifference(period, previousPeriod);
const getRealUsage = (period, previousPeriod) => period.measurementValue - previousPeriod?.measurementValue;
const getRealPrice = (period, previousPeriod) => period.finalPrice ?? getRealUsage(period, previousPeriod) * period.pricePerUnit;
const getPriceDifference = (period, previousPeriod, utilityType) => getRealPrice(period, previousPeriod) - getPredictedObligation(period, previousPeriod, utilityType);


export default function Example({ utilityType, property }) {
  const router = useRouter();
  const { organizationId } = router.query;
  const [groupMeterPeriods, { error, loading, data, called }] = useMutation(GROUP_METER_PERIODS);

  console.log('EEEEEE', utilityType)
  const icon = { icon: getIcon(utilityType.icon) }
  const firstGroupedPeriodIdx = utilityType.meterPeriods.findIndex(period => period.paymentGroup)
  const freePeriods = utilityType.meterPeriods.filter((period, i) => firstGroupedPeriodIdx === -1 || i < firstGroupedPeriodIdx + 1)
  console.log('PPPPPPPP1111', firstGroupedPeriodIdx, freePeriods, utilityType.meterPeriods)


  const handleGroupMeterPeriods = () => {
    const endDate = moment(freePeriods[freePeriods.length - 1]?.measurementDate).format("L")
    const startDate = moment(freePeriods[0]?.measurementDate).format("L")

    const sumOfPriceDifference = freePeriods.reduce((acc, period, i, self) => {
      if (i === self.length - 1) {
        return acc
      }
      return acc + getPriceDifference(period, self[i + 1], utilityType)
    }, 0);

    const paymentGroup = {
      endDate,
      startDate,
      sumAmout: sumOfPriceDifference * 100,
      meterPeriods: {
        data: freePeriods
          .filter((_, i, self) => i < self.length - 1)
          .map(period => ({
            id: period.id,
            measurementDate: period.measurementDate,
            measurementValue: period.measurementValue,
            pricePerUnit: period.pricePerUnit,
            propertyId: period.property.id,
            propertyVer: period.property.ver,
            utilityTypeId: utilityType.id,
            utilityTypeVer: utilityType.ver,
          }))
      }
    }
    groupMeterPeriods({ variables: { paymentGroup } })
  }

  const buttonClosePeriod = <Button
    onClick={handleGroupMeterPeriods}
    isSecondary
  >
    Zamknij okres
  </Button>
  const button = <Button
    pathname={`/${organizationId}/meter-period/new`}
    query={{
      utilityTypeId: utilityType.id,
      utilityTypeVer: utilityType.ver,
      propertyId: property.id,
      propertyVer: property.ver,
      previousMeasurementValue: utilityType.meterPeriods[0]?.measurementValue,
      removed: [
        'previousMeasurementValue'
      ],
      hidden: [
        "utilityType",
        "property",
        ...(utilityType.meterPeriods?.length ? [] : ["meterPeriodPriceIngredients/data", "finalPrice", "pricePerUnit"]),
      ],
      redirect: router.asPath,
    }}

  >
    Dodaj pomiar
  </Button>
  if (!utilityType.meterPeriods.length) {
    return <NoMeterPeriodsState title={utilityType.label} icon={icon} button={button} />
  }
  return (
    <div
      key={utilityType.id}
      className="relative bg-white pt-5 px-4  sm:pt-6 sm:px-6 shadow rounded-lg overflow-hidden" //pb-16
    >
      <div className="flex items-end mb-2">
        <div className=" bg-indigo-500 rounded-md p-3">
          <icon.icon className="h-7 w-7 text-white" aria-hidden="true" />

        </div>
        <div className="ml-3 self-start">
          <p className="text-sm font-medium text-gray-500">
            {utilityType.label}
          </p>
          {utilityType.stat && (
            <p
              className={classNames(
                utilityType.stat >= 0 ? "text-green-600" : "text-red-600",
                "text-2xl font-semibold "
              )}
            >
              {utilityType.stat} zł
            </p>
          )}
          {/* <p
                className={classNames(
                  utilityType.stan === "nadpłata" ? "text-green-600" : "text-red-600",
                  "ml-2 flex items-baseline text-sm font-semibold"
                )}
              >
                {utilityType.stan}
              </p> */}
        </div>

        <div className="flex gap-2 ml-auto">
          {freePeriods.length > 1 && buttonClosePeriod}{button}
        </div>
      </div>
      {/* <div className="ml-2">
        <p className="text-sm font-semibold">{utilityType.rentAdvance} w czynszu</p>
      </div> */}
      <PeriodsTable utilityType={utilityType} freePeriods={freePeriods} />

      {/* <div className="absolute bottom-0 inset-x-0 bg-gray-50 px-4 py-4 sm:px-6">
        <div className="text-sm">
          <a
            href="#"
            className="font-medium text-indigo-600 hover:text-indigo-500"
          >
            View all<span className="sr-only"> {utilityType.label} stats</span>
          </a>
        </div>
      </div> */}
    </div>
  );
}

function PeriodsTable({ utilityType, freePeriods }) {
  return (
    <div className="-mx-6 flex flex-col mb-3 mt-0">
      <div className="overflow-x-auto ">
        <div className="align-middle inline-block min-w-full ">
          <div className="shadow overflow-hidden border-b border-gray-200 ">
            <table className="min-w-full divide-y divide-gray-200">
              <thead className="bg-gray-50">
                <tr>
                  <th
                    scope="col"
                    className="px-2 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Data
                  </th>
                  <th
                    scope="col"
                    className="px-2 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Zaliczka w czynszu
                  </th>
                  <th
                    scope="col"
                    className="px-2 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Cena za jednostkę
                  </th>
                  <th
                    scope="col"
                    className="px-2 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Stan licznika
                  </th>
                  <th
                    scope="col"
                    className="px-2 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Przewidywany
                  </th>
                  <th
                    scope="col"
                    className="px-2 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Rzeczywista
                  </th>
                  <th
                    scope="col"
                    className="px-2 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Dopłata
                  </th>
                  {/* <th scope="col" className="relative px-2 py-3">
                    <span className="sr-only">Edit</span>
                  </th> */}
                </tr>
              </thead>
              <tbody className="bg-white divide-y divide-gray-200">
                {freePeriods.length > 1 && <MeterPeriodsSummaryRow
                  periods={freePeriods}
                  utilityType={utilityType}
                />}
                {utilityType.meterPeriods?.map((period, i, self) => {
                  const isPaymentGroup = !!period.paymentGroup
                  const isFirstInPaymentGroup = isPaymentGroup && self.findIndex(p => p.paymentGroup?.id === period.paymentGroup.id) === i

                  return (
                    <>
                      {isPaymentGroup && isFirstInPaymentGroup && <MeterPeriodsSummaryRow
                        periods={self.filter(p => p.paymentGroup?.id === period.paymentGroup.id)}
                        utilityType={utilityType}
                      />}
                      <MeterPeriod
                        period={period}
                        previousPeriod={self[i + 1]}
                        utilityType={utilityType}
                        isFirstInPeriod={i === self.length - 1}
                      />
                    </>
                  )
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
}


function MeterPeriodsSummaryRow({ periods, utilityType, paymentGroup }) {
  console.log('PPPPPP', periods)
  const endOfPeriod = periods.length > 0 && moment(periods[periods.length - 1]?.measurementDate).format("L")
  const startOfPeriod = periods.length > 0 && moment(periods[0]?.measurementDate).format("L")

  const sumOfPredictedObligation = periods.reduce((acc, period, i, self) => {
    if (i === self.length - 1) {
      return acc
    }
    return acc + getPredictedObligation(period, self[i + 1], utilityType)
  }, 0).toFixed(2);
  const sumOfRealPrice = periods.reduce((acc, period, i, self) => {
    if (i === self.length - 1) {
      return acc
    }
    return acc + getRealPrice(period, self[i + 1])
  }, 0).toFixed(2);
  const sumOfPriceDifference = periods.reduce((acc, period, i, self) => {
    if (i === self.length - 1) {
      return acc
    }
    return acc + getPriceDifference(period, self[i + 1], utilityType)
  }, 0).toFixed(2);
  console.log('PPPPPP000', sumOfRealPrice, sumOfPriceDifference)
  return (
    <tr key={paymentGroup?.id ?? 'summary-payment-row'} className="text-center  bg-gray-100">
      <td className="px-2 py-2 whitespace-nowrap" colSpan='2'>
        {/* <div className="flex items-center"> */}
        {/* <div className="flex-shrink-0 h-10 w-10">
                          <img className="h-10 w-10 rounded-full" src={person.image} alt="" />
                        </div> */}
        {/* <div className="ml-4"> */}
        <div className="text-sm font-medium text-gray-900">
          {/* {moment(period.measurementDate).format("L")} */}
          {startOfPeriod} - {endOfPeriod}
        </div>
        {/* <div className="text-xs text-gray-500">
          opłata do
          <br />
          10.06.2021
        </div> */}
        {/* </div> */}
        {/* </div> */}
      </td>
      <td className="px-2 py-2 whitespace-nowrap" colSpan='2'>
        <div className="text-sm font-medium text-gray-900">
          {/* {getIsContractObligationUnitBased(utilityType)
              ? getContractPaidUnitsIncluded(utilityType) + " " + utilityType.measurementUnit
              : parseFloat(getContractFinancialObligation(utilityType)).toFixed(2) + " zł"
            } */}
        </div>
        {getContractSigner(utilityType) && (
          <div className="flex items-center justify-center">
            <UserIcon className="w-4 h-4 text-gray-400" aria-hidden="true" />
            <div className="text-sm text-gray-500 ml-1">
              {getContractSigner(utilityType)?.firstname}{" "}
              {getContractSigner(utilityType)?.surname}
            </div>
          </div>
        )}
      </td>

      <td className="px-2 py-2 whitespace-nowrap bg-green-700">
        <div className="text-sm font-medium text-white">
          {sumOfPredictedObligation} zł
        </div>
        {/* <div className="flex items-center justify-center">
              <CheckIcon className="w-4 h-4 text-white" aria-hidden="true" />
              <div className="text-xs text-white ml-1">Opłacone</div>
            </div> */}
      </td>
      <td className="px-2 py-2 whitespace-nowrap bg-green-700">
        <div className="text-sm font-medium text-white">
          {sumOfRealPrice} zł
        </div>
        {/* <div className="flex items-center justify-center">
              <CheckIcon className="w-4 h-4 text-white" aria-hidden="true" />
              <div className="text-xs text-white ml-1">Opłacone</div>
            </div> */}
      </td>
      <td className="px-2 py-2 whitespace-nowrap bg-red-700">
        <div className="text-sm font-medium text-white">
          {sumOfPriceDifference} zł
        </div>
        {/* <div className="flex items-center justify-center">
              <XIcon className="w-4 h-4 text-white" aria-hidden="true" />
              <div className="text-xs text-white ml-1">
                Czekam na
                <br />
                najemce
              </div>
            </div> */}
      </td>
    </tr>
  )
}

function MeterPeriod({ period, utilityType, isFirstInPeriod, previousPeriod }) {
  const {
    pathname,
    query: { organizationId, propertyId },
  } = useRouter();

  console.log(
    "periodperiodpreviousPeriod",
    period.measurementValue,
    previousPeriod?.measurementValue
  );
  console.log("periodperiodutilityType", utilityType);

  const predictedObligation = getPredictedObligation(period, previousPeriod, utilityType).toFixed(2)
  const realPrice = getRealPrice(period, previousPeriod).toFixed(2)
  const priceDifference = getPriceDifference(period, previousPeriod, utilityType).toFixed(2)

  return (
    <Link
      href={{
        pathname: pathname,
        query: {
          panel: "meter-period",
          propertyId,
          organizationId,
          id: period.id,
        },
      }}
      scroll={false}
    >
      <tr key={period.id} className="text-center">
        <td className="px-2 py-2 whitespace-nowrap">
          {/* <div className="flex items-center"> */}
          {/* <div className="flex-shrink-0 h-10 w-10">
                          <img className="h-10 w-10 rounded-full" src={person.image} alt="" />
                        </div> */}
          {/* <div className="ml-4"> */}
          <div className="text-sm font-medium text-gray-900">
            {moment(period.measurementDate).format("L")}
          </div>
          {/* <div className="text-xs text-gray-500">
          opłata do
          <br />
          10.06.2021
        </div> */}
          {/* </div> */}
          {/* </div> */}
        </td>
        <td className="px-2 py-2 whitespace-nowrap">
          <div className="text-sm font-medium text-gray-900">
            {getIsContractObligationUnitBased(utilityType)
              ? getContractPaidUnitsIncluded(utilityType) + " " + utilityType.measurementUnit
              : parseFloat(getContractFinancialObligation(utilityType)).toFixed(2) + " zł"
            }
          </div>
          {getContractSigner(utilityType) && (
            <div className="flex items-center justify-center">
              <UserIcon className="w-4 h-4 text-gray-400" aria-hidden="true" />
              <div className="text-sm text-gray-500 ml-1">
                {getContractSigner(utilityType)?.firstname}{" "}
                {getContractSigner(utilityType)?.surname}
              </div>
            </div>
          )}
        </td>
        <td className="px-2 py-2 whitespace-nowrap">
          {/* <div className="text-sm text-gray-900">Start: 201923</div> */}
          {period.pricePerUnit ? (
            <div className="text-sm text-gray-900">
              {parseFloat(period.pricePerUnit).toFixed(2)} zł
            </div>
          ) : (
            <div className="text-sm text-gray-500">Brak ceny</div>
          )}
          {/* <div className="text-sm text-gray-500">Koniec: 202223</div> */}
        </td>
        <td className="px-2 py-2 whitespace-nowrap">
          {/* <div className="text-sm text-gray-900">Start: 201923</div> */}
          <div className="text-sm text-gray-500">
            {period.measurementValue} {utilityType.measurementUnit}
          </div>
          {/* <div className="text-sm text-gray-500">Koniec: 202223</div> */}
        </td>
        {!isFirstInPeriod && (
          <>
            <td className="px-2 py-2 whitespace-nowrap bg-green-500">
              <div className="text-sm font-medium text-white">
                {predictedObligation} zł
              </div>
              {/* <div className="flex items-center justify-center">
              <CheckIcon className="w-4 h-4 text-white" aria-hidden="true" />
              <div className="text-xs text-white ml-1">Opłacone</div>
            </div> */}
            </td>
            <td className="px-2 py-2 whitespace-nowrap bg-green-500">
              <div className="text-sm font-medium text-white">
                {realPrice} zł
              </div>
              {/* <div className="flex items-center justify-center">
              <CheckIcon className="w-4 h-4 text-white" aria-hidden="true" />
              <div className="text-xs text-white ml-1">Opłacone</div>
            </div> */}
            </td>
            <td className="px-2 py-2 whitespace-nowrap bg-red-500">
              <div className="text-sm font-medium text-white">
                {priceDifference} zł
              </div>
              {/* <div className="flex items-center justify-center">
              <XIcon className="w-4 h-4 text-white" aria-hidden="true" />
              <div className="text-xs text-white ml-1">
                Czekam na
                <br />
                najemce
              </div>
            </div> */}
            </td>
          </>
        )}
      </tr>
    </Link>
  );
}
