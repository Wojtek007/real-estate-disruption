import * as moment from "moment";
import { sumFreePeriods } from "./utility-type";

const removeDuplicateById = (payer, index, self) => index === self.findIndex((p) => (p.id === payer.id))


export const printMeterPeriodDate = ({ period, previousPeriod, utilityType }) =>
    moment(period.measurementDate).format("L");
export const printMeterPeriodPrepaidAmount = ({ period, previousPeriod, utilityType }) => {
    if (!previousPeriod) {
        return;
    }
    const prepaidPeriods = getPrepaidPeriods({ period, previousPeriod, utilityType });

    // console.log('getPrepaidPeriods', prepaidPeriods)

    return prepaidPeriods
        .map(prepaidPeriod => {
            // console.log('getPrepaidPeriods', prepaidPeriod)

            return printPrepaidObligation({ prepaidPeriod })
        }).join(' & ');
}
export const printPrepaidObligation = ({ prepaidPeriod: { paidUnits, measurementUnit, financialObligation } }) =>
    financialObligation
        ? `${parseFloat(financialObligation).toFixed(2)} zł`
        : `${paidUnits} ${measurementUnit}`

export const printMeterPeriodOwner = ({ period, previousPeriod, utilityType }) => {
    if (!previousPeriod) {
        return;
    }
    const prepaidPeriods = getPrepaidPeriods({ period, previousPeriod, utilityType });

    // console.log('getPrepaidPeriods', prepaidPeriods)

    return prepaidPeriods
        .filter(removeDuplicateById)
        .map(({ payer }) => `${payer.firstname ?? ''} ${payer.surname ?? ''}`).join(' & ');
}
export const printMeterPeriodPrice = ({ period, previousPeriod, utilityType }) =>
    `${parseFloat(period.pricePerUnit).toFixed(2)} zł`;

export const printMeterPeriodMeasurment = ({ period, previousPeriod, utilityType }) =>
    `${period.measurementValue} ${utilityType.measurementUnit}`;

export const printMeterPeriodExpectedPrice = ({ period, previousPeriod, utilityType }) => {
    if (!previousPeriod) {
        return;
    }

    return `${getExpectedPrice(period, previousPeriod, utilityType).toFixed(2)} zł`;
}
export const printMeterPeriodRealPrice = ({ period, previousPeriod, utilityType }) => {
    if (!previousPeriod) {
        return;
    }
    return `${getRealPrice(period, previousPeriod).toFixed(2)} zł`
}
export const printMeterPeriodPriceDifference = ({ period, previousPeriod, utilityType }) => {
    if (!previousPeriod) {
        return;
    }
    // console.log('printMeterPeriodPriceDifference expect:', getExpectedPrice(period, previousPeriod, utilityType), 'real:', getRealPrice(period, previousPeriod),)
    return `${(getRealPrice(period, previousPeriod) - getExpectedPrice(period, previousPeriod, utilityType)).toFixed(2)} zł`;
}

export const getValidContractObligation = (period, previousPeriod, utilityType) => {
    const startOfPeriod = previousPeriod.measurementDate;
    const endOfPeriod = period.measurementDate;

    const validContractObligations = utilityType.contractObligationTypes[0].contractObligations
        .filter(contractObligation => areDatesOverlaping({
            startA: getContractValidityPeriod(contractObligation.contract).startDate,
            endA: getContractValidityPeriod(contractObligation.contract).endDate,
            startB: startOfPeriod,
            endB: endOfPeriod
        }))
        .sort((a, b) => getContractValidityPeriod(a.contract).startDate < getContractValidityPeriod(b.contract).startDate)
    return validContractObligations
}

export const getPrepaidPeriods = ({ period, previousPeriod, utilityType }) => {
    // console.log('getPrepaidPeriods', printMeterPeriodDate({ period }), period, previousPeriod, utilityType)
    //  get all contracts that function for given period
    const startOfPeriod = previousPeriod.measurementDate;
    const endOfPeriod = period.measurementDate;
    // console.log("getPrepaidPeriods-START", printMeterPeriodDate({ period }), utilityType.contractObligationTypes[0].contractObligations);
    const validContractObligations = getValidContractObligation(period, previousPeriod, utilityType)
    // console.log("getPrepaidPeriods period:",
    //     printMeterPeriodDate({ period: previousPeriod }),
    //     printMeterPeriodDate({ period }),
    //     " contracts:",
    //     validContractObligations.map(contractObligation =>
    //         moment(getContractValidityPeriod(contractObligation.contract).startDate).format("L") + "-" +
    //         moment(getContractValidityPeriod(contractObligation.contract).endDate).format("L"))
    //     ,
    //     'result:',
    //     validContractObligations
    // );
    // if (validContractObligations.length > 1) {
    //     console.log('expectedPriceInPeriods 0',

    //         printMeterPeriodDate({ period: previousPeriod }),
    //         printMeterPeriodDate({ period }),
    //         validContractObligations
    //             .map((contractObligation, i, self) => ({
    //                 nextOneStart: self[i + 1] && getContractValidityPeriod(self[i + 1].contract).startDate,
    //                 startDate: max(getContractValidityPeriod(contractObligation.contract).startDate, startOfPeriod),
    //                 startDate1: getContractValidityPeriod(contractObligation.contract).startDate,
    //                 startDate2: startOfPeriod,
    //                 endDate: self[i + 1]
    //                     ? min(getContractValidityPeriod(self[i + 1].contract).startDate, min(getContractValidityPeriod(contractObligation.contract).endDate, endOfPeriod))
    //                     : min(getContractValidityPeriod(contractObligation.contract).endDate, endOfPeriod),
    //                 endDate1: getContractValidityPeriod(contractObligation.contract).endDate,
    //                 endDate2: endOfPeriod,

    //                 financialObligation: !contractObligation.isUnitBased && (contractObligation?.financialObligation ?? 0),

    //             })))
    // }

    return validContractObligations
        .map((contractObligation, i, self) => {
            console.log('contractObligation', contractObligation.isUnitBased);
            return ({
                startDate: max(getContractValidityPeriod(contractObligation.contract).startDate, startOfPeriod),
                endDate: self[i + 1] // if there is another prepaid period after it thann take it
                    ? getContractValidityPeriod(self[i + 1].contract).startDate
                    : min(getContractValidityPeriod(contractObligation.contract).endDate, endOfPeriod),
                // isUnitBased: contractObligation.isUnitBased,
                paidUnits: contractObligation.paidUnits && (contractObligation.paidUnits ?? 0),
                measurementUnit: contractObligation.paidUnits && utilityType.measurementUnit,
                financialObligation: contractObligation.financialObligation && (contractObligation?.financialObligation ?? 0),
                payer: contractObligation?.contract.signer
            })
        })
        .filter(contractObligation => contractObligation.endDate > contractObligation.startDate)
}
const getContractValidityPeriod = contract => {
    const { startDate, endDate } = contract;
    // console.log('getPrepaidPeriods VALIDITY', contract)
    const termination = (contract.refersToContract?.contracts ?? contract.contracts).find(c => c.type.effect === "termination")
    // TODO check if it properly takes termination of the parent contract of annex
    if (termination) {
        const terminationDate = termination.startDate;
        return { startDate, endDate: terminationDate }
    }
    return { startDate, endDate }
}
const areDatesOverlaping = ({ startA, endA, startB, endB }) => max(startA, startB) < min(endA, endB)
const max = (a, b) => a > b ? a : b
const min = (a, b) => a < b ? a : b

const getDurationFromPreviousMeasurment = (startDate, endDate) =>
    moment.duration(moment(endDate).diff(moment(startDate)));

export const getMonthDifference = ({ startDate, endDate }) =>
    Math.round(getDurationFromPreviousMeasurment(startDate, endDate).asMonths());

const sum = array => array.reduce((acc, ele) => acc + parseFloat(ele), 0);

const getExpectedPrice = (period, previousPeriod, utilityType) => {
    const prepaidPeriods = getPrepaidPeriods({ period, previousPeriod, utilityType });

    // console.log('getPrepaidPeriods', prepaidPeriods)
    // if (prepaidPeriods.length > 1) {
    //     console.log('expectedPriceInPeriods 1', prepaidPeriods)
    // }
    const expectedPriceInPeriods = prepaidPeriods
        .map(({ paidUnits, financialObligation, startDate, endDate }) =>
            financialObligation
                ? financialObligation * getMonthDifference({ startDate, endDate })
                : paidUnits * period.pricePerUnit * getMonthDifference({ startDate, endDate })
        )

    // if (expectedPriceInPeriods.length > 1) {
    //     console.log('expectedPriceInPeriods 2', expectedPriceInPeriods)
    // }
    const expectedPrice = sum(expectedPriceInPeriods)

    return expectedPrice
}
const getRealPrice = (period, previousPeriod) =>
    period.finalPrice ?? ((period.measurementValue - previousPeriod?.measurementValue) * period.pricePerUnit);


export const printSumOfExpectedPrice = ({ periods, utilityType }) => {
    const sum = periods.reduce((acc, period, i, self) => {
        if (i === self.length - 1) {
            return acc
        }
        return acc + getExpectedPrice(period, self[i + 1], utilityType)
    }, 0)

    return `${sum.toFixed(2)} zł`
}

export const printSumOfRealPrice = ({ periods }) => {
    const sum = periods.reduce((acc, period, i, self) => {
        if (i === self.length - 1) {
            return acc
        }
        return acc + getRealPrice(period, self[i + 1])
    }, 0)

    return `${sum.toFixed(2)} zł`
}

export const getSumOfPriceDifference = ({ periods, utilityType }) => {
    const sum = periods.reduce((acc, period, i, self) => {
        if (i === self.length - 1) {
            return acc
        }
        return acc + (getRealPrice(period, self[i + 1]) - getExpectedPrice(period, self[i + 1], utilityType))
    }, 0)

    return sum
}

export const printSumOfPriceDifference = ({ periods, utilityType, paymentGroup }) => {

    const sumFromPeriods = getSumOfPriceDifference({ periods, utilityType });
    const savedSum = paymentGroup?.sumAmout;

    return paymentGroup && savedSum !== sumFromPeriods ? 'Zmienione po obliczeniu' : `${sumFromPeriods.toFixed(2)} zł`
}

export const printPaymentGroupOwner = ({ periods, utilityType }) => {
    const paymentGroupPayers = periods.reduce((acc, period, i, self) => {
        if (i === self.length - 1) {
            return acc
        }
        const prepaidPeriods = getPrepaidPeriods({ period, previousPeriod: self[i + 1], utilityType });
        const payers = prepaidPeriods.map(({ payer }) => payer)

        return [...acc, ...payers].filter(removeDuplicateById);
    }, [])

    console.log('paymentGroupPayers', paymentGroupPayers, paymentGroupPayers.map((payer) => `${payer.firstname} ${payer.surname}`).join(' & '))

    return paymentGroupPayers.map((payer) => `${payer.firstname ?? ''} ${payer.surname ?? ""}`).join(' & ');
}
