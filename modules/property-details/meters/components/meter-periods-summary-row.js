import {
    PaperClipIcon,
    BellIcon,
    UserIcon,
    XIcon,
    CheckIcon,
} from "@heroicons/react/solid";

import * as moment from "moment";
import {
    printMeterPeriodDate,
    printSumOfExpectedPrice,
    printSumOfRealPrice,
    printSumOfPriceDifference,
    printPaymentGroupOwner
} from '../domain/meter-period'

function classNames(...classes) {
    return classes.filter(Boolean).join(" ");
}

export default function MeterPeriodsSummaryRow({ periods, utilityType, paymentGroup, isPotential, closePeriodBtn }) {
    console.log('PPPPPP', periods)
    const startOfPeriod = printMeterPeriodDate({ period: periods[periods.length - 1] })
    const endOfPeriod = printMeterPeriodDate({ period: periods[0] })

    // const sumOfPredictedObligation = periods.reduce((acc, period, i, self) => {
    //     if (i === self.length - 1) {
    //         return acc
    //     }
    //     return acc + getPredictedObligation(period, self[i + 1], utilityType)
    // }, 0).toFixed(2);
    // const sumOfRealPrice = periods.reduce((acc, period, i, self) => {
    //     if (i === self.length - 1) {
    //         return acc
    //     }
    //     return acc + getRealPrice(period, self[i + 1])
    // }, 0).toFixed(2);
    // const sumOfPriceDifference = periods.reduce((acc, period, i, self) => {
    //     if (i === self.length - 1) {
    //         return acc
    //     }
    //     return acc + getPriceDifference(period, self[i + 1], utilityType)
    // }, 0).toFixed(2);
    // console.log('PPPPPP000', sumOfRealPrice, sumOfPriceDifference)
    return (
        <tr key={paymentGroup?.id ?? 'summary-payment-row'} className="text-center  bg-gray-100">
            <td className="px-2 py-2 whitespace-nowrap" colSpan='2'>
                {/* <div className="flex items-center"> */}
                {/* <div className="flex-shrink-0 h-10 w-10">
                            <img className="h-10 w-10 rounded-full" src={person.image} alt="" />
                          </div> */}
                {/* <div className="ml-4"> */}
                <div className="text-sm font-medium text-gray-900">
                    {/* {moment(period.measurementDate).format("L")} */}
                    {isPotential ? 'Potencjalny okres' : 'Zamknięty okres'} {startOfPeriod} - {endOfPeriod}
                </div>
                {/* <div className="text-xs text-gray-500">
            opłata do
            <br />
            10.06.2021
          </div> */}
                {/* </div> */}
                {/* </div> */}
            </td>
            <td className="px-2 py-2 whitespace-nowrap" >
                <div className="text-sm font-medium text-gray-900">
                    {/* {getIsContractObligationUnitBased(utilityType)
                ? getContractPaidUnitsIncluded(utilityType) + " " + utilityType.measurementUnit
                : parseFloat(getContractFinancialObligation(utilityType)).toFixed(2) + " zł"
              } */}
                </div>
                {printPaymentGroupOwner({ periods, utilityType }) && (
                    <div className="flex items-center justify-center">
                        <UserIcon className="w-4 h-4 text-gray-400" aria-hidden="true" />
                        <div className="text-sm text-gray-500 ml-1">
                            {printPaymentGroupOwner({ periods, utilityType })}
                        </div>
                    </div>
                )}
            </td>
            <td className="px-2 py-2 whitespace-nowrap" >
                {closePeriodBtn}
            </td>

            <td className={classNames("px-2 py-2 whitespace-nowrap", isPotential ? 'bg-green-300' : 'bg-green-700')}>
                <div className="text-sm font-medium text-white">
                    {printSumOfExpectedPrice({ periods, utilityType })}
                </div>
                {/* <div className="flex items-center justify-center">
                <CheckIcon className="w-4 h-4 text-white" aria-hidden="true" />
                <div className="text-xs text-white ml-1">Opłacone</div>
              </div> */}
            </td>
            <td className={classNames("px-2 py-2 whitespace-nowrap", isPotential ? 'bg-green-300' : 'bg-green-700')}>
                <div className="text-sm font-medium text-white">
                    {printSumOfRealPrice({ periods })}
                </div>
                {/* <div className="flex items-center justify-center">
                <CheckIcon className="w-4 h-4 text-white" aria-hidden="true" />
                <div className="text-xs text-white ml-1">Opłacone</div>
              </div> */}
            </td>
            <td className={classNames("px-2 py-2 whitespace-nowrap", isPotential ? 'bg-red-300' : 'bg-red-700')}>
                <div className="text-sm font-medium text-white">
                    {printSumOfPriceDifference({ periods, utilityType, paymentGroup })}
                </div>
                {/* <div className="flex items-center justify-center">
                <XIcon className="w-4 h-4 text-white" aria-hidden="true" />
                <div className="text-xs text-white ml-1">
                  Czekam na
                  <br />
                  najemce
                </div>
              </div> */}
            </td>
        </tr>
    )
}