import {
    PaperClipIcon,
    BellIcon,
    UserIcon,
    XIcon,
    CheckIcon,
} from "@heroicons/react/solid";
import {
    FireIcon,
    BeakerIcon,
    LightningBoltIcon,
} from "@heroicons/react/outline";
import {
    AdjustmentsIcon,
    CalendarIcon,
    ReceiptRefundIcon,
} from "@heroicons/react/solid";
import Link from "next/link";
import * as moment from "moment";
import { useRouter } from "next/router";
import Button from "@components/button";
import NoMeterPeriodsState from './no-meter-periods-state'
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";
import PeriodsTable from './meter-periods-table'

import { getUtilityIcon } from '../domain/utility-type'
import {
    getSumOfPriceDifference,
    printMeterPeriodDate
} from '../domain/meter-period'


const GROUP_METER_PERIODS = gql`
mutation MyMutation($paymentGroup: versionedPaymentGroupMeterPeriods_insert_input = {}) {
  insertPaymentGroupMeterPeriod(object: $paymentGroup) {
    id
  }
}
`;

function classNames(...classes) {
    return classes.filter(Boolean).join(" ");
}




export default function Example({ utilityType, property }) {
    const router = useRouter();
    const { organizationId } = router.query;
    const [groupMeterPeriods, { error, loading, data, called }] = useMutation(GROUP_METER_PERIODS);

    console.log('EEEEEE', utilityType)
    const icon = { icon: getUtilityIcon(utilityType) }
    // const firstGroupedPeriodIdx = utilityType.meterPeriods.findIndex(period => period.paymentGroup)
    // const freePeriods = utilityType.meterPeriods.filter((period, i) => firstGroupedPeriodIdx === -1 || i < firstGroupedPeriodIdx + 1)
    // console.log('PPPPPPPP1111', firstGroupedPeriodIdx, freePeriods, utilityType.meterPeriods)
    const freePeriods = (() => {
        const firstGroupedPeriodIdx = utilityType.meterPeriods.findIndex(period => period.paymentGroup)
        const freePeriods = utilityType.meterPeriods.filter((period, i) => firstGroupedPeriodIdx === -1 || i < firstGroupedPeriodIdx + 1)
        return freePeriods
    })()

    const handleGroupMeterPeriods = () => {
        const startDate = printMeterPeriodDate({ period: freePeriods[freePeriods.length - 1] })
        const endDate = printMeterPeriodDate({ period: freePeriods[0] })

        const sumOfPriceDifference = getSumOfPriceDifference({ periods: freePeriods, utilityType })

        const paymentGroup = {
            endDate,
            startDate,
            sumAmout: sumOfPriceDifference * 100,
            meterPeriods: {
                data: freePeriods
                    .filter((_, i, self) => i < self.length - 1)
                    .map(period => ({
                        id: period.id,
                        finalPrice: period.finalPrice,
                        measurementDate: period.measurementDate,
                        measurementValue: period.measurementValue,
                        pricePerUnit: period.pricePerUnit,
                        propertyId: period.property.id,
                        propertyVer: period.property.ver,
                        utilityTypeId: utilityType.id,
                        utilityTypeVer: utilityType.ver,
                    }))
            }
        }
        console.log('paymentGroupXXX', paymentGroup)
        groupMeterPeriods({ variables: { paymentGroup } })
    }

    const buttonClosePeriod = <Button
        onClick={handleGroupMeterPeriods}
        isSecondary
    >
        Zamknij okres
    </Button>
    const button = <Button
        pathname={`/${organizationId}/meter-period/new`}
        query={{
            utilityTypeId: utilityType.id,
            utilityTypeVer: utilityType.ver,
            propertyId: property.id,
            propertyVer: property.ver,
            previousMeasurementValue: utilityType.meterPeriods[0]?.measurementValue,
            removed: [
                'previousMeasurementValue'
            ],
            hidden: [
                "utilityType",
                "property",
                ...(utilityType.meterPeriods?.length ? [] : ["meterPeriodPriceIngredients/data", "finalPrice", "pricePerUnit"]),
            ],
            redirect: router.asPath,
        }}

    >
        Dodaj pomiar
    </Button>
    if (!utilityType.meterPeriods.length) {
        return <NoMeterPeriodsState title={utilityType.label} icon={icon} button={button} />
    }
    return (
        <div
            key={utilityType.id}
            className="relative bg-white pt-5 px-4  sm:pt-6 sm:px-6 shadow rounded-lg overflow-hidden" //pb-16
        >
            <div className="flex items-end mb-2">
                <div className=" bg-indigo-500 rounded-md p-3">
                    <icon.icon className="h-7 w-7 text-white" aria-hidden="true" />

                </div>
                <div className="ml-3 self-start">
                    <p className="text-sm font-medium text-gray-500">
                        {utilityType.label}
                    </p>
                    {utilityType.stat && (
                        <p
                            className={classNames(
                                utilityType.stat >= 0 ? "text-green-600" : "text-red-600",
                                "text-2xl font-semibold "
                            )}
                        >
                            {utilityType.stat} zł
                        </p>
                    )}
                    {/* <p
                  className={classNames(
                    utilityType.stan === "nadpłata" ? "text-green-600" : "text-red-600",
                    "ml-2 flex items-baseline text-sm font-semibold"
                  )}
                >
                  {utilityType.stan}
                </p> */}
                </div>

                <div className="flex gap-2 ml-auto">
                    {button}
                </div>
            </div>
            {/* <div className="ml-2">
          <p className="text-sm font-semibold">{utilityType.rentAdvance} w czynszu</p>
        </div> */}
            <PeriodsTable
                utilityType={utilityType}
                freePeriods={freePeriods}
                closePeriodBtn={freePeriods.length > 1 && buttonClosePeriod}
            />

            {/* <div className="absolute bottom-0 inset-x-0 bg-gray-50 px-4 py-4 sm:px-6">
          <div className="text-sm">
            <a
              href="#"
              className="font-medium text-indigo-600 hover:text-indigo-500"
            >
              View all<span className="sr-only"> {utilityType.label} stats</span>
            </a>
          </div>
        </div> */}
        </div>
    );
}