/* This example requires Tailwind CSS v2.0+ */
import { PlusIcon } from "@heroicons/react/solid";

export default function Example({ title, button, icon }) {
  return (
    <div className="relative block w-full border-2 border-gray-300 border-dashed rounded-lg p-12 text-center hover:border-gray-400 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
      <icon.icon className="mx-auto h-12 w-12 text-gray-400" aria-hidden="true" />
      <h3 className="mt-2 text-sm font-medium text-gray-900">
        Brak pomiarów wartości licznika - {title.toLowerCase()}
      </h3>
      <p className="mt-1 text-sm text-gray-500">
        Dodaj startowy pomiar licznika tego rodzaju
      </p>
      <div className="mt-6">
        {/* <button
          type="button"
          className="inline-flex items-center px-4 py-2 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        > */}

        {button}
        {/* </button> */}
      </div>
    </div>
  );
}
