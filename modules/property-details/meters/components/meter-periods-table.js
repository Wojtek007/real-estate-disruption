

import MeterPeriod from './meter-period-row';
import MeterPeriodsSummaryRow from './meter-periods-summary-row'

export default function PeriodsTable({ utilityType, freePeriods, closePeriodBtn }) {//freePeriods
    return (
        <div className="-mx-6 flex flex-col mb-3 mt-0">
            <div className="overflow-x-auto ">
                <div className="align-middle inline-block min-w-full ">
                    <div className="shadow overflow-hidden border-b border-gray-200 ">
                        <table className="min-w-full divide-y divide-gray-200">
                            <thead className="bg-gray-50">
                                <tr>
                                    <th
                                        scope="col"
                                        className="px-2 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                    >
                                        Data
                                    </th>
                                    <th
                                        scope="col"
                                        className="px-2 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                    >
                                        Zaliczka w czynszu
                                    </th>
                                    <th
                                        scope="col"
                                        className="px-2 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                    >
                                        Cena za jednostkę
                                    </th>
                                    <th
                                        scope="col"
                                        className="px-2 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                    >
                                        Stan licznika
                                    </th>
                                    <th
                                        scope="col"
                                        className="px-2 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                    >
                                        Przewidywany
                                    </th>
                                    <th
                                        scope="col"
                                        className="px-2 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                    >
                                        Rzeczywista
                                    </th>
                                    <th
                                        scope="col"
                                        className="px-2 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                    >
                                        Dopłata
                                    </th>
                                    {/* <th scope="col" className="relative px-2 py-3">
                      <span className="sr-only">Edit</span>
                    </th> */}
                                </tr>
                            </thead>
                            <tbody className="bg-white divide-y divide-gray-200">
                                {freePeriods.length > 1 && <MeterPeriodsSummaryRow
                                    periods={freePeriods}
                                    utilityType={utilityType}
                                    isPotential
                                    closePeriodBtn={closePeriodBtn}
                                />}
                                {utilityType.meterPeriods?.map((period, i, self) => {
                                    const findLastIndex = (array, callbackFn) => {
                                        const index = array.slice().reverse().findIndex(callbackFn);
                                        const count = array.length - 1
                                        const finalIndex = index >= 0 ? count - index : index;
                                        // console.log('periodsInPaymentGroup- last Inndx', index, count, finalIndex)

                                        return finalIndex;
                                    }
                                    const isPaymentGroup = !!period.paymentGroup
                                    const firstInPaymentGroupIdx = isPaymentGroup && self.findIndex(p => p.paymentGroup?.id === period.paymentGroup.id)
                                    const isFirstPeriodOfPaymentGroup = !!period.paymentGroup && firstInPaymentGroupIdx === i
                                    const lastInPaymentGroupIdx = isFirstPeriodOfPaymentGroup && findLastIndex(self, p => p.paymentGroup?.id === period.paymentGroup.id)
                                    const periodsInPaymentGroup = isFirstPeriodOfPaymentGroup && self.filter((_, u) => u >= firstInPaymentGroupIdx && u <= lastInPaymentGroupIdx + 1)
                                    console.log('periodsInPaymentGroup', self, firstInPaymentGroupIdx, lastInPaymentGroupIdx, periodsInPaymentGroup)
                                    return (
                                        <>
                                            {isFirstPeriodOfPaymentGroup && <MeterPeriodsSummaryRow
                                                periods={periodsInPaymentGroup}
                                                utilityType={utilityType}
                                            />}
                                            <MeterPeriod
                                                period={period}
                                                previousPeriod={self[i + 1]}
                                                utilityType={utilityType}
                                                isFirstInPeriod={i === self.length - 1}
                                            />
                                        </>
                                    )
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
}
