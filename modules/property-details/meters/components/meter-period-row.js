import {
  PaperClipIcon,
  BellIcon,
  UserIcon,
  XIcon,
  CheckIcon,
} from "@heroicons/react/solid";

import Link from "next/link";
import * as moment from "moment";
import { useRouter } from "next/router";

import {
  printMeterPeriodDate,
  printMeterPeriodPrepaidAmount,
  printMeterPeriodOwner,
  printMeterPeriodPrice,
  printMeterPeriodMeasurment,
  printMeterPeriodExpectedPrice,
  printMeterPeriodRealPrice,
  printMeterPeriodPriceDifference,
} from '../domain/meter-period'

export default function MeterPeriod({ period, utilityType, isFirstInPeriod, previousPeriod }) {
  const {
    pathname,
    query: { organizationId, propertyId },
  } = useRouter();
  const meterPeriodData = { period, utilityType, isFirstInPeriod, previousPeriod }
  console.log(
    "periodperiodpreviousPeriod",
    period.measurementValue,
    previousPeriod?.measurementValue
  );
  console.log("periodperiodutilityType", utilityType);

  // const predictedObligation = getPredictedObligation(period, previousPeriod, utilityType).toFixed(2)
  // const realPrice = getRealPrice(period, previousPeriod).toFixed(2)
  // const priceDifference = getPriceDifference(period, previousPeriod, utilityType).toFixed(2)

  return (
    <Link
      href={{
        pathname: pathname,
        query: {
          panel: "meter-period",
          propertyId,
          organizationId,
          id: period.id,
        },
      }}
      scroll={false}
    >
      <tr key={period.id} className="text-center cursor-pointer">
        <td className="px-2 py-2 whitespace-nowrap">
          {/* <div className="flex items-center"> */}
          {/* <div className="flex-shrink-0 h-10 w-10">
                            <img className="h-10 w-10 rounded-full" src={person.image} alt="" />
                          </div> */}
          {/* <div className="ml-4"> */}
          <div className="text-sm font-medium text-gray-900">
            {printMeterPeriodDate(meterPeriodData)}
          </div>
          {/* <div className="text-xs text-gray-500">
            opłata do
            <br />
            10.06.2021
          </div> */}
          {/* </div> */}
          {/* </div> */}
        </td>
        <td className="px-2 py-2 whitespace-nowrap">
          <div className="text-sm font-medium text-gray-900">
            {printMeterPeriodPrepaidAmount(meterPeriodData)}
            {/* {getIsContractObligationUnitBased(utilityType)
              ? getContractPaidUnitsIncluded(utilityType) + " " + utilityType.measurementUnit
              : parseFloat(getContractFinancialObligation(utilityType)).toFixed(2) + " zł"
            } */}
          </div>
          {printMeterPeriodOwner(meterPeriodData) && (
            <div className="flex items-center justify-center">
              <UserIcon className="w-4 h-4 text-gray-400" aria-hidden="true" />
              <div className="text-sm text-gray-500 ml-1">
                {printMeterPeriodOwner(meterPeriodData)}
              </div>
            </div>
          )}
        </td>
        <td className="px-2 py-2 whitespace-nowrap">
          {/* <div className="text-sm text-gray-900">Start: 201923</div> */}
          {period.pricePerUnit ? (
            <div className="text-sm text-gray-900">
              {printMeterPeriodPrice(meterPeriodData)}
            </div>
          ) : period.finalPrice ? (
            <div className="text-sm text-gray-500">Wyliczana</div>
          ) : (
            <div className="text-sm text-gray-500">Brak</div>
          )}
          {/* <div className="text-sm text-gray-500">Koniec: 202223</div> */}
        </td>
        <td className="px-2 py-2 whitespace-nowrap">
          {/* <div className="text-sm text-gray-900">Start: 201923</div> */}
          <div className="text-sm text-gray-500">
            {printMeterPeriodMeasurment(meterPeriodData)}
          </div>
          {/* <div className="text-sm text-gray-500">Koniec: 202223</div> */}
        </td>
        {!isFirstInPeriod && (
          <>
            <td className="px-2 py-2 whitespace-nowrap bg-green-500">
              <div className="text-sm font-medium text-white">
                {printMeterPeriodExpectedPrice(meterPeriodData)}
              </div>
              {/* <div className="flex items-center justify-center">
                <CheckIcon className="w-4 h-4 text-white" aria-hidden="true" />
                <div className="text-xs text-white ml-1">Opłacone</div>
              </div> */}
            </td>
            <td className="px-2 py-2 whitespace-nowrap bg-green-500">
              <div className="text-sm font-medium text-white">
                {printMeterPeriodRealPrice(meterPeriodData)}
              </div>
              {/* <div className="flex items-center justify-center">
                <CheckIcon className="w-4 h-4 text-white" aria-hidden="true" />
                <div className="text-xs text-white ml-1">Opłacone</div>
              </div> */}
            </td>
            <td className="px-2 py-2 whitespace-nowrap bg-red-500">
              <div className="text-sm font-medium text-white">
                {printMeterPeriodPriceDifference(meterPeriodData)}
              </div>
              {/* <div className="flex items-center justify-center">
                <XIcon className="w-4 h-4 text-white" aria-hidden="true" />
                <div className="text-xs text-white ml-1">
                  Czekam na najemce
                </div>
              </div> */}
            </td>
          </>
        )}
      </tr>
    </Link>
  );
}
