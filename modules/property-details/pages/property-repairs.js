import Link from "next/link";
import { useRouter } from "next/router";
import { useSubscription, useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";
import DetailsTemplate from "../components/template";
import RepairCard from "../components/repair-card";
import Button from "@components/button";

export const SUBSCRIBE_PROPERTY = (method) => `
  ${method} MyQuery2415345($propertyId: uuid = "") {
    properties(where: { id: { _eq: $propertyId } }) {
      name
      id
      ver
      repairs {
        name
        id
        createdAt
        notification {
          id
          ver
          reminderDate
          tasks {
            cost
            executionDate
            id
            name
            reponsiblePerson {
              id
              firstname
              surname
            }
            ver
          }
        }
        ver
      }
    }
  }
`;
const SUBSCRIBE_PROPERTY_GQL = gql`
  ${SUBSCRIBE_PROPERTY("subscription")}
`;

export default function Example({ property: staticProperty }) {
  const router = useRouter();
  const { propertyId, organizationId } = router.query;
  const { loading, error, data } = useSubscription(SUBSCRIBE_PROPERTY_GQL, {
    variables: { propertyId },
  });

  const property = data?.properties[0] ?? staticProperty;
  console.log("property", property, propertyId, data, error);

  // if (loading) {
  //   return <span>Wczytuję...</span>;
  // }
  if (error) {
    console.error(error);
    return <span>Błąd! {error?.message ?? JSON.stringify(error)}</span>;
  }
  // if (!property) {
  //   return <span>Nieruchomość o tym identyfikatorze nie istnieje</span>;
  // }

  const adRepairButton = (
    <Button
      pathname={`/${organizationId}/repair/new`}
      query={{
        propertyId: property.id,
        propertyVer: property.ver,
        hidden: ["property"],
        redirect: router.asPath,
      }}
      isSecondary={property?.repairs.length > 0}
    >
      Dodaj planowaną naprawę
    </Button>
  );

  return (
    <DetailsTemplate name={property?.name} buttons={[adRepairButton]}>
      {loading
        ? <span>Wczytuję...</span>
        : property?.repairs.map((repair) => (
          <RepairCard repair={repair} />
        ))}
    </DetailsTemplate>
  );
}
