import { useRouter } from "next/router";
import Link from "next/link";
import { useSubscription } from "@apollo/react-hooks";
import gql from "graphql-tag";
import DetailsTemplate from "../components/template";
import InformationCard from "../components/information-card";
import * as moment from "moment";
import Button from "@components/button";

export const GET_PROPERTY = (method) => `
  ${method} MyQuery087436173645734($propertyId: uuid = "") {
    properties(where: { id: { _eq: $propertyId } }) {
      portalLogins {
        id
        ver
        portalType
        webAddress
        login
        password
      }
      address {
        id
        ver
        apartmentNumber
        city
        country
        postalCode
        region
        street
        streetNumber
      }
      id
      ver
      buildingEntranceCode
      estateEntranceCode
      flatArea
      garagePlace
      landRegisterNumber
      name
      notes
      owner {
        id
        ver
        email
        firstname
        nickname
        notes
        pesel
        phone
        surname
      }
      policyNumber
     
      buildingAdministrator {
        address {
          id
          ver
          apartmentNumber
          city
          country
          postalCode
          region
          street
          streetNumber
        }
        id
        email
        name
        notes
        phone
        taxIdentificationNumber
        employees {
          role
          person {
            id
            ver
            firstname
            phone
            surname
          }
        }
      }

      contracts(
        where: {
          _and: {
            category: {key: {_eq: "rent"}}, 
            type: {effect: {_eq: "aggreament"}}, 
            endDate: {_gte: "${moment().format()}"}
            _not: {contracts: {type: {effect: {_eq: "termination"}}}}
          }
        }) {
        startDate
        endDate
        id
        ver
        signer {
            id
            ver
            email
            firstname
            nickname
            notes
            pesel
            phone
            surname
        }
        
      }
    }
  }
`;
const GET_PROPERTY_GQL = gql`
  ${GET_PROPERTY("subscription")}
`;

const apartmentInfo = [
  { label: "Adres", value: "ul. Sienna 72a/203, 00-833 Warszawa" },
  { label: "Powierzchnia", value: "80 m2" },
  { label: "Księga wieczysta", value: "WAW-234/2321/2323" },
  { label: "Numer polisy", value: "2345-6676-575" },
  { label: "Kod wejścia", value: "23K232" },
  { label: "Miejsce garażowe", value: "-3/56" },
  {
    label: "Notatki",
    value:
      "Fugiat ipsum ipsum deserunt culpa aute sint do nostrud anim incididunt cillum culpa consequat. Excepteur qui ipsum aliquip consequat sint. Sit id mollit nulla mollit nostrud in ea officia proident. Irure nostrud pariatur mollit ad adipisicing reprehenderit deserunt qui eu.",
  },
];

const administratorInfo = [
  { label: "Nazwa firmy", value: "SB Starówka", id: "company" },
  {
    label: "Adres",
    value: "ul Krogulska 2, 05-530 Górna wioska",
  },
  { label: "Email", value: "moc@gazeta.pl" },
  { label: "Telefon", value: "+48 777 888 999" },
  { label: "Hydraulik", desc: "Tomasz Zrury", value: "+48 777 888 999" },
  { label: "Nadzorca", desc: "Grzegorz Zorny", value: "+48 777 888 999" },
  {
    label: "Notatki",
    value:
      "Fugiat ipsum ipsum deserunt culpa aute sint do nostrud anim incididunt cillum culpa consequat. Excepteur qui ipsum aliquip consequat sint. Sit id mollit nulla mollit nostrud in ea officia proident. Irure nostrud pariatur mollit ad adipisicing reprehenderit deserunt qui eu.",
  },
];
const tenantInfo = [
  { label: "Imie i nazwisko", value: "Margarett Tacher", id: "person" },
  {
    label: "Adres zameldowania",
    value: "ul Broniewskiego 2, 05-530 Góra Kalwaria",
  },
  { label: "Numer konta", value: "1231 2313 4544 1231 2313 4544" },
  { label: "Email", value: "moc@gazeta.pl" },
  { label: "Telefon", value: "+48 777 888 999" },
  { label: "Pesel", value: "89120810101" },
  {
    label: "Notatki",
    value:
      "Fugiat ipsum ipsum deserunt culpa aute sint do nostrud anim incididunt cillum culpa consequat. Excepteur qui ipsum aliquip consequat sint. Sit id mollit nulla mollit nostrud in ea officia proident. Irure nostrud pariatur mollit ad adipisicing reprehenderit deserunt qui eu.",
  },
];
const ownerInfo = [
  { label: "Imie i nazwisko", value: "Józef Piłsudski", id: "person" },
  {
    label: "Adres zamieszkania",
    value: "ul Gończa 2, 05-530 Góra Kalwaria",
  },
  { label: "Numer konta", value: "1231 2313 4544 1231 2313 4544" },
  { label: "Email", value: "siła@gazeta.pl" },
  { label: "Telefon", value: "+48 777 888 999" },
  { label: "Pesel", value: "89120810101" },
  {
    label: "Notatki",
    value:
      "Fugiat ipsum ipsum deserunt culpa aute sint do nostrud anim incididunt cillum culpa consequat. Excepteur qui ipsum aliquip consequat sint. Sit id mollit nulla mollit nostrud in ea officia proident. Irure nostrud pariatur mollit ad adipisicing reprehenderit deserunt qui eu.",
  },
];
export default function Example({ property: staticProperty }) {
  const router = useRouter();
  const { propertyId, organizationId } = router.query;
  const { loading, error, data } = useSubscription(GET_PROPERTY_GQL, {
    variables: { propertyId },
  });

  const property = data?.properties[0] ?? staticProperty;
  console.log("aaaproperty", property);

  // if (loading) {
  //   return <span>Wczytuję...</span>;
  // }
  if (error) {
    console.error(error);
    return <span>Błąd! {error?.message ?? JSON.stringify(error)}</span>;
  }

  // if (!property) {
  //   return <span>Nieruchomość o tym identyfikatorze nie istnieje</span>;
  // }

  const {
    buildingEntranceCode,
    estateEntranceCode,
    flatArea,
    garagePlace,
    landRegisterNumber,
    name,
    notes,
    policyNumber,
    address,
    owner,
    contracts,
    buildingAdministrator,
    portalLogins,
  } = property ?? {};
  const tenant = contracts && contracts.length && contracts[0]?.signer;

  const renderAddress = (address) => {
    if (!address) return;
    const {
      apartmentNumber,
      city,
      country,
      postalCode,
      region,
      street,
      streetNumber,
    } = address;
    return `${street ? "ul. " + street : ""} ${streetNumber ?? ""}${apartmentNumber ? "/" + apartmentNumber : ""
      }${street ? ", " : ""}${postalCode ?? ""} ${city ?? ""} ${region ?? ""} ${country ?? ""
      }`;
  };

  const apartmentInfo = [
    {
      label: "Adres",
      value: renderAddress(address),
    },
    { label: "Powierzchnia", value: flatArea },
    { label: "Księga wieczysta", value: landRegisterNumber },
    { label: "Numer polisy", value: policyNumber },
    { label: "Kod wejścia do klatki", value: buildingEntranceCode },
    { label: "Kod wejścia na osiedle", value: estateEntranceCode },
    { label: "Miejsce garażowe", value: garagePlace },
    {
      label: "Notatki",
      value: notes,
    },
  ];
  const renderPerson = (person) => {
    if (!person) return [];

    const {
      id,
      email,
      firstname,
      nickname,
      notes,
      pesel,
      phone,
      surname,
      address,
    } = person;
    return [
      {
        label: "Imie i nazwisko",
        value: `${firstname ?? ""} ${surname ?? ""} ${nickname ? " - " + nickname : ""
          }`,
        panel: "person",
        id,
      },
      {
        label: "Adres zameldowania",
        value: renderAddress(address),
      },
      // { label: "Numer konta", value: "1231 2313 4544 1231 2313 4544" },
      { label: "Email", value: email },
      { label: "Telefon", value: phone },
      { label: "Pesel", value: pesel },
      {
        label: "Notatki",
        value: notes,
      },
    ];
  };
  const renderCompany = (company) => {
    if (!company) return [];

    const {
      id,
      email,
      name,
      notes,
      phone,
      taxIdentificationNumber,
      address,
      employees = [],
    } = company;
    return [
      { label: "Nazwa firmy", value: name, panel: "company", id },
      {
        label: "Adres",
        value: renderAddress(address),
      },
      { label: "Email", value: email },
      { label: "Telefon", value: phone },
      { label: "NIP", value: taxIdentificationNumber },
      ...employees.map((employee) => ({
        label: employee.role,
        desc: `${employee.person.firstname} ${employee.person.surname}`,
        value: employee.person.phone,
      })),
      {
        label: "Notatki",
        value: notes,
      },
    ];
  };
  const renderPortalLogins = (portalLogins) => {
    if (!portalLogins || portalLogins.length === 0) return [];

    return portalLogins.map(portalLogin => {

      const {
        id,
        ver,
        portalType,
        webAddress,
        login,
        password,
      } = portalLogin;
      return [
        { label: "", value: portalType },
        { label: `Adres portalu`, value: webAddress, copy: true },
        { label: `Login`, value: login, copy: true },
        { label: `Hasło`, value: password, copy: true },
      ];
    }).flat()
  };

  const editBtn = (label = "Edytuj") => <Button
    isSecondary
    pathname={`/${organizationId}/property/${propertyId}/edit`}
    query={{ redirect: router.asPath }}
  >
    <a href='#'>{label}</a>
  </Button>

  return (
    <DetailsTemplate name={name}>
      {loading
        ? <span>Wczytuję...</span>
        : (<>
          <InformationCard
            title="Dane mieszkania"
            data={apartmentInfo}
            button={editBtn()}
          />
          <InformationCard
            title="Dane najemcy"
            data={renderPerson(tenant)}
            button={
              tenant ? (
                <Button
                  isSecondary
                  pathname={`/${organizationId}/person/${tenant.id}/edit`}
                  query={{ redirect: router.asPath }}
                >
                  <a href='#'>Edytuj</a>
                </Button>
              ) : <Button
                isSecondary
                pathname={`/${organizationId}/properties/${propertyId}/contracts`}
              >
                <a href='#'>Dodaj umowę najmu</a>
              </Button>
            }
          />
          <InformationCard
            title="Dane administracji"
            data={renderCompany(buildingAdministrator)}
            button={
              buildingAdministrator ? (
                <Button
                  isSecondary
                  pathname={`/${organizationId}/company/${buildingAdministrator.id}/edit`}
                  query={{ redirect: router.asPath }}
                >
                  <a href='#'>Edytuj</a>
                </Button>
              ) : editBtn('Dodaj administratora')
            }
          />
          <InformationCard
            title="Dane właściciela"
            data={renderPerson(owner)}
            button={
              owner ? (
                <Button
                  isSecondary
                  pathname={`/${organizationId}/person/${owner.id}/edit`}
                  query={{ redirect: router.asPath }}
                >
                  <a href='#'>Edytuj</a>
                </Button>
              ) : editBtn('Dodaj właściciela')
            }
          />
          <InformationCard
            title="Portale do logowania"
            data={renderPortalLogins(portalLogins)}
            button={editBtn()}
          />
        </>)
      }
    </DetailsTemplate >
  );
}
