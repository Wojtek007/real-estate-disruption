import Link from "next/link";
import { useRouter } from "next/router";
import { useSubscription, useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";
import DetailsTemplate from "../components/template";
import ContractsCard from "../components/contracts-card";
import DefectCard from "../components/defect-card";
import Button from "@components/button";

/**
 * zrob state machine to work without mutations
 * show actions
 * make actions change state
 * make actions show field to put extra data
 * modify DB schema
 * save changne of status to BE
 * show naprawa panel with proper tasks
 * show nnastepna akcja on the card
 * design side panel with all information
 */

export const SUBSCRIBE_PROPERTY = (method) => `
  ${method} MyQuery8067414($propertyId: uuid = "") {
    properties(where: { id: { _eq: $propertyId } }) {
      name
    id
    ver
    defects(order_by: {createdAt: asc}) {
      name
      id
      createdAt
      isForContractor
      isOwnerAcceptanceNecessary
      notes
      fixResults {
        id 
        ver
        date
        cost
        serviceman{
          id
          ver
        }
      }
      notification {
        id
        reminderDate
        appointments {
          id
          date
        }
        tasks {
          id
          cost
          executionDate
          name
          reponsiblePerson {
            id
            firstname
            surname
          }
          decision
          isDone
          config
        }
      }
    
      urgency {
        color
        icon
        id
        key
        name
      }
      estimatedCost
    }
    }
  }
`;

const SUBSCRIBE_PROPERTY_GQL = gql`
  ${SUBSCRIBE_PROPERTY("subscription")}
`;

export default function Example({ property: staticProperty }) {
  const router = useRouter();
  const { propertyId, organizationId } = router.query;
  const { loading, error, data } = useSubscription(SUBSCRIBE_PROPERTY_GQL, {
    variables: { propertyId },
  });

  const property = data?.properties[0] ?? staticProperty;
  console.log("property - defect", property, propertyId, data, error);

  // if (loading) {
  //   return <span>Wczytuję...</span>;
  // }
  if (error) {
    console.error(error);
    return <span>Błąd! {error?.message ?? JSON.stringify(error)}</span>;
  }

  // if (!property) {
  //   return <span>Nieruchomość o tym identyfikatorze nie istnieje</span>;
  // }

  const addDefectButton = (
    <Button
      pathname={`/${organizationId}/defect/new`}
      query={{
        propertyId: property.id,
        propertyVer: property.ver,
        hidden: ["property", "source_check", "notification/data/precedingPeriod"],
        redirect: router.asPath,
      }}
      isSecondary={property?.defects?.length > 0}
    >
      Dodaj usterkę
    </Button>
  );

  return (
    <DetailsTemplate name={property?.name} buttons={[addDefectButton]}>
      {loading
        ? <span>Wczytuję...</span>
        : <ul className="grid grid-cols-1 gap-6 sm:grid-cols-1 lg:grid-cols-2">
          {property?.defects.map((defect) => (
            <DefectCard defect={defect} property={property} />
          ))}
        </ul>
      }
    </DetailsTemplate>
  );
}
