import { useRouter } from "next/router";
import Link from "next/link";
import { useSubscription, useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";
import DetailsTemplate from "../components/template";
import MeterCard from "../meters/components/utility-type-card";

/**
 * z którego kontraktu bierze zaliczke
 * jak ilosc do oplaty ze stanu licznikow obliczona
 * jak rzewidywany obliczonny na podstawie ilu miesiecy zaokralglonych
 * jak rzeczywista obliczona
 * jak doplata obliczona na podstawaie przeszlych wynikow
 *
 * czy oplacono przewidywany i czy oplacono doplate
 *
 * wygenerowanie raportu dla wlasciciela i info czy wyslany juz
 *
 *
 */

export const SUBSCRIBE_PROPERTY = (method) => `
  ${method} MyQuery123213243($propertyId: uuid = "") {
    properties(where: { id: { _eq: $propertyId } }) {
      name
      id
      ver
      contracts(
        where: {
          _and: {
            category: {key: {_eq: "rent"}}, 
            type: {effect: {_eq: "aggreament"}}, 
          }
        }) {
        startDate
        endDate
        id
        ver
        signer {
            id
            ver
            firstname           
            surname
        }
        
      }
    }
  }
`;
const SUBSCRIBE_PROPERTY_GQL = gql`
  ${SUBSCRIBE_PROPERTY("subscription")}
`;
export const SUBSCRIBE_UTILITY_TYPES = (method) => `
${method} MyQuery1342344132($propertyId: uuid) {
    utilityTypes {
      icon
      id
      key
      label
      measurementUnit
      ver
      meterPeriods(
        where: { propertyId: { _eq: $propertyId } }
        order_by: { measurementDate: desc }
      ) {
        id
        measurementDate
        measurementValue
        pricePerUnit
        finalPrice
        createdAt
        updatedAt
        property{
          id
          ver
        }
        paymentGroup{
          id
          ver
          payer{
            id
            surname
            firstname
          }
          startDate
          endDate
          sumAmout
        }
      }
      contractObligationTypes {
        label
        id
        isUnitBased
        key
        ver
        contractObligations(
          where: { contract: { subjectPropertyId: { _eq: $propertyId } } }
          order_by: { contract: { createdAt: asc } }
        ) {
          id
          financialObligation
          paidUnits
          contract {
            id
            signer {
              id
              firstname
              surname
            }
            startDate
            endDate
            type {
              name
              id
              effect
              ver
            }
            refersToContract {
              id
              startDate
              endDate
              type {
                id
                effect
              }
              contracts {
                id
                startDate
                endDate
                type {
                  id
                  effect
                }
              }
            }
            contracts {
              id
              startDate
              endDate
              type {
                id
                effect
              }
            }
          }
        }
      }
    }
  }
`;
const SUBSCRIBE_UTILITY_TYPES_GQL = gql`
  ${SUBSCRIBE_UTILITY_TYPES("subscription")}
`;
export default function Example({
  property: staticProperty,
  utilityTypes: staticUtilityTypes,
}) {
  const router = useRouter();
  const { propertyId } = router.query;

  const { loading, error, data } = useSubscription(SUBSCRIBE_PROPERTY_GQL, {
    variables: { propertyId },
  });
  const {
    loading: loadingUtility,
    error: errorUtility,
    data: dataUtility,
  } = useSubscription(SUBSCRIBE_UTILITY_TYPES_GQL, {
    variables: { propertyId },
  });

  const utilityTypes = dataUtility?.utilityTypes ?? staticUtilityTypes;

  const property = data?.properties[0] ?? staticProperty;
  console.log("utilityTypes", utilityTypes, loadingUtility, errorUtility);
  console.log("property", property, propertyId, data, error);

  // if (loading || loadingUtility) {
  //   return <span>Wczytuję...</span>;
  // }
  if (error || errorUtility) {
    console.error(error || errorUtility);
    return (
      <span>
        Błąd!{" "}
        {error?.message ??
          errorUtility?.message ??
          JSON.stringify(error + errorUtility)}
      </span>
    );
  }

  // if (!property) {
  //   return <span>Nieruchomość o tym identyfikatorze nie istnieje</span>;
  // }

  return (
    <DetailsTemplate name={property?.name}>
      {loading
        ? <span>Wczytuję...</span>
        : utilityTypes?.map((utilityType) => (
          <MeterCard utilityType={utilityType} property={property} />
        ))}
    </DetailsTemplate>
  );
}
