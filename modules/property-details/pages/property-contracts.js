import { useRouter } from "next/router";
import Link from "next/link";
import { useSubscription, useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";
import DetailsTemplate from "../components/template";
import ContractsCard from "../components/contracts-card";

export const SUBSCRIBE_PROPERTY = (method) => `
  ${method} MyQuery21122($propertyId: uuid = "") {
    properties(where: { id: { _eq: $propertyId } }) {
      name
      id
      ver
    
      rentContracts: contracts(
        where: { category: { key: { _eq: "rent" } }, type: {} }
        order_by: { startDate: desc }
      ) {
        ...contractsFields
      }
      managementContracts: contracts(
        where: { category: { key: { _eq: "management" } } }
        order_by: { startDate: desc }
      ) {
        ...contractsFields
      }
      insuranceContracts: contracts(
        where: { category: { key: { _eq: "insurance" } } }
        order_by: { startDate: desc }
      ) {
        ...contractsFields
      }
    }
  }

  fragment contractsFields on contracts {
    documentLink{
      id
      name
      extension
      hasUploadFinished
      awsLink
      createdAt
    }
    startDate
    endDate
    id
    ver
    signer {
      surname
      firstname
      id
      ver
    }
    notification {
      id
      ver
      reminderDate
      createdAt
      updatedAt
      precedingPeriod {
        id
        ver
        monthsBefore
        name
        weeksBefore
      }
    }
    type {
      name
      id
      effect
      ver
    }
    refersToContract {
      id
      type {
        id
        effect
      }
      contracts {
        id
        type {
          id
          effect
        }
      }
    }
    contracts {
      id
      type {
        id
        effect
      }
    }
  }
`;
const SUBSCRIBE_PROPERTY_GQL = gql`
  ${SUBSCRIBE_PROPERTY("subscription")}
`;

export const GET_CONTRACT_CATEGORIES = `
  query MyQuery13784980362 {
    contractCategories {
      id
      key
      label
      ver
    }
  }
`;

const GET_CONTRACT_CATEGORIES_GQL = gql`
  ${GET_CONTRACT_CATEGORIES}
`;

export default function Example({
  property: staticProperty,
  contractCategories: staticContractCategories,
}) {
  const router = useRouter();
  const { propertyId } = router.query;
  const { loading, error, data } = useSubscription(SUBSCRIBE_PROPERTY_GQL, {
    variables: { propertyId },
  });
  const {
    loading: loadingCategories,
    error: errorCategories,
    data: dataCategories,
  } = useQuery(GET_CONTRACT_CATEGORIES_GQL);

  const contractCategories =
    dataCategories?.contractCategories ?? staticContractCategories;

  const property = data?.properties[0] ?? staticProperty;
  console.log("property", property, propertyId, data, error);

  // if (loading || loadingCategories) {
  //   return <span>Wczytuję...</span>;
  // }
  if (error || errorCategories) {
    console.error(error || errorCategories);
    return (
      <span>
        Błąd!{" "}
        {error?.message ??
          errorCategories?.message ??
          JSON.stringify(error + errorCategories)}
      </span>
    );
  }

  // if (!property) {
  //   return <span>Nieruchomość o tym identyfikatorze nie istnieje</span>;
  // }

  return (
    <DetailsTemplate name={property?.name}>
      {loading
        ? <span>Wczytuję...</span>
        : contractCategories?.map((category) => (
          <ContractsCard
            title={category.label}
            contracts={property[`${category.key}Contracts`]}
            showButton
            category={category}
            property={property}
          />
        ))}
    </DetailsTemplate>
  );
}
