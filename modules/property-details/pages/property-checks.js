import Link from "next/link";
import { useRouter } from "next/router";
import { useSubscription, useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";
import DetailsTemplate from "../components/template";
import ContractsCard from "../components/contracts-card";
import CheckCard from "../components/check-card";
import Button from "@components/button";

export const SUBSCRIBE_PROPERTY = (method) => `
  ${method} MyQuery873648932($propertyId: uuid) {
    properties(where: {id: {_eq: $propertyId}}) {
      id
      ver
      name
      checks {
        id
        ver
        name
        recurrenceInterval {
          id
          ver
          name
        }
        checkResults(limit: 1, order_by: {date: desc}) {
          ...checkResultsFields
        }
        initialCheckResult {
          ...checkResultsFields
        }
      }
    }
  }
  

  fragment checkResultsFields on checkResults {
    cost
    date
    id
    nextCheckNotification {
      appointments {
        attendeeCompany {
          id
          name
        }
        attendeePerson {
          id
          firstname
          surname
        }
        date
        id
        notes
        plannedCost
      }
      id
      ver
      tasks {
        config
        cost
        decision
        id
        isDone
        name
        reponsiblePerson {
          firstname
          id
          surname
        }
      }
      precedingPeriod {
        id
        ver
        monthsBefore
        name
        weeksBefore
      }
      reminderDate
      resultingCheckResult{
        id
      }
    }
  }
  `;
// notification {
//   id
//   ver
//   reminderDate
// }
const SUBSCRIBE_PROPERTY_GQL = gql`
  ${SUBSCRIBE_PROPERTY("subscription")}
`;

export default function Example({ property: staticProperty }) {
  const router = useRouter();
  const { propertyId, organizationId } = router.query;
  const { loading, error, data } = useSubscription(SUBSCRIBE_PROPERTY_GQL, {
    variables: { propertyId },
  });

  const property = data?.properties[0] ?? staticProperty;
  console.log("property", property, propertyId, data, error);

  // if (loading) {
  //   return <span>Wczytuję...</span>;
  // }
  if (error) {
    console.error(error);
    return <span>Błąd! {error?.message ?? JSON.stringify(error)}</span>;
  }

  // if (!property) {
  //   return <span>Nieruchomość o tym identyfikatorze nie istnieje</span>;
  // }

  const addCheckButton = (
    <Button
      pathname={`/${organizationId}/check/new`}
      query={{
        propertyId: property?.id,
        propertyVer: property?.ver,
        hidden: [
          "property",
          "check",
          "initialCheckResult/data/nextCheckNotification/data/reminderDate",
          "initialCheckResult/data/parentNotification",
        ],
        redirect: router.asPath,
      }}
      isSecondary={property?.checks.length > 0}
    >
      Dodaj regularny przegląd
    </Button>
  );
  return (
    <DetailsTemplate name={property?.name} buttons={[addCheckButton]}>
      {loading
        ? <span>Wczytuję...</span>
        : property?.checks.map((check) => (
          <CheckCard check={check} property={property} />
        ))}
    </DetailsTemplate>
  );
}
