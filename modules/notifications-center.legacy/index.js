import { useState } from "react";
import { useSubscription } from "@apollo/react-hooks";
import gql from "graphql-tag";

import NotificationsFeed from "./components-smart/notifications-feed";

import PropertiesList from "./components-smart/properties-list";
import ContractEndEvent from "./notifications/contract-end";
import CheckComingEvent from "./notifications/check-coming";
import CheckDoneEvent from "./notifications/check-done";
import RepairComingEvent from "./notifications/repair-coming";

import ContractEndTasks from "./tasks/contract-end";
import CheckComingTasks from "./tasks/check-coming";
import CheckDoneTasks from "./tasks/check-done";
import RepairComingTasks from "./tasks/repair-coming";

import ContractEndWaiting from "./waiting/contract-end";
import CheckComingWaiting from "./waiting/check-coming";
import CheckDoneWaiting from "./waiting/check-done";
import RepairComingWaiting from "./waiting/repair-coming";

import { clasifyNotifictions } from "./domain/notification";

export const GET_NOTIFICATIONS = (method) => `
  ${method} MyQuery1231234 {
    notifications {
      id
      ver
      reminderDate
      createdAt
      updatedAt
      tasks {
        id
        name
        cost
        executionDate
        isBlocking
        isDone
        isClosed
        type
        createdAt
        reponsiblePerson {
          id
          surname
          firstname
        }
      }
      decisions{
        key
        name
      }
      appointments {
        attendeePerson {
          id
          surname
          firstname
        }
        attendeeCompany {
          id
          name
        }
        date
        plannedCost
        notes
        id
      }
      contracts {
        endDate
        signer { 
          id
          firstname
          surname
        }
        subjectProperty {
          id
          name
        }
        id
        startDate
      }
      defects {
        id
        isForContractor
        isOwnerAcceptanceNecessary
        name
        notes
        createdAt
        property {
          id
          name
        }
     
        urgency {
          id
          name
          key
          icon
          color
        }
        estimatedCost
      }
      precedingPeriod {
        id
        monthsBefore
        name
        weeksBefore
      }
      previousCheckResult {
        checks {
          id
          name
          property {
            id
            name
          }
          recurrenceInterval {
            id
            name
          }
        }
        cost
        date
        id
      }
      resultingCheckResult {
        cost
        date
        id
      }
    }
  }
  `;
const GET_NOTIFICATIONS_GQL = gql`
  ${GET_NOTIFICATIONS("subscription")}
`;

export default function Home({ notifications: staticNotifications = [] }) {
  const [lookup, setLookup] = useState("");
  const { loading, error, data } = useSubscription(GET_NOTIFICATIONS_GQL);
  const notifications = data?.notifications ?? staticNotifications;
  console.log("looookup mian", lookup);
  console.log("notifications", notifications);

  if (loading) {
    return <span>Wczytuję...</span>;
  }
  if (error) {
    console.error(error);
    return <span>Błąd! {error?.message ?? JSON.stringify(error)}</span>;
  }

  const { events, doing, waiting } = clasifyNotifictions(notifications, lookup);

  console.log("notifications classify ", events, doing, waiting);

  return (
    <div className="flex-1 flex items-stretch overflow-hidden">
      <main className="flex-1 overflow-y-auto border-r border-gray-300">
        {/* Primary column */}
        <section
          aria-labelledby="primary-heading"
          className="min-w-0 flex-1 h-full flex flex-col overflow-hidden lg:order-last"
        >
          <h2
            id="primary-heading"
            className="text-2xl font-extrabold text-gray-300 tracking-tight sm:text-3xl self-center mt-3"
          >
            Oś czasu
          </h2>
          {/* Your content */}
          <div className="mt-2 px-1 py-5 sm:p-2 overflow-y-auto  space-y-6">
            {events.map((notification) => {
              switch (notification.type) {
                case "CHECK-DONE":
                  return (
                    <CheckDoneEvent
                      notification={notification}
                      setLookup={setLookup}
                      isLookup={notification.id === lookup}
                    />
                  );
                case "CHECK-PLANNING":
                  return (
                    <CheckComingEvent
                      notification={notification}
                      setLookup={setLookup}
                      isLookup={notification.id === lookup}
                    />
                  );
                case "CONTRACT":
                  return (
                    <ContractEndEvent
                      notification={notification}
                      setLookup={setLookup}
                      isLookup={notification.id === lookup}
                    />
                  );
                case "DEFECT":
                  return (
                    <RepairComingEvent
                      notification={notification}
                      setLookup={setLookup}
                      isLookup={notification.id === lookup}
                    />
                  );
              }
            })}

            {/* <NotificationsFeed /> */}
          </div>
        </section>
      </main>

      {/* Secondary column (hidden on smaller screens) */}
      <section className="hidden lg:block w-1/5 overflow-y-auto opacity-80 hover:opacity-100 h-full flex flex-col">
        <h3
          id="tasks-heading"
          className="text-2xl font-extrabold text-gray-300 tracking-tight sm:text-3xl text-center self-center mt-3"
        >
          Zadania
        </h3>
        {/* Your content */}
        <div className="mt-2 px-1 py-5 sm:p-2 space-y-6">
          {doing.map((notification) => {
            switch (notification.type) {
              case "CHECK-DONE":
                return (
                  <CheckDoneTasks
                    notification={notification}
                    setLookup={setLookup}
                  />
                );
              case "CHECK-PLANNING":
                return (
                  <CheckComingTasks
                    notification={notification}
                    setLookup={setLookup}
                  />
                );
              case "CONTRACT":
                return (
                  <ContractEndTasks
                    notification={notification}
                    setLookup={setLookup}
                  />
                );
              case "DEFECT":
                return <RepairComingTasks notification={notification} />;
            }
          })}
        </div>
      </section>
      <section className="hidden lg:block w-1/5 overflow-y-auto opacity-80 hover:opacity-100 mr-20 h-full flex flex-col">
        <h3
          id="tasks-heading"
          className="text-2xl font-extrabold text-gray-300 tracking-tight sm:text-3xl text-center self-center mt-3"
        >
          Oczekiwanie
        </h3>
        {/* Your content */}
        <div className="mt-2 px-1 py-5 sm:p-2 space-y-6">
          {waiting.map((notification) => {
            console.log("waitingwaiting", notification.type);
            switch (notification.type) {
              case "CHECK-DONE":
                // return <CheckDoneTasks notification={notification} />;
                return (
                  <CheckDoneWaiting
                    notification={notification}
                    setLookup={setLookup}
                  />
                );
              case "CHECK-PLANNING":
                // return <CheckComingTasks notification={notification} />;
                return (
                  <CheckComingWaiting
                    notification={notification}
                    setLookup={setLookup}
                  />
                );
              case "CONTRACT":
                return (
                  <ContractEndWaiting
                    notification={notification}
                    setLookup={setLookup}
                  />
                );
              case "DEFECT":
                // return <RepairComingTasks notification={notification} />;
                return (
                  <RepairComingWaiting
                    notification={notification}
                    setLookup={setLookup}
                  />
                );
            }
          })}
        </div>
      </section>

      {/* Secondary column (hidden on smaller screens) */}
      <aside className="shadow absolute right-0 hidden w-80 bg-white overflow-y-auto lg:block transform translate-x-3/4 hover:translate-x-0 transition-transform">
        {/* Your content */}
        <div className="px-4 py-5 sm:p-6">
          <PropertiesList />
        </div>
      </aside>
    </div>
  );
}
