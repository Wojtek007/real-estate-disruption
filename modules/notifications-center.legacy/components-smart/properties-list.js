import { useRouter } from "next/router";
import Link from "next/link";
import { useSubscription } from "@apollo/react-hooks";
import gql from "graphql-tag";

import { DotsVerticalIcon } from "@heroicons/react/solid";
import { PlusIcon } from "@heroicons/react/solid";

const projects = [
  {
    name: "Graph API",
    initials: "GA",
    href: "#",
    members: 16,
    bgColor: "bg-pink-600",
  },
  {
    name: "Component Design",
    initials: "CD",
    href: "#",
    members: 12,
    bgColor: "bg-purple-600",
  },
  {
    name: "Templates",
    initials: "T",
    href: "#",
    members: 16,
    bgColor: "bg-yellow-500",
  },
  {
    name: "React Components",
    initials: "RC",
    href: "#",
    members: 8,
    bgColor: "bg-green-500",
  },
];
const PROPERTIES_LIST = gql`
  query MyQuery12897398123 {
    properties(order_by: { name: asc }) {
      name
      id
    }
  }
`;
function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}
const getBgColors = (i) => {
  const bgColors = [
    "bg-pink-600",
    "bg-purple-600",
    "bg-yellow-500",
    "bg-green-500",
  ];

  return bgColors[i % bgColors.length];
};

export default function Example({ user }) {
  const router = useRouter();
  const { organizationId } = router.query;
  const { loading, error, data } = useSubscription(PROPERTIES_LIST);

  const { properties } = data ?? {};

  if (loading) {
    return <span>Wczytuję...</span>;
  }
  if (error) {
    console.error(error);
    return <span>Błąd! {error?.message ?? JSON.stringify(error)}</span>;
  }

  //   const properties = data.properties.map((property) => (
  //     <div key={property.id}>{property.label}</div>
  //   ));

  return (
    <div>
      <Link
        href={{
          pathname: `/${organizationId}/property/new`,
          query: {
            redirect: `/${organizationId}/notifications`,
          },
        }}
      >
        <a>
          <button
            type="button"
            className="w-full mb-4 inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
          >
            <PlusIcon
              className="-ml-1 mr-6 h-9 w-9 text-white"
              aria-hidden="true"
            />
            Dodaj nieruchomość
          </button>
        </a>
      </Link>
      <h2 className="text-gray-500 text-xs font-medium uppercase tracking-wide text-center">
        Twoje nieruchomości
      </h2>
      <ul className="mt-3 grid grid-cols-1 gap-5 ">
        {properties.map((property, i) => (
          <li
            key={property.id}
            className="col-span-1 flex shadow-sm rounded-md"
          >
            <span
              className={classNames(
                getBgColors(i),
                "flex-shrink-0 flex items-center justify-center w-16 text-white text-sm font-medium rounded-l-md"
              )}
            >
              <label htmlFor={`select-${property.name}`} className="sr-only">
                Wybierz {property.name}
              </label>
              <input
                id={`select-${property.name}`}
                type="checkbox"
                name={`select-${property.name}`}
                className="h-4 w-4 text-indigo-600 focus:ring-indigo-500 border-gray-300 rounded"
              />
            </span>
            <label htmlFor="message-type" className="sr-only">
              Wybierz {property.name}
            </label>
            {/* <div
              className={classNames(
                getBgColors(i),
                "flex-shrink-0 flex items-center justify-center w-16 text-white text-sm font-medium rounded-l-md"
              )}
            >
              IHI
            </div> */}
            <div className="flex-1 flex items-center justify-between border-t border-r border-b border-gray-200 bg-white rounded-r-md truncate">
              <div className="flex-1 px-4 py-2 text-sm truncate">
                <Link
                  href={`/${organizationId}/properties/${property.id}/info`}
                >
                  <a className="text-gray-900 font-medium hover:text-gray-600">
                    {property.name}
                  </a>
                </Link>
                {/* <p className="text-gray-500">22 powiadomień/miesiąc</p> */}
              </div>
              {/* <div className="flex-shrink-0 pr-2">
                <button className="w-8 h-8 bg-white inline-flex items-center justify-center text-gray-400 rounded-full bg-transparent hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                  <span className="sr-only">Open options</span>
                  <DotsVerticalIcon className="w-5 h-5" aria-hidden="true" />
                </button>
              </div> */}
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
}
