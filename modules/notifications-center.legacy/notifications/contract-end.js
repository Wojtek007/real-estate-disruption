/* This example requires Tailwind CSS v2.0+ */
import { Fragment } from "react";
import {
  BriefcaseIcon,
  CalendarIcon,
  CheckIcon,
  ChevronDownIcon,
  ChevronRightIcon,
  CurrencyDollarIcon,
  UserIcon,
  LocationMarkerIcon,
  PencilIcon,
} from "@heroicons/react/solid";
import { Menu, Transition } from "@headlessui/react";
import * as moment from "moment";
import { getNotificationDate } from "../domain/notification/contract";
import Notification from "./template";

export default function Example({ notification, setLookup, isLookup }) {
  const tenant = notification.contracts[0].signer;
  console.log("WRRRRRRR", notification);
  return (
    <Notification
      breadcrumbs={[{ label: "Umowa" }, { label: "Koniec" }]}
      property={notification.contracts[0].subjectProperty}
      notifyDate={getNotificationDate(notification)}
      title="Zbliża się koniec umowy najmu"
      setLookup={setLookup}
      isLookup={isLookup}
      actions={[
        isLookup
          ? { label: "Wróć do oczekiwania" }
          : {
            label: "Wysłano zapytanie najemcy",
            // state: "WAITING_FOR_TENANT_RESPONSE",
            tasks: [
              { name: "Oczekuję na decyzję najemcy", isBlocking: true },
            ],
          },
        {
          label: "Najemca jest chętny przedłużyć",
          closeTasks: true, //closeTasks not doing anythingn yet
          tasks: [
            { name: "Przygotować dokumenty" },
            { name: "Umówić spotkanie", type: "SETUP_APPOINTMENT" },
            { name: "Powiadom właściciela" },
            { name: "Przejżyj remonty" },
          ],
        },
        {
          label: "Najemca nie chce przedłużyć",
          closeTasks: true,
          tasks: [
            { name: "Przygotować dokumenty" },
            { name: "Umówić spotkanie", type: "SETUP_APPOINTMENT" },
            { name: "Powiadom właściciela" },
            { name: "Przygotuj rozliczenie" },
            { name: "Przejżyj remonty" },
          ],
        }, //closeTasks not doing anythingn yet
        // { label: "Wyslano zapytanie właścicielowi" },
        { label: "Przełóż" },
      ]}
      notification={notification}
    >
      <div className="mt-1 flex flex-col">
        <div className="mt-2 flex items-center text-sm text-gray-500">
          <UserIcon
            className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
            aria-hidden="true"
          />
          {tenant?.firstname} {tenant?.surname}
        </div>
        <div className="mt-2 flex items-center text-sm text-gray-500">
          <CalendarIcon
            className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
            aria-hidden="true"
          />
          Koniec {moment(notification.contracts[0].endDate).format("L")}
        </div>
      </div>
    </Notification>
  );
}
