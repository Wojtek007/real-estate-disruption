/* This example requires Tailwind CSS v2.0+ */
import { ChevronDoubleLeftIcon } from "@heroicons/react/solid";
const exampleTasks = [
  { name: "Zaplanować spotkanie" },
  { name: "Przygotować dokumenty" },
  { name: "Podpisać dokumenty" },
];

/* This example requires Tailwind CSS v2.0+ */
import {
  ChevronUpIcon,
  CalendarIcon,
  QuestionMarkCircleIcon,
  FolderIcon,
  HomeIcon,
  InboxIcon,
  UsersIcon,
  ClockIcon,
} from "@heroicons/react/outline";

const navigation = [
  {
    name: "Czy przedłużyć",
    hoverName: "Decyduj",
    href: "#",
    icon: QuestionMarkCircleIcon,
    current: false,
  },
  { name: "Sienna 72a / Tomasz Szukała", current: false },
  // { name: "Sienna 72a", href: "#", icon: HomeIcon, current: false },
  // { name: "Tomasz Szukała", href: "#", icon: UsersIcon, current: false },
  // { name: "Od 23.02.2021", href: "#", icon: CalendarIcon, current: false },
  // { name: "Documents", href: "#", icon: InboxIcon, current: false },
  // { name: "Reports", href: "#", icon: ChartBarIcon, current: false },
];

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

function List() {
  return (
    <nav className="space-y-1 mt-5 mb-2 px-2 " aria-label="Sidebar">
      {navigation.map((item) => (
        <a
          key={item.name}
          href={item.href}
          className={classNames(
            item.current
              ? "bg-gray-100 text-gray-900"
              : "text-gray-600 hover:bg-gray-50 hover:text-gray-900",
            "group flex items-center px-3 py-1 text-sm font-medium rounded-md"
          )}
          aria-current={item.current ? "page" : undefined}
        >
          {item.icon && (
            <item.icon
              className={classNames(
                item.current
                  ? "text-gray-500"
                  : "text-gray-400 group-hover:text-gray-500",
                "flex-shrink-0 -ml-1 mr-3 h-6 w-6"
              )}
              aria-hidden="true"
            />
          )}
          <span className="truncate">{item.name}</span>
        </a>
      ))}
    </nav>
  );
}

export default function Example({ title, property, tasks = exampleTasks }) {
  return (
    <div
      key={1111}
      className="relative flex flex-col bg-white rounded-2xl  border-white border-2"
    >
      {/* <div className="flex-1 relative pt-4 px-2 pb-3 md:px-3"> */}
      {/* <div className="absolute left-2 top-0 p-2 inline-block bg-gray-200 rounded-xl transform -translate-y-1/2">
        <ChevronUpIcon className="h-4 w-4 text-indigo-700" aria-hidden="true" />
      </div> */}
      <div className="absolute right-2 top-0 p-2 inline-block bg-indigo-600 rounded-xl transform -translate-y-1/2">
        <a
          className={classNames(
            "text-white",
            "group flex items-center text-xs font-normal rounded-md"
          )}
        >
          <span className="truncate">Czekam 10 dni</span>
          <ClockIcon
            className={classNames(
              "text-white ",
              "flex-shrink-0 ml-3 -mr-1 h-4 w-4"
            )}
            aria-hidden="true"
          />
        </a>
      </div>
      <List />
      {/* <h3 className="text-xl font-medium text-gray-900">{title}</h3>
        <p className="mt-4 text-base text-gray-500">
          {" "}
          Ac tincidunt sapien vehicula erat auctor pellentesque rhoncus. Et
          magna sit morbi lobortis.
        </p> */}
      {/* </div> */}
      {/* <div className="py-3 px-6 flex bg-gray-100 justify-between rounded-2xl">
        <button
          type="button"
          className="rounded-md text-sm font-medium text-indigo-600 hover:text-indigo-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        >
          Tak
        </button>
        <button
          type="button"
          className=" rounded-md text-sm font-medium text-gray-700 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        >
          Nie
        </button>
      </div> */}
      {/* <div className="p-3 bg-gray-50 rounded-bl-2xl rounded-br-2xl md:px-3">
        <a className="text-sm font-medium text-indigo-700 hover:text-indigo-600">
          <span aria-hidden="true"> &larr;</span> Decyduj
        </a>
      </div> */}
      {/* <div className="absolute left-2 bottom-0 p-2 inline-block bg-gray-50 rounded-xl transform translate-y-1/2 border-indigo-700 border">
        <a className="text-sm font-medium text-indigo-700 hover:text-indigo-600">
          <span aria-hidden="true"> &larr;</span> Decyduj
        </a>
      </div> */}
    </div>
  );
  return (
    <div key={111} className="pt-6">
      <div className="flow-root bg-gray-50 rounded-lg px-6 pb-8">
        <div className="-mt-6">
          <div>
            <span className="inline-flex items-center justify-center p-3 bg-indigo-500 rounded-md shadow-lg">
              <CalendarIcon className="h-6 w-6 text-white" aria-hidden="true" />
            </span>
          </div>
          <h3 className="mt-8 text-lg font-medium text-gray-900 tracking-tight">
            {title}
          </h3>
          <p className="mt-5 text-base text-gray-500">
            Ac tincidunt sapien vehicula erat auctor pellentesque rhoncus. Et
            magna sit morbi lobortis.
          </p>
        </div>
      </div>
    </div>
  );

  return (
    <div className="bg-white shadow overflow-hidden sm:rounded-md ">
      <ul className="divide-y divide-gray-200">
        <li key={"position.id"}>
          <div className="block hover:bg-gray-50">
            <div className="px-2 py-2 sm:px-6">
              <div className="flex items-center">
                <ChevronDoubleLeftIcon
                  className="flex-shrink-0 h-5 w-5 text-gray-400 mr-2 hover:text-gray-600"
                  aria-hidden="true"
                />
                <p className="text-sm font-medium text-indigo-600">
                  {title}
                  <br />
                  {property?.name}
                </p>
                {/* <div className="ml-2 flex-shrink-0 flex">
                    <p className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                      {position.type}
                    </p>
                  </div> */}
              </div>
              <div className="mt-1">
                {tasks.map((task) => (
                  <div className="relative flex items-start">
                    <div className="flex items-center h-5">
                      <input
                        id={task.id}
                        aria-describedby="offers-description"
                        name={task.name}
                        type="checkbox"
                        className="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded"
                      />
                    </div>
                    <div className="ml-3 text-sm">
                      <label htmlFor={task.id} className="text-gray-600">
                        {task.name}
                      </label>
                      {/* <span id="offers-description" className="text-gray-500">
                          <span className="sr-only">Offers </span>when they are
                          accepted or rejected by candidates.
                        </span> */}
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  );
}
