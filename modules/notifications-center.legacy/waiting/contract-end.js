import Waiting from "./template";
import {
  ChevronUpIcon,
  CalendarIcon,
  QuestionMarkCircleIcon,
  FolderIcon,
  HomeIcon,
  InboxIcon,
  UsersIcon,
  DocumentTextIcon,
  ClockIcon,
} from "@heroicons/react/outline";

export default function Example({ notification, setLookup }) {
  return (
    <Waiting
      property={notification.contracts[0].subjectProperty}
      notification={notification}
      setLookup={setLookup}
      config={{
        categoryIcon: DocumentTextIcon,
        subjectIcon: QuestionMarkCircleIcon,
        subjectText: "Czy przedłużyć umowę",
        kindIcon: ClockIcon,
        kindText: "Czekam 10 dni",
      }}
    />
  );
}
