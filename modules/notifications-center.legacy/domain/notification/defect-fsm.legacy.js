const states = {
  WAITING_FOR_START: WAITING_FOR_START,
};

const calculateInitialState = (notification) => states.WAITING_FOR_START;
const calculateCurrentState = (notification) => {};

function createMachine(notification, stateMachineDefinition) {
  const machine = {
    value:
      calculateCurrentState(notification) ??
      calculateInitialState(notification), //stateMachineDefinition.initialState,
    transition(currentState, event) {
      const currentStateDefinition = stateMachineDefinition[currentState];
      const destinationTransition = currentStateDefinition.transitions[event];
      if (!destinationTransition) {
        return;
      }
      const destinationState = destinationTransition.target;
      const destinationStateDefinition =
        stateMachineDefinition[destinationState];
      destinationTransition.action();
      currentStateDefinition.actions.onExit();
      destinationStateDefinition.actions.onEnter();
      machine.value = destinationState;
      return machine.value;
    },
  };
  return machine;
}

export const defectMachine = (notification) =>
  createMachine(notification, {
    // initialState: "off",
    off: {
      actions: {
        onEnter() {
          console.log("off: onEnter");
        },
        onExit() {
          console.log("off: onExit");
        },
      },
      transitions: {
        switch: {
          target: "on",
          action() {
            console.log('transition action for "switch" in "off" state');
          },
        },
      },
    },
    on: {
      actions: {
        onEnter() {
          console.log("on: onEnter");
        },
        onExit() {
          console.log("on: onExit");
        },
      },
      transitions: {
        switch: {
          target: "off",
          action() {
            console.log('transition action for "switch" in "on" state');
          },
        },
      },
    },
  });
