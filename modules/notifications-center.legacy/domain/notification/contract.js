import * as moment from "moment";

export const getNotificationDate = (notification) => {
  if (notification.reminderDate) {
    return moment(notification.reminderDate).format("L");
  }

  const { monthsBefore, weeksBefore } = notification.precedingPeriod;

  if (notification.contracts.length) {
    const { endDate } = notification.contracts[0];

    return moment(endDate)
      .subtract(monthsBefore, "months")
      .subtract(weeksBefore, "weeks")
      .format("L");
  }
  if (notification.previousCheckResult.length) {
    const { date } = notification.previousCheckResult[0];

    return moment(date)
      .subtract(monthsBefore, "months")
      .subtract(weeksBefore, "weeks")
      .format("L");
  }
};
