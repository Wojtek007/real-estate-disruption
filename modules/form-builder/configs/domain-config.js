export const addressConfig = () => ({
  create: {
    formTitle: "",
    errorMessage: "Error during add address",
    gqlType: "versionedAddresses_insert_input",
    gqlMutation: "insertAddress",
    successRedirect: "/",
    abortRedirect: "/",
  },
  objectDef: { objectName: "address", objectTable: "addresses", idKey: "id" },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "text",
            label: "street_number",
            isObligatory: false,
            desc: null,
            id: "streetNumber",
          },
        ],
        [
          {
            type: "text",
            label: "street",
            isObligatory: false,
            desc: null,
            id: "street",
          },
        ],
        [
          {
            type: "text",
            label: "apartment_number",
            isObligatory: false,
            desc: null,
            id: "apartmentNumber",
          },
        ],
        [
          {
            type: "text",
            label: "postal_code",
            isObligatory: false,
            desc: null,
            id: "postalCode",
          },
        ],
        [
          {
            type: "text",
            label: "city",
            isObligatory: false,
            desc: null,
            id: "city",
          },
        ],
        [
          {
            type: "text",
            label: "country",
            isObligatory: false,
            desc: null,
            id: "country",
          },
        ],
        [
          {
            type: "text",
            label: "region",
            isObligatory: false,
            desc: null,
            id: "region",
          },
        ],
      ],
    },
  ],
});

export const bankAccountConfig = () => ({
  create: {
    formTitle: null,
    errorMessage: "Error during add bank account",
    gqlType: "versionedBankAccounts_insert_input",
    gqlMutation: "insertBankAccount",
    successRedirect: "/",
    abortRedirect: "/",
  },
  objectDef: {
    objectName: "bankAccount",
    objectTable: "bank_accounts",
    idKey: "id",
  },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "objectDropdown",
            label: "person_owner",
            isObligatory: false,
            desc: null,
            id: "personOwner",
            create: { type: null, title: null },
            objectDef: {
              singularName: "person",
              pluralName: "people",
              idKey: "id",
              descriptorData: ["firstname", "surname"],
            },
          },
        ],
        [
          {
            type: "objectDropdown",
            label: "company_owner",
            isObligatory: false,
            desc: null,
            id: "companyOwner",
            create: { type: null, title: null },
            objectDef: {
              singularName: "company",
              pluralName: "companies",
              idKey: "id",
              descriptorData: ["name"],
            },
          },
        ],
        [
          {
            type: "text",
            label: "account_number",
            isObligatory: true,
            errors: [
              {
                check: (x) => !x,
                message: "Pole account_number jest wymagane",
              },
            ],
            desc: null,
            id: "accountNumber",
          },
        ],
      ],
    },
  ],
});

export const billingPeriodConfig = () => ({
  create: {
    formTitle: null,
    errorMessage: "Error during add billing period",
    gqlType: "versionedBillingPeriods_insert_input",
    gqlMutation: "insertBillingPeriod",
    successRedirect: "/",
    abortRedirect: "/",
  },
  objectDef: {
    objectName: "billingPeriod",
    objectTable: "billing_periods",
    idKey: "id",
  },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "text",
            label: "poziom_slodkosci",
            isObligatory: false,
            desc: null,
            id: "poziomSlodkosci",
          },
        ],
      ],
    },
  ],
});

export const checkConfig = () => ({
  create: {
    formTitle: null,
    errorMessage: "Error during add check",
    gqlType: "versionedChecks_insert_input",
    gqlMutation: "insertCheck",
    successRedirect: "/",
    abortRedirect: "/",
  },
  objectDef: { objectName: "check", objectTable: "checks", idKey: "id" },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "objectDropdown",
            label: "property",
            isObligatory: true,
            errors: [
              { check: (x) => !x, message: "Pole property jest wymagane" },
            ],
            desc: null,
            id: "property",
            create: { type: null, title: null },
            objectDef: {
              singularName: "property",
              pluralName: "properties",
              idKey: "id",
              descriptorData: ["name"],
            },
          },
        ],
        [
          {
            type: "text",
            label: "name",
            isObligatory: true,
            errors: [{ check: (x) => !x, message: "Pole name jest wymagane" }],
            desc: null,
            id: "name",
          },
        ],
        [
          {
            type: "objectDropdown",
            label: "recurrence_interval",
            isObligatory: true,
            errors: [
              {
                check: (x) => !x,
                message: "Pole recurrence_interval jest wymagane",
              },
            ],
            desc: null,
            id: "recurrenceInterval",
            create: { type: "Nothing", title: null },
            objectDef: {
              singularName: "recurrenceInterval",
              pluralName: "recurrenceIntervals",
              idKey: "id",
              descriptorData: ["name"],
            },
          },
        ],
      ],
    },
    {
      header: "Notyfikacja",
      fields: [
        [
          {
            type: "timestamp with time zone",
            label: "Data przypomnienia",
            isObligatory: true,
            errors: [
              {
                check: (x) => !x,
                message: "Pole Data przypomnienia jest wymagane",
              },
            ],
            desc: null,
            id: "notification/data/reminderDate",
          },
        ],
      ],
    },
  ],
});

export const companyConfig = () => ({
  create: {
    formTitle: null,
    errorMessage: "Error during add company",
    gqlType: "versionedCompanies_insert_input",
    gqlMutation: "insertCompany",
    successRedirect: "/",
    abortRedirect: "/",
  },
  objectDef: { objectName: "company", objectTable: "companies", idKey: "id" },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "text",
            label: "notes",
            isObligatory: false,
            desc: null,
            id: "notes",
          },
        ],
        [
          {
            type: "text",
            label: "name",
            isObligatory: true,
            errors: [
              { check: (x) => !x, message: "Firma musi posiadać nazwę" },
            ],
            desc: null,
            id: "name",
          },
        ],
        [
          {
            type: "text",
            label: "email",
            isObligatory: false,
            desc: null,
            id: "email",
          },
        ],
        [
          {
            type: "text",
            label: "phone",
            isObligatory: false,
            desc: null,
            id: "phone",
          },
        ],
        [
          {
            type: "text",
            label: "tax_identification_number",
            isObligatory: false,
            desc: null,
            id: "taxIdentificationNumber",
          },
        ],
      ],
    },
    {
      header: "Pracownicy",
      type: "inline-creator",
      objectsKey: "employees/data",
      fields: [
        [
          {
            type: "text",
            label: "role",
            isObligatory: false,
            desc: null,
            id: "role",
          },
        ],
        [
          {
            type: "text",
            label: "nickname",
            isObligatory: false,
            desc: null,
            id: "person/data/nickname",
          },
          {
            type: "text",
            label: "email",
            isObligatory: false,
            desc: null,
            id: "person/data/email",
          },
          {
            type: "text",
            label: "phone",
            isObligatory: false,
            desc: null,
            id: "person/data/phone",
          },
          {
            type: "text",
            label: "notes",
            isObligatory: false,
            desc: null,
            id: "person/data/notes",
          },
          {
            type: "text",
            label: "pesel",
            isObligatory: false,
            desc: null,
            id: "person/data/pesel",
          },
          {
            type: "text",
            label: "firstname",
            isObligatory: false,
            desc: null,
            id: "person/data/firstname",
          },
          {
            type: "text",
            label: "surname",
            isObligatory: true,
            errors: [
              { check: (x) => !x, message: "Pole surname jest wymagane" },
            ],
            desc: null,
            id: "person/data/surname",
          },
        ],
      ],
    },
    {
      header: "Adres",
      fields: [
        [
          {
            type: "text",
            label: "street_number",
            isObligatory: false,
            desc: null,
            id: "address/data/streetNumber",
          },
        ],
        [
          {
            type: "text",
            label: "street",
            isObligatory: false,
            desc: null,
            id: "address/data/street",
          },
        ],
        [
          {
            type: "text",
            label: "apartment_number",
            isObligatory: false,
            desc: null,
            id: "address/data/apartmentNumber",
          },
        ],
        [
          {
            type: "text",
            label: "postal_code",
            isObligatory: false,
            desc: null,
            id: "address/data/postalCode",
          },
        ],
        [
          {
            type: "text",
            label: "city",
            isObligatory: false,
            desc: null,
            id: "address/data/city",
          },
        ],
        [
          {
            type: "text",
            label: "country",
            isObligatory: false,
            desc: null,
            id: "address/data/country",
          },
        ],
        [
          {
            type: "text",
            label: "region",
            isObligatory: false,
            desc: null,
            id: "address/data/region",
          },
        ],
      ],
    },
  ],
});

export const contractTypeConfig = () => ({
  create: {
    formTitle: null,
    errorMessage: "Error during add contract type",
    gqlType: "versionedContractTypes_insert_input",
    gqlMutation: "insertContractType",
    successRedirect: "/",
    abortRedirect: "/",
  },
  objectDef: {
    objectName: "contractType",
    objectTable: "contract_types",
    idKey: "id",
  },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "text",
            label: "name",
            isObligatory: true,
            errors: [{ check: (x) => !x, message: "Pole name jest wymagane" }],
            desc: null,
            id: "name",
          },
        ],
        [
          {
            type: "objectDropdown",
            label: "allowed_for_kind",
            isObligatory: false,
            desc: null,
            id: "allowedForKind",
            create: { type: null, title: null },
            objectDef: {
              singularName: "contractCategory",
              pluralName: "contractCategories",
              idKey: "id",
              descriptorData: ["label"],
            },
          },
        ],
        [
          {
            type: "objectDropdown",
            label: "refered_contract_type",
            isObligatory: false,
            desc: null,
            id: "referedContractType",
            create: { type: null, title: null },
            objectDef: {
              singularName: "contractType",
              pluralName: "contractTypes",
              idKey: "id",
              descriptorData: ["name"],
            },
          },
        ],
        [
          {
            type: "text",
            label: "effect",
            isObligatory: true,
            errors: [
              { check: (x) => !x, message: "Pole effect jest wymagane" },
            ],
            desc: null,
            id: "effect",
          },
        ],
      ],
    },
  ],
});

export const contractConfig = () => ({
  create: {
    formTitle: "Dodaj umowę",
    errorMessage: "Error during add contract",
    gqlType: "versionedContracts_insert_input",
    gqlMutation: "insertContract",
    successRedirect: "/",
    abortRedirect: "/",
  },
  objectDef: { objectName: "contract", objectTable: "contracts", idKey: "id" },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "objectDropdown",
            label: "",
            isObligatory: true,
            errors: [{ check: (x) => !x, message: "Pole  jest wymagane" }],
            desc: null,
            id: "subjectProperty",
            create: { type: null, title: null },
            objectDef: {
              singularName: "property",
              pluralName: "properties",
              idKey: "id",
              descriptorData: ["name"],
            },
          },
        ],
        [
          {
            type: "objectDropdown",
            label: "Sygnatariusz",
            isObligatory: true,
            errors: [
              { check: (x) => !x, message: "Sygnatariusz jest wymagany." },
            ],
            desc: null,
            id: "signer",
            create: { type: "slideOver", title: null },
            objectDef: {
              singularName: "person",
              pluralName: "people",
              idKey: "id",
              descriptorData: ["firstname", "surname"],
            },
          },
        ],
        [
          {
            type: "objectDropdown",
            label: "Typ dokumentu",
            isObligatory: false,
            desc: null,
            id: "type",
            create: { type: null, title: null },
            objectDef: {
              singularName: "contractType",
              pluralName: "contractTypes",
              idKey: "id",
              descriptorData: ["name"],
            },
          },
        ],
        [
          {
            type: "text",
            label: "document_link",
            isObligatory: false,
            desc: null,
            id: "documentLink",
          },
        ],
        [
          {
            type: "objectDropdown",
            label: "category",
            isObligatory: true,
            errors: [
              { check: (x) => !x, message: "Pole category jest wymagane" },
            ],
            desc: null,
            id: "category",
            create: { type: null, title: null },
            objectDef: {
              singularName: "contractCategory",
              pluralName: "contractCategories",
              idKey: "id",
              descriptorData: ["label"],
            },
          },
        ],
        [
          {
            type: "timestamp with time zone",
            label: "start_date",
            isObligatory: false,
            desc: null,
            id: "startDate",
          },
        ],
        [
          {
            type: "timestamp with time zone",
            label: "end_date",
            isObligatory: false,
            desc: null,
            id: "endDate",
          },
        ],
        [
          {
            type: "objectDropdown",
            label: "Nawiązuje do umowy",
            isObligatory: false,
            desc: null,
            id: "refersToContract",
            create: { type: null, title: null },
            objectDef: {
              singularName: "contract",
              pluralName: "contracts",
              idKey: "id",
              descriptorData: ["startDate", "endDate"],
            },
          },
        ],
      ],
    },
    {
      header: "Zobowiązania umowne",
      type: "inline-creator",
      objectsKey: "contractObligations/data",
      fields: [
        [
          {
            type: "integer",
            label: "paid_units",
            isObligatory: false,
            desc: null,
            id: "paidUnits",
          },
        ],
        [
          {
            type: "objectDropdown",
            label: "Typ zobowiązania",
            isObligatory: true,
            errors: [
              {
                check: (x) => !x,
                message: "Pole Typ zobowiązania jest wymagane",
              },
            ],
            desc: null,
            id: "obligationType",
            create: { type: null, title: null },
            objectDef: {
              singularName: "contractObligationType",
              pluralName: "contractObligationTypes",
              idKey: "id",
              descriptorData: ["label"],
            },
          },
        ],
        [
          {
            type: "integer",
            label: "Kwota zobowiązania",
            isObligatory: true,
            errors: [
              {
                check: (x) => !x,
                message: "Pole Kwota zobowiązania jest wymagane",
              },
            ],
            desc: null,
            id: "financialObligation",
          },
        ],
      ],
    },
    {
      header: "Notyfikacja",
      fields: [
        [
          {
            type: "timestamp with time zone",
            label: "Data przypomnienia",
            isObligatory: true,
            errors: [
              {
                check: (x) => !x,
                message: "Pole Data przypomnienia jest wymagane",
              },
            ],
            desc: null,
            id: "notification/data/reminderDate",
          },
        ],
      ],
    },
  ],
});

export const defectConfig = () => ({
  create: {
    formTitle: null,
    errorMessage: "Error during add defect",
    gqlType: "versionedDefects_insert_input",
    gqlMutation: "insertDefect",
    successRedirect: "/",
    abortRedirect: "/",
  },
  objectDef: { objectName: "defect", objectTable: "defects", idKey: "id" },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "objectDropdown",
            label: "source_check",
            isObligatory: false,
            desc: null,
            id: "sourceCheck",
            create: { type: null, title: null },
            objectDef: {
              singularName: "check",
              pluralName: "checks",
              idKey: "id",
              descriptorData: ["name"],
            },
          },
        ],
        [
          {
            type: "objectDropdown",
            label: "property",
            isObligatory: true,
            errors: [
              { check: (x) => !x, message: "Pole property jest wymagane" },
            ],
            desc: null,
            id: "property",
            create: { type: null, title: null },
            objectDef: {
              singularName: "property",
              pluralName: "properties",
              idKey: "id",
              descriptorData: ["name"],
            },
          },
        ],
        [
          {
            type: "text",
            label: "name",
            isObligatory: true,
            errors: [{ check: (x) => !x, message: "Pole name jest wymagane" }],
            desc: null,
            id: "name",
          },
        ],
        [
          {
            type: "objectDropdown",
            label: "tag",
            isObligatory: false,
            desc: null,
            id: "tag",
            create: { type: null, title: null },
            objectDef: {
              singularName: "tag",
              pluralName: "tags",
              idKey: "id",
              descriptorData: ["name"],
            },
          },
        ],
        [
          {
            type: "objectDropdown",
            label: "status",
            isObligatory: false,
            desc: null,
            id: "status",
            create: { type: null, title: null },
            objectDef: {
              singularName: "defectStatus",
              pluralName: "defectStatuses",
              idKey: "id",
              descriptorData: ["name"],
            },
          },
        ],
        [
          {
            type: "objectDropdown",
            label: "reporting_message",
            isObligatory: false,
            desc: null,
            id: "reportingMessage",
            create: { type: null, title: null },
            objectDef: {
              singularName: "message",
              pluralName: "messages",
              idKey: "id",
              descriptorData: ["name"],
            },
          },
        ],
        [
          {
            type: "textArea",
            label: "notes",
            isObligatory: false,
            desc: null,
            id: "notes",
          },
        ],
        [
          {
            type: "objectDropdown",
            label: "Zgłoszone przez",
            isObligatory: true,
            errors: [
              {
                check: (x) => !x,
                message: "Pole Zgłoszone przez jest wymagane",
              },
            ],
            desc: null,
            id: "reporter",
            create: { type: null, title: null },
            objectDef: {
              singularName: "person",
              pluralName: "people",
              idKey: "id",
              descriptorData: ["firstname", "surname"],
            },
          },
        ],
      ],
    },
  ],
});

export const employeeConfig = () => ({
  create: {
    formTitle: null,
    errorMessage: "Error during add employee",
    gqlType: "versionedEmployees_insert_input",
    gqlMutation: "insertEmployee",
    successRedirect: "/",
    abortRedirect: "/",
  },
  objectDef: { objectName: "employee", objectTable: "employees", idKey: "id" },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "text",
            label: "role",
            isObligatory: false,
            desc: null,
            id: "role",
          },
        ],
      ],
    },
    {
      header: null,
      type: "inline-creator",
      objectsKey: "companies/data",
      fields: [
        [
          {
            type: "text",
            label: "notes",
            isObligatory: false,
            desc: null,
            id: "notes",
          },
        ],
        [
          {
            type: "text",
            label: "name",
            isObligatory: true,
            errors: [
              { check: (x) => !x, message: "Firma musi posiadać nazwę" },
            ],
            desc: null,
            id: "name",
          },
        ],
        [
          {
            type: "text",
            label: "email",
            isObligatory: false,
            desc: null,
            id: "email",
          },
        ],
        [
          {
            type: "text",
            label: "phone",
            isObligatory: false,
            desc: null,
            id: "phone",
          },
        ],
        [
          {
            type: "text",
            label: "tax_identification_number",
            isObligatory: false,
            desc: null,
            id: "taxIdentificationNumber",
          },
        ],
        [
          {
            type: "text",
            label: "street_number",
            isObligatory: false,
            desc: null,
            id: "address/data/streetNumber",
          },
          {
            type: "text",
            label: "street",
            isObligatory: false,
            desc: null,
            id: "address/data/street",
          },
          {
            type: "text",
            label: "apartment_number",
            isObligatory: false,
            desc: null,
            id: "address/data/apartmentNumber",
          },
          {
            type: "text",
            label: "postal_code",
            isObligatory: false,
            desc: null,
            id: "address/data/postalCode",
          },
          {
            type: "text",
            label: "city",
            isObligatory: false,
            desc: null,
            id: "address/data/city",
          },
          {
            type: "text",
            label: "country",
            isObligatory: false,
            desc: null,
            id: "address/data/country",
          },
          {
            type: "text",
            label: "region",
            isObligatory: false,
            desc: null,
            id: "address/data/region",
          },
        ],
      ],
    },
    {
      header: null,
      fields: [
        [
          {
            type: "text",
            label: "nickname",
            isObligatory: false,
            desc: null,
            id: "person/data/nickname",
          },
        ],
        [
          {
            type: "text",
            label: "email",
            isObligatory: false,
            desc: null,
            id: "person/data/email",
          },
        ],
        [
          {
            type: "text",
            label: "phone",
            isObligatory: false,
            desc: null,
            id: "person/data/phone",
          },
        ],
        [
          {
            type: "text",
            label: "notes",
            isObligatory: false,
            desc: null,
            id: "person/data/notes",
          },
        ],
        [
          {
            type: "text",
            label: "pesel",
            isObligatory: false,
            desc: null,
            id: "person/data/pesel",
          },
        ],
        [
          {
            type: "text",
            label: "firstname",
            isObligatory: false,
            desc: null,
            id: "person/data/firstname",
          },
        ],
        [
          {
            type: "text",
            label: "surname",
            isObligatory: true,
            errors: [
              { check: (x) => !x, message: "Pole surname jest wymagane" },
            ],
            desc: null,
            id: "person/data/surname",
          },
        ],
      ],
    },
  ],
});

export const guaranteeConfig = () => ({
  create: {
    formTitle: null,
    errorMessage: "Error during add guarantee",
    gqlType: "versionedGuarantees_insert_input",
    gqlMutation: "insertGuarantee",
    successRedirect: "/",
    abortRedirect: "/",
  },
  objectDef: {
    objectName: "guarantee",
    objectTable: "guarantees",
    idKey: "id",
  },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "text",
            label: "poziom_slodkosci",
            isObligatory: false,
            desc: null,
            id: "poziomSlodkosci",
          },
        ],
      ],
    },
  ],
});

export const meterPeriodConfig = () => ({
  create: {
    formTitle: "",
    errorMessage: "Error during add meter period",
    gqlType: "versionedMeterPeriods_insert_input",
    gqlMutation: "insertMeterPeriod",
    successRedirect: "/",
    abortRedirect: "/",
  },
  objectDef: {
    objectName: "meterPeriod",
    objectTable: "meter_periods",
    idKey: "id",
  },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "objectDropdown",
            label: "utility_type",
            isObligatory: true,
            errors: [
              { check: (x) => !x, message: "Pole utility_type jest wymagane" },
            ],
            desc: null,
            id: "utilityType",
            create: { type: null, title: null },
            objectDef: {
              singularName: "utilityType",
              pluralName: "utilityTypes",
              idKey: "id",
              descriptorData: ["label"],
            },
          },
        ],
        [
          {
            type: "timestamp with time zone",
            label: "Data pomiaru",
            isObligatory: true,
            errors: [
              { check: (x) => !x, message: "Pole Data pomiaru jest wymagane" },
            ],
            desc: null,
            id: "measurementDate",
          },
        ],
        [
          {
            type: "integer",
            label: "Zmierzona wartość",
            isObligatory: true,
            errors: [
              {
                check: (x) => !x,
                message: "Pole Zmierzona wartość jest wymagane",
              },
            ],
            desc: null,
            id: "measurementValue",
          },
        ],
        [
          {
            type: "integer",
            label: "Cena rzeczywista za okres",
            isObligatory: false,
            desc: "Wartość otrzymanego rachunku za okres, jeżeli chcesz nadpisać kalkulacje.",
            id: "manualRealPriceForPeriod",
          },
        ],
        [
          {
            type: "integer",
            label: "Cena za jednostkę",
            isObligatory: false,
            desc: "Jeżeli puste to zostanie użyta cena za poprzedni okres.",
            id: "pricePerUnit",
          },
        ],
        [
          {
            type: "objectDropdown",
            label: "property",
            isObligatory: true,
            errors: [
              { check: (x) => !x, message: "Pole property jest wymagane" },
            ],
            desc: null,
            id: "property",
            create: { type: null, title: null },
            objectDef: {
              singularName: "property",
              pluralName: "properties",
              idKey: "id",
              descriptorData: ["name"],
            },
          },
        ],
      ],
    },
  ],
});

export const notificationConfig = () => ({
  create: {
    formTitle: "",
    errorMessage: "Error during add notification",
    gqlType: "versionedNotifications_insert_input",
    gqlMutation: "insertNotification",
    successRedirect: "/",
    abortRedirect: "/",
  },
  objectDef: {
    objectName: "notification",
    objectTable: "notifications",
    idKey: "id",
  },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "timestamp with time zone",
            label: "Data przypomnienia",
            isObligatory: true,
            errors: [
              {
                check: (x) => !x,
                message: "Pole Data przypomnienia jest wymagane",
              },
            ],
            desc: null,
            id: "reminderDate",
          },
        ],
      ],
    },
  ],
});

export const paymentConfig = () => ({
  create: {
    formTitle: null,
    errorMessage: "Error during add payment",
    gqlType: "versionedPayments_insert_input",
    gqlMutation: "insertPayment",
    successRedirect: "/",
    abortRedirect: "/",
  },
  objectDef: { objectName: "payment", objectTable: "payments", idKey: "id" },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "text",
            label: "poziom_slodkosci",
            isObligatory: false,
            desc: null,
            id: "poziomSlodkosci",
          },
        ],
      ],
    },
  ],
});

export const personConfig = () => ({
  create: {
    formTitle: null,
    errorMessage: "Error during add person",
    gqlType: "versionedPeople_insert_input",
    gqlMutation: "insertPerson",
    successRedirect: "/",
    abortRedirect: "/",
  },
  objectDef: { objectName: "person", objectTable: "people", idKey: "id" },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "text",
            label: "nickname",
            isObligatory: false,
            desc: null,
            id: "nickname",
          },
        ],
        [
          {
            type: "text",
            label: "email",
            isObligatory: false,
            desc: null,
            id: "email",
          },
        ],
        [
          {
            type: "text",
            label: "phone",
            isObligatory: false,
            desc: null,
            id: "phone",
          },
        ],
        [
          {
            type: "text",
            label: "notes",
            isObligatory: false,
            desc: null,
            id: "notes",
          },
        ],
        [
          {
            type: "text",
            label: "pesel",
            isObligatory: false,
            desc: null,
            id: "pesel",
          },
        ],
        [
          {
            type: "text",
            label: "firstname",
            isObligatory: false,
            desc: null,
            id: "firstname",
          },
        ],
        [
          {
            type: "text",
            label: "surname",
            isObligatory: true,
            errors: [
              { check: (x) => !x, message: "Pole surname jest wymagane" },
            ],
            desc: null,
            id: "surname",
          },
        ],
      ],
    },
  ],
});

export const propertyConfig = () => ({
  create: {
    formTitle: "Dodaj nową nieruchomość do zarządzania",
    errorMessage: "Error during add property",
    gqlType: "versionedProperties_insert_input",
    gqlMutation: "insertProperty",
    successRedirect: "/",
    abortRedirect: "/",
  },
  objectDef: { objectName: "property", objectTable: "properties", idKey: "id" },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "text",
            label: "flat_area",
            isObligatory: false,
            desc: null,
            id: "flatArea",
          },
        ],
        [
          {
            type: "text",
            label: "land_register_number",
            isObligatory: false,
            desc: null,
            id: "landRegisterNumber",
          },
        ],
        [
          {
            type: "text",
            label: "policy_number",
            isObligatory: false,
            desc: null,
            id: "policyNumber",
          },
        ],
        [
          {
            type: "objectDropdown",
            label: "Najemca nieruchomości",
            isObligatory: false,
            desc: null,
            id: "tenant",
            create: { type: "slideOver", title: "Dodaj najemce nieruchomości" },
            objectDef: {
              singularName: "person",
              pluralName: "people",
              idKey: "id",
              descriptorData: ["firstname", "surname"],
            },
          },
        ],
        [
          {
            type: "text",
            label: "building_entrance_code",
            isObligatory: false,
            desc: null,
            id: "buildingEntranceCode",
          },
        ],
        [
          {
            type: "objectDropdown",
            label: "Administrator nieruchomości",
            isObligatory: false,
            desc: null,
            id: "buildingAdministrator",
            create: {
              type: "slideOver",
              title: "Dodaj administratora budynku",
            },
            objectDef: {
              singularName: "company",
              pluralName: "companies",
              idKey: "id",
              descriptorData: ["name"],
            },
          },
        ],
        [
          {
            type: "textArea",
            label: "Notatka",
            isObligatory: false,
            desc: null,
            id: "notes",
          },
        ],
        [
          {
            type: "text",
            label: "garage_place",
            isObligatory: false,
            desc: null,
            id: "garagePlace",
          },
        ],
        [
          {
            type: "text",
            label: "Nazwa nieruchomości",
            isObligatory: true,
            errors: [
              { check: (x) => !x, message: "Nieruchomość musi posiadać nazwę" },
            ],
            desc: "Ta nazwa będzie używana tylko wewnętrznie w aplikacji. Może to być np. ulica + miasto lub nazwisko właściciela.",
            id: "name",
          },
        ],
        [
          {
            type: "objectDropdown",
            label: "Właściciel nieruchomości",
            isObligatory: false,
            desc: null,
            id: "owner",
            create: {
              type: "slideOver",
              title: "Dodaj właściciela nieruchomości",
            },
            objectDef: {
              singularName: "person",
              pluralName: "people",
              idKey: "id",
              descriptorData: ["firstname", "surname"],
            },
          },
        ],
        [
          {
            type: "text",
            label: "estate_entrance_code",
            isObligatory: false,
            desc: null,
            id: "estateEntranceCode",
          },
        ],
      ],
    },
    {
      header: "Adres",
      fields: [
        [
          {
            type: "text",
            label: "street_number",
            isObligatory: false,
            desc: null,
            id: "address/data/streetNumber",
          },
        ],
        [
          {
            type: "text",
            label: "street",
            isObligatory: false,
            desc: null,
            id: "address/data/street",
          },
        ],
        [
          {
            type: "text",
            label: "apartment_number",
            isObligatory: false,
            desc: null,
            id: "address/data/apartmentNumber",
          },
        ],
        [
          {
            type: "text",
            label: "postal_code",
            isObligatory: false,
            desc: null,
            id: "address/data/postalCode",
          },
        ],
        [
          {
            type: "text",
            label: "city",
            isObligatory: false,
            desc: null,
            id: "address/data/city",
          },
        ],
        [
          {
            type: "text",
            label: "country",
            isObligatory: false,
            desc: null,
            id: "address/data/country",
          },
        ],
        [
          {
            type: "text",
            label: "region",
            isObligatory: false,
            desc: null,
            id: "address/data/region",
          },
        ],
      ],
    },
  ],
});

export const taskConfig = () => ({
  create: {
    formTitle: null,
    errorMessage: "Error during add task",
    gqlType: "versionedTasks_insert_input",
    gqlMutation: "insertTask",
    successRedirect: "/",
    abortRedirect: "/",
  },
  objectDef: { objectName: "task", objectTable: "tasks", idKey: "id" },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "text",
            label: "name",
            isObligatory: true,
            errors: [{ check: (x) => !x, message: "Pole name jest wymagane" }],
            desc: null,
            id: "name",
          },
        ],
        [
          {
            type: "integer",
            label: "cost",
            isObligatory: false,
            desc: null,
            id: "cost",
          },
        ],
        [
          {
            type: "timestamp with time zone",
            label: "execution_date",
            isObligatory: false,
            desc: null,
            id: "executionDate",
          },
        ],
        [
          {
            type: "objectDropdown",
            label: "reponsible_person",
            isObligatory: false,
            desc: null,
            id: "reponsiblePerson",
            create: { type: null, title: null },
            objectDef: {
              singularName: "person",
              pluralName: "people",
              idKey: "id",
              descriptorData: ["firstname", "surname"],
            },
          },
        ],
        [
          {
            type: "objectDropdown",
            label: "notification",
            isObligatory: true,
            errors: [
              { check: (x) => !x, message: "Pole notification jest wymagane" },
            ],
            desc: null,
            id: "notification",
            create: { type: "Nothing", title: null },
            objectDef: {
              singularName: "notification",
              pluralName: "notifications",
              idKey: "id",
              descriptorData: ["name"],
            },
          },
        ],
      ],
    },
  ],
});

export const contractCategoryConfig = () => ({
  create: {
    formTitle: "",
    errorMessage: "Error during add contract category",
    gqlType: "versionedContractCategories_insert_input",
    gqlMutation: "insertContractCategory",
    successRedirect: "/",
    abortRedirect: "/",
  },
  objectDef: {
    objectName: "contractCategory",
    objectTable: "contract_categories",
    idKey: "id",
  },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "text",
            label: "key",
            isObligatory: true,
            errors: [{ check: (x) => !x, message: "Pole key jest wymagane" }],
            desc: null,
            id: "key",
          },
        ],
        [
          {
            type: "text",
            label: "label",
            isObligatory: true,
            errors: [{ check: (x) => !x, message: "Pole label jest wymagane" }],
            desc: null,
            id: "label",
          },
        ],
      ],
    },
  ],
});

export const recurrenceIntervalConfig = () => ({
  create: {
    formTitle: "",
    errorMessage: "Error during add recurrence interval",
    gqlType: "versionedRecurrenceIntervals_insert_input",
    gqlMutation: "insertRecurrenceInterval",
    successRedirect: "/",
    abortRedirect: "/",
  },
  objectDef: {
    objectName: "recurrenceInterval",
    objectTable: "recurrence_intervals",
    idKey: "id",
  },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "text",
            label: "name",
            isObligatory: true,
            errors: [{ check: (x) => !x, message: "Pole name jest wymagane" }],
            desc: null,
            id: "name",
          },
        ],
      ],
    },
  ],
});

export const defectStatusConfig = () => ({
  create: {
    formTitle: "",
    errorMessage: "Error during add defect status",
    gqlType: "versionedDefectStatuses_insert_input",
    gqlMutation: "insertDefectStatus",
    successRedirect: "/",
    abortRedirect: "/",
  },
  objectDef: {
    objectName: "defectStatus",
    objectTable: "defect_statuses",
    idKey: "id",
  },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "text",
            label: "name",
            isObligatory: true,
            errors: [{ check: (x) => !x, message: "Pole name jest wymagane" }],
            desc: null,
            id: "name",
          },
        ],
        [
          {
            type: "text",
            label: "color",
            isObligatory: false,
            desc: null,
            id: "color",
          },
        ],
        [
          {
            type: "text",
            label: "icon",
            isObligatory: false,
            desc: null,
            id: "icon",
          },
        ],
      ],
    },
  ],
});

export const tagConfig = () => ({
  create: {
    formTitle: "",
    errorMessage: "Error during add tag",
    gqlType: "versionedTags_insert_input",
    gqlMutation: "insertTag",
    successRedirect: "/",
    abortRedirect: "/",
  },
  objectDef: { objectName: "tag", objectTable: "tags", idKey: "id" },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "text",
            label: "color",
            isObligatory: false,
            desc: null,
            id: "color",
          },
        ],
        [
          {
            type: "text",
            label: "name",
            isObligatory: true,
            errors: [{ check: (x) => !x, message: "Pole name jest wymagane" }],
            desc: null,
            id: "name",
          },
        ],
      ],
    },
  ],
});

export const utilityTypeConfig = () => ({
  create: {
    formTitle: "",
    errorMessage: "Error during add utility type",
    gqlType: "versionedUtilityTypes_insert_input",
    gqlMutation: "insertUtilityType",
    successRedirect: "/",
    abortRedirect: "/",
  },
  objectDef: {
    objectName: "utilityType",
    objectTable: "utility_types",
    idKey: "id",
  },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "text",
            label: "key",
            isObligatory: true,
            errors: [{ check: (x) => !x, message: "Pole key jest wymagane" }],
            desc: null,
            id: "key",
          },
        ],
        [
          {
            type: "text",
            label: "icon",
            isObligatory: false,
            desc: null,
            id: "icon",
          },
        ],
        [
          {
            type: "text",
            label: "label",
            isObligatory: true,
            errors: [{ check: (x) => !x, message: "Pole label jest wymagane" }],
            desc: null,
            id: "label",
          },
        ],
        [
          {
            type: "text",
            label: "measurement_unit",
            isObligatory: false,
            desc: null,
            id: "measurementUnit",
          },
        ],
      ],
    },
  ],
});

export const contractObligationConfig = () => ({
  create: {
    formTitle: "",
    errorMessage: "Error during add contract obligation",
    gqlType: "versionedContractObligations_insert_input",
    gqlMutation: "insertContractObligation",
    successRedirect: "/",
    abortRedirect: "/",
  },
  objectDef: {
    objectName: "contractObligation",
    objectTable: "contract_obligations",
    idKey: "id",
  },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "integer",
            label: "paid_units",
            isObligatory: false,
            desc: null,
            id: "paidUnits",
          },
        ],
        [
          {
            type: "objectDropdown",
            label: "Typ zobowiązania",
            isObligatory: true,
            errors: [
              {
                check: (x) => !x,
                message: "Pole Typ zobowiązania jest wymagane",
              },
            ],
            desc: null,
            id: "obligationType",
            create: { type: null, title: null },
            objectDef: {
              singularName: "contractObligationType",
              pluralName: "contractObligationTypes",
              idKey: "id",
              descriptorData: ["label"],
            },
          },
        ],
        [
          {
            type: "integer",
            label: "Kwota zobowiązania",
            isObligatory: true,
            errors: [
              {
                check: (x) => !x,
                message: "Pole Kwota zobowiązania jest wymagane",
              },
            ],
            desc: null,
            id: "financialObligation",
          },
        ],
        [
          {
            type: "objectDropdown",
            label: "contract",
            isObligatory: true,
            errors: [
              { check: (x) => !x, message: "Pole contract jest wymagane" },
            ],
            desc: null,
            id: "contract",
            create: { type: null, title: null },
            objectDef: {
              singularName: "contract",
              pluralName: "contracts",
              idKey: "id",
              descriptorData: ["startDate", "endDate"],
            },
          },
        ],
      ],
    },
  ],
});

export const contractObligationTypeConfig = () => ({
  create: {
    formTitle: "",
    errorMessage: "Error during add contract obligation type",
    gqlType: "versionedContractObligationTypes_insert_input",
    gqlMutation: "insertContractObligationType",
    successRedirect: "/",
    abortRedirect: "/",
  },
  objectDef: {
    objectName: "contractObligationType",
    objectTable: "contract_obligation_types",
    idKey: "id",
  },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "text",
            label: "key",
            isObligatory: true,
            errors: [{ check: (x) => !x, message: "Pole key jest wymagane" }],
            desc: null,
            id: "key",
          },
        ],
        [
          {
            type: "text",
            label: "label",
            isObligatory: true,
            errors: [{ check: (x) => !x, message: "Pole label jest wymagane" }],
            desc: null,
            id: "label",
          },
        ],
        [
          {
            type: "objectDropdown",
            label: "related_utility_type",
            isObligatory: false,
            desc: null,
            id: "relatedUtilityType",
            create: { type: null, title: null },
            objectDef: {
              singularName: "utilityType",
              pluralName: "utilityTypes",
              idKey: "id",
              descriptorData: ["label"],
            },
          },
        ],
      ],
    },
  ],
});

export const repairConfig = () => ({
  create: {
    formTitle: "",
    errorMessage: "Error during add repair",
    gqlType: "versionedRepairs_insert_input",
    gqlMutation: "insertRepair",
    successRedirect: "/",
    abortRedirect: "/",
  },
  objectDef: { objectName: "repair", objectTable: "repairs", idKey: "id" },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "objectDropdown",
            label: "property",
            isObligatory: true,
            errors: [
              { check: (x) => !x, message: "Pole property jest wymagane" },
            ],
            desc: null,
            id: "property",
            create: { type: null, title: null },
            objectDef: {
              singularName: "property",
              pluralName: "properties",
              idKey: "id",
              descriptorData: ["name"],
            },
          },
        ],
        [
          {
            type: "text",
            label: "Co jest do naprawy?",
            isObligatory: true,
            errors: [
              {
                check: (x) => !x,
                message: "Pole Co jest do naprawy? jest wymagane",
              },
            ],
            desc: null,
            id: "name",
          },
        ],
      ],
    },
    {
      header: "Notyfikacja",
      fields: [
        [
          {
            type: "timestamp with time zone",
            label: "Data przypomnienia",
            isObligatory: true,
            errors: [
              {
                check: (x) => !x,
                message: "Pole Data przypomnienia jest wymagane",
              },
            ],
            desc: null,
            id: "notification/data/reminderDate",
          },
        ],
      ],
    },
  ],
});

export const messageConfig = () => ({
  create: {
    formTitle: "",
    errorMessage: "Error during add message",
    gqlType: "versionedMessages_insert_input",
    gqlMutation: "insertMessage",
    successRedirect: "/",
    abortRedirect: "/",
  },
  objectDef: { objectName: "message", objectTable: "messages", idKey: "id" },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "textArea",
            label: "content",
            isObligatory: true,
            errors: [
              { check: (x) => !x, message: "Pole content jest wymagane" },
            ],
            desc: null,
            id: "content",
          },
        ],
        [
          {
            type: "objectDropdown",
            label: "author",
            isObligatory: false,
            desc: null,
            id: "author",
            create: { type: null, title: null },
            objectDef: {
              singularName: "person",
              pluralName: "people",
              idKey: "id",
              descriptorData: ["firstname", "surname"],
            },
          },
        ],
        [
          {
            type: "text",
            label: "subject",
            isObligatory: false,
            desc: null,
            id: "subject",
          },
        ],
      ],
    },
  ],
});

export const mediaFileConfig = () => ({
  create: {
    formTitle: "",
    errorMessage: "Error during add media file",
    gqlType: "versionedMediaFiles_insert_input",
    gqlMutation: "insertMediaFile",
    successRedirect: "/",
    abortRedirect: "/",
  },
  objectDef: {
    objectName: "mediaFile",
    objectTable: "media_files",
    idKey: "id",
  },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "text",
            label: "extension",
            isObligatory: false,
            desc: null,
            id: "extension",
          },
        ],
        [
          {
            type: "text",
            label: "name",
            isObligatory: true,
            errors: [{ check: (x) => !x, message: "Pole name jest wymagane" }],
            desc: null,
            id: "name",
          },
        ],
      ],
    },
  ],
});

export const mediaAttachedToDefectConfig = () => ({
  create: {
    formTitle: "",
    errorMessage: "Error during add media attached to defect",
    gqlType: "versionedMediaAttachedToDefects_insert_input",
    gqlMutation: "insertMediaAttachedToDefect",
    successRedirect: "/",
    abortRedirect: "/",
  },
  objectDef: {
    objectName: "mediaAttachedToDefect",
    objectTable: "media_attached_to_defects",
    idKey: "id",
  },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "objectDropdown",
            label: "media_file",
            isObligatory: true,
            errors: [
              { check: (x) => !x, message: "Pole media_file jest wymagane" },
            ],
            desc: null,
            id: "mediaFile",
            create: { type: null, title: null },
            objectDef: {
              singularName: "mediaFile",
              pluralName: "mediaFiles",
              idKey: "id",
              descriptorData: ["name"],
            },
          },
        ],
        [
          {
            type: "objectDropdown",
            label: "defect",
            isObligatory: true,
            errors: [
              { check: (x) => !x, message: "Pole defect jest wymagane" },
            ],
            desc: null,
            id: "defect",
            create: { type: null, title: null },
            objectDef: {
              singularName: "defect",
              pluralName: "defects",
              idKey: "id",
              descriptorData: ["name"],
            },
          },
        ],
      ],
    },
  ],
});

export const config = {
  address: addressConfig,
  bankAccount: bankAccountConfig,
  billingPeriod: billingPeriodConfig,
  check: checkConfig,
  company: companyConfig,
  contractType: contractTypeConfig,
  contract: contractConfig,
  defect: defectConfig,
  employee: employeeConfig,
  guarantee: guaranteeConfig,
  meterPeriod: meterPeriodConfig,
  notification: notificationConfig,
  payment: paymentConfig,
  person: personConfig,
  property: propertyConfig,
  task: taskConfig,
  contractCategory: contractCategoryConfig,
  recurrenceInterval: recurrenceIntervalConfig,
  defectStatus: defectStatusConfig,
  tag: tagConfig,
  utilityType: utilityTypeConfig,
  contractObligation: contractObligationConfig,
  contractObligationType: contractObligationTypeConfig,
  repair: repairConfig,
  message: messageConfig,
  mediaFile: mediaFileConfig,
  mediaAttachedToDefect: mediaAttachedToDefectConfig,
};
