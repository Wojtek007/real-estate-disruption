const propertyForm = (user) => ({
  createTitle: "Dodaj nową nieruchomość do zarządzania",
  objectDef: {
    objectName: "property",
    objectTable: "properties",
    gqlType: "properties_insert_input",
    gqlMutation: "insertProperty",
    idKey: "id",
  },
  extraProperties: { knownBy: { data: { userId: user.sub } } },
  errors: {
    add: "Error during add property",
  },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "text",
            label: "Nazwa nieruchomości",
            obligatory: true,
            errors: [
              { check: (x) => !x, message: "Nieruchomość musi posiadać nazwę" },
            ],
            desc: "Ta nazwa będzie używana tylko wewnętrznie w aplikacji. Może to być np. ulica + miasto lub nazwisko właściciela.",
            id: "label",
          },
        ],
        [
          {
            type: "textArea",
            label: "Notatka",
            id: "notes",
          },
        ],
      ],
    },
    {
      header: "Osoby",
      fields: [
        [
          {
            type: "objectDropdown",
            label: "Właściciel nieruchomości",
            id: "owner",
            create: { type: "slideOver", formComponent: null },
            objectDef: {
              objectName: "person",
              objectTable: "people",
              idKey: "id",
              descriptorData: ["surname", "firstname"],
              descriptor: (x) => `${x.firstname} ${x.surname}`,
            },
          },
        ],
        [
          {
            type: "objectDropdown",
            label: "Najemca nieruchomości",
            id: "tenant",
            create: { type: "slideOver", formComponent: null },
            objectDef: {
              objectName: "person",
              objectTable: "people",
              idKey: "id",
              descriptorData: ["surname", "firstname"],
              descriptor: (x) => `${x.firstname} ${x.surname}`,
            },
          },
        ],
        [
          {
            type: "objectDropdown",
            label: "Administrator nieruchomości",
            id: "buildingAdministrator",
            create: { type: "slideOver", formComponent: null },
            objectDef: {
              objectName: "company",
              objectTable: "companies",
              idKey: "id",
              descriptorData: ["name"],
              descriptor: (x) => x.name,
            },
          },
        ],
      ],
    },
  ],
});
