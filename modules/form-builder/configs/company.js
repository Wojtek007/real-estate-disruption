export const companyConfig = ({ user }) => ({
  create: {
    formTitle: "Dodaj nową firmę",
    extraProperties: { knownBy: { data: { userId: user.sub } } },
    errorMessage: "Error during creation of company",
    gqlType: "companies_insert_input",
    gqlMutation: "insertCompany",
  },
  objectDef: {
    objectName: "company",
    objectTable: "companies",
    idKey: "id",
  },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "text",
            label: "Nazwa firmy",
            isObligatory: true,
            errors: [
              { check: (x) => !x, message: "Firma musi posiadać nazwę" },
            ],
            id: "name",
          },
        ],
        [
          {
            type: "textArea",
            label: "Notatka",
            id: "notes",
          },
        ],
      ],
    },
    {
      header: "Pracownicy firmy",
      type: "inline-creator",
      objectsKey: "employees/data",
      fields: [
        [
          {
            type: "text",
            label: "Imię",
            id: "person/data/firstname",
          },
          {
            type: "text",
            label: "Nazwisko",
            isObligatory: true,
            errors: [
              { check: (x) => !x, message: "Osoba musi posiadać nazwisko" },
            ],
            id: "person/data/surname",
          },
          {
            type: "text",
            label: "Telefon",
            id: "person/data/phone",
          },
          {
            type: "text",
            label: "Rola",
            id: "role",
          },
        ],
      ],
    },
  ],
});
