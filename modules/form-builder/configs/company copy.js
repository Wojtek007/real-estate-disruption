const propertyForm = (user) => ({
  createTitle: "Dodaj nową firmę",
  objectDef: {
    objectName: "company",
    objectTable: "companies",
    gqlType: "companies_insert_input",
    gqlMutation: "insertCompany",
    idKey: "id",
  },
  extraProperties: { propertyManagers: { data: { userId: user.sub } } },
  errors: {
    add: "Error during add company",
  },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "text",
            label: "Nazwa firmy",
            obligatory: true,
            errors: [
              { check: (x) => !x, message: "Firma musi posiadać nazwę" },
            ],
            id: "name",
          },
        ],
        [
          {
            type: "textArea",
            label: "Notatka",
            id: "notes",
          },
        ],
      ],
    },
    {
      header: "Osoby",
      comment:
        "tutaj jest suka hard bo czy to ma byc section type, wtedy bedzie kazdy field in this section is part of this nested object and you add in section so it adds new row",
      fields: [
        [
          {
            type: "objectDropdown",
            label: "Pracownicy firmy",
            id: "employee",
            create: { type: "inline", formComponent: null },
            objectDef: {
              objectName: "person",
              objectTable: "people",
              idKey: "id",
              descriptorData: ["surname", "firstname"],
              descriptor: (x) => `${x.firstname} ${x.surname}`,
            },
          },
        ],
      ],
    },
  ],
});
