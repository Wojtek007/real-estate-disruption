import FormBuilder from "../engine/form-builder";
import { companyConfig } from "../configs/company";

// request.post({
//   url: url,
//   headers: {'content-type' : 'application/json', 'x-hasura-admin-secret': admin_secret},
//   body: JSON.stringify({
//     query: query,
//     variables: variables
//   })
// }, function(error, response, body){
//    console.log('response:',response.body);
//    console.log('body.data:',response.body);

//   console.log('error:',error);
//     // {"data":{"users":[{"organization_id":"7e8e52aa-dab2-400e-be20-aa723db7ca76"}]}}

// });

export const propertyConfig = ({ user }) => ({
  create: {
    formTitle: "Dodaj nową nieruchomość do zarządzania",
    errorMessage: "Error during add property",
    gqlType: "versioned_properties_insert_input",
    gqlMutation: "insert_versioned_properties_one",
    successRedirect: "/",
    abortRedirect: "/",
  },
  objectDef: {
    objectName: "property",
    objectTable: "properties",
    idKey: "id",
  },
  formSections: [
    {
      header: "Podstawowe",
      fields: [
        [
          {
            type: "text",
            label: "Nazwa nieruchomości",
            isObligatory: true,
            errors: [
              { check: (x) => !x, message: "Nieruchomość musi posiadać nazwę" },
            ],
            desc: "Ta nazwa będzie używana tylko wewnętrznie w aplikacji. Może to być np. ulica + miasto lub nazwisko właściciela.",
            id: "name",
          },
        ],
        [
          {
            type: "textArea",
            label: "Notatka",
            id: "notes",
          },
        ],
      ],
    },
    {
      header: "Osoby",
      fields: [
        [
          {
            type: "objectDropdown",
            label: "Właściciel nieruchomości",
            id: "ownerId",
            create: {
              type: "slideOver",
              formComponent: null,
              title: "Dodaj właściciela nieruchomości",
            },
            objectDef: {
              objectName: "person",
              objectTable: "people",
              idKey: "id",
              descriptorData: ["surname", "firstname"],
              descriptor: (x) => `${x.firstname} ${x.surname}`,
            },
          },
        ],
        [
          {
            type: "objectDropdown",
            label: "Najemca nieruchomości",
            id: "tenantId",
            create: {
              type: "slideOver",
              formComponent: null,
              title: "Dodaj najemce nieruchomości",
            },
            objectDef: {
              objectName: "person",
              objectTable: "people",
              idKey: "id",
              descriptorData: ["surname", "firstname"],
              descriptor: (x) => `${x.firstname} ${x.surname}`,
            },
          },
        ],
        [
          {
            type: "objectDropdown",
            label: "Administrator nieruchomości",
            id: "buildingAdministratorId",
            create: {
              type: "slideOver",
              formComponent: FormBuilder,
              formConfig: companyConfig({ user }),
              title: "Dodaj administratora budynku",
            },
            objectDef: {
              objectName: "company",
              objectTable: "companies",
              idKey: "id",
              descriptorData: ["name"],
              descriptor: (x) => x.name,
            },
          },
        ],
      ],
    },
  ],
});
