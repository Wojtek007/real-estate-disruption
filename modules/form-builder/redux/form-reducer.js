export const formInitialState = (initialObject) => initialObject || {};

const actionTypes = {
  CHANGE_VALUE: "CHANGE_VALUE",
};

const resolvePath = (object, path, defaultValue) =>
  path.split("/").reduce((o, p) => (o ? o[p] : defaultValue), object);

export const setPath = (obj, incomingPath, val, delimeter = "/") => {
  const path = Array.isArray(incomingPath)
    ? incomingPath
    : incomingPath.split(delimeter);
  const idx = path[0];
  // console.log("SEEEEET START,obj, path, val", obj, path, val);

  if (path.length == 1) {
    return {
      ...obj,
      [idx]: val,
    };
  } else {
    const remainingPath = path.slice(1);

    if (Array.isArray(obj)) {
      return obj.map(
        (ele, i) => (i == idx ? setPath(ele || {}, remainingPath, val) : ele) // double == because here is string taken from path
      );
    } else {
      return {
        ...obj,
        [idx]: setPath(obj[idx] || {}, remainingPath, val),
      };
    }
  }
};

export const accessByIdSelector = (id) => (state) => resolvePath(state, id);
export const isValidSelector = (formSections) => (state) => {
  const topLevelObligatoryFields = formSections
    .filter((section) => section.type !== "inline-creator")
    .reduce((acc, section) => {
      console.log("PPPPPP", section);
      const sectionObigatoryFields = section.fields
        .flat()
        .filter((field) => field.isObligatory);
      return [...acc, ...sectionObigatoryFields];
    }, []);

  const inlineCreatorObligatoryFields = formSections
    .filter((section) => section.type === "inline-creator")
    .reduce((acc, section) => {
      const inlineObjects = accessByIdSelector(section.objectsKey)(state);

      const sectionObigatoryFields =
        inlineObjects
          ?.map((inlineObject, i) =>
            section.fields
              .flat()
              .filter((field) => field.isObligatory)
              .map((field) => ({
                ...field,
                id: `${section.objectsKey}/${i}/${field.id}`,
              }))
          )
          .flat() ?? [];

      return [...acc, ...sectionObigatoryFields];
    }, []);

  console.log(
    "Validation topLevelObligatoryFields",
    topLevelObligatoryFields,
    "inlineCreatorObligatoryFields",
    inlineCreatorObligatoryFields
  );

  const formValidation = [
    ...topLevelObligatoryFields,
    ...inlineCreatorObligatoryFields,
  ].reduce(
    (acc, field) => {
      const fieldValue =
        field.type === "objectDropdown"
          ? accessByIdSelector(`${field.id}Id`)(state)
          : accessByIdSelector(field.id)(state);

      console.log("eeeerrrrr", field);
      const activeErrors = field.errors.filter(
        (error) => typeof error.check === "function" && error.check(fieldValue)
      );

      return {
        isValid: acc.isValid && activeErrors.length === 0,
        errors: [
          ...acc.errors,
          ...activeErrors.map(({ message }) => ({ message, id: field.id })),
        ],
      };
    },
    { isValid: true, errors: [] }
  );
  console.log("Validation RESULT", formValidation);
  return formValidation;
};
export const changeValue = (key, value) => {
  console.log("changeValue : : Field renderer STATE key, value", key, value);
  return {
    type: actionTypes.CHANGE_VALUE,
    payload: { key, value },
  };
};

export const formReducer = (state, action) => {
  switch (action.type) {
    case actionTypes.CHANGE_VALUE:
      const { key, value } = action.payload;

      return setPath(state, key, value);
    default:
      throw new Error();
  }
};
