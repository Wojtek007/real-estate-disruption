import { useReducer } from "react";
import { useMutation } from "@apollo/react-hooks";
import { useRouter } from "next/router";

import gql from "graphql-tag";
import { PlusIcon as PlusIconSolid } from "@heroicons/react/solid";

import FormPageTemplate from "../components/form-page-template.js";
import FormSection from "../components/form-section.js";
import InputTextArea from "../inputs/text-area.js";
import InputFileDrop from "../inputs/file-drop.js";
import InputText from "../inputs/text.js";
import InputDropdown from "../inputs/dropdown.js";
import FieldRenderer from "./field-renderer";

import {
  formReducer,
  formInitialState,
  changeValue,
  accessByIdSelector,
  isValidSelector,
} from "../redux/form-reducer.js";

export default function Example({
  config,
  columns = 4,
  mode = "create",
  initialObject,
  onSuccess,
  onCancel,
}) {
  const router = useRouter();
  console.log("router.query", router.query);
  const { hidden, ...initialState } = router.query;
  const [state, dispatch] = useReducer(
    formReducer,
    formInitialState(
      columns === 4 ? (initialObject ? initialObject : initialState) : {}
    ) // TODO FIX that it is not based if this is slide over or not. It will filter initial state to have only keys that are present in the form.
  );
  console.log("Field renderer STATE", state);

  console.log("CONFIG", config);
  const { idKey, singularName, pluralName } = config.objectDef;
  const {
    formTitle,
    extraProperties,
    errorMessage,
    gqlMutation,
    gqlType,
    abortRedirect,
    successRedirect,
  } = config[mode];

  const ADD_OBJECT = gql`
  mutation MyMutation($${singularName}: ${gqlType}!) {
    ${gqlMutation}(object: $${singularName}) {
      id
      ver
    }
  }
`;

  const [addObject, { error, loading, data, called }] = useMutation(ADD_OBJECT);

  const delay = (ms) => new Promise((res) => setTimeout(res, ms));

  const onSubmit = async (e) => {
    e.preventDefault();
    try {
      const cleanState = Object.entries(state).reduce(
        (newState, [key, value]) => ({
          ...newState,
          [key]: !value ? null : value,
        }),
        {}
      );
      const newObject = {
        ...cleanState,
        ...extraProperties,
      };
      const newObjectData = await addObject({
        variables: { [singularName]: newObject },
      });
      console.log("newObjectData", newObjectData.data[gqlMutation][idKey]);
      await delay(1000);
      if (onSuccess) {
        onSuccess(
          newObjectData.data[gqlMutation].id,
          newObjectData.data[gqlMutation].ver
        );
      } else if (successRedirect) {
        router.push(successRedirect); // TODO here probably I should go to details of created property
      }
    } catch (error) {
      console.error(errorMessage, error);
    }
  };
  const onAbort = (e) => {
    e.preventDefault();
    if (onCancel) {
      onCancel();
    } else if (abortRedirect) {
      router.push(abortRedirect);
    }
  };

  console.log(
    "mutation error,loading, data,called",
    error,
    loading,
    data,
    called
  );

  const { isValid, errors } = isValidSelector(config.formSections)(state);
  console.log("Validation result: isValid, errors", isValid, errors);

  const renderFields = (section, i) =>
    section.fields.map((fieldsRow) => {
      const fieldId = (id) =>
        section.objectsKey ? `${section.objectsKey}/${i}/${id}` : id;
      const onChange = (id, value) => {
        console.log(
          "onChangeBuilder : : Field renderer STATE key, value",
          id,
          value
        );
        return dispatch(changeValue(fieldId(id), value));
      };

      const passedState = section.objectsKey
        ? accessByIdSelector(section.objectsKey)(state)
        : state;

      const getError = (field) =>
        errors.find((error) => error.id === fieldId(field.id));

      // if (fieldsRow.length === 1) {
      //   return (
      //     <FieldRenderer
      //       config={fieldsRow[0]}
      //       onChange={onChange}
      //       state={passedState}
      //       error={getError(fieldsRow[0])}
      //     />
      //   );
      // }

      const rowColumns =
        columns < fieldsRow.length ? columns : fieldsRow.length;

      const validFieldsInRow = fieldsRow.filter(
        ({ id }) => !hidden?.includes(id)
      );
      console.log(
        "fieldsRowa,validFieldsInRow.length",
        fieldsRow,
        validFieldsInRow.length
      );
      return (
        validFieldsInRow.length > 0 && (
          <div className={`grid grid-cols-${rowColumns} gap-6`}>
            {validFieldsInRow.map((field) => (
              <div className={`col-span-${rowColumns} lg:col-span-1`}>
                <FieldRenderer
                  config={field}
                  onChange={onChange}
                  state={passedState}
                  error={getError(field)}
                />
              </div>
            ))}
          </div>
        )
      );
    });

  const onAddNestedObject = (section) => {
    const currentArrayOfNestedObjects = accessByIdSelector(section.objectsKey)(
      state
    );
    // if there is nothing than set array with one object
    // otherwise add object to array
    if (currentArrayOfNestedObjects) {
      dispatch(
        changeValue(section.objectsKey, [...currentArrayOfNestedObjects, {}])
      );
    } else {
      dispatch(changeValue(section.objectsKey, [{}]));
    }
  };
  const onRemoveNestedObject = (section) => {
    const currentArrayOfNestedObjects = accessByIdSelector(section.objectsKey)(
      state
    );
    dispatch(
      changeValue(
        section.objectsKey,
        currentArrayOfNestedObjects.filter(
          (_, i) => i !== currentArrayOfNestedObjects.length - 1
        )
      )
    );
  };

  // to have in case of inline creator per box.... maybe something with callinng here multiple Form ssectiosn without title
  return (
    <FormPageTemplate
      title={formTitle}
      onAbort={onAbort}
      onSubmit={onSubmit}
      isSubmitDisabled={!isValid}
      isSubmitLoading={loading}
      isSubmitDone={called && !loading && !error}
      error={error?.message}
      submitLabel={initialObject ? "Zapisz zmiany" : "Stwórz"}
      submitPending={initialObject ? "Zmiany zapisane!" : "Stworzono!"}
    >
      {config.formSections.map((section) => (
        <>
          <FormSection
            title={section.header}
            desc=""
            isNarrow={columns < 3}
            onAddClick={
              section.type === "inline-creator"
                ? () => onAddNestedObject(section)
                : false
            }
            onMinusClick={
              section.type === "inline-creator" &&
              accessByIdSelector(section.objectsKey)(state)?.length
                ? () => onRemoveNestedObject(section)
                : false
            }
          >
            {!section.objectsKey && renderFields(section)}
          </FormSection>

          {section.objectsKey &&
            accessByIdSelector(section.objectsKey)(state)?.map((_, i) => (
              <FormSection title="" desc="" isNarrow={columns < 3}>
                {renderFields(section, i)}{" "}
              </FormSection>
            ))}
        </>
      ))}
    </FormPageTemplate>
  );
}
