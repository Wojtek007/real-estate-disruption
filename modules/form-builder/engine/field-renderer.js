import { useReducer } from "react";
import { useMutation } from "@apollo/react-hooks";
import { useRouter } from "next/router";

import gql from "graphql-tag";

import FormPageTemplate from "../components/form-page-template";
import FormSection from "../components/form-section";
import InputTextArea from "../inputs/text-area";
import InputFileDrop from "../inputs/file-drop";
import InputText from "../inputs/text";
import InputDropdown from "../inputs/dropdown";
import FieldRenderer from "./field-renderer";
import DatePicker from "../inputs/date-picker";

import {
  formReducer,
  formInitialState,
  changeValue,
  accessByIdSelector,
} from "../redux/form-reducer.js";

export default function Example({ config, onChange, state, error }) {
  console.log("CONFIG- FIELD", config);

  const { type, label, isObligatory, id, desc, objectDef, create } = config;

  switch (type) {
    case "text":
    case "integer":
      return (
        <InputText
          label={`${label}${isObligatory ? "*" : ""}`}
          key={id}
          onChange={(value) => onChange(id, value)}
          value={accessByIdSelector(id)(state)}
          desc={desc}
          error={error}
        />
      );
    case "textArea":
      return (
        <InputTextArea
          label={`${label}${isObligatory ? "*" : ""}`}
          key={id}
          onChange={(value) => onChange(id, value)}
          value={accessByIdSelector(id)(state)}
          desc={desc}
          error={error}
        />
      );
    case "objectDropdown":
      // when there is many2many the value and onChange have one more key nested
      return (
        <InputDropdown
          label={`${label}${isObligatory ? "*" : ""}`}
          objectId={id}
          key={id}
          onChange={onChange} // inside it is changing id and ver
          value={accessByIdSelector(`${id}Id`)(state)}
          desc={desc}
          objectDef={objectDef}
          create={create}
          error={error}
        />
      );
    case "timestamp with time zone":
      return (
        <DatePicker
          key={id}
          label={`${label}${isObligatory ? "*" : ""}`}
          onChange={(value) => onChange(id, value)}
          value={accessByIdSelector(id)(state)}
          dateFormat="dd-MM-yyyy"
          isObligatory={isObligatory}
          errors={[error]}
        />
      );

    default:
      return <div>HHHHHH nie ma renderera {type}</div>;
  }
}
