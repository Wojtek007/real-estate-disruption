import DatePicker, { registerLocale } from "react-datepicker";
import pl from "date-fns/locale/pl"; // the locale you want
registerLocale("pl", pl); // register it with the name you want
// import "./data-picker.css";
import { ExclamationCircleIcon } from "@heroicons/react/solid";

export default function Example({ label, key, onChange, value, desc, error }) {
  return (
    <div>
      <label htmlFor={key} className="block text-sm font-medium text-gray-700">
        {label}
      </label>
      <div className="mt-1 relative rounded-md">
        <DatePicker
          id={key}
          utcOffset={0}
          showTimeSelect={false}
          timeFormat="HH:mm"
          locale="pl"
          customInput={<Input error={error} />}
          selected={value}
          dateFormat={"dd-MM-yyyy"}
          onChange={onChange}
        />
        {/* <input
          type="text"
          name={key}
          id={key}
          autoComplete={autoComplete}
          onChange={(e) => onChange(e.target.value)}
          value={value}
          className={error ? errorStyle : regularStyle}
          aria-invalid={!!error}
          aria-describedby={`${key}-error`}
        /> */}
        {error && (
          <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
            <ExclamationCircleIcon
              className="h-5 w-5 text-red-500"
              aria-hidden="true"
            />
          </div>
        )}
      </div>
      {desc && <p className="mt-2 text-sm text-gray-500">{desc}</p>}
      {error && (
        <p className="mt-2 text-sm text-red-600" id={`${key}-error`}>
          {error.message}
        </p>
      )}
    </div>
  );
}

const Input = ({
  onChange,
  placeholder,
  value,
  isSecure,
  id,
  onClick,
  error,
}) => {
  const regularStyle =
    "shadow-sm mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md";
  const errorStyle =
    "shadow-sm block w-full pr-10 border-red-300 text-red-900 placeholder-red-300 focus:outline-none focus:ring-red-500 focus:border-red-500 sm:text-sm rounded-md";

  return (
    <input
      type="text"
      name={id}
      id={id}
      onChange={onChange}
      onClick={onClick}
      isSecure={isSecure}
      value={value}
      className={error ? errorStyle : regularStyle}
      aria-invalid={!!error}
      aria-describedby={`${id}-error`}
      placeholder={placeholder}
    />
    //   <input
    //     onChange={onChange}
    //     placeholder={placeholder}
    //     value={value}
    //     isSecure={isSecure}
    //     id={id}
    //     onClick={onClick}
    //     className={`form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5 ${
    //       errors.length > 0 &&
    //       "border-red-300 text-red-900 placeholder-red-300 focus:border-red-300 focus:shadow-outline-red"
    //     }`}
    //   />
  );
};

const DateField = ({
  key,
  label,
  value,
  onChange,
  extraOptions = { disabled: false },
  showTimeSelect = false,
  dateFormat = "dd-MM-yyyy" /*"dd-MM-yyyy HH:mm"*/,
  extraClass,
  errors = [],
  isObligatory,
}) => {
  console.log("STAT DateField value", value);
  const inputComponent = (
    <div className="mt-1 sm:mt-0 col-span-2">
      <div className={extraClass + " rounded-md shadow-sm"}>
        <DatePicker
          id={key}
          utcOffset={0}
          showTimeSelect={showTimeSelect}
          timeFormat="HH:mm"
          locale="pl"
          //   customInput={<Input errors={errors} />}
          selected={value}
          dateFormat={dateFormat}
          onChange={onChange}
        />
      </div>
    </div>
  );

  if (!label) {
    return inputComponent;
  }

  return (
    <div
      key={key}
      className="mt-6 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:pt-5"
    >
      <label
        for={key}
        className="block text-sm font-medium leading-5 text-gray-700 sm:mt-px sm:pt-2"
      >
        {label}
      </label>
      {inputComponent}
      {errors.length > 0 && (
        <div className="mt-1 sm:mt-0 col-start-2 col-span-2">
          {errors.map((error) => (
            <p className="mt-2 text-sm text-red-600">{error}</p>
          ))}
        </div>
      )}
    </div>
  );
};
