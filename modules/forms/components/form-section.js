import {
  PlusIcon as PlusIconSolid,
  MinusIcon as MinusIconSolid,
} from "@heroicons/react/solid";

export default function FormSection({
  children,
  onSubmit,
  onAddClick,
  onMinusClick,
  isSubmitable,
  title = "Section title",
  desc = "Opis sekcji",
  isNarrow,
}) {
  return (
    <div className={!isNarrow && "md:grid md:grid-cols-3 md:gap-6 mt-2"}>
      <div className="md:col-span-1">
        <div className="px-4 sm:px-0 mt-2">
          <div className="flex items-center">
            {title && (
              <h3 className="text-lg font-medium leading-6 text-gray-900">
                {title}
              </h3>
            )}
            {onAddClick && (
              <button
                type="button"
                onClick={onAddClick}
                className="ml-2 inline-flex items-center p-2 border border-transparent rounded-full shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              >
                <PlusIconSolid className="h-5 w-5" aria-hidden="true" />
              </button>
            )}
            {onMinusClick && (
              <button
                type="button"
                onClick={onMinusClick}
                className="ml-2 inline-flex items-center p-2 border border-transparent rounded-full shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              >
                <MinusIconSolid className="h-5 w-5" aria-hidden="true" />
              </button>
            )}
          </div>
          <p className="mt-1 text-sm text-gray-600">{desc}</p>
        </div>
      </div>
      {children && (
        <div className="mt-5 md:mt-0 md:col-span-2">
          <form onSubmit={onSubmit}>
            <div className="shadow sm:rounded-md">
              <div className="px-4 py-5 bg-white space-y-6 sm:p-6">
                {children}
              </div>
              {isSubmitable && (
                <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                  <button
                    type="submit"
                    className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                  >
                    Zapisz
                  </button>
                </div>
              )}
            </div>
          </form>
        </div>
      )}
    </div>
  );
}
