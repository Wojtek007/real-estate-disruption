import { accessByIdSelector } from "../redux/selectors.js";
import { changeValue } from "../redux/actions.js";
import { initialInlineObject } from "../redux/initial-state";
import FieldRenderer from "./field-renderer";
import FormSection from "./form-section";
import { useEffect } from "react";

const Section = ({
  section,
  errors,
  state,
  isNarrow,
  dispatch,
  hiddenFields,
  initialObject,
}) => {
  const columns = isNarrow ? 3 : 6;
  const currentArrayOfNestedObjects = accessByIdSelector(section.objectsKey)(
    state
  ) ?? [];

  useEffect(() => {
    console.log('changeValue-11111', state, currentArrayOfNestedObjects)
    const isInitialValueNotExisting = currentArrayOfNestedObjects.length === 0;
    if (isInitialValueNotExisting) {
      dispatch(changeValue(
        section.objectsKey,
        section.templateObjects
          .map((obj) => Object.entries(obj)
            .reduce((acc, [key, v]) => {

              if (typeof v !== 'function') {
                return { ...acc, [key]: v };
              }
              return acc
            }, {})
          )
      ));
    }
  }, [])

  const calculateSum = () => currentArrayOfNestedObjects.reduce((acc, ele) => {
    console.log('YYYYYYY22222222', acc, ele, section.sumByIdForResult)
    if (!ele[section.sumByIdForResult]) {
      return acc
    }
    return isNaN(parseFloat(ele[section.sumByIdForResult])) ? acc : acc + parseFloat(ele[section.sumByIdForResult])
  }, 0).toFixed(2)
  console.log('YYYYYYY911111', currentArrayOfNestedObjects, state, calculateSum())

  useEffect(() => {
    console.log('changeValue-2222', calculateSum())
    dispatch(changeValue(section.resultField, calculateSum()))
  }, [calculateSum()])

  const onAddNestedObject = (section) => {
    // if there is nothing than set array with one object
    // otherwise add object to array
    if (currentArrayOfNestedObjects) {
      dispatch(
        changeValue(section.objectsKey, [
          ...currentArrayOfNestedObjects,
          initialInlineObject(section.objectsKey, initialObject),
        ])
      );
    } else {
      dispatch(
        changeValue(section.objectsKey, [
          initialInlineObject(section.objectsKey, initialObject),
        ])
      );
    }
  };
  const onRemoveNestedObject = (section) => {
    dispatch(
      changeValue(
        section.objectsKey,
        currentArrayOfNestedObjects.filter(
          (_, i) => i !== currentArrayOfNestedObjects.length - 1
        )
      )
    );
  };

  const isValidField = ({ id, dynamic }) => {
    return (
      !hiddenFields?.includes(`${section.objectsKey}/${id}`) &&
      (dynamic?.isHidden ? !dynamic.isHidden(currentArrayOfNestedObjects[i]) : true)
    );
  }

  return (
    <>
      <FormSection
        title={section.header}
        desc=""
        isNarrow={isNarrow}
      // onAddClick={() => onAddNestedObject(section)}
      // onMinusClick={
      //   currentArrayOfNestedObjects?.length
      //     ? () => onRemoveNestedObject(section)
      //     : null
      // }
      />
      <div className="flex flex-col">
        <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
            <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
              <table className="min-w-full divide-y divide-gray-200">
                <thead className="bg-gray-50">
                  <tr>
                    {section.fields.map((fieldsRow) => {
                      // console.log(
                      //   "UUUUUU",
                      //   fieldsRow,
                      //   fieldsRow.map(({ id }) => `${section.objectsKey}/${id}`)
                      // );
                      // const validFieldsInRow = fieldsRow.filter(
                      //   ({ id }) => !hiddenFields?.includes(`${section.objectsKey}/${id}`)
                      // );
                      const validFieldsInRow = fieldsRow.filter(isValidField);
                      if (validFieldsInRow.length === 0) {
                        return;
                      }
                      // const rowColumns =
                      //   columns < validFieldsInRow.length
                      //     ? columns
                      //     : validFieldsInRow.length;

                      return validFieldsInRow.map((field) => (
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                          {field.label}
                        </th>
                      ))
                    })}
                    {/* <th scope="col" className="relative px-6 py-3">
                    <span className="sr-only">Edit</span>
                  </th> */}
                  </tr>
                </thead>
                <tbody className="bg-white divide-y divide-gray-200">
                  {section.templateObjects.map((templateObject, i) => (
                    <tr key={templateObject.key}>
                      {section.fields.map((fieldsRow) => {

                        const validFieldsInRow = fieldsRow.filter(isValidField);
                        if (validFieldsInRow.length === 0) {
                          return;
                        }

                        return validFieldsInRow.map((field) => {

                          if (typeof templateObject[field.id] === 'string') {
                            return (
                              <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">{templateObject[field.id]}</td>
                            )
                          }
                          if (typeof templateObject[field.id] === 'function') {
                            console.log('&&&&&&&&&&', state, templateObject[field.id](state, currentArrayOfNestedObjects[i] ?? {}))

                            const { label: toRemove, ...fieldToPass } = field;
                            const calculateRowValues = (id, value) => {
                              console.log('YYYYYYYY 00000', state)

                              const a = Object.entries(templateObject)
                                .reduce((acc, [key, v]) => {
                                  if (id === key) {
                                    return acc;
                                  }
                                  if (typeof v !== 'function') {
                                    return { ...acc, [key]: v };
                                  }
                                  console.log('YYYYYYYY 1111', key, v(state, acc))
                                  return { ...acc, [key]: v(state, acc) };
                                }, { [id]: value })
                              console.log('YYYYYYYY 9999999', a, id, value)
                              return a
                            }
                            const onChange = (id, value) => {
                              console.log('YYYYYYYY', value, calculateRowValues(id, value))
                              return dispatch(changeValue(`${section.objectsKey}/${i}`, calculateRowValues(id, value)));
                            }
                            return (
                              <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                {/* {templateObject[field.id](state, currentArrayOfNestedObjects[i] ?? {})} */}
                                <FieldRenderer
                                  fieldConfig={fieldToPass}
                                  onChange={onChange}
                                  state={currentArrayOfNestedObjects[i] && currentArrayOfNestedObjects[i][field.id]
                                    ? currentArrayOfNestedObjects[i]
                                    : { [field.id]: templateObject[field.id](state, currentArrayOfNestedObjects[i] ?? {}) }}
                                // error={error}
                                // isObligatoryOverride={
                                //   (field.dynamic?.isObligatory &&
                                //     field.dynamic.isObligatory(currentArrayOfNestedObjects[i])) ||
                                //   (validFieldsInRow.length === 1 && section.isObligatory)
                                // }
                                />
                              </td>
                            )
                          }
                        })
                      })}

                      {/* <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">{templateObject.name}</td>
                      <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">{templateObject.title}</td>
                      <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">{templateObject.email}</td>
                      <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">{templateObject.role}</td> */}
                      {/* <td className="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                      <a href="#" className="text-indigo-600 hover:text-indigo-900">
                        Edit
                      </a>
                    </td> */}
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      {/* {currentArrayOfNestedObjects?.map((_, i) => (
        <FormSection title="" desc="" isNarrow={isNarrow}>
          {section.fields.map((fieldsRow) => {
            console.log(
              "UUUUUU",
              fieldsRow,
              fieldsRow.map(({ id }) => `${section.objectsKey}/${id}`)
            );
            // const validFieldsInRow = fieldsRow.filter(
            //   ({ id }) => !hiddenFields?.includes(`${section.objectsKey}/${id}`)
            // );
            const validFieldsInRow = fieldsRow.filter(({ id, dynamic }) => {
              return (
                !hiddenFields?.includes(`${section.objectsKey}/${id}`) &&
                (dynamic?.isHidden ? !dynamic.isHidden(currentArrayOfNestedObjects[i]) : true)
              );
            });
            if (validFieldsInRow.length === 0) {
              return;
            }
            const rowColumns =
              columns < validFieldsInRow.length
                ? columns
                : validFieldsInRow.length;

            return (
              <div className={`grid grid-cols-${rowColumns} gap-6`}>
                {validFieldsInRow.map((field) => {
                  const fieldId = (id) => `${section.objectsKey}/${i}/${id}`;
                  const onChange = (id, value) =>
                    dispatch(changeValue(fieldId(id), value));
                  const error = errors.find(
                    (error) => error.id === fieldId(field.id)
                  );
                  console.log(
                    "FIELD RENDERER log inline creator",
                    fieldId(field.id)
                  );

                  return (
                    <div className={`col-span-${rowColumns} lg:col-span-1`}>
                      <FieldRenderer
                        fieldConfig={field}
                        onChange={onChange}
                        state={currentArrayOfNestedObjects[i]}
                        error={error}
                        isObligatoryOverride={
                          (field.dynamic?.isObligatory &&
                            field.dynamic.isObligatory(currentArrayOfNestedObjects[i])) ||
                          (validFieldsInRow.length === 1 && section.isObligatory)
                        }
                      />
                    </div>
                  );
                })}
              </div>
            );
          })}
        </FormSection>
      ))} */}
    </>
  );
};



export default Section;
