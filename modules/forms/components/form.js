import { useReducer, useState, useEffect } from "react";
import { useRouter } from "next/router";

import { formReducer } from "../redux/reducer.js";
import { isValidSelector } from "../redux/selectors.js";
import { initialState } from "../redux/initial-state.js";
import { useObjectMutation } from "../hooks/use-object-mutation";
import { delay } from "../utils/delay";
import { getFormSections } from "../domain/config";

import FormPageTemplate from "./form-page-template";
import InlineCreatorSection from "./section-inline-creator";
import CalculationTableSection from "./section-calculation-table";
import StandardSection from "./section-standard";

const Engine = ({
  objectName,
  title,
  object,
  hiddenFields,
  removedFields = [],
  redirect,
  onSuccess,
  onAbort,
  isNarrow,
  submitLabel,
  submitSuccessLabel,
}) => {
  const router = useRouter();
  const [timeToRedirect, setTimeToRedirect] = useState(0);
  const [state, dispatch] = useReducer(
    formReducer,
    initialState(object, getFormSections({ objectName }))
  );
  console.log("UUUUU -1", object, state);
  console.log("FORM STATE", state);
  const [mutateObject, { error, loading, data, called }] = useObjectMutation({
    objectName,
  });
  useEffect(() => {
    if (timeToRedirect > 0) {
      delay(500).then(() => setTimeToRedirect(timeToRedirect - 1));
    }
  }, [timeToRedirect]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      console.log(
        "SUBMITING",
        state,
        "onSuccess:",
        !!onSuccess,
        "redirect:",
        !!redirect
      );
      const stateNoRemoved = Object.fromEntries(
        Object.entries(state).filter(([key]) => !removedFields.includes(key))
      )
      const { id, ver } = await mutateObject({ state: stateNoRemoved });
      setTimeToRedirect(3);
      await delay(3);
      if (onSuccess) {
        onSuccess({
          id,
          ver,
        });
      } else if (redirect) {
        router.push(redirect);
      } else {
        router.push(router.asPath.split("?")[0]);
      }
    } catch (error) {
      console.error("errorMessage", error);
    }
  };

  const handleAbort = (e) => {
    e.preventDefault();
    if (onAbort) {
      onAbort();
    } else if (redirect) {
      router.push(redirect);
    } else {
      router.push(router.asPath.split("?")[0]);
    }
  };

  const { isValid, errors } = isValidSelector({ objectName, state });

  return (
    <FormPageTemplate
      title={title}
      onAbort={handleAbort}
      onSubmit={handleSubmit}
      isSubmitDisabled={!isValid}
      isSubmitLoading={loading}
      isSubmitDone={called && !loading && !error}
      error={error?.message}
      submitLabel={submitLabel}
      submitSuccessLabel={submitSuccessLabel + " " + (timeToRedirect > 0 ? timeToRedirect : 'Przekierowuje...')}

    >
      {getFormSections({ objectName })
        .filter(
          ({ dynamic, objectsKey }) => !(dynamic?.isHidden && dynamic.isHidden(state)) && !hiddenFields?.includes(objectsKey)
        )
        .map((section) => {
          console.log("errors-form", errors);

          switch (section.type) {
            case "inline-creator":
              return (
                <InlineCreatorSection
                  section={section}
                  errors={errors}
                  isNarrow={isNarrow}
                  dispatch={dispatch}
                  hiddenFields={hiddenFields}
                  state={state}
                  initialObject={object}
                />
              );
            case "calculation-table":
              return (
                <CalculationTableSection
                  section={section}
                  errors={errors}
                  isNarrow={isNarrow}
                  dispatch={dispatch}
                  hiddenFields={hiddenFields}
                  state={state}
                  initialObject={object}
                />
              );

            case "plain-fields":
            default:
              const hasAnyValidFields = () =>
                section.fields
                  .map((fieldsRow) =>
                    fieldsRow.filter(
                      ({ id, dynamic }) =>
                        !hiddenFields?.includes(id) &&
                        (dynamic?.isHidden ? !dynamic.isHidden(state) : true)
                    )
                  )
                  .filter((validFieldsInRow) => validFieldsInRow.length > 0)
                  .length > 0;

              if (!hasAnyValidFields()) {
                return null;
              }
              return (
                <StandardSection
                  section={section}
                  errors={errors}
                  isNarrow={isNarrow}
                  dispatch={dispatch}
                  hiddenFields={hiddenFields}
                  state={state}
                />
              );
          }
        })}
    </FormPageTemplate>
  );
};

export default Engine;
