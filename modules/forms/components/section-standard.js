import { changeValue } from "../redux/actions.js";
import FieldRenderer from "./field-renderer";
import FormSection from "./form-section";

const Section = ({
  section,
  errors,
  isNarrow,
  dispatch,
  hiddenFields,
  state,
}) => {
  console.log("errors", errors);
  const columns = isNarrow ? 3 : 6;

  return (
    <FormSection title={section.header} desc="" isNarrow={isNarrow}>
      {section.fields.map((fieldsRow) => {
        const rowColumns =
          columns < fieldsRow.length ? columns : fieldsRow.length;

        const validFieldsInRow = fieldsRow.filter(({ id, dynamic }) => {
          return (
            !hiddenFields?.includes(id) &&
            (dynamic?.isHidden ? !dynamic.isHidden(state) : true)
          );
        });

        if (validFieldsInRow.length === 0) {
          return;
        }

        return (
          <div className={`grid grid-cols-${rowColumns} gap-6`}>
            {validFieldsInRow.map((field) => {
              const onChange = (id, value) => dispatch(changeValue(id, value));
              const error = errors.find((error) => error.id === field.id);
              console.log("FIELD RENDERER log classic sections", field.id);

              return (
                <div className={`col-span-${rowColumns} lg:col-span-1`}>
                  <FieldRenderer
                    fieldConfig={field}
                    onChange={onChange}
                    state={state}
                    error={error}
                    isObligatoryOverride={
                      (field.dynamic?.isObligatory &&
                        field.dynamic.isObligatory(state)) ||
                      (validFieldsInRow.length === 1 && section.isObligatory)
                    }
                  />
                </div>
              );
            })}
          </div>
        );
      })}
    </FormSection>
  );
};

export default Section;
