import InputTextArea from "../inputs/text-area";
import InputText from "../inputs/text";
import InputCurency from "../inputs/currency";
import InputNumber from "../inputs/number";
import InputDropdown from "../inputs/dropdown";
import InputFileDrop from "../inputs/file-drop";
import InputBoolean from "../inputs/boolean";
import DatePicker from "../inputs/date-picker";

import { accessByIdSelector } from "../redux/selectors.js";

export default function Example({
  fieldConfig,
  onChange,
  state,
  error,
  isObligatoryOverride,
}) {
  console.log("fieldConfig- FIELD", fieldConfig, state, error);

  const {
    type,
    label,
    id,
    desc,
    objectDef,
    create,
    isObligatory: isObligatoryConfig,
  } = fieldConfig;

  const isObligatory = isObligatoryOverride || isObligatoryConfig;
  // console.log("FIELD RENDERER log", id);

  switch (type) {
    case "text":
      return (
        <InputText
          label={label && `${label}${isObligatory ? "*" : ""}`}
          key={id}
          onChange={(value) => onChange(id, value)}
          value={accessByIdSelector(id)(state)}
          desc={desc}
          error={error}
        />
      );
    case "numeric":
    case "integer":
      return (
        <InputNumber
          label={label && `${label}${isObligatory ? "*" : ""}`}
          key={id}
          onChange={(value) => onChange(id, value)}
          value={accessByIdSelector(id)(state)}
          desc={desc}
          error={error}
        />
      );
    case "currency":
      return (
        <InputCurency
          label={label && `${label}${isObligatory ? "*" : ""}`}
          key={id}
          onChange={(value) => onChange(id, value)}
          value={accessByIdSelector(id)(state)}
          desc={desc}
          error={error}
        />
      );

    case "textArea":
      return (
        <InputTextArea
          label={label && `${label}${isObligatory ? "*" : ""}`}
          key={id}
          onChange={(value) => onChange(id, value)}
          value={accessByIdSelector(id)(state)}
          desc={desc}
          error={error}
        />
      );
    case "boolean":
      return (
        <InputBoolean
          label={label && `${label}${isObligatory ? "*" : ""}`}
          key={id}
          onChange={(value) => onChange(id, value)}
          value={accessByIdSelector(id)(state)}
          desc={desc}
          error={error}
        />
      );
    case "objectDropdown":
      console.log("YYYYYY", fieldConfig, fieldConfig.create.type);
      if (fieldConfig.create.type === "file") {
        return (
          <InputFileDrop
            label={label && `${label}${isObligatory ? "*" : ""}`}
            objectId={id}
            key={id}
            onChange={onChange} // inside it is changing id and ver
            value={accessByIdSelector(`${id}Id`)(state)}
            desc={desc}
            objectDef={objectDef}
            create={create}
            error={error}
          />
        );
      }
      // when there is many2many the value and onChange have one more key nested
      return (
        <InputDropdown
          label={label && `${label}${isObligatory ? "*" : ""}`}
          objectId={id}
          key={id}
          isObligatory={isObligatory}
          onChange={onChange} // inside it is changing id and ver
          value={accessByIdSelector(`${id}Id`)(state)}
          desc={desc}
          objectDef={objectDef}
          create={create}
          error={error}
        />
      );
    case "timestamp with time zone":
      console.log("EEEEEEE state", id, state, accessByIdSelector(id)(state));
      return (
        <DatePicker
          key={id}
          label={label && `${label}${isObligatory ? "*" : ""}`}
          onChange={(value) => {
            console.log("EEEEEEE CAHNGE", id, value);
            return onChange(id, value);
          }}
          value={accessByIdSelector(id)(state)}
          dateFormat="dd-MM-yyyy"
          isObligatory={isObligatory}
          errors={[error]}
        />
      );

    default:
      return <div>Brakuje renderea pola typu {type}</div>;
  }
}
