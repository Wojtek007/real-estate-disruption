import { accessByIdSelector } from "../redux/selectors.js";
import { changeValue } from "../redux/actions.js";
import { initialInlineObject } from "../redux/initial-state";
import FieldRenderer from "./field-renderer";
import FormSection from "./form-section";

const Section = ({
  section,
  errors,
  state,
  isNarrow,
  dispatch,
  hiddenFields,
  initialObject,
}) => {
  const columns = isNarrow ? 3 : 6;
  const currentArrayOfNestedObjects = accessByIdSelector(section.objectsKey)(
    state
  );

  const onAddNestedObject = (section) => {
    // if there is nothing than set array with one object
    // otherwise add object to array
    if (currentArrayOfNestedObjects) {
      dispatch(
        changeValue(section.objectsKey, [
          ...currentArrayOfNestedObjects,
          initialInlineObject(section.objectsKey, initialObject),
        ])
      );
    } else {
      dispatch(
        changeValue(section.objectsKey, [
          initialInlineObject(section.objectsKey, initialObject),
        ])
      );
    }
  };
  const onRemoveNestedObject = (section) => {
    console.log('onRemoveNestedObject:start', currentArrayOfNestedObjects)
    const revertedIndexToRemove = currentArrayOfNestedObjects.slice().reverse().findIndex(element => !element.deleted || element.deleted !== true)
    const indexToRemove = currentArrayOfNestedObjects.length - 1 - revertedIndexToRemove

    console.log('onRemoveNestedObject:indexToRemove', revertedIndexToRemove, currentArrayOfNestedObjects, currentArrayOfNestedObjects.slice().reverse())
    const elementToRemove = currentArrayOfNestedObjects[indexToRemove];
    console.log('onRemoveNestedObject:elementToRemove', indexToRemove, elementToRemove)

    // if this nested object has id than put delete to true
    if (elementToRemove.id) {
      dispatch(
        changeValue(
          section.objectsKey,
          currentArrayOfNestedObjects.map(
            (ele, i) => i === indexToRemove ? { ...ele, deleted: true } : ele
          )
        )
      );
    } else {     // if it hasnt than remove
      dispatch(
        changeValue(
          section.objectsKey,
          currentArrayOfNestedObjects.filter(
            (_, i) => i !== indexToRemove
          )
        )
      );
    }

    // todo don't show onnes with delete flag

  };
  console.log('onRemoveNestedObject:currentArrayOfNestedObjects', currentArrayOfNestedObjects, errors)

  return (
    <>
      <FormSection
        title={section.header}
        desc=""
        isNarrow={isNarrow}
        onAddClick={() => onAddNestedObject(section)}
        onMinusClick={
          currentArrayOfNestedObjects?.filter(obj => obj.deleted !== true).length
            ? () => onRemoveNestedObject(section)
            : null
        }
      />
      {currentArrayOfNestedObjects?.map((obj, i) => obj.deleted !== true && (
        <FormSection title="" desc="" isNarrow={isNarrow}>
          {section.fields.map((fieldsRow) => {
            console.log(
              "UUUUUU",
              fieldsRow,
              fieldsRow.map(({ id }) => `${section.objectsKey}/${id}`)
            );
            // const validFieldsInRow = fieldsRow.filter(
            //   ({ id }) => !hiddenFields?.includes(`${section.objectsKey}/${id}`)
            // );
            const validFieldsInRow = fieldsRow.filter(({ id, dynamic }) => {
              return (
                !hiddenFields?.includes(`${section.objectsKey}/${id}`) &&
                (dynamic?.isHidden ? !dynamic.isHidden(currentArrayOfNestedObjects[i]) : true)
              );
            });
            if (validFieldsInRow.length === 0) {
              return;
            }
            const rowColumns =
              columns < validFieldsInRow.length
                ? columns
                : validFieldsInRow.length;

            return (
              <div className={`grid grid-cols-${rowColumns} gap-6`}>
                {validFieldsInRow.map((field) => {
                  const fieldId = (id) => `${section.objectsKey}/${i}/${id}`;
                  const onChange = (id, value) =>
                    dispatch(changeValue(fieldId(id), value));
                  const error = errors.find(
                    (error) => error.id === fieldId(field.id)
                  );
                  console.log(
                    "FIELD RENDERER log inline creator",
                    fieldId(field.id)
                  );

                  return (
                    <div className={`col-span-${rowColumns} lg:col-span-1`}>
                      <FieldRenderer
                        fieldConfig={field}
                        onChange={onChange}
                        state={currentArrayOfNestedObjects[i]}
                        error={error}
                        isObligatoryOverride={
                          (field.dynamic?.isObligatory &&
                            field.dynamic.isObligatory(currentArrayOfNestedObjects[i])) ||
                          (validFieldsInRow.length === 1 && section.isObligatory)
                        }
                      />
                    </div>
                  );
                })}
              </div>
            );
          })}
        </FormSection>
      ))}
    </>
  );
};

export default Section;
