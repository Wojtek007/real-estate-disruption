import { useRouter } from "next/router";
import { validateObjectName } from "./service";
import FormBuilder from "./components/form";
import { getCreationFormTitle } from "./domain/config/";

const Page = ({ object, objectName, onSuccess, onAbort, hiddenFields, overrideTitle }) => {
  return (
    <FormBuilder
      title={overrideTitle ?? getCreationFormTitle({ objectName, state: object })}
      submitLabel={"Stwórz"}
      submitSuccessLabel={"Stworzono!"}
      objectName={objectName}
      object={object}
      onSuccess={onSuccess}
      onAbort={onAbort}
      hiddenFields={hiddenFields}
      isNarrow
    />
  );
};

export default Page;
