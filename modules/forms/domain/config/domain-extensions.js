export const extension = (objectName, formSections) => {
  console.log("DDDDD", objectName, formSections);
  const extendedFormSections = formSections.map((section) => {
    if (!extensionDictionary[objectName]) {
      return section;
    }
    const sectionExtensionDictionary =
      extensionDictionary[objectName][section.objectsKey ?? "__BASIC__"];
    // enchance fields
    const fields = sectionExtensionDictionary
      ? section.fields.map((fieldRow) =>
        fieldRow.map((field) => ({
          ...field,
          ...sectionExtensionDictionary[field.id],
        }))
      )
      : section.fields;

    // enchance section
    const newSection = sectionExtensionDictionary["__SECTION__"]
      ? { ...section, fields, ...sectionExtensionDictionary["__SECTION__"] }
      : { ...section, fields };
    return newSection;
  });
  console.log("DDDDD -result", objectName, extendedFormSections);

  return extendedFormSections;
};

export const extensionDictionary = {
  contract: {
    __GENERAL__: {
      formTitle: (state) => {
        switch (state.categoryId) {
          case '3c96dfac-3382-434c-a646-877e4c98b252'://Umowy najmu mieszkania
            return "0e02eb25-fd98-473a-ad2b-2e884bbd3615" === state.typeId // umowa najmu
              ? 'Dodaj umowę najmu'
              : "54d6733d-d6ff-4ce7-8762-d41f00d8e312" === state.typeId // rozwiazanie umowy
                ? 'Dodaj rozwiązanie umowy najmu'
                : "Dodaj aneks umowy najmu"
          case '40511c2e-dff0-478a-a83d-f4cfc8177509'://Umowy zarządzania mieszkaniem
            return "0e02eb25-fd98-473a-ad2b-2e884bbd3615" === state.typeId // umowa najmu
              ? 'Dodaj umowę zarządzania'
              : "54d6733d-d6ff-4ce7-8762-d41f00d8e312" === state.typeId // rozwiazanie umowy
                ? 'Dodaj rozwiązanie umowy zarządzania'
                : "Dodaj aneks umowy zarządzania"
          case 'd3781e58-8972-479d-ae8c-0c667e75b923'://Umowy ubezpieczenia
            return "0e02eb25-fd98-473a-ad2b-2e884bbd3615" === state.typeId // umowa najmu
              ? 'Dodaj ubezpieczenie'
              : "54d6733d-d6ff-4ce7-8762-d41f00d8e312" === state.typeId // rozwiazanie umowy
                ? 'Dodaj rozwiązanie ubezpieczenia'
                : "Dodaj aneks ubezpieczenia"
        }
      }
    },
    __BASIC__: {
      // refersToContract: {
      //   dynamic: {
      //     isHidden: (state) =>
      //       "0e02eb25-fd98-473a-ad2b-2e884bbd3615" === state.typeId, // umowa najmu
      //     isObligatory: (state) =>
      //       "0e02eb25-fd98-473a-ad2b-2e884bbd3615" !== state.typeId, // other than umowa najmu
      //   },
      // },
      startDate: {
        dynamic: {
          isObligatory: (state) =>
            "ee33c218-90f9-4a7c-942c-2f3d7692edbd" !== state.typeId, // other than aneks
        },
      },
      endDate: {
        dynamic: {
          isHidden: (state) =>
            "54d6733d-d6ff-4ce7-8762-d41f00d8e312" === state.typeId, // rozwiazanie umowy
          isObligatory: (state) =>
            "0e02eb25-fd98-473a-ad2b-2e884bbd3615" === state.typeId, // umowa najmu
        },
      },
      "notification/data/precedingPeriod": {
        dynamic: {
          isHidden: (state) =>
            "54d6733d-d6ff-4ce7-8762-d41f00d8e312" === state.typeId ||  // rozwiazanie umowy
            ("ee33c218-90f9-4a7c-942c-2f3d7692edbd" === state.typeId && !state.endDate), // is annex without end date
          isObligatory: (state) =>
            "54d6733d-d6ff-4ce7-8762-d41f00d8e312" !== state.typeId, // other than rozwiazanie
        },
      }
    },
    "contractObligations/data": {
      __SECTION__: {
        dynamic: {
          isHidden: (state) =>
            "54d6733d-d6ff-4ce7-8762-d41f00d8e312" === state.typeId  // rozwiazanie umowy
            || "3c96dfac-3382-434c-a646-877e4c98b252" !== state.categoryId // nie jest umowa najmu
        },
      },
      paidUnits: {
        dynamic: {
          isHidden: (state) =>
            !state.obligationTypeVer || 4 !== state.obligationTypeVer, // ogrzewanie
          isObligatory: (state) => 4 === state.obligationTypeVer,
        },
      },
      financialObligation: {
        dynamic: {
          isHidden: (state) =>
            !state.obligationTypeVer || 4 === state.obligationTypeVer, // ogrzewanie
          isObligatory: (state) => 4 !== state.obligationTypeVer,
        },
      }
    },
  },
  'meterPeriod': {
    __BASIC__: {
      finalPrice: {
        dynamic: {
          isHidden: (state) =>
            "5ce42970-e386-4881-9ae7-f1a7c5b68bbe" != state.utilityTypeId, // utility electricty
        },
      },
      pricePerUnit: {
        dynamic: {
          isHidden: (state) =>
            "5ce42970-e386-4881-9ae7-f1a7c5b68bbe" === state.utilityTypeId, // utility electricty
        },
      },
    },
    "meterPeriodPriceIngredients/data": {
      __SECTION__: {
        resultField: 'finalPrice',
        sumByIdForResult: 'price',
        templateObjects: [
          ...[
            { "name": 'Energia elektryczna czynna całodobowa', },
            { "name": 'Opłata dystrybucyjna zmienna', },
            { "name": 'Opłata OZE całodobowa', },
            { "name": 'Opłata kogeneracyjna całodobowa', },

          ].map(obj => ({
            ...obj,
            "nettoPricePerUnit": (_, row) => !!row.bruttoPricePerUnit ? (row.bruttoPricePerUnit / 1.23).toFixed(4) : '',
            "bruttoPricePerUnit": (_, row) => !!row.nettoPricePerUnit ? (row.nettoPricePerUnit * 1.23).toFixed(4) : '',
            "quantity": state => !!state.measurementValue ? state.measurementValue - state.previousMeasurementValue : '',
            "price": (state, row) => !!state.measurementValue && !!row.bruttoPricePerUnit ? ((state.measurementValue - state.previousMeasurementValue) * row.bruttoPricePerUnit).toFixed(2) : '',
          })),
          ...[
            { "name": 'Opłata dystrybucyjna stała', },
            { "name": 'Opłata przejściowa', },
            { "name": 'Opłata abonamentowa', },
            { "name": 'Opłata mocowa', }
          ].map(obj => ({
            ...obj,
            "nettoPricePerUnit": (_, row) => !!row.bruttoPricePerUnit ? (row.bruttoPricePerUnit / 1.23).toFixed(4) : '',
            "bruttoPricePerUnit": (_, row) => !!row.nettoPricePerUnit ? (row.nettoPricePerUnit * 1.23).toFixed(4) : '',
            "quantity": () => 10,
            "price": (_, row) => !!row.quantity && !!row.bruttoPricePerUnit ? (row.quantity * row.bruttoPricePerUnit).toFixed(2) : '',
          }))
        ],
        dynamic: {
          isHidden: (state) =>
            "5ce42970-e386-4881-9ae7-f1a7c5b68bbe" !== state.utilityTypeId, // utility electricty
        },
      },
    }

  },
  defect: {
    __BASIC__: {

      "notification/data/reminderDate": {
        dynamic: {
          isObligatory: (state) =>
            "0a20935c-cd38-4387-a725-e39444da62ec" !== state.urgencyId, // other than odrzucone
        },
      }
    },
  }

};


