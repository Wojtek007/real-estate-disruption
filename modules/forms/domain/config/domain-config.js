
export const addressConfig = () => (
  { "create": { "formTitle": "", "errorMessage": "Error during add address", "gqlType": "versionedAddresses_insert_input", "gqlMutation": "insertAddress", "successRedirect": "/", "abortRedirect": "/" }, "objectDef": { "objectName": "address", "objectTable": "addresses", "idKey": "id" }, "formSections": [{ "header": "Podstawowe", "fields": [[{ "type": "text", "label": "Ulica", "isObligatory": false, "desc": null, "id": "street" }], [{ "type": "text", "label": "Numer budynku", "isObligatory": false, "desc": null, "id": "streetNumber" }], [{ "type": "text", "label": "Numer mieszkania", "isObligatory": false, "desc": null, "id": "apartmentNumber" }], [{ "type": "text", "label": "Kod pocztowy", "isObligatory": false, "desc": null, "id": "postalCode" }], [{ "type": "text", "label": "Miasto", "isObligatory": false, "desc": null, "id": "city" }], [{ "type": "text", "label": "Region", "isObligatory": false, "desc": null, "id": "region" }], [{ "type": "text", "label": "Kraj", "isObligatory": false, "desc": null, "id": "country" }]] }] }
)

export const checkConfig = () => (
  { "create": { "formTitle": "Dodaj regularny przegląd", "errorMessage": "Error during add check", "gqlType": "versionedChecks_insert_input", "gqlMutation": "insertCheck", "successRedirect": "/", "abortRedirect": "/" }, "objectDef": { "objectName": "check", "objectTable": "checks", "idKey": "id" }, "formSections": [{ "header": "Podstawowe", "fields": [[{ "type": "text", "label": "Nazwa przeglądu", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole Nazwa przeglądu jest wymagane" }], "desc": null, "id": "name" }], [{ "type": "objectDropdown", "label": "property", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole property jest wymagane" }], "desc": null, "id": "property", "create": { "type": null, "title": null }, "objectDef": { "singularName": "property", "pluralName": "properties", "idKey": "id", "descriptorData": ["name"] } }], [{ "type": "objectDropdown", "label": "Regularność przeglądu", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole Regularność przeglądu jest wymagane" }], "desc": null, "id": "recurrenceInterval", "create": { "type": "Nothing", "title": null }, "objectDef": { "singularName": "recurrenceInterval", "pluralName": "recurrenceIntervals", "idKey": "id", "descriptorData": ["name"] } }]] }, { "header": "Rezultat ostatniego przeglądu", "fields": [[{ "type": "timestamp with time zone", "label": "Data przeglądu", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole Data przeglądu jest wymagane" }], "desc": null, "id": "initialCheckResult/data/date" }], [{ "type": "currency", "label": "Koszt przeglądu", "isObligatory": false, "desc": null, "id": "initialCheckResult/data/cost" }], [{ "type": "objectDropdown", "label": "parent_notification", "isObligatory": false, "desc": null, "id": "initialCheckResult/data/parentNotification", "create": { "type": null, "title": null }, "objectDef": { "singularName": "notification", "pluralName": "notifications", "idKey": "id", "descriptorData": ["name"] } }]] }, { "header": "Notyfikacja", "isObligatory": true, "fields": [[{ "type": "timestamp with time zone", "label": "Data przypomnienia", "isObligatory": false, "desc": null, "id": "initialCheckResult/data/nextCheckNotification/data/reminderDate" }], [{ "type": "objectDropdown", "label": "Okres poprzedzenia", "isObligatory": false, "desc": null, "id": "initialCheckResult/data/nextCheckNotification/data/precedingPeriod", "create": { "type": null, "title": null }, "objectDef": { "singularName": "precedingPeriod", "pluralName": "precedingPeriods", "idKey": "id", "descriptorData": ["name"] } }]] }] }
)

export const companyConfig = () => (
  { "create": { "formTitle": "Dodaj firmę", "errorMessage": "Error during add company", "gqlType": "versionedCompanies_insert_input", "gqlMutation": "insertCompany", "successRedirect": "/", "abortRedirect": "/" }, "objectDef": { "objectName": "company", "objectTable": "companies", "idKey": "id" }, "formSections": [{ "header": "Podstawowe", "fields": [[{ "type": "text", "label": "Nazwa firmy", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Firma musi posiadać nazwę" }], "desc": null, "id": "name" }], [{ "type": "text", "label": "Email", "isObligatory": false, "desc": null, "id": "email" }], [{ "type": "text", "label": "Firmowy numer telefonu", "isObligatory": false, "desc": null, "id": "phone" }], [{ "type": "text", "label": "NIP", "isObligatory": false, "desc": null, "id": "taxIdentificationNumber" }], [{ "type": "text", "label": "Notatka", "isObligatory": false, "desc": null, "id": "notes" }]] }, { "header": "Adres", "fields": [[{ "type": "text", "label": "Ulica", "isObligatory": false, "desc": null, "id": "address/data/street" }], [{ "type": "text", "label": "Numer budynku", "isObligatory": false, "desc": null, "id": "address/data/streetNumber" }], [{ "type": "text", "label": "Numer mieszkania", "isObligatory": false, "desc": null, "id": "address/data/apartmentNumber" }], [{ "type": "text", "label": "Kod pocztowy", "isObligatory": false, "desc": null, "id": "address/data/postalCode" }], [{ "type": "text", "label": "Miasto", "isObligatory": false, "desc": null, "id": "address/data/city" }], [{ "type": "text", "label": "Region", "isObligatory": false, "desc": null, "id": "address/data/region" }], [{ "type": "text", "label": "Kraj", "isObligatory": false, "desc": null, "id": "address/data/country" }]] }, { "header": "Pracownicy", "type": "inline-creator", "objectsKey": "employees/data", "fields": [[{ "type": "text", "label": "Rola w firmie", "isObligatory": false, "desc": null, "id": "role" }], [{ "type": "text", "label": "Imie", "isObligatory": false, "desc": null, "id": "person/data/firstname" }, { "type": "text", "label": "Nazwisko", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole Nazwisko jest wymagane" }], "desc": null, "id": "person/data/surname" }, { "type": "text", "label": "Pseudonim", "isObligatory": false, "desc": null, "id": "person/data/nickname" }, { "type": "text", "label": "Email", "isObligatory": false, "desc": null, "id": "person/data/email" }, { "type": "text", "label": "Numer telefonu", "isObligatory": false, "desc": null, "id": "person/data/phone" }, { "type": "text", "label": "Notatka", "isObligatory": false, "desc": null, "id": "person/data/notes" }, { "type": "text", "label": "Numer PESEL", "isObligatory": false, "desc": null, "id": "person/data/pesel" }]] }] }
)

export const contractTypeConfig = () => (
  { "create": { "formTitle": null, "errorMessage": "Error during add contract type", "gqlType": "versionedContractTypes_insert_input", "gqlMutation": "insertContractType", "successRedirect": "/", "abortRedirect": "/" }, "objectDef": { "objectName": "contractType", "objectTable": "contract_types", "idKey": "id" }, "formSections": [{ "header": "Podstawowe", "fields": [[{ "type": "text", "label": "name", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole name jest wymagane" }], "desc": null, "id": "name" }], [{ "type": "objectDropdown", "label": "allowed_for_kind", "isObligatory": false, "desc": null, "id": "allowedForKind", "create": { "type": null, "title": null }, "objectDef": { "singularName": "contractCategory", "pluralName": "contractCategories", "idKey": "id", "descriptorData": ["label"] } }], [{ "type": "objectDropdown", "label": "refered_contract_type", "isObligatory": false, "desc": null, "id": "referedContractType", "create": { "type": null, "title": null }, "objectDef": { "singularName": "contractType", "pluralName": "contractTypes", "idKey": "id", "descriptorData": ["name"] } }], [{ "type": "text", "label": "effect", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole effect jest wymagane" }], "desc": null, "id": "effect" }]] }] }
)

export const contractConfig = () => (
  { "create": { "formTitle": "Dodaj kontrakt", "errorMessage": "Error during add contract", "gqlType": "versionedContracts_insert_input", "gqlMutation": "insertContract", "successRedirect": "/", "abortRedirect": "/" }, "objectDef": { "objectName": "contract", "objectTable": "contracts", "idKey": "id" }, "formSections": [{ "header": "Podstawowe", "fields": [[{ "type": "objectDropdown", "label": "Sygnatariusz", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Sygnatariusz jest wymagany." }], "desc": null, "id": "signer", "create": { "type": "slideOver", "title": null }, "objectDef": { "singularName": "person", "pluralName": "people", "idKey": "id", "descriptorData": ["firstname", "surname"] } }], [{ "type": "objectDropdown", "label": "Typ dokumentu", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole Typ dokumentu jest wymagane" }], "desc": null, "id": "type", "create": { "type": null, "title": null }, "objectDef": { "singularName": "contractType", "pluralName": "contractTypes", "idKey": "id", "descriptorData": ["name"] } }], [{ "type": "objectDropdown", "label": "Nawiązuje do umowy", "isObligatory": false, "desc": null, "id": "refersToContract", "create": { "type": null, "title": null }, "objectDef": { "singularName": "contract", "pluralName": "contracts", "idKey": "id", "descriptorData": ["signer", "type", "startDate"] } }], [{ "type": "objectDropdown", "label": "", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole  jest wymagane" }], "desc": null, "id": "subjectProperty", "create": { "type": null, "title": null }, "objectDef": { "singularName": "property", "pluralName": "properties", "idKey": "id", "descriptorData": ["name"] } }], [{ "type": "objectDropdown", "label": "category", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole category jest wymagane" }], "desc": null, "id": "category", "create": { "type": null, "title": null }, "objectDef": { "singularName": "contractCategory", "pluralName": "contractCategories", "idKey": "id", "descriptorData": ["label"] } }], [{ "type": "objectDropdown", "label": "Plik kontraktu", "isObligatory": false, "desc": null, "id": "documentLink", "create": { "type": "file", "title": null }, "objectDef": { "singularName": "mediaFile", "pluralName": "mediaFiles", "idKey": "id", "descriptorData": ["name"] } }], [{ "type": "timestamp with time zone", "label": "Data początku obowiązywania kontraktu", "isObligatory": false, "desc": null, "id": "startDate" }], [{ "type": "timestamp with time zone", "label": "Data końca obowiązywania kontraktu", "isObligatory": false, "desc": null, "id": "endDate" }]] }, { "header": "Notyfikacja", "fields": [[{ "type": "timestamp with time zone", "label": "Data przypomnienia", "isObligatory": false, "desc": null, "id": "notification/data/reminderDate" }], [{ "type": "objectDropdown", "label": "Okres poprzedzenia", "isObligatory": false, "desc": null, "id": "notification/data/precedingPeriod", "create": { "type": null, "title": null }, "objectDef": { "singularName": "precedingPeriod", "pluralName": "precedingPeriods", "idKey": "id", "descriptorData": ["name"] } }]] }, { "header": "Zobowiązania umowne", "type": "inline-creator", "objectsKey": "contractObligations/data", "fields": [[{ "type": "objectDropdown", "label": "Typ zobowiązania", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole Typ zobowiązania jest wymagane" }], "desc": null, "id": "obligationType", "create": { "type": null, "title": null }, "objectDef": { "singularName": "contractObligationType", "pluralName": "contractObligationTypes", "idKey": "id", "descriptorData": ["label"] } }], [{ "type": "numeric", "label": "Ilość opłaconych jednostek", "isObligatory": false, "desc": null, "id": "paidUnits" }], [{ "type": "currency", "label": "Kwota zobowiązania", "isObligatory": false, "desc": null, "id": "financialObligation" }]] }] }
)

export const defectConfig = () => (
  { "create": { "formTitle": "Dodaj usterkę", "errorMessage": "Error during add defect", "gqlType": "versionedDefects_insert_input", "gqlMutation": "insertDefect", "successRedirect": "/", "abortRedirect": "/" }, "objectDef": { "objectName": "defect", "objectTable": "defects", "idKey": "id" }, "formSections": [{ "header": "Podstawowe", "fields": [[{ "type": "text", "label": "Opis uszkodzenia", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole Opis uszkodzenia jest wymagane" }], "desc": null, "id": "name" }], [{ "type": "textArea", "label": "Notatka", "isObligatory": false, "desc": null, "id": "notes" }], [{ "type": "boolean", "label": "Czy zgoda właściciela wymagana", "isObligatory": false, "desc": null, "id": "isOwnerAcceptanceNecessary" }], [{ "type": "boolean", "label": "Wymaga zewnętrznego specjalisty", "isObligatory": false, "desc": null, "id": "isForContractor" }], [{ "type": "objectDropdown", "label": "Pilność", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole Pilność jest wymagane" }], "desc": null, "id": "urgency", "create": { "type": null, "title": null }, "objectDef": { "singularName": "urgencyLevel", "pluralName": "urgencyLevels", "idKey": "id", "descriptorData": ["name"] } }], [{ "type": "objectDropdown", "label": "property", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole property jest wymagane" }], "desc": null, "id": "property", "create": { "type": null, "title": null }, "objectDef": { "singularName": "property", "pluralName": "properties", "idKey": "id", "descriptorData": ["name"] } }], [{ "type": "textArea", "label": "Wiadomość zgłoszeniowa", "isObligatory": false, "desc": null, "id": "reportingMessage" }], [{ "type": "currency", "label": "Szacowany koszt", "isObligatory": false, "desc": null, "id": "estimatedCost" }]] }, { "header": "Notyfikacja", "fields": [[{ "type": "timestamp with time zone", "label": "Data przypomnienia", "isObligatory": false, "desc": null, "id": "notification/data/reminderDate" }], [{ "type": "objectDropdown", "label": "Okres poprzedzenia", "isObligatory": false, "desc": null, "id": "notification/data/precedingPeriod", "create": { "type": null, "title": null }, "objectDef": { "singularName": "precedingPeriod", "pluralName": "precedingPeriods", "idKey": "id", "descriptorData": ["name"] } }]] }] }
)

export const employeeConfig = () => (
  { "create": { "formTitle": "Dodaj pracownika", "errorMessage": "Error during add employee", "gqlType": "versionedEmployees_insert_input", "gqlMutation": "insertEmployee", "successRedirect": "/", "abortRedirect": "/" }, "objectDef": { "objectName": "employee", "objectTable": "employees", "idKey": "id" }, "formSections": [{ "header": "Podstawowe", "fields": [[{ "type": "text", "label": "Rola w firmie", "isObligatory": false, "desc": null, "id": "role" }]] }, { "header": "Osoba", "fields": [[{ "type": "text", "label": "Imie", "isObligatory": false, "desc": null, "id": "person/data/firstname" }], [{ "type": "text", "label": "Nazwisko", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole Nazwisko jest wymagane" }], "desc": null, "id": "person/data/surname" }], [{ "type": "text", "label": "Pseudonim", "isObligatory": false, "desc": null, "id": "person/data/nickname" }], [{ "type": "text", "label": "Email", "isObligatory": false, "desc": null, "id": "person/data/email" }], [{ "type": "text", "label": "Numer telefonu", "isObligatory": false, "desc": null, "id": "person/data/phone" }], [{ "type": "text", "label": "Numer PESEL", "isObligatory": false, "desc": null, "id": "person/data/pesel" }], [{ "type": "text", "label": "Notatka", "isObligatory": false, "desc": null, "id": "person/data/notes" }]] }, { "header": null, "type": "inline-creator", "objectsKey": "companies/data", "fields": [[{ "type": "text", "label": "Nazwa firmy", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Firma musi posiadać nazwę" }], "desc": null, "id": "name" }], [{ "type": "text", "label": "Email", "isObligatory": false, "desc": null, "id": "email" }], [{ "type": "text", "label": "Firmowy numer telefonu", "isObligatory": false, "desc": null, "id": "phone" }], [{ "type": "text", "label": "NIP", "isObligatory": false, "desc": null, "id": "taxIdentificationNumber" }], [{ "type": "text", "label": "Notatka", "isObligatory": false, "desc": null, "id": "notes" }], [{ "type": "text", "label": "Numer budynku", "isObligatory": false, "desc": null, "id": "address/data/streetNumber" }, { "type": "text", "label": "Numer mieszkania", "isObligatory": false, "desc": null, "id": "address/data/apartmentNumber" }, { "type": "text", "label": "Kod pocztowy", "isObligatory": false, "desc": null, "id": "address/data/postalCode" }, { "type": "text", "label": "Miasto", "isObligatory": false, "desc": null, "id": "address/data/city" }, { "type": "text", "label": "Region", "isObligatory": false, "desc": null, "id": "address/data/region" }, { "type": "text", "label": "Kraj", "isObligatory": false, "desc": null, "id": "address/data/country" }, { "type": "text", "label": "Ulica", "isObligatory": false, "desc": null, "id": "address/data/street" }]] }] }
)

export const guaranteeConfig = () => (
  { "create": { "formTitle": "Dodaj gwarancję", "errorMessage": "Error during add guarantee", "gqlType": "versionedGuarantees_insert_input", "gqlMutation": "insertGuarantee", "successRedirect": "/", "abortRedirect": "/" }, "objectDef": { "objectName": "guarantee", "objectTable": "guarantees", "idKey": "id" }, "formSections": [{ "header": "Podstawowe", "fields": [[{ "type": "text", "label": "poziom_slodkosci", "isObligatory": false, "desc": null, "id": "poziomSlodkosci" }]] }] }
)

export const meterPeriodPriceIngredientConfig = () => (
  { "create": { "formTitle": "", "errorMessage": "Error during add meter period price ingredient", "gqlType": "versionedMeterPeriodPriceIngredients_insert_input", "gqlMutation": "insertMeterPeriodPriceIngredient", "successRedirect": "/", "abortRedirect": "/" }, "objectDef": { "objectName": "meterPeriodPriceIngredient", "objectTable": "meter_period_price_ingredients", "idKey": "id" }, "formSections": [{ "header": "Podstawowe", "fields": [[{ "type": "text", "label": "Nazwa", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole Nazwa jest wymagane" }], "desc": null, "id": "name" }], [{ "type": "currency", "label": "Cena netto", "isObligatory": false, "desc": null, "id": "nettoPricePerUnit" }], [{ "type": "currency", "label": "Cena brutto", "isObligatory": false, "desc": null, "id": "bruttoPricePerUnit" }], [{ "type": "numeric", "label": "Ilość", "isObligatory": false, "desc": null, "id": "quantity" }], [{ "type": "currency", "label": "Cena", "isObligatory": false, "desc": null, "id": "price" }]] }, { "header": null, "type": "inline-creator", "objectsKey": "meterPeriods/data", "fields": [[{ "type": "integer", "label": "Zmierzona wartość", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole Zmierzona wartość jest wymagane" }], "desc": null, "id": "measurementValue" }], [{ "type": "timestamp with time zone", "label": "Data pomiaru", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole Data pomiaru jest wymagane" }], "desc": null, "id": "measurementDate" }], [{ "type": "currency", "label": "Cena za jednostkę", "isObligatory": false, "desc": "", "id": "pricePerUnit" }], [{ "type": "currency", "label": "Cena całkowita", "isObligatory": false, "desc": null, "id": "finalPrice" }], [{ "type": "objectDropdown", "label": "utility_type", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole utility_type jest wymagane" }], "desc": null, "id": "utilityType", "create": { "type": null, "title": null }, "objectDef": { "singularName": "utilityType", "pluralName": "utilityTypes", "idKey": "id", "descriptorData": ["label"] } }], [{ "type": "objectDropdown", "label": "property", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole property jest wymagane" }], "desc": null, "id": "property", "create": { "type": null, "title": null }, "objectDef": { "singularName": "property", "pluralName": "properties", "idKey": "id", "descriptorData": ["name"] } }], [{ "type": "objectDropdown", "label": "payment_group", "isObligatory": false, "desc": null, "id": "paymentGroup", "create": { "type": "hidden", "title": null }, "objectDef": { "singularName": "paymentGroupMeterPeriod", "pluralName": "paymentGroupMeterPeriods", "idKey": "id", "descriptorData": ["name"] } }]] }] }
)

export const meterPeriodConfig = () => (
  { "create": { "formTitle": "Dodaj okres rozliczeniowy za media", "errorMessage": "Error during add meter period", "gqlType": "versionedMeterPeriods_insert_input", "gqlMutation": "insertMeterPeriod", "successRedirect": "/", "abortRedirect": "/" }, "objectDef": { "objectName": "meterPeriod", "objectTable": "meter_periods", "idKey": "id" }, "formSections": [{ "header": "Podstawowe", "fields": [[{ "type": "integer", "label": "Zmierzona wartość", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole Zmierzona wartość jest wymagane" }], "desc": null, "id": "measurementValue" }], [{ "type": "timestamp with time zone", "label": "Data pomiaru", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole Data pomiaru jest wymagane" }], "desc": null, "id": "measurementDate" }], [{ "type": "currency", "label": "Cena za jednostkę", "isObligatory": false, "desc": "", "id": "pricePerUnit" }], [{ "type": "currency", "label": "Cena całkowita", "isObligatory": false, "desc": null, "id": "finalPrice" }], [{ "type": "objectDropdown", "label": "utility_type", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole utility_type jest wymagane" }], "desc": null, "id": "utilityType", "create": { "type": null, "title": null }, "objectDef": { "singularName": "utilityType", "pluralName": "utilityTypes", "idKey": "id", "descriptorData": ["label"] } }], [{ "type": "objectDropdown", "label": "property", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole property jest wymagane" }], "desc": null, "id": "property", "create": { "type": null, "title": null }, "objectDef": { "singularName": "property", "pluralName": "properties", "idKey": "id", "descriptorData": ["name"] } }]] }, { "header": "Obliczanie należności", "type": "calculation-table", "objectsKey": "meterPeriodPriceIngredients/data", "fields": [[{ "type": "text", "label": "Nazwa", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole Nazwa jest wymagane" }], "desc": null, "id": "name" }], [{ "type": "currency", "label": "Cena netto", "isObligatory": false, "desc": null, "id": "nettoPricePerUnit" }], [{ "type": "currency", "label": "Cena brutto", "isObligatory": false, "desc": null, "id": "bruttoPricePerUnit" }], [{ "type": "numeric", "label": "Ilość", "isObligatory": false, "desc": null, "id": "quantity" }], [{ "type": "currency", "label": "Cena", "isObligatory": false, "desc": null, "id": "price" }]] }] }
)

export const notificationConfig = () => (
  { "create": { "formTitle": "Dodaj notyfikację", "errorMessage": "Error during add notification", "gqlType": "versionedNotifications_insert_input", "gqlMutation": "insertNotification", "successRedirect": "/", "abortRedirect": "/" }, "objectDef": { "objectName": "notification", "objectTable": "notifications", "idKey": "id" }, "formSections": [{ "header": "Podstawowe", "fields": [[{ "type": "timestamp with time zone", "label": "Data przypomnienia", "isObligatory": false, "desc": null, "id": "reminderDate" }], [{ "type": "objectDropdown", "label": "Okres poprzedzenia", "isObligatory": false, "desc": null, "id": "precedingPeriod", "create": { "type": null, "title": null }, "objectDef": { "singularName": "precedingPeriod", "pluralName": "precedingPeriods", "idKey": "id", "descriptorData": ["name"] } }]] }] }
)

export const personConfig = () => (
  { "create": { "formTitle": "Dodaj osobę", "errorMessage": "Error during add person", "gqlType": "versionedPeople_insert_input", "gqlMutation": "insertPerson", "successRedirect": "/", "abortRedirect": "/" }, "objectDef": { "objectName": "person", "objectTable": "people", "idKey": "id" }, "formSections": [{ "header": "Podstawowe", "fields": [[{ "type": "text", "label": "Imie", "isObligatory": false, "desc": null, "id": "firstname" }], [{ "type": "text", "label": "Nazwisko", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole Nazwisko jest wymagane" }], "desc": null, "id": "surname" }], [{ "type": "text", "label": "Pseudonim", "isObligatory": false, "desc": null, "id": "nickname" }], [{ "type": "text", "label": "Email", "isObligatory": false, "desc": null, "id": "email" }], [{ "type": "text", "label": "Numer telefonu", "isObligatory": false, "desc": null, "id": "phone" }], [{ "type": "text", "label": "Numer PESEL", "isObligatory": false, "desc": null, "id": "pesel" }], [{ "type": "text", "label": "Notatka", "isObligatory": false, "desc": null, "id": "notes" }]] }] }
)

export const precedingPeriodConfig = () => (
  { "create": { "formTitle": "", "errorMessage": "Error during add preceding period", "gqlType": "versionedPrecedingPeriods_insert_input", "gqlMutation": "insertPrecedingPeriod", "successRedirect": "/", "abortRedirect": "/" }, "objectDef": { "objectName": "precedingPeriod", "objectTable": "preceding_periods", "idKey": "id" }, "formSections": [{ "header": "Podstawowe", "fields": [[{ "type": "text", "label": "name", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole name jest wymagane" }], "desc": null, "id": "name" }], [{ "type": "integer", "label": "months_before", "isObligatory": false, "desc": null, "id": "monthsBefore" }], [{ "type": "integer", "label": "weeks_before", "isObligatory": false, "desc": null, "id": "weeksBefore" }]] }] }
)

export const propertyConfig = () => (
  {
    "create": {
      "formTitle": "Dodaj nieruchomość",
      "errorMessage": "Error during add property",
      "gqlType": "versionedProperties_insert_input",
      "gqlMutation": "insertProperty",
      "successRedirect": "/",
      "abortRedirect": "/"
    },
    "objectDef": {
      "objectName": "property",
      "objectTable": "properties",
      "idKey": "id"
    },
    "formSections": [{
      "header": "Podstawowe",
      "fields": [[{
        "type": "text",
        "label": "Nazwa nieruchomości",
        "isObligatory": true,
        "errors": [{
          "check": (x) => !x,
          "message": "Nieruchomość musi posiadać nazwę"
        }],
        "desc": "Ta nazwa będzie używana tylko wewnętrznie w aplikacji. Może to być np. ulica + miasto lub nazwisko właściciela.",
        "id": "name"
      }],
      [{
        "type": "objectDropdown",
        "label": "Właściciel nieruchomości",
        "isObligatory": false,
        "desc": null,
        "id": "owner",
        "create": {
          "type": "slideOver",
          "title": "Dodaj właściciela nieruchomości"
        },
        "objectDef": {
          "singularName": "person",
          "pluralName": "people",
          "idKey": "id",
          "descriptorData": ["firstname",
            "surname"]
        }
      }],
      [{
        "type": "objectDropdown",
        "label": "Administrator nieruchomości",
        "isObligatory": false,
        "desc": null,
        "id": "buildingAdministrator",
        "create": {
          "type": "slideOver",
          "title": "Dodaj administratora budynku"
        },
        "objectDef": {
          "singularName": "company",
          "pluralName": "companies",
          "idKey": "id",
          "descriptorData": ["name"]
        }
      }],
      [{
        "type": "text",
        "label": "Powierzchnia mieszkania",
        "isObligatory": false,
        "desc": null,
        "id": "flatArea"
      }],
      [{
        "type": "text",
        "label": "Numer księgi wieczystej",
        "isObligatory": false,
        "desc": null,
        "id": "landRegisterNumber"
      }],
      [{
        "type": "text",
        "label": "Numer polisy",
        "isObligatory": false,
        "desc": null,
        "id": "policyNumber"
      }],
      [{
        "type": "text",
        "label": "Kod wejścia posiadłości",
        "isObligatory": false,
        "desc": null,
        "id": "estateEntranceCode"
      }],
      [{
        "type": "text",
        "label": "Kod wejścia budynku",
        "isObligatory": false,
        "desc": null,
        "id": "buildingEntranceCode"
      }],
      [{
        "type": "text",
        "label": "Numer miejsca garażowego",
        "isObligatory": false,
        "desc": null,
        "id": "garagePlace"
      }],
      [{
        "type": "textArea",
        "label": "Notatka",
        "isObligatory": false,
        "desc": null,
        "id": "notes"
      }]]
    },
    {
      "header": "Adres",
      "fields": [[{
        "type": "text",
        "label": "Ulica",
        "isObligatory": false,
        "desc": null,
        "id": "address/data/street"
      }],
      [{
        "type": "text",
        "label": "Numer budynku",
        "isObligatory": false,
        "desc": null,
        "id": "address/data/streetNumber"
      }],
      [{
        "type": "text",
        "label": "Numer mieszkania",
        "isObligatory": false,
        "desc": null,
        "id": "address/data/apartmentNumber"
      }],
      [{
        "type": "text",
        "label": "Kod pocztowy",
        "isObligatory": false,
        "desc": null,
        "id": "address/data/postalCode"
      }],
      [{
        "type": "text",
        "label": "Miasto",
        "isObligatory": false,
        "desc": null,
        "id": "address/data/city"
      }],
      [{
        "type": "text",
        "label": "Region",
        "isObligatory": false,
        "desc": null,
        "id": "address/data/region"
      }],
      [{
        "type": "text",
        "label": "Kraj",
        "isObligatory": false,
        "desc": null,
        "id": "address/data/country"
      }]]
    },
    {
      "header": "Dane logowania do portali",
      "type": "inline-creator",
      "objectsKey": "portalLogins/data",
      "fields": [[{
        "type": "text",
        "label": "Typ portalu",
        "isObligatory": true,
        "errors": [{
          "check": (x) => !x,
          "message": "Pole Typ portalu jest wymagane"
        }],
        "desc": null,
        "id": "portalType"
      }],
      [{
        "type": "text",
        "label": "Adres internetowy portalu",
        "isObligatory": false,
        "desc": null,
        "id": "webAddress"
      }],
      [{
        "type": "text",
        "label": "Login",
        "isObligatory": false,
        "desc": null,
        "id": "login"
      }],
      [{
        "type": "text",
        "label": "Hasło",
        "isObligatory": false,
        "desc": null,
        "id": "password"
      }]]
    }]
  }
)

export const taskConfig = () => (
  { "create": { "formTitle": "Dodaj zadanie", "errorMessage": "Error during add task", "gqlType": "versionedTasks_insert_input", "gqlMutation": "insertTask", "successRedirect": "/", "abortRedirect": "/" }, "objectDef": { "objectName": "task", "objectTable": "tasks", "idKey": "id" }, "formSections": [{ "header": "Podstawowe", "fields": [[{ "type": "text", "label": "Nazwa zadania", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole Nazwa zadania jest wymagane" }], "desc": null, "id": "name" }], [{ "type": "currency", "label": "Koszt zadania", "isObligatory": false, "desc": null, "id": "cost" }], [{ "type": "timestamp with time zone", "label": "Data wykonania zadania", "isObligatory": false, "desc": null, "id": "executionDate" }], [{ "type": "objectDropdown", "label": "Odpowiedzialna osoba", "isObligatory": false, "desc": null, "id": "reponsiblePerson", "create": { "type": null, "title": null }, "objectDef": { "singularName": "person", "pluralName": "people", "idKey": "id", "descriptorData": ["firstname", "surname"] } }], [{ "type": "objectDropdown", "label": "notification", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole notification jest wymagane" }], "desc": null, "id": "notification", "create": { "type": "Nothing", "title": null }, "objectDef": { "singularName": "notification", "pluralName": "notifications", "idKey": "id", "descriptorData": ["name"] } }], [{ "type": "boolean", "label": "is_done", "isObligatory": false, "desc": null, "id": "isDone" }], [{ "type": "JSONB", "label": "config", "isObligatory": false, "desc": null, "id": "config" }], [{ "type": "text", "label": "decision", "isObligatory": false, "desc": null, "id": "decision" }]] }] }
)

export const contractCategoryConfig = () => (
  { "create": { "formTitle": "", "errorMessage": "Error during add contract category", "gqlType": "versionedContractCategories_insert_input", "gqlMutation": "insertContractCategory", "successRedirect": "/", "abortRedirect": "/" }, "objectDef": { "objectName": "contractCategory", "objectTable": "contract_categories", "idKey": "id" }, "formSections": [{ "header": "Podstawowe", "fields": [[{ "type": "text", "label": "key", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole key jest wymagane" }], "desc": null, "id": "key" }], [{ "type": "text", "label": "label", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole label jest wymagane" }], "desc": null, "id": "label" }]] }] }
)

export const recurrenceIntervalConfig = () => (
  { "create": { "formTitle": "", "errorMessage": "Error during add recurrence interval", "gqlType": "versionedRecurrenceIntervals_insert_input", "gqlMutation": "insertRecurrenceInterval", "successRedirect": "/", "abortRedirect": "/" }, "objectDef": { "objectName": "recurrenceInterval", "objectTable": "recurrence_intervals", "idKey": "id" }, "formSections": [{ "header": "Podstawowe", "fields": [[{ "type": "text", "label": "name", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole name jest wymagane" }], "desc": null, "id": "name" }], [{ "type": "integer", "label": "months_interval", "isObligatory": false, "desc": null, "id": "monthsInterval" }]] }] }
)

export const urgencyLevelConfig = () => (
  { "create": { "formTitle": "", "errorMessage": "Error during add urgency level", "gqlType": "versionedUrgencyLevels_insert_input", "gqlMutation": "insertUrgencyLevel", "successRedirect": "/", "abortRedirect": "/" }, "objectDef": { "objectName": "urgencyLevel", "objectTable": "urgency_levels", "idKey": "id" }, "formSections": [{ "header": "Podstawowe", "fields": [[{ "type": "text", "label": "name", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole name jest wymagane" }], "desc": null, "id": "name" }], [{ "type": "text", "label": "color", "isObligatory": false, "desc": null, "id": "color" }], [{ "type": "text", "label": "icon", "isObligatory": false, "desc": null, "id": "icon" }], [{ "type": "text", "label": "key", "isObligatory": false, "desc": null, "id": "key" }]] }] }
)

export const fixResultConfig = () => (
  { "create": { "formTitle": "Stwórz rezultat naprawy", "errorMessage": "Error during add fix result", "gqlType": "versionedFixResults_insert_input", "gqlMutation": "insertFixResult", "successRedirect": "/", "abortRedirect": "/" }, "objectDef": { "objectName": "fixResult", "objectTable": "fix_results", "idKey": "id" }, "formSections": [{ "header": "Podstawowe", "fields": [[{ "type": "timestamp with time zone", "label": "Data", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole Data jest wymagane" }], "desc": null, "id": "date" }], [{ "type": "currency", "label": "Koszt", "isObligatory": false, "desc": null, "id": "cost" }], [{ "type": "objectDropdown", "label": "", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole  jest wymagane" }], "desc": null, "id": "defect", "create": { "type": null, "title": null }, "objectDef": { "singularName": "defect", "pluralName": "defects", "idKey": "id", "descriptorData": ["name"] } }], [{ "type": "objectDropdown", "label": "Serwisant", "isObligatory": false, "desc": null, "id": "serviceman", "create": { "type": "slideOver", "title": null }, "objectDef": { "singularName": "person", "pluralName": "people", "idKey": "id", "descriptorData": ["firstname", "surname"] } }]] }] }
)

export const utilityTypeConfig = () => (
  { "create": { "formTitle": "", "errorMessage": "Error during add utility type", "gqlType": "versionedUtilityTypes_insert_input", "gqlMutation": "insertUtilityType", "successRedirect": "/", "abortRedirect": "/" }, "objectDef": { "objectName": "utilityType", "objectTable": "utility_types", "idKey": "id" }, "formSections": [{ "header": "Podstawowe", "fields": [[{ "type": "text", "label": "key", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole key jest wymagane" }], "desc": null, "id": "key" }], [{ "type": "text", "label": "icon", "isObligatory": false, "desc": null, "id": "icon" }], [{ "type": "text", "label": "label", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole label jest wymagane" }], "desc": null, "id": "label" }], [{ "type": "text", "label": "measurement_unit", "isObligatory": false, "desc": null, "id": "measurementUnit" }]] }] }
)

export const contractObligationConfig = () => (
  { "create": { "formTitle": "", "errorMessage": "Error during add contract obligation", "gqlType": "versionedContractObligations_insert_input", "gqlMutation": "insertContractObligation", "successRedirect": "/", "abortRedirect": "/" }, "objectDef": { "objectName": "contractObligation", "objectTable": "contract_obligations", "idKey": "id" }, "formSections": [{ "header": "Podstawowe", "fields": [[{ "type": "objectDropdown", "label": "Typ zobowiązania", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole Typ zobowiązania jest wymagane" }], "desc": null, "id": "obligationType", "create": { "type": null, "title": null }, "objectDef": { "singularName": "contractObligationType", "pluralName": "contractObligationTypes", "idKey": "id", "descriptorData": ["label"] } }], [{ "type": "numeric", "label": "Ilość opłaconych jednostek", "isObligatory": false, "desc": null, "id": "paidUnits" }], [{ "type": "currency", "label": "Kwota zobowiązania", "isObligatory": false, "desc": null, "id": "financialObligation" }], [{ "type": "objectDropdown", "label": "contract", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole contract jest wymagane" }], "desc": null, "id": "contract", "create": { "type": null, "title": null }, "objectDef": { "singularName": "contract", "pluralName": "contracts", "idKey": "id", "descriptorData": ["signer", "type", "startDate"] } }]] }] }
)

export const contractObligationTypeConfig = () => (
  { "create": { "formTitle": "", "errorMessage": "Error during add contract obligation type", "gqlType": "versionedContractObligationTypes_insert_input", "gqlMutation": "insertContractObligationType", "successRedirect": "/", "abortRedirect": "/" }, "objectDef": { "objectName": "contractObligationType", "objectTable": "contract_obligation_types", "idKey": "id" }, "formSections": [{ "header": "Podstawowe", "fields": [[{ "type": "text", "label": "key", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole key jest wymagane" }], "desc": null, "id": "key" }], [{ "type": "text", "label": "label", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole label jest wymagane" }], "desc": null, "id": "label" }], [{ "type": "objectDropdown", "label": "related_utility_type", "isObligatory": false, "desc": null, "id": "relatedUtilityType", "create": { "type": null, "title": null }, "objectDef": { "singularName": "utilityType", "pluralName": "utilityTypes", "idKey": "id", "descriptorData": ["label"] } }], [{ "type": "boolean", "label": "is_unit_based", "isObligatory": false, "desc": null, "id": "isUnitBased" }]] }] }
)

export const messageConfig = () => (
  { "create": { "formTitle": "", "errorMessage": "Error during add message", "gqlType": "versionedMessages_insert_input", "gqlMutation": "insertMessage", "successRedirect": "/", "abortRedirect": "/" }, "objectDef": { "objectName": "message", "objectTable": "messages", "idKey": "id" }, "formSections": [{ "header": "Podstawowe", "fields": [[{ "type": "textArea", "label": "content", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole content jest wymagane" }], "desc": null, "id": "content" }], [{ "type": "text", "label": "subject", "isObligatory": false, "desc": null, "id": "subject" }], [{ "type": "objectDropdown", "label": "author", "isObligatory": false, "desc": null, "id": "author", "create": { "type": null, "title": null }, "objectDef": { "singularName": "person", "pluralName": "people", "idKey": "id", "descriptorData": ["firstname", "surname"] } }]] }] }
)

export const mediaFileConfig = () => (
  { "create": { "formTitle": "", "errorMessage": "Error during add media file", "gqlType": "versionedMediaFiles_insert_input", "gqlMutation": "insertMediaFile", "successRedirect": "/", "abortRedirect": "/" }, "objectDef": { "objectName": "mediaFile", "objectTable": "media_files", "idKey": "id" }, "formSections": [{ "header": "Podstawowe", "fields": [[{ "type": "text", "label": "extension", "isObligatory": false, "desc": null, "id": "extension" }], [{ "type": "text", "label": "name", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole name jest wymagane" }], "desc": null, "id": "name" }], [{ "type": "boolean", "label": "has_upload_finished", "isObligatory": false, "desc": null, "id": "hasUploadFinished" }], [{ "type": "text", "label": "aws_link", "isObligatory": false, "desc": null, "id": "awsLink" }]] }] }
)

export const mediaAttachedToDefectConfig = () => (
  { "create": { "formTitle": "", "errorMessage": "Error during add media attached to defect", "gqlType": "versionedMediaAttachedToDefects_insert_input", "gqlMutation": "insertMediaAttachedToDefect", "successRedirect": "/", "abortRedirect": "/" }, "objectDef": { "objectName": "mediaAttachedToDefect", "objectTable": "media_attached_to_defects", "idKey": "id" }, "formSections": [{ "header": "Podstawowe", "fields": [[{ "type": "objectDropdown", "label": "media_file", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole media_file jest wymagane" }], "desc": null, "id": "mediaFile", "create": { "type": null, "title": null }, "objectDef": { "singularName": "mediaFile", "pluralName": "mediaFiles", "idKey": "id", "descriptorData": ["name"] } }], [{ "type": "objectDropdown", "label": "defect", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole defect jest wymagane" }], "desc": null, "id": "defect", "create": { "type": null, "title": null }, "objectDef": { "singularName": "defect", "pluralName": "defects", "idKey": "id", "descriptorData": ["name"] } }]] }] }
)

export const appointmentConfig = () => (
  { "create": { "formTitle": "Dodaj wizytę specjalisty", "errorMessage": "Error during add appointment", "gqlType": "versionedAppointments_insert_input", "gqlMutation": "insertAppointment", "successRedirect": "/", "abortRedirect": "/" }, "objectDef": { "objectName": "appointment", "objectTable": "appointments", "idKey": "id" }, "formSections": [{ "header": "Podstawowe", "fields": [[{ "type": "timestamp with time zone", "label": "Data wizyty", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole Data wizyty jest wymagane" }], "desc": null, "id": "date" }], [{ "type": "objectDropdown", "label": "Umówiona firma", "isObligatory": false, "desc": null, "id": "attendeeCompany", "create": { "type": "slideOver", "title": null }, "objectDef": { "singularName": "company", "pluralName": "companies", "idKey": "id", "descriptorData": ["name"] } }], [{ "type": "objectDropdown", "label": "Umówiona osoba", "isObligatory": false, "desc": null, "id": "attendeePerson", "create": { "type": "slideOver", "title": null }, "objectDef": { "singularName": "person", "pluralName": "people", "idKey": "id", "descriptorData": ["firstname", "surname"] } }], [{ "type": "currency", "label": "Planowany koszt", "isObligatory": false, "desc": null, "id": "plannedCost" }], [{ "type": "textArea", "label": "Notatka", "isObligatory": false, "desc": null, "id": "notes" }], [{ "type": "objectDropdown", "label": "notification", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole notification jest wymagane" }], "desc": null, "id": "notification", "create": { "type": null, "title": null }, "objectDef": { "singularName": "notification", "pluralName": "notifications", "idKey": "id", "descriptorData": ["name"] } }]] }] }
)

export const checkResultConfig = () => (
  {
    "create": {
      "formTitle": "Dodaj rezultat przeglądu",
      "errorMessage": "Error during add check result",
      "gqlType": "versionedCheckResults_insert_input",
      "gqlMutation": "insertCheckResult",
      "successRedirect": "/",
      "abortRedirect": "/"
    },
    "objectDef": {
      "objectName": "checkResult",
      "objectTable": "check_results",
      "idKey": "id"
    },
    "formSections": [{
      "header": "Podstawowe",
      "fields": [
        [{
          "type": "timestamp with time zone",
          "label": "Data przeglądu",
          "isObligatory": true,
          "errors": [{
            "check": (x) => !x,
            "message": "Pole Data przeglądu jest wymagane"
          }],
          "desc": null,
          "id": "date"
        }],
        [{
          "type": "currency",
          "label": "Koszt przeglądu",
          "isObligatory": false,
          "desc": null,
          "id": "cost"
        }],
        [{
          "type": "objectDropdown",
          "label": "check",
          "isObligatory": false,
          "desc": null,
          "id": "check",
          "create": {
            "type": null,
            "title": null
          },
          "objectDef": {
            "singularName": "check",
            "pluralName": "checks",
            "idKey": "id",
            "descriptorData": ["name"]
          }
        }],
        [{
          "type": "objectDropdown",
          "label": "parent_notification",
          "isObligatory": false,
          "desc": null,
          "id": "parentNotification",
          "create": {
            "type": null,
            "title": null
          },
          "objectDef": {
            "singularName": "notification",
            "pluralName": "notifications",
            "idKey": "id",
            "descriptorData": ["name"]
          }
        }]]
    },
    {
      "header": "Notyfikacja",
      "fields": [[{
        "type": "timestamp with time zone",
        "label": "Data przypomnienia",
        "isObligatory": false,
        "desc": null,
        "id": "nextCheckNotification/data/reminderDate"
      }],
      [{
        "type": "objectDropdown",
        "label": "Okres poprzedzenia",
        "isObligatory": false,
        "desc": null,
        "id": "nextCheckNotification/data/precedingPeriod",
        "create": {
          "type": null,
          "title": null
        },
        "objectDef": {
          "singularName": "precedingPeriod",
          "pluralName": "precedingPeriods",
          "idKey": "id",
          "descriptorData": ["name"]
        }
      }]]
    },
    {
      "header": "Wynikowe usterki",
      "type": "inline-creator",
      "objectsKey": "defects/data",
      "fields": [[{
        "type": "text",
        "label": "Opis uszkodzenia",
        "isObligatory": true,
        "errors": [{
          "check": (x) => !x,
          "message": "Pole Opis uszkodzenia jest wymagane"
        }],
        "desc": null,
        "id": "name"
      }],
      [{
        "type": "textArea",
        "label": "Notatka",
        "isObligatory": false,
        "desc": null,
        "id": "notes"
      }],
      [{
        "type": "boolean",
        "label": "Wymaga zewnętrznego specjalisty",
        "isObligatory": false,
        "desc": null,
        "id": "isForContractor"
      }],
      [{
        "type": "objectDropdown",
        "label": "Pilność",
        "isObligatory": true,
        "errors": [{
          "check": (x) => !x,
          "message": "Pole Pilność jest wymagane"
        }],
        "desc": null,
        "id": "urgency",
        "create": {
          "type": null,
          "title": null
        },
        "objectDef": {
          "singularName": "urgencyLevel",
          "pluralName": "urgencyLevels",
          "idKey": "id",
          "descriptorData": ["name"]
        }
      }],
      [{
        "type": "objectDropdown",
        "label": "property",
        "isObligatory": true,
        "errors": [{
          "check": (x) => !x,
          "message": "Pole property jest wymagane"
        }],
        "desc": null,
        "id": "property",
        "create": {
          "type": null,
          "title": null
        },
        "objectDef": {
          "singularName": "property",
          "pluralName": "properties",
          "idKey": "id",
          "descriptorData": ["name"]
        }
      }],
      [{
        "type": "textArea",
        "label": "Wiadomość zgłoszeniowa",
        "isObligatory": false,
        "desc": null,
        "id": "reportingMessage"
      }],
      [{
        "type": "timestamp with time zone",
        "label": "Data przypomnienia",
        "isObligatory": false,
        "desc": null,
        "id": "notification/data/reminderDate"
      },
      {
        "type": "objectDropdown",
        "label": "Okres poprzedzenia",
        "isObligatory": false,
        "desc": null,
        "id": "notification/data/precedingPeriod",
        "create": {
          "type": null,
          "title": null
        },
        "objectDef": {
          "singularName": "precedingPeriod",
          "pluralName": "precedingPeriods",
          "idKey": "id",
          "descriptorData": ["name"]
        }
      }],
      [{
        "type": "currency",
        "label": "Szacowany koszt",
        "isObligatory": false,
        "desc": null,
        "id": "estimatedCost"
      }]]
    }]
  }
)

export const notificationStateConfig = () => (
  {
    "create": {
      "formTitle": "",
      "errorMessage": "Error during add notification state",
      "gqlType": "versionedNotificationStates_insert_input",
      "gqlMutation": "insertNotificationState",
      "successRedirect": "/",
      "abortRedirect": "/"
    },
    "objectDef": { "objectName": "notificationState", "objectTable": "notification_states", "idKey": "id" }, "formSections": [{ "header": "Podstawowe", "fields": [[{ "type": "text", "label": "key", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole key jest wymagane" }], "desc": null, "id": "key" }]] }]
  }
)

export const decisionConfig = () => (
  { "create": { "formTitle": "", "errorMessage": "Error during add decision", "gqlType": "versionedDecisions_insert_input", "gqlMutation": "insertDecision", "successRedirect": "/", "abortRedirect": "/" }, "objectDef": { "objectName": "decision", "objectTable": "decisions", "idKey": "id" }, "formSections": [{ "header": "Podstawowe", "fields": [[{ "type": "text", "label": "name", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole name jest wymagane" }], "desc": null, "id": "name" }], [{ "type": "objectDropdown", "label": "notification", "isObligatory": false, "desc": null, "id": "notification", "create": { "type": null, "title": null }, "objectDef": { "singularName": "notification", "pluralName": "notifications", "idKey": "id", "descriptorData": ["name"] } }], [{ "type": "text", "label": "key", "isObligatory": false, "desc": null, "id": "key" }]] }] }
)

export const paymentGroupMeterPeriodConfig = () => (
  { "create": { "formTitle": "", "errorMessage": "Error during add payment group meter period", "gqlType": "versionedPaymentGroupMeterPeriods_insert_input", "gqlMutation": "insertPaymentGroupMeterPeriod", "successRedirect": "/", "abortRedirect": "/" }, "objectDef": { "objectName": "paymentGroupMeterPeriod", "objectTable": "payment_group_meter_periods", "idKey": "id" }, "formSections": [{ "header": "Podstawowe", "fields": [[{ "type": "timestamp with time zone", "label": "start_date", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole start_date jest wymagane" }], "desc": null, "id": "startDate" }], [{ "type": "timestamp with time zone", "label": "end_date", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole end_date jest wymagane" }], "desc": null, "id": "endDate" }], [{ "type": "objectDropdown", "label": "payer", "isObligatory": false, "desc": null, "id": "payer", "create": { "type": null, "title": null }, "objectDef": { "singularName": "person", "pluralName": "people", "idKey": "id", "descriptorData": ["firstname", "surname"] } }], [{ "type": "currency", "label": "sum_amout", "isObligatory": true, "errors": [{ "check": (x) => !x, "message": "Pole sum_amout jest wymagane" }], "desc": null, "id": "sumAmout" }]] }] }
)

export const config = {
  address: addressConfig,
  check: checkConfig,
  company: companyConfig,
  contractType: contractTypeConfig,
  contract: contractConfig,
  defect: defectConfig,
  employee: employeeConfig,
  guarantee: guaranteeConfig,
  meterPeriodPriceIngredient: meterPeriodPriceIngredientConfig,
  meterPeriod: meterPeriodConfig,
  notification: notificationConfig,
  person: personConfig,
  precedingPeriod: precedingPeriodConfig,
  property: propertyConfig,
  task: taskConfig,
  contractCategory: contractCategoryConfig,
  recurrenceInterval: recurrenceIntervalConfig,
  urgencyLevel: urgencyLevelConfig,
  fixResult: fixResultConfig,
  utilityType: utilityTypeConfig,
  contractObligation: contractObligationConfig,
  contractObligationType: contractObligationTypeConfig,
  message: messageConfig,
  mediaFile: mediaFileConfig,
  mediaAttachedToDefect: mediaAttachedToDefectConfig,
  appointment: appointmentConfig,
  checkResult: checkResultConfig,
  notificationState: notificationStateConfig,
  decision: decisionConfig,
  paymentGroupMeterPeriod: paymentGroupMeterPeriodConfig,
}
