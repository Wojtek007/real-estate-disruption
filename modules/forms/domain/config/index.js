import { config } from "./domain-config";
import { extension, extensionDictionary } from "./domain-extensions";

export const getFormSections = ({ objectName }) => {
  const res = extension(objectName, config[objectName]().formSections);

  return res;
};
export const getCreationFormTitle = ({ objectName, state }) => {

  console.log('ssssss', extensionDictionary[objectName], extensionDictionary[objectName]?.__GENERAL__?.formTitle(state))
  return extensionDictionary[objectName]?.__GENERAL__?.formTitle(state) ??
    config[objectName]().create.formTitle;
}
export const getEditFormTitle = ({ objectName, state }) =>
  (extensionDictionary[objectName]?.__GENERAL__?.formTitle(state) ??
    config[objectName]().create.formTitle)?.replace("Dodaj", "Edytuj");

export const getObjectConfig = ({ objectName }) => {
  console.log('camelObjectName - config', objectName)
  return ({
    ...config[objectName]().create,
    ...config[objectName]().objectDef,
  })
};
