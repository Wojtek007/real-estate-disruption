import Select from "react-select";
import { useState } from "react";

import { useSubscription } from "@apollo/react-hooks";
import gql from "graphql-tag";
import SlideOver from "../components/slide-over-template";
import { PlusIcon as PlusIconSolid } from "@heroicons/react/solid";
import FormBuilder from "../create-form-slider";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function Example({
  label,
  key,
  objectId,
  isObligatory,
  onChange,
  value,
  autoComplete,
  desc,
  create,
  objectDef,
  error,
}) {
  console.log("objectDef", label, key, objectDef, objectId);
  const { pluralName, descriptorData, idKey, descriptor } = objectDef;

  const SUB_OBJECTS_LIST = gql`
  subscription MySubscription1235345 {
    ${pluralName}(order_by: { ${descriptorData[0]}: asc }) {
      id
      ver
      ${descriptorData.join(" ")}
    }
  }
`;

  const [isSlideOverOpen, setIsSlideOverOpen] = useState(false);
  const {
    loading,
    error: fetchError,
    data,
  } = useSubscription(SUB_OBJECTS_LIST);
  console.log("isSlideOverOpen", isSlideOverOpen);
  console.log(label, "loading, fetchError, data", loading, fetchError, data);

  console.log(
    "REQUEST",
    `
  subscription MySubscription134545 {
    ${pluralName}(order_by: { ${descriptorData[0]}: asc }) {
      id
      ver
      ${descriptorData.join(" ")}
    }
  }
`
  );

  return (
    <div>
      {isSlideOverOpen && (
        <div className="filter blur-sm h-screen w-full fixed top-0 left-0 bg-gray-100 backdrop-blur-lg bg-opacity-70 z-10"></div>
      )}
      <SlideOver
        isOpen={isSlideOverOpen}
        closePanel={() => setIsSlideOverOpen(false)}
        title={create?.title}
      >
        {create && (
          <FormBuilder
            objectName={objectDef.singularName}
            onSuccess={({ id, ver }) => {
              onChange(`${objectId}Id`, id);
              onChange(`${objectId}Ver`, ver);
              setIsSlideOverOpen(false);
            }}
            onCancel={() => setIsSlideOverOpen(false)}
          />
        )}
      </SlideOver>
      <label
        htmlFor={objectId}
        className="block text-sm font-medium text-gray-700"
      >
        {label}
      </label>
      {(() => {
        if (loading) {
          return <div>Wczytuję opcje...</div>;
        }
        if (fetchError) {
          console.error(fetchError);
          return (
            <div>Błąd! {fetchError?.message ?? JSON.stringify(fetchError)}</div>
          );
        }

        const options = data[pluralName].map((option) => ({
          value: option[idKey],
          label: descriptorData.map((wordKey) => option[wordKey]).join(" "), //descriptor(option),
        }));

        return (
          <div className="flex ">
            <Select
              id={objectId}
              name={objectId}
              autoComplete={autoComplete}
              isClearable={!isObligatory}
              className={classNames(
                "flex-grow",
                error &&
                "border-red-300 text-red-900 placeholder-red-300 focus:border-red-300 focus:shadow-outline-red"
              )}
              //   menuPosition="fixed"
              onChange={(option) => {
                onChange(`${objectId}Id`, option?.value);
                const ver = data[pluralName].find((o) => {
                  return o?.id === option?.value;
                })?.ver;

                onChange(`${objectId}Ver`, ver);
              }}
              value={options.find((option) => option.value === value)}
              options={options}
            //   styles={{
            //     menuPortal: (base) => ({
            //       // this just updates the menuPortal style to trigger a rerender
            //       top: selectTopPosition,
            //       ...base,
            //     }),
            //     // input: base => ({
            //     //     ...base,
            //     //     borderColor: '#feb2b2',
            //     // })
            //   }}
            //   closeMenuOnScroll={handleCloseMenuOnScroll}
            //   isMulti={isMulti}
            />
            {create.type === "slideOver" && (
              <button
                type="button"
                onClick={() => setIsSlideOverOpen(true)}
                className="ml-2 inline-flex items-center p-2 border border-transparent rounded-full shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              >
                <PlusIconSolid className="h-5 w-5" aria-hidden="true" />
              </button>
            )}
          </div>
        );
      })()}

      {desc && <p className="mt-2 text-sm text-gray-500">{desc}</p>}
      {(error || fetchError) && (
        <p className="mt-2 text-sm text-red-600" id={`${key}-error`}>
          {error?.message || fetchError?.message}
        </p>
      )}
    </div>
  );
}
