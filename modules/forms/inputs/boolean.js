import { ExclamationCircleIcon } from "@heroicons/react/solid";

// export default function Example({
//   label,
//   key,
//   onChange,
//   value,
//   autoComplete,
//   desc,
//   error,
// }) {
//   const regularStyle =
//     "mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md";
//   const errorStyle =
//     "block w-full pr-10 border-red-300 text-red-900 placeholder-red-300 focus:outline-none focus:ring-red-500 focus:border-red-500 sm:text-sm rounded-md";

//   return (
//     <div>
//       <label htmlFor={key} className="block text-sm font-medium text-gray-700">
//         {label}
//       </label>
//       <div className="mt-1 relative rounded-md shadow-sm">
//         <input
//           type="text"
//           name={key}
//           id={key}
//           autoComplete={autoComplete}
//           onChange={(e) => onChange(e.target.value)}
//           value={value}
//           className={error ? errorStyle : regularStyle}
//           aria-invalid={!!error}
//           aria-describedby={`${key}-error`}
//         />
//         {error && (
//           <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
//             <ExclamationCircleIcon
//               className="h-5 w-5 text-red-500"
//               aria-hidden="true"
//             />
//           </div>
//         )}
//       </div>
//       {desc && <p className="mt-2 text-sm text-gray-500">{desc}</p>}
//       {error && (
//         <p className="mt-2 text-sm text-red-600" id={`${key}-error`}>
//           {error.message}
//         </p>
//       )}
//     </div>
//   );
// }
/* This example requires Tailwind CSS v2.0+ */
import { useState } from "react";
import { Switch } from "@headlessui/react";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function Example({
  label,
  key,
  onChange: setEnabled,
  value: enabled,
  desc,
  error,
}) {
  return (
    <>
      <Switch.Group as="div" className="flex items-center">
        <Switch
          checked={enabled}
          onChange={setEnabled}
          className={classNames(
            enabled ? "bg-indigo-600" : "bg-gray-200",
            "relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
          )}
        >
          <span className="sr-only">Use setting</span>
          <span
            className={classNames(
              enabled ? "translate-x-5" : "translate-x-0",
              "pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200"
            )}
          >
            <span
              className={classNames(
                enabled
                  ? "opacity-0 ease-out duration-100"
                  : "opacity-100 ease-in duration-200",
                "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity"
              )}
              aria-hidden="true"
            >
              <svg
                className="h-3 w-3 text-gray-400"
                fill="none"
                viewBox="0 0 12 12"
              >
                <path
                  d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                  stroke="currentColor"
                  strokeWidth={2}
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
              </svg>
            </span>
            <span
              className={classNames(
                enabled
                  ? "opacity-100 ease-in duration-200"
                  : "opacity-0 ease-out duration-100",
                "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity"
              )}
              aria-hidden="true"
            >
              <svg
                className="h-3 w-3 text-indigo-600"
                fill="currentColor"
                viewBox="0 0 12 12"
              >
                <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
              </svg>
            </span>
          </span>
        </Switch>
        <Switch.Label as="span" className="ml-3">
          <span className="text-sm font-medium text-gray-900">{label}</span>
          {/* <span className="text-sm text-gray-500">(Save 10%)</span> */}
        </Switch.Label>
      </Switch.Group>
      {desc && <p className="mt-2 text-sm text-gray-500">{desc}</p>}
      {error && (
        <p className="mt-2 text-sm text-red-600" id={`${key}-error`}>
          {error.message}
        </p>
      )}
    </>
  );
}
