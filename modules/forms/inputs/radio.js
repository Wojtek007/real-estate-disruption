export default function Example() {
    return (
        <fieldset>
            <div>
                <legend className="text-base font-medium text-gray-900">Push Notifications</legend>
                <p className="text-sm text-gray-500">These are delivered via SMS to your mobile phone.</p>
            </div>
            <div className="mt-4 space-y-4">
                <div className="flex items-center">
                    <input
                        id="push_everything"
                        name="push_notifications"
                        type="radio"
                        className="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300"
                    />
                    <label htmlFor="push_everything" className="ml-3 block text-sm font-medium text-gray-700">
                        Everything
                    </label>
                </div>
                <div className="flex items-center">
                    <input
                        id="push_email"
                        name="push_notifications"
                        type="radio"
                        className="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300"
                    />
                    <label htmlFor="push_email" className="ml-3 block text-sm font-medium text-gray-700">
                        Same as email
                    </label>
                </div>
                <div className="flex items-center">
                    <input
                        id="push_nothing"
                        name="push_notifications"
                        type="radio"
                        className="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300"
                    />
                    <label htmlFor="push_nothing" className="ml-3 block text-sm font-medium text-gray-700">
                        No push notifications
                    </label>
                </div>
            </div>
        </fieldset>
    );
}
