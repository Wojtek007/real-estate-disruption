import { ExclamationCircleIcon } from "@heroicons/react/solid";

export default function Example({
  label,
  key,
  onChange,
  value,
  autoComplete,
  desc,
  error,
}) {
  const regularStyle =
    "mt-1 pl-12 pr-2 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md";
  const errorStyle =
    "block w-full pl-12 pr-12 border-red-300 text-red-900 placeholder-red-300 focus:outline-none focus:ring-red-500 focus:border-red-500 sm:text-sm rounded-md";
  const handleOnChange = newValue => onChange(newValue.replaceAll(',', '.').trim())

  return (
    <div>
      <label htmlFor={key} className="block text-sm font-medium text-gray-700">
        {label}
      </label>
      <div className="mt-1 relative rounded-md shadow-sm">
        <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
          <span className="text-gray-500 sm:text-sm">PLN</span>
        </div>
        <input
          type="text"
          name={key}
          id={key}
          autoComplete={false}
          onChange={(e) => handleOnChange(e.target.value)}
          value={value}
          className={error ? errorStyle : regularStyle}
          aria-invalid={!!error}
          aria-describedby="price-currency"
          placeholder="0.00"
        />
        {error && (
          <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
            <ExclamationCircleIcon
              className="h-5 w-5 text-red-500"
              aria-hidden="true"
            />
          </div>
        )}
      </div>
      {desc && <p className="mt-2 text-sm text-gray-500">{desc}</p>}
      {error && (
        <p className="mt-2 text-sm text-red-600" id={`${key}-error`}>
          {error.message}
        </p>
      )}
    </div>
  );
}
