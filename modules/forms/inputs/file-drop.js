import { useRouter } from "next/router";
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";
import React, { useState } from "react";
import AWS from "aws-sdk";
import axios from "axios";

const ADD_FILE = gql`
  mutation MyMutation(
    $mediaFile: versionedMediaFiles_insert_input = {
      awsLink: ""
      extension: ""
      hasUploadFinished: false
      name: ""
    }
  ) {
    insertMediaFile(object: $mediaFile) {
      awsLink
      createdAt
      extension
      hasUploadFinished
      id
      ver
      name
    }
  }
`;

export default function Example({
  label,
  key,
  objectId,
  onChange,
  value,
  autoComplete,
  desc,
  create,
  objectDef,
  error,
}) {
  const {
    query: { organizationId },
  } = useRouter();
  const [progress, setProgress] = useState(value ? 100 : 0);
  const [transferError, setTransferError] = useState(null);
  const [selectedFile, setSelectedFile] = useState(null);
  const [uploadState, setUploadState] = useState({});
  const [addFile, { error: mutationError, loading, data, called }] =
    useMutation(ADD_FILE);

  console.log(
    "OOOOOO start",
    label,
    key,
    objectId,
    value,
    autoComplete,
    desc,
    create,
    objectDef,
    error
  );

  const handleUpload = async (e) => {
    const file = e.target.files[0];
    setSelectedFile(file.name);
    setProgress(1);
    // Split the filename to get the name and type
    const fileParts = file.name.split(".");
    const fileName = fileParts[0];
    const fileType = fileParts[1];
    const {
      data: {
        insertMediaFile: { id, ver },
      },
    } = await addFile({
      variables: {
        mediaFile: {
          name: fileName,
          extension: fileType,
          hasUploadFinished: false,
        },
      },
    });
    console.log("OOOOOOOO hasura", id, ver);
    try {
      const res = await axios.post("/api/file-upload", {
        id,
        ver,
        fileName,
        fileType,
        organizationId,
      });
      //   .then((res) => {
      const signedRequest = res.data.signedRequest;
      const url = res.data.url;
      setUploadState({
        ...uploadState,
        url,
      });

      const fileNameJoin = `${fileName.replaceAll(",", "")}.${fileType}`;
      var options = {
        headers: {
          "Content-Type": fileType,
          "Content-Disposition": `attachment; filename="${fileNameJoin}"`,
        },
        onUploadProgress: (evt) => {
          setProgress(Math.round((evt.loaded / evt.total) * 98) + 1);
        },
      };
      await axios.put(signedRequest, file, options);
      const {
        data: {
          insertMediaFile: { ver: finishedVer },
        },
      } = await addFile({
        variables: {
          mediaFile: {
            id,
            name: fileName,
            extension: fileType,
            hasUploadFinished: true,
            awsLink: url,
          },
        },
      });
      onChange(`${objectId}Id`, id);
      onChange(`${objectId}Ver`, finishedVer);
      //  .then((_) => {
      setProgress(100);
      setUploadState({ ...uploadState, success: true });
      // mutate();
      //   });
    } catch (error) {
      console.log("error", "We could not upload your image", error);
      setTransferError(error);
    }
    //       .catch((error) => {
    //         console.log("error", "We could not upload your image", error);
    //         setTransferError(error);
    //       });
    // //   })
    //   .catch((error) => {
    //     console.log("error", "We could not upload your image", error);
    //     setTransferError(error);
    //   });
  };

  const handleRemoveFile = () => {
    onChange(`${objectId}Id`, undefined);
    onChange(`${objectId}Ver`, undefined);
    //  .then((_) => {
    setProgress(0);
    setUploadState({});

  }
  console.log("OOOOOO progress", progress, selectedFile, uploadState);
  return (
    <div id={key}>
      <label
        className="block text-sm font-medium text-gray-700"
        htmlFor={objectId}
      >
        {label}
      </label>
      <div className="mt-1 flex justify-center px-6 pt-5 pb-6 border-2 border-gray-300 border-dashed rounded-md">
        {progress === 100 ? (
          <div className="space-y-1 text-center">
            <svg
              className="mx-auto h-12 w-12 text-gray-400"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              aria-hidden="true"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width={2}
                d="M9 12l2 2 4-4M7.835 4.697a3.42 3.42 0 001.946-.806 3.42 3.42 0 014.438 0 3.42 3.42 0 001.946.806 3.42 3.42 0 013.138 3.138 3.42 3.42 0 00.806 1.946 3.42 3.42 0 010 4.438 3.42 3.42 0 00-.806 1.946 3.42 3.42 0 01-3.138 3.138 3.42 3.42 0 00-1.946.806 3.42 3.42 0 01-4.438 0 3.42 3.42 0 00-1.946-.806 3.42 3.42 0 01-3.138-3.138 3.42 3.42 0 00-.806-1.946 3.42 3.42 0 010-4.438 3.42 3.42 0 00.806-1.946 3.42 3.42 0 013.138-3.138z"
              />
            </svg>
            <div className="text-sm font-medium text-indigo-600">
              Plik jest wrzucony
            </div>
            <p className="text-xs text-gray-500 text-center">
              {selectedFile}
            </p>
            <p className="cursor-pointer text-xs text-gray-500 text-center" onClick={handleRemoveFile}>
              Usuń plik
            </p>
          </div>
        ) : progress > 0 ? (
          <div className="space-y-1 text-center">
            <svg
              className="mx-auto h-12 w-12 text-gray-400"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              aria-hidden="true"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M7 16a4 4 0 01-.88-7.903A5 5 0 1115.9 6L16 6a5 5 0 011 9.9M15 13l-3-3m0 0l-3 3m3-3v12"
              />
            </svg>
            <div className="text-sm font-medium text-indigo-600">
              Przysyłanie pliku {selectedFile} ukończone w {progress}%
              {/* <p className="pl-1">lub przeciągnij i upuść tutaj</p> */}
            </div>
            <p className="text-xs text-gray-500">
              Nie opuszczaj tej strony aż do zakończenia przesyłania
            </p>
          </div>
        ) : (
          <div className="space-y-1 text-center">
            <svg
              className="mx-auto h-12 w-12 text-gray-400"
              stroke="currentColor"
              fill="none"
              viewBox="0 0 48 48"
              aria-hidden="true"
            >
              <path
                d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                strokeWidth={2}
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
            <div className="flex text-sm text-gray-600">
              <label
                htmlFor={objectId}
                className="relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500"
              >
                <span>Wrzuć plik</span>

                <input
                  id={objectId}
                  name={objectId}
                  type="file"
                  className="sr-only"
                  onChange={handleUpload}
                />
              </label>
              {/* <p className="pl-1">lub przeciągnij i upuść tutaj</p> */}
            </div>
            <p className="text-xs text-gray-500">
              Wszystkie formaty aż do rozmiaru 4GB
            </p>
          </div>
        )}
      </div>
      {(error || transferError || mutationError) && (
        <p className="mt-2 text-sm text-red-600" id={`${key}-error`}>
          {error?.message ||
            mutationError?.message ||
            transferError?.message ||
            JSON.stringify(transferError)}
        </p>
      )}
    </div>
  );
}
