import { useRouter } from "next/router";
import { validateObjectName } from "./service";
import FormBuilder from "./components/form";
import { getCreationFormTitle, getEditFormTitle } from "./domain/config";
import { useObjectQuery } from "./hooks/use-object-query";

const snakeToCamel = (str) =>
  str
    .toLowerCase()
    .replace(/([-_][a-z])/g, (group) =>
      group.toUpperCase().replace("-", "").replace("_", "")
    );

const Page = ({ objectId, objectName, onSuccess, onAbort, hiddenFields, overrideTitle }) => {
  const camelObjectName = snakeToCamel(objectName);
  const { loading, error, object } = useObjectQuery({ objectName, objectId });
  console.log("SUBMITING loading, error,object", loading, error, object);
  if (loading) {
    return <p>Wczytuje obiekt do edycji...</p>;
  }
  if (error) {
    return <p>Błąd: {error.message ?? JSON.stringify(error)}</p>;
  }

  return (
    <FormBuilder
      title={overrideTitle ?? getEditFormTitle({ objectName: camelObjectName, state: object })}
      submitLabel={"Zapisz zmiany"}
      submitSuccessLabel={"Zmiany zapisane!"}
      objectName={objectName}
      object={object}
      onSuccess={onSuccess}
      onAbort={onAbort}
      hiddenFields={hiddenFields}
      isNarrow
    />
  );
};

export default Page;
