import { useRouter } from "next/router";
import { validateObjectName } from "./service";
import FormBuilder from "./components/form";
import { getCreationFormTitle } from "./domain/config/";

const snakeToCamel = (str) =>
  str
    .toLowerCase()
    .replace(/([-_][a-z])/g, (group) =>
      group.toUpperCase().replace("-", "").replace("_", "")
    );

const Page = () => {
  const router = useRouter();
  const { objectName, redirect, hidden, organizationId, removed, ...initialObject } =
    router.query;
  const camelObjectName = snakeToCamel(objectName);
  console.log("camelObjectName", camelObjectName);
  const isValidObject = validateObjectName({ camelObjectName });

  if (!isValidObject) {
    router.push("/404");
  }

  return (
    <FormBuilder
      title={getCreationFormTitle({ objectName: camelObjectName, state: initialObject })}
      submitLabel={"Stwórz"}
      submitSuccessLabel={"Stworzono!"}
      objectName={camelObjectName}
      object={initialObject} // maybe I should filter values to ones only present in config, but on other side better to fail fast with this than allow shit to be in codebase
      hiddenFields={hidden}
      removedFields={removed}
      redirect={redirect ?? '/'}
    />
  );
};

export default Page;
