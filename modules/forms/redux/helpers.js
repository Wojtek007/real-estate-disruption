export const resolvePath = (object, path, defaultValue) =>
  path.split("/").reduce((o, p) => (o ? o[p] : defaultValue), object);

export const setPath = (obj, incomingPath, val, delimeter = "/") => {
  const path = Array.isArray(incomingPath)
    ? incomingPath
    : incomingPath.split(delimeter);
  const idx = path[0];
  // console.log("SEEEEET START,obj, path, val", obj, path, val);

  if (path.length == 1) {
    if (Array.isArray(obj)) {
      return obj.map((ele, i) => (i == idx ? val : ele)); // double == because here is string taken from path
    } else {
      return {
        ...obj,
        [idx]: val,
      };
    }

  } else {
    const remainingPath = path.slice(1);

    if (Array.isArray(obj)) {
      return obj.map(
        (ele, i) => (i == idx ? setPath(ele || {}, remainingPath, val) : ele) // double == because here is string taken from path
      );
    } else {
      return {
        ...obj,
        [idx]: setPath(obj[idx] || {}, remainingPath, val),
      };
    }
  }
};
