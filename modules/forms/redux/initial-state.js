import { setPath } from "./helpers";

export const initialState = (startingObject = {}, formSections) => {
  console.log("initialStateinitialState- START", startingObject, formSections);
  // REMOVE ONES THAT ARE INLINEMULTIPLE
  const inlineMultiplePaths = formSections
    .filter((section) => section.type === "inline-creator")
    .map((section) => section.objectsKey);
  const initialObject = Object.entries(startingObject)
    .map(([path, value]) => [path.split("_").join("/"), value])
    .filter(([path]) =>
      inlineMultiplePaths.every((sectionPath) => !path.startsWith(sectionPath))
    )
    .reduce((acc, [path, value]) => setPath(acc, path, value), {});

  console.log("initialStateinitialState - initial state", initialObject);
  return initialObject;
};

export const initialInlineObject = (sectionObjectsKey, initialObject = {}) => {
  console.log(
    "initialStateinitialState - inline",
    sectionObjectsKey,
    initialObject
  );
  // Create object with data mathcinng section objects Key
  const inlineInitialObject = Object.fromEntries(
    Object.entries(initialObject)
      .map(([path, value]) => [path.split("_").join("/"), value])
      .filter(([path]) => path.startsWith(sectionObjectsKey))
      .map(([path, value]) => [
        path.replace(sectionObjectsKey, "").replace("/", ""),
        value,
      ])
  );
  console.log(
    "initialStateinitialState - filteredInitialObject",
    inlineInitialObject
  );
  return inlineInitialObject;
};