import { getFormSections } from "../domain/config";
import { resolvePath } from "./helpers";

export const accessByIdSelector = (id) => (state) => resolvePath(state, id);
export const isValidSelector = ({ objectName, state }) => {
  const formSections = getFormSections({ objectName });
  const topLevelObligatoryFields = formSections
    .filter((section) => !["inline-creator", "calculation-table"].includes(section.type))
    .reduce((acc, section) => {
      console.log("PPPPPP", section);
      const sectionObigatoryFields = section.fields
        .flat()
        .filter((field) => field.isObligatory || (field.dynamic?.isObligatory && field.dynamic.isObligatory(state)));
      return [...acc, ...sectionObigatoryFields];
    }, []);

  const inlineCreatorObligatoryFields = formSections
    .filter((section) => section.type === "inline-creator")
    .reduce((acc, section) => {
      console.log("UUUUU 00", section.objectsKey, state);
      const inlineObjects = accessByIdSelector(section.objectsKey)(state);
      console.log("UUUUU 111", inlineObjects);
      const sectionObigatoryFields =
        inlineObjects
          ?.map((inlineObject, i) =>
            section.fields
              .flat()
              .filter((field) => field.isObligatory)
              .map((field) => ({
                ...field,
                id: `${section.objectsKey}/${i}/${field.id}`,
              }))
          )
          .flat() ?? [];

      return [...acc, ...sectionObigatoryFields];
    }, []);

  console.log(
    "Validation topLevelObligatoryFields",
    topLevelObligatoryFields,
    "inlineCreatorObligatoryFields",
    inlineCreatorObligatoryFields
  );

  const formValidation = [
    ...topLevelObligatoryFields,
    ...inlineCreatorObligatoryFields,
  ].reduce(
    (acc, field) => {
      const fieldValue =
        field.type === "objectDropdown"
          ? accessByIdSelector(`${field.id}Id`)(state)
          : accessByIdSelector(field.id)(state);

      console.log("eeeerrrrr", field, fieldValue);
      const trimedValue = typeof fieldValue === 'string' ? fieldValue.trim() : fieldValue

      const activeErrors = field.errors?.filter(
        (error) => typeof error.check === "function" && error.check(trimedValue)
      ) ?? (trimedValue ? [] : [{ message: "Wymagane" }]);

      return {
        isValid: acc.isValid && activeErrors.length === 0,
        errors: [
          ...acc.errors,
          ...activeErrors.map(({ message }) => ({ message, id: field.id })),
        ],
      };
    },
    { isValid: true, errors: [] }
  );
  console.log("Validation RESULT", formValidation);
  return formValidation;
};
