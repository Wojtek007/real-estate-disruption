export const actionTypes = {
  CHANGE_VALUE: "CHANGE_VALUE",
};

export const changeValue = (key, value) => {
  console.log("changeValue : : Field renderer STATE key, value", key, value);
  return {
    type: actionTypes.CHANGE_VALUE,
    payload: { key, value },
  };
};
