import { actionTypes } from "./actions";
import { setPath } from "./helpers";

export const formReducer = (state, action) => {
  console.log("NEw-action", action, "pre-change state", state);
  switch (action.type) {
    case actionTypes.CHANGE_VALUE:
      const { key, value } = action.payload;

      return setPath(state, key, value);
    default:
      throw new Error();
  }
};
