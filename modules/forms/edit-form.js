import { useRouter } from "next/router";
import { validateObjectName } from "./service";
import { useObjectQuery } from "./hooks/use-object-query";
import FormBuilder from "./components/form";
import { getEditFormTitle } from "./domain/config/";
const snakeToCamel = (str) =>
  str
    .toLowerCase()
    .replace(/([-_][a-z])/g, (group) =>
      group.toUpperCase().replace("-", "").replace("_", "")
    );
const Page = () => {
  const router = useRouter();
  const { objectName, redirect, objectId, hidden, organizationId, removed, ...initialObject } = router.query;
  const camelObjectName = snakeToCamel(objectName);
  console.log("camelObjectName", camelObjectName);
  const isValidObject = validateObjectName({ objectName });
  const { loading, error, object } = useObjectQuery({ objectName: camelObjectName, objectId });

  if (!isValidObject) {
    router.push("/404");
    return <p>Redirect</p>;
  }
  if (loading) {
    return <p>Wczytuje obiekt do edycji...</p>;
  }
  if (error) {
    return <p>Błąd: {error.message ?? JSON.stringify(error)}</p>;
  }

  return (
    <FormBuilder
      title={getEditFormTitle({ objectName: camelObjectName, state: object })}
      submitLabel={"Zapisz zmiany"}
      submitSuccessLabel={"Zmiany zapisane!"}
      objectName={camelObjectName}
      object={{ ...initialObject, ...object }}
      hiddenFields={hidden}
      removedFields={removed}
      redirect={redirect ?? '/'}
    />
  );
};

export default Page;
