import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";
import { getObjectConfig, getFormSections } from "../domain/config";
import { setPath } from "../redux/helpers";
const snakeToCamel = (str) =>
  str
    .toLowerCase()
    .replace(/([-_][a-z])/g, (group) =>
      group.toUpperCase().replace("-", "").replace("_", "")
    );


export const useObjectQuery = ({ objectName, objectId }) => {
  const { objectTable } = getObjectConfig({ objectName });
  const formSections = getFormSections({ objectName });

  console.log("SUBMITING formSections to build query", formSections);
  const fieldIds = formSections
    .map((section) =>
      section.fields
        .map((fieldsRow) =>
          fieldsRow
            .map((field) =>
              field.type === "objectDropdown"
                ? [`${field.id}Id`, `${field.id}Ver`]
                : [field.id]
            )
            .flat()
            .map((fieldId) =>
              section.objectsKey ? `${section.objectsKey}/${fieldId}` : fieldId
            )
        )
        .flat()
    )
    .flat();

  console.log("SUBMITING query fields", fieldIds);
  const desiredQueryObject = fieldIds.reduce((acc, fieldId) => {
    const queryPath = fieldId.split("/").filter((chunk) => chunk !== "data");

    return setPath(acc, queryPath, null);
  }, {});

  const transformToGqlFields = (objToTransform) =>
    "id\n" +
    Object.entries(objToTransform)
      .map(([key, value]) =>
        value ? `${key} {\n${transformToGqlFields(value)} \n}` : key
      )
      .join("\n");

  console.log("SUBMITING query shape", desiredQueryObject);
  console.log(
    "SUBMITING graph query itslef",
    ` query MyQuery123($${objectName}Id: uuid) {
      ${snakeToCamel(objectTable)}(where: { id: { _eq: $${objectName}Id } }) {
`
    ,
    transformToGqlFields(desiredQueryObject)
  );

  const GET_OBJECT_QUERY = gql`
      query MyQuery123($${objectName}Id: uuid) {
      ${snakeToCamel(objectTable)}(where: { id: { _eq: $${objectName}Id } }) {
          ${transformToGqlFields(desiredQueryObject)}         
        }
      }
    `;
  const { loading, error, data } = useQuery(GET_OBJECT_QUERY, {
    variables: { [`${objectName}Id`]: objectId },
  });
  console.log("TTTTTTT RAW BACKEND OBJECT", data?.[snakeToCamel(objectTable)][0]);

  const transformToMutableForm = (objectOrArray) => {
    console.log("TTTTTTT", objectOrArray);
    if (!objectOrArray) {
      return null;
    }
    if (Array.isArray(objectOrArray)) {
      return objectOrArray.map(transformToMutableForm);
    }
    return Object.fromEntries(
      Object.entries(objectOrArray)
        .filter(([key]) => key !== "__typename")
        .map(([key, value]) =>
          value instanceof Object
            ? [key, { data: transformToMutableForm(value) }]
            : [key, value]
        )
    );
  };
  const object = transformToMutableForm(data?.[snakeToCamel(objectTable)][0] ?? {});
  console.log("TTTTTTT FINAL", object);

  return { loading, error, data, object };
};
