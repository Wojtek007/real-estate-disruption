import { useMutation } from "@apollo/react-hooks";

import gql from "graphql-tag";

import { getObjectConfig } from "../domain/config";

export const useObjectMutation = ({ objectName }) => {
  const { gqlType, gqlMutation } = getObjectConfig({
    objectName,
  });

  const ADD_OBJECT = gql`
    mutation MyMutation($${objectName}: ${gqlType}!) {
      ${gqlMutation}(object: $${objectName}) {
        id
        ver
      }
    }
  `;

  const [addObject, mutationResult] = useMutation(ADD_OBJECT);

  const handleAddObject = async ({ state }) => {
    const cleanState = Object.entries(state).reduce(
      (newState, [key, value]) => ({
        ...newState,
        ...(value ? { [key]: value } : {}),
      }),
      {}
    );

    const newObjectData = await addObject({
      variables: { [objectName]: cleanState },
    });

    return newObjectData.data[gqlMutation];
  };

  return [handleAddObject, mutationResult];
};
