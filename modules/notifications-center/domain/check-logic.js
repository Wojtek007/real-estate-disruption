import { types } from "./constants";

const keys = {
  SCHEDULING: "SCHEDULING",
  REQUEST_APPOINTMENT: "REQUEST_APPOINTMENT",
};

export const initializeCheck = (notification) => [
  {
    name: "Wyślij zapytanie do wykonawcy",
    key: keys.REQUEST_APPOINTMENT,
    types: [types.BLOCK],
    ancestor: keys.SCHEDULING,
    order: 100,
  },
  {
    name: "Umów przegląd",
    key: keys.SCHEDULING,
    types: [types.SCHEDULE],
    order: 200,
  },
  {
    name: "Odbierz rezultat przeglądu",
    types: [types.RESULT, types.FINISHING],
    blocker: keys.SCHEDULING,
    order: 300,
  },
];
