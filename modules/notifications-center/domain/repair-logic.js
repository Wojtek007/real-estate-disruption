import { types } from "./constants";

const keys = {
  SCHEDULING: "SCHEDULING",
  FIX_DONE: "FIX_DONE",
  SEND_FOR_APPROVAL: "SEND_FOR_APPROVAL",
  BUY_MATERIALS: "BUY_MATERIALS",
  FIX_APPROVAL: "FIX_APPROVAL",
  POSITIVE: "POSITIVE",
  NEGATIVE: "NEGATIVE",
  FINISH: 'FINISH'
};

const refusalTasks = [
  {
    name: "Powiadom najemce",
    key: keys.FINISH,
    types: [types.FINISHING],
    order: 300,
  }]

const fixingTasks = [
  {
    name: "Umów naprawę",
    key: keys.SCHEDULING,
    types: [types.SCHEDULE],
    order: 300,
  },
  {
    name: "Kup materialy",
    key: keys.BUY_MATERIALS,
    ancestor: keys.FIX_DONE,
    order: 400,
  },
  {
    name: "Wykonanie naprawy",
    key: keys.FIX_DONE,
    types: [types.RESULT],
    blocker: keys.SCHEDULING,
    order: 500,
  },
  {
    name: "Powiadom właściciela",
    blocker: keys.FIX_DONE,
    order: 600
  },
  {
    name: "Zwrot kosztów",
    types: [types.FINISHING],
    blocker: keys.FIX_DONE,
    order: 700,
  },
];

export const initializeRepair = (notification) =>
  notification.defects[0].isOwnerAcceptanceNecessary
    ? [
      {
        name: "Wyślij zapytanie do właściciela",
        key: keys.SEND_FOR_APPROVAL,
        types: [types.BLOCK],
        ancestor: keys.FIX_APPROVAL,
        order: 100,
      },
      {
        name: "Czy akceptuje naprawe",
        types: [types.DECIDE],
        key: keys.FIX_APPROVAL,
        decisions: [
          { name: "Zgoda", key: keys.POSITIVE, tasks: fixingTasks },
          { name: "Odmowa", key: keys.NEGATIVE, tasks: refusalTasks },
        ],
        order: 200,
      },
    ]
    : fixingTasks;
