import { getNotificationDate } from "../index";
import * as moment from "moment";

const isNotificationWaiting = (notification) => {
  const hasBlockingTask =
    notification?.tasks.length > 0 &&
    notification.tasks.some((task) => task.isBlocking && !task.isClosed);

  const isCheckThatHasAppointment =
    notification.previousCheckResult.length > 0 &&
    notification.appointments.length > 0;

  return hasBlockingTask || isCheckThatHasAppointment;
};

const isNotificationDoing = (notification) => notification?.tasks.length > 0;
const isNotificationEvent = (notification) => true;
const enhanceWithStateMachine = (notification) => {
  return notification;
  // if (!notification.type === "DEFECT") {
  // }
  // // if there is no decisions
  // // than determine start state based on defects params

  // console.log("DDDEFECT - start", notification);
  // // determine if it is waiting or todo
  // const defectState = defectMachine(notification);
  // //than build actions user can take from this point
  // console.log("DDDEFECT-ennd", defectState);
};
const enhanceNotifyDate = (notification) => {
  return {
    ...notification,
    notifyDate: getNotificationDate(notification)
  };


}
const enhanceType = (notification) => {
  if (notification.previousCheckResult.length) {
    return {
      ...notification,
      type: notification.appointments?.length ? "CHECK-DONE" : "CHECK-PLANNING",
    };
  }
  if (notification.contracts.length) {
    return {
      ...notification,
      type: "CONTRACT",
    };
  }
  if (notification.defects.length) {
    return {
      ...notification,
      type: "DEFECT",
    };
  }
};

export const clasifyNotifictions = (notifications, lookup) =>
  notifications
    .map(enhanceType)
    .map(notification => ({
      ...notification,
      notifyDate: getNotificationDate(notification)
    }))
    .sort((a, b) => {
      console.log('SORTTT', moment(a.notifyDate).diff(moment(b.notifyDate), 'days'), a.notifyDate.format("L"), b.notifyDate.format("L"));
      const diff = moment(a.notifyDate).diff(moment(b.notifyDate), 'days');
      return diff !== NaN ? diff : -1
    })
    .map(enhanceWithStateMachine)
    .reduce(
      ({ events, doing, waiting }, notification, i, self) => {
        if (i === 0) {
          console.log('SORTTT - endnddnnd', self.map(it => it.notifyDate.format("L")))
        }
        if (notification.id === lookup) {
          return { events: [...events, notification], doing, waiting };
        }
        if (isNotificationWaiting(notification)) {
          return { events, doing, waiting: [...waiting, notification] };
        }
        if (isNotificationDoing(notification)) {
          return { events, doing: [...doing, notification], waiting };
        }
        if (isNotificationEvent(notification)) {
          return { events: [...events, notification], doing, waiting };
        }
      },
      {
        events: [],
        doing: [],
        waiting: [],
      }
    );
