import { types } from "./constants";

const keys = {
  SCHEDULING: "SCHEDULING",
  SEND_FOR_APPROVAL: "SEND_FOR_APPROVAL",
  GET_DECISION: "GET_DECISION",
  POSITIVE: "POSITIVE",
  NEGATIVE: "NEGATIVE",
  FINISH: 'FINISH'
};

const sharedTasks = [
  { name: "Przygotować dokumenty", order: 300 },
  {
    name: "Umów spotkanie",
    key: keys.SCHEDULING,
    types: [types.SCHEDULE],
    order: 400,
  },
  { name: "Przejrzyj remonty", order: 500 },
  { name: "Powiadom właściciela", order: 600 },
];

const extendTasks = [
  ...sharedTasks,
  {
    name: "Podpisanie umowy",
    types: [types.FINISHING, types.RESULT],
    key: keys.FINISH,
    blocker: keys.SCHEDULING,
    order: 700,
  },
];
const refuseTasks = [
  ...sharedTasks,
  {
    name: "Oddanie nieruchomości",
    types: [types.FINISHING, types.RESULT],
    key: keys.FINISH,
    blocker: keys.SCHEDULING,
    order: 700,
  },
];

export const initializeContract = (notification) => [
  {
    name: "Wyślij zapytanie do najemcy",
    key: keys.SEND_FOR_APPROVAL,
    types: [types.BLOCK],
    ancestor: keys.GET_DECISION,
    order: 100,
  },
  {
    name: "Obierz decyzje od najemcy",
    types: [types.DECIDE],
    key: keys.GET_DECISION,
    decisions: [
      { name: "Przedłużenie", key: keys.POSITIVE, tasks: extendTasks },
      { name: "Zakończenie", key: keys.NEGATIVE, tasks: refuseTasks },
    ],
    order: 200,
  },
];
