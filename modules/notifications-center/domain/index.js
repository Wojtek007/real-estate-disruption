import * as moment from "moment";
import {
  ClipboardIcon,
  DocumentTextIcon,
  BriefcaseIcon,
} from "@heroicons/react/outline";
import { initializeContract } from "./contract-logic";
import { initializeCheck } from "./check-logic";
import { initializeRepair } from "./repair-logic";


const getDefectTitle = (notification) => {
  switch (notification.defects[0].urgency.key) {
    case "urgent":
      return "Naprawa nagłej usterki"
    case "refused":
      return "Odrzucona usterka"
    case "postponed":
      return "Planowana naprawa"
    default:
      return "Naprawa"
  }
}

export const getEvent = (notification) => {
  console.log("XXXXXX", notification);
  const waitingDays = getWaitingTime(notification);
  const appointmentDate = getAppointmentDate(notification);

  if (notification.previousCheckResult?.length) {
    const check = notification.previousCheckResult[0].checks[0] ?? notification.previousCheckResult[0].check
    return {
      title: `Przegląd - ${check.name}`,
      categoryIcon: ClipboardIcon,
      propertyId: check.property.id,
      propertyName: check.property.name,
      date: getNotificationDate(notification).format("L"),
      initialTasks: !notification.tasks.length && initializeCheck(notification),
      removeTasks: notification.tasks.length && !notification.appointments.length && notification.tasks.map(t => ({ ...t, deleted: true })),
      appointmentDate,
      waitingDays,
      type: "CHECK",
      slideOverHrefQuery: getSlideOverHrefQuery(notification),
    };
  }
  if (notification.contracts?.length) {
    const { surname, firstname } = notification.contracts[0].signer;
    const propertyName = notification.contracts[0].subjectProperty.name;
    const title = (() => {
      const effect = notification.contracts[0].type.effect;
      switch (notification.contracts[0].category.key) {
        case 'rent':
          return effect === 'annex' ? "Przedłużenie aneksu najmu" : "Przedłużenie umowy najmu"
        case "management":
          return effect === 'annex' ? "Przedłużenie aneksu zarządu" : "Przedłużenie umowy zarządu"
        case "insurance":
          return effect === 'annex' ? "Przedłużenie aneksu ubezpieczenia" : "Przedłużenie umowy ubezpieczenia"
        default:
          return 'Przedłużenie umowy'
      }
    })()
    return {
      title,
      categoryIcon: DocumentTextIcon,
      propertyId: notification.contracts[0].subjectProperty.id,
      propertyName: propertyName, //`${propertyName} - ${firstname ?? ""} ${surname ?? ""}`,
      date: getNotificationDate(notification).format("L"),
      initialTasks: !notification.tasks.length && initializeContract(notification),
      removeTasks: notification.tasks.length && !notification.appointments.length && notification.tasks.map(t => ({ ...t, deleted: true })),
      appointmentDate,
      waitingDays,
      type: "CONTRACT",
      slideOverHrefQuery: getSlideOverHrefQuery(notification),
    };
  }
  if (notification.defects?.length) {
    return {
      title: `${getDefectTitle(notification)} - ${notification.defects[0].name}`,
      categoryIcon: BriefcaseIcon,
      propertyId: notification.defects[0].property.id,
      propertyName: notification.defects[0].property.name,
      date: getNotificationDate(notification).format("L"),
      initialTasks: !notification.tasks.length && initializeRepair(notification),
      removeTasks: notification.tasks.length && !notification.appointments.length && notification.tasks.map(t => ({ ...t, deleted: true })),
      appointmentDate,
      waitingDays,
      type: "DEFECT",
      slideOverHrefQuery: getSlideOverHrefQuery(notification),
    };
  }
};


const getSlideOverHrefQuery = (notification) => {
  if (notification.contracts?.length) {
    return {
      panel: "contract",
      id: notification.contracts[0].id,
      propertyId: notification.contracts[0].subjectProperty.id,

    }

  }
  if (notification.previousCheckResult?.length) {
    const check = notification.previousCheckResult[0].checks[0] ?? notification.previousCheckResult[0].check
    return {
      panel: "check",
      propertyId: check.property.id,
      id: check.id,
    }
  }
  if (notification.defects?.length) {
    return {
      panel: "defect",
      propertyId: notification.defects[0].property.id,
      id: notification.defects[0].id
    }
  }
};

export const getNotificationDate = (notification) => {
  console.log('getNotificationDate', notification)
  if (notification.defects?.length && notification.defects[0].fixResults.length) {
    return moment(notification.defects[0].fixResults[0].date)
  }
  if (notification.appointments[0]) {
    return moment(notification.appointments[0].date)
  }

  if (notification.reminderDate) {
    return moment(notification.reminderDate);
  }

  const { monthsBefore, weeksBefore } = notification.precedingPeriod ?? {};

  if (notification.contracts?.length) {
    const { endDate } = notification.contracts[0];

    return moment(endDate)
      .subtract(monthsBefore, "months")
      .subtract(weeksBefore, "weeks")

  }
  if (notification.previousCheckResult?.length) {
    const { date } = notification.previousCheckResult[0];
    const check = notification.previousCheckResult[0].check ?? notification.previousCheckResult[0].checks[0]

    return moment(date).add(check.recurrenceInterval.monthsInterval, "months")
      .subtract(monthsBefore, "months")
      .subtract(weeksBefore, "weeks")

  }
};
const getTaskByKey = (tasks, key) => key && tasks.find((t) => t.config.key === key);

const isTaskDone = (task, notification) => {
  const isScheduleType = task.config.types?.includes("SCHEDULE");
  const isResultType = task.config.types?.includes("RESULT");
  // console.log('BBBBBBB', task.name,  notification.contracts[0].contracts)
  const isReferedByNonAnnexContract = notification.contracts && !!notification.contracts[0]?.contracts.filter(c => c.type.effect !== 'annex').length
  const isDefectFixed = notification.defects && !!notification.defects[0].fixResults.length

  const isDone = isScheduleType
    ? !!notification.appointments.length
    : isResultType ?
      (isReferedByNonAnnexContract || isDefectFixed)
      : !!task.isDone;

  return isDone;
};

const createTask = (task, notification) => {
  const isBlockerExisting = !!task.config.blocker;
  const blockingTask =
    isBlockerExisting && getTaskByKey(notification.tasks, task.config.blocker);
  const isBlocked = blockingTask && !isTaskDone(blockingTask, notification);

  const isScheduleType = task.config.types?.includes("SCHEDULE");
  const isResultType = task.config.types?.includes("RESULT");
  const isDone = isTaskDone(task, notification);
  const ancestorOfBlockerTask = getTaskByKey(
    notification.tasks,
    task.config.ancestor
  );
  const isAncestorOfBlockerDone =
    ancestorOfBlockerTask && isTaskDone(ancestorOfBlockerTask, notification);
  const isDeprecated = !isDone && isAncestorOfBlockerDone;
  // console.log('DEPRECETED 0 ', task.name, 'isDeprecated:', isDeprecated, 'isAncestorOfBlockerDone', isAncestorOfBlockerDone, 'ancestorOfBlockerTask', ancestorOfBlockerTask, 'task.config.ancestor', task.config.ancestor)

  const isDecisionType = task.config.types?.includes("DECIDE");
  const decisions =
    isDecisionType &&
    task.config.decisions.map((decision) => ({
      ...decision,
      isChoosen: task.decision === decision.key,
    }));
  // if (notification.contracts?.length) {
  //   console.log('START- LLLLLL222', notification.contracts[0].signer.surname, isScheduleType, isResultType, task.config.types,
  //     task, notification)
  // }

  const getAppointmentCategoryQueryParams = () => {
    if (notification.previousCheckResult?.length) {
      return { id: notification.previousCheckResult[0].id, panel: "check" };
    }
    if (notification.contracts?.length) {
      return {
        id: notification.contracts[0].id,
        panel: "contract",
        hidden: [
          "notification",
          "attendeeCompany",
          "plannedCost",
        ],
      };
    }
    if (notification.defects?.length) {
      return { id: notification.defects[0].id, panel: "defect" };
    }
  };

  const getResultQueryParams = () => {
    if (notification.previousCheckResult?.length) {
      console.log(
        "LLLLLL111",
        task,
        notification,

      );
      const check = notification.previousCheckResult[0].checks[0] ?? notification.previousCheckResult[0].check

      return {
        parentNotificationId: notification.id,
        parentNotificationVer: notification.ver,
        checkId: check.id,
        checkVer: check.ver,
        date: notification.appointments[0].date,
        cost: notification.appointments[0].plannedCost,
        // defects_data_propertyId:
        //   check.property.id,
        // defects_data_propertyVer:
        //   check.property.ver,
        nextCheckNotification_data_precedingPeriodId:
          notification.precedingPeriod.id,
        nextCheckNotification_data_precedingPeriodVer:
          notification.precedingPeriod.ver,
        hidden: [
          "parentNotification",
          "defects/data",
          "check",
          "defects/data/notification/data/precedingPeriod",
          "defects/data/property",
          "defects/data/reportingMessage",
          "nextCheckNotification/data/precedingPeriod",
          "nextCheckNotification/data/reminderDate",
        ],
        tab: "add-check-result",
        id: notification.previousCheckResult[0].id, // check result or check? depends what is panel about
        panel: "check",
      };
    }
    if (notification.contracts?.length) {
      const contract = notification.contracts[0]
      const hasPositiveDecision = !!notification.tasks.some(task => task.decision === "POSITIVE")
      console.log(
        "LLLLLL222", hasPositiveDecision,
        contract.signer.firstname,
        notification.precedingPeriod.id, notification.precedingPeriod.ver,
        task,
        notification,
      );


      return {
        signerId: contract.signer?.id,
        signerVer: contract.signer?.ver,
        categoryId: contract.category.id,
        categoryVer: contract.category.ver,
        subjectPropertyId: contract.subjectProperty.id,
        subjectPropertyVer: contract.subjectProperty.ver,
        ...(hasPositiveDecision ? {
          typeId: "0e02eb25-fd98-473a-ad2b-2e884bbd3615",
          typeVer: 1,
          refersToContractId: contract.id,
          refersToContractVer: contract.ver,
          notification_data_precedingPeriodId: notification.precedingPeriod.id,
          notification_data_precedingPeriodVer: notification.precedingPeriod.ver,
        } : {
          refersToContractId: contract.id,
          refersToContractVer: contract.ver,
          typeId: "54d6733d-d6ff-4ce7-8762-d41f00d8e312",
          typeVer: 3,
        }),
        hidden: [
          "category",
          "subjectProperty",
          "refersToContract",
          "type",
          "notification/data/reminderDate",
          "notification/data/precedingPeriod",
        ],
        tab: "add-contract",
        id: notification.contracts[0].id,
        panel: "contract"
      };
    }
    if (notification.defects?.length) {
      console.log(
        "LLLLLL333",
        task,
        notification,

      );
      return {
        id: notification.defects[0].id,
        panel: "defect",
        tab: 'add-fix-result',
        hidden: ['defect'],
        defectId: notification.defects[0].id,
        defectVer: notification.defects[0].ver
      };
    }
  };

  const hasAppointment = notification.appointments.length;
  const appointmentDate =
    hasAppointment &&
    isScheduleType &&
    moment(notification.appointments[0].date).format("L");


  const hasDefectFix = !!(notification.defects?.length && notification.defects[0].fixResults.length)
  const resultDate =
    hasDefectFix &&
    isResultType &&
    moment(notification.defects[0].fixResults[0].date).format("L");

  const date = resultDate || appointmentDate

  const isDecisionMade = !!task.decision
  const info =
    isDecisionType &&
    isDecisionMade &&
    `Decyzja: ${task.config.decisions.find(d => d.key === task.decision)?.name}`

  const hrefQuery = isScheduleType
    ? {
      notificationId: notification.id,
      notificationVer: notification.ver,
      hidden: ["notification"],
      tab: hasAppointment ? "edit-appointment" : "new-appointment",
      objectId: hasAppointment && notification.appointments[0].id,
      ...getAppointmentCategoryQueryParams(),
    }
    : isResultType & !isBlocked
      ? getResultQueryParams()
      : null;
  return {
    ...task,
    date,
    info,
    isDone,
    isBlocked,
    isDeprecated,
    hrefQuery,
    decisions,
  };
};
const getAppointmentDate = (notification) => {
  const hasAppointment = notification.appointments.length;

  return hasAppointment
    ? moment(notification.appointments[0].date).fromNow()
    : "";
};
const getWaitingTime = (notification) => {
  const blockingTasks = notification.tasks.filter((task) =>
    task.config.types?.includes("BLOCK")
  );

  const waitingTimes = blockingTasks
    .map((blockingTask) => {
      const isBlockingTaskDone = blockingTask.isDone;
      const ancestorOfBlockerTask = getTaskByKey(
        notification.tasks,
        blockingTask.config.ancestor
      );
      const isAncestorOfBlockerDone = isTaskDone(
        ancestorOfBlockerTask,
        notification
      );

      const blockadeDate = moment(blockingTask.updatedAt).fromNow(true);

      return isBlockingTaskDone && !isAncestorOfBlockerDone && blockadeDate;
    })
    .filter(Boolean);

  return blockingTasks.length ? waitingTimes[0] : "";
};
const getFinishedDays = (notification) => {
  const hasDefectFix = notification.defects?.length && notification.defects[0].fixResults.length;

  return hasDefectFix
    ? moment(notification.defects.length && notification.defects[0].fixResults[0].date).fromNow()
    : "";
};

export const getTodo = (notification) => {
  if (!notification.tasks.length) {
    return null;
  }
  // console.log('DEPRECETED ---------------------------------------')

  const tasks = notification.tasks.map((task) =>
    createTask(task, notification)
  );
  const appointmentDate = getAppointmentDate(notification);
  const waitingDays = getWaitingTime(notification);
  const finishedDays = getFinishedDays(notification)

  console.log("44444 NOTIFICATION.tasks", tasks);

  if (notification.previousCheckResult?.length) {
    const check = notification.previousCheckResult[0].checks[0] ?? notification.previousCheckResult[0].check
    return {
      title: `Przegląd - ${check.name}`,
      categoryIcon: ClipboardIcon,
      propertyName: check.property.name,
      // date: getNotificationDate(notification),
      // initialTasks: !notification.tasks.length && initializeCheck(notification),
      tasks,
      appointmentDate,
      waitingDays,
      finishedDays,
      type: "CHECK",
    };
  }
  if (notification.contracts?.length) {
    const { surname, firstname } = notification.contracts[0].signer;
    const propertyName = notification.contracts[0].subjectProperty.name;
    return {
      title: notification.contracts[0].type.effect === 'annex' ? "Przedłużenie aneksu" : "Przedłużenie umowy",
      categoryIcon: DocumentTextIcon,
      propertyName: `${propertyName} - ${firstname ?? ""} ${surname ?? ""}`,
      // date: getNotificationDate(notification),
      // initialTasks:
      //   !notification.tasks.length && initializeContract(notification),
      tasks,
      appointmentDate,
      waitingDays,
      finishedDays,
      type: "CONTRACT",
    };
  }
  if (notification.defects?.length) {

    return {
      title: `${getDefectTitle(notification)} - ${notification.defects[0].name}`,
      categoryIcon: BriefcaseIcon,
      propertyName: notification.defects[0].property.name,
      // date: getNotificationDate(notification),
      // initialTasks:
      //   !notification.tasks.length && initializeRepair(notification),
      tasks,
      appointmentDate,
      waitingDays,
      finishedDays,
      type: "DEFECT",
    };
  }
};
