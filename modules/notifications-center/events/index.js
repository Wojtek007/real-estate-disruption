import { useRouter } from "next/router";
import Link from "next/link";
import {
  ChevronDoubleLeftIcon,
  ChevronDoubleRightIcon,
  CalendarIcon
} from "@heroicons/react/solid";
import {
  ShieldCheckIcon,
  ChevronUpIcon,
  QuestionMarkCircleIcon,
  FolderIcon,
  HomeIcon,
  InboxIcon,
  UsersIcon,
  DocumentTextIcon,
  ClockIcon,
} from "@heroicons/react/outline";

export default function Example({
  event,
  addTasks,
  onEnterHover,
  onLeaveHover,
  isHighlighted,
  className
}) {
  const {
    pathname,
    query: { organizationId },
  } = useRouter();
  return (
    <li className={className} key={event.id} onMouseEnter={onEnterHover} onMouseLeave={onLeaveHover}>
      <a className="block hover:bg-gray-50">
        <div
          className={`flex items-center px-2 py-1 sm:px-4  border-${isHighlighted ? "indigo-600" : "white"
            } border-2 rounded-md`}
        >
          <div className="min-w-0 flex-1 flex items-center">
            <p className="text-xs text-gray-500">
              <time dateTime={event.date}>{event.date}</time>
            </p>

            <div className="min-w-0 flex-1 px-4 md:grid md:grid-cols-2 md:gap-4 items-center">
              <div className=" block">
                <div className="flex items-center">
                  <div className=" px-1 py-1 inline-block bg-indigo-600 rounded-xl mr-4">
                    <div className="text-white group flex items-center text-xs font-normal rounded-md">
                      <event.categoryIcon className="text-white flex-shrink-0 h-4 w-4" />
                    </div>
                  </div>
                  {/* <span className="inline-block h-6 w-6 rounded-full overflow-hidden bg-gray-100">
                          <DocumentTextIcon className="text-gray-400  h-4 w-4" />
                           <svg
                            className="h-full w-full text-gray-300"
                            fill="currentColor"
                            viewBox="0 0 24 24"
                          >
                            <path d="M24 20.993V24H0v-2.996A14.977 14.977 0 0112.004 15c4.904 0 9.26 2.354 11.996 5.993zM16.002 8.999a4 4 0 11-8 0 4 4 0 018 0z" />
                          </svg> 
                        </span> */}
                  <Link
                    href={{
                      pathname: pathname,
                      query: {
                        organizationId,
                        ...event.slideOverHrefQuery
                      },
                    }}
                    scroll={false}

                  ><a className="cursor-pointer text-xs text-gray-700 font-medium mt-1 hover:underline">
                      {event.title}
                    </a></Link>
                  {/* <p className="mt-2 flex items-center text-sm text-gray-500">
                          <CheckCircleIcon
                            className="flex-shrink-0 mr-1.5 h-5 w-5 text-green-400"
                            aria-hidden="true"
                          />
                          {application.stage}
                        </p> */}
                </div>
              </div>
              <div>
                <Link
                  href={`/${organizationId}/properties/${event.propertyId}/info`}
                >
                  <a className="cursor-pointer flex items-center px-3 text-xs font-medium rounded-md text-indigo-600 hover:underline">
                    {/* {application.applicant.name} */}
                    <HomeIcon className="text-gray-400 flex-shrink-0 mr-1 h-5 w-5" />
                    <span className="mt-1">{event.propertyName}</span>
                  </a>
                </Link>
                {/* <p className="mt-2 flex items-center text-sm text-gray-500">
                        <MailIcon
                        className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
                        aria-hidden="true"
                        />
                        <span className="truncate">
                        {application.applicant.email}
                        </span>
                    </p> */}
              </div>
            </div>
          </div>
          {event.initialTasks ? (
            <div className='cursor-pointer' onClick={() => addTasks(event.initialTasks)}>
              <ChevronDoubleRightIcon
                className="h-5 w-5 text-gray-400 hover:text-gray-900"
                aria-hidden="true"
              />
            </div>
          ) : event.waitingDays ? (
            <div>
              <ClockIcon
                className="h-5 w-5 text-gray-400 hover:text-gray-900"
                aria-hidden="true"
              />
            </div>
          ) : event.appointmentDate ? (
            <div>
              <CalendarIcon
                className="h-5 w-5 text-indigo-500 hover:text-indigo-700"
                aria-hidden="true"
              />
            </div>
          ) : event.removeTasks.length > 0 ? (
            <div className='cursor-pointer' onClick={() => addTasks(event.removeTasks)}>
              <ChevronDoubleLeftIcon
                className="h-5 w-5 text-gray-400 hover:text-gray-900"
                aria-hidden="true"
              />
            </div>
          ) : null}
        </div>
      </a>
    </li>
  );
}
