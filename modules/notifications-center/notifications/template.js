/* This example requires Tailwind CSS v2.0+ */
import { Fragment } from "react";
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";

import {
  BriefcaseIcon,
  CalendarIcon,
  CheckIcon,
  ChevronDownIcon,
  ChevronRightIcon,
  CurrencyDollarIcon,
  UserIcon,
  HomeIcon,
  PencilIcon,
} from "@heroicons/react/solid";
import { Menu, Transition } from "@headlessui/react";
import Link from "next/link";
import FeedDivider from "../components-smart/feed-divider";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

const actionsExample = [
  { label: "Powiadomiono najemce" },
  { label: "Zapytano właściciela" },
  { label: "Przełóż" },
];

const exampleBody = (
  <div className="mt-1 flex flex-col">
    {/*sm:flex-row sm:flex-wrap sm:mt-0 sm:space-x-6 */}
    {/* <div className="mt-2 flex items-center text-sm text-gray-500">
  <HomeIcon
    className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
    aria-hidden="true"
  />
  Sienna 72a/203
</div> */}
    {/* <div className="mt-2 flex items-center text-sm text-gray-500">
  <BriefcaseIcon
    className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
    aria-hidden="true"
  />
  Full-time
</div> */}
    <div className="mt-2 flex items-center text-sm text-gray-500">
      <UserIcon
        className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
        aria-hidden="true"
      />
      Jan Kowalski
    </div>
    <div className="mt-2 flex items-center text-sm text-gray-500">
      <CalendarIcon
        className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
        aria-hidden="true"
      />
      Koniec 9 Stycz 2020
    </div>
  </div>
);

const ADD_TASKS = gql`
  mutation MyMutation($tasks: [versionedTasks_insert_input!]!) {
    insertTasks(objects: $tasks) {
      returning {
        id
      }
      affected_rows
    }
  }
`;
export default function Example({
  property = { name: "Sienna 72a/200" },
  breadcrumbs = [{ label: "Umowa" }, { label: "Przedłużenie" }],
  title = "Zbliża się koniec umowy najmu",
  children = exampleBody,
  actions = actionsExample,
  notifyDate,
  notification,
  setLookup,
  isLookup,
}) {
  console.log("template notification", notification);
  // const ADD_APPOINTMENTS = gql`
  //   mutation MyMutation($appointments: [versionedAppointments_insert_input!]!) {
  //     insertAppointments(objects: $appointments) {
  //       returning {
  //         id
  //       }
  //       affected_rows
  //     }
  //   }
  // `;

  const [addTasks, { error: errorTasks, loading: loadingTasks }] =
    useMutation(ADD_TASKS);
  // const [
  //   addAppointments,
  //   { error: errorAppointments, loading: loadingAppointments },
  // ] = useMutation(ADD_APPOINTMENTS);

  const handleAddTasks = async (newTasks) => {
    if (loadingTasks) return;

    const newTasksToCreate =
      newTasks?.map((task) => ({
        name: task.name,
        isBlocking: !!task.isBlocking,
        type: task.type,
        notificationId: notification.id,
        notificationVer: notification.ver,
      })) ?? [];

    const oldTasksToClose = notification.tasks
      .filter((task) => !task.isClosed || !task.isDone)
      .map((task) => ({
        id: task.id,
        name: task.name,
        isBlocking: task.isBlocking,
        isClosed: true,
        type: task.type,
        notificationId: notification.id,
        notificationVer: notification.ver,
      }));

    await addTasks({
      variables: {
        tasks: [...newTasksToCreate, ...oldTasksToClose],
      },
    });

    setLookup(null);
  };
  // const handleAddAppointments = (appointments) =>
  //   !loadingAppointments &&
  //   addAppointments({
  //     variables: {
  //       appointments: appointments.map((appointment) => ({
  //         name: task,
  //         notificationId: notification.id,
  //         notificationVer: notification.ver,
  //       })),
  //     },
  //   });
  return (
    <>
      {notifyDate && <FeedDivider text={notifyDate} />}
      <div className="rounded-lg bg-white shadow">
        <div className="bg-white grid grid-cols-4 rounded-lg">
          <div className="col-span-3 flex items-center justify-between ">
            <div className="flex-1 min-w-0 px-6 py-4">
              <nav className="flex" aria-label="Breadcrumb">
                {/* <div className="flex items-center text-sm text-gray-500">
                <HomeIcon
                  className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
                  aria-hidden="true"
                />
                Sienna 72a/203
              </div> */}
                <ol className="flex items-center space-x-4" role="list">
                  <li>
                    <div className="flex items-center">
                      <HomeIcon
                        className="flex-shrink-0 h-5 w-5 text-gray-400"
                        aria-hidden="true"
                      />
                      <a className="ml-4 text-sm font-medium text-gray-500 hover:text-gray-700">
                        {property.name}
                      </a>
                    </div>
                  </li>
                  {breadcrumbs.map(({ label }) => (
                    <li>
                      <div className="flex items-center">
                        <ChevronRightIcon
                          className="flex-shrink-0 h-5 w-5 text-gray-400"
                          aria-hidden="true"
                        />
                        <a className="ml-4 text-sm font-medium text-gray-500 hover:text-gray-700">
                          {label}
                        </a>
                      </div>
                    </li>
                  ))}
                </ol>
              </nav>
              <h2 className="mt-3 text-xl font-bold leading-7 text-gray-900 sm:text-2xl">
                {title}
              </h2>
              {children}
              {errorTasks && (
                <p className="text-red-800">
                  {errorTasks.message ?? JSON.stringify(errorTasks)}
                </p>
              )}
              {/* {errorAppointments && (
                <p className="text-red-800">
                  {errorAppointments.message ??
                    JSON.stringify(errorAppointments)}
                </p>
              )} */}
            </div>
          </div>
          <div className="flex flex-col">
            {actions.map((action, i, self) => {
              const btn = (
                <button
                  type="button"
                  onClick={() => handleAddTasks(action.tasks)}
                  className={classNames(
                    i > 0 && "-mt-px",
                    i === 0 && "rounded-tr-lg",
                    i === self.length - 1 && "rounded-br-lg",
                    "flex-auto px-4 py-1 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:z-10 focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500"
                  )}
                >
                  {action.label}
                </button>
              );
              return action.href ? (
                <Link href={action.href} scroll={false}>
                  {btn}
                </Link>
              ) : (
                btn
              );
            })}
          </div>
        </div>
      </div>
    </>
  );
}
