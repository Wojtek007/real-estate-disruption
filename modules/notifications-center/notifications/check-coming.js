/* This example requires Tailwind CSS v2.0+ */
import { Fragment } from "react";
import { useRouter } from "next/router";

import {
  BriefcaseIcon,
  CalendarIcon,
  CheckIcon,
  ChevronDownIcon,
  ChevronRightIcon,
  CurrencyDollarIcon,
  UserIcon,
  LocationMarkerIcon,
  PencilIcon,
} from "@heroicons/react/solid";
import { Menu, Transition } from "@headlessui/react";
import { getNotificationDate } from "../domain/notification/contract";

import Notification from "./template";

export default function Example({ notification, setLookup }) {
  const {
    asPath,
    pathname,
    query: { organizationId },
  } = useRouter();
  const check = notification.previousCheckResult[0];
  // const appointment = check.appointments[0];
  // const executorName =
  //   appointment.attendeeCompany?.name ??
  //   `${appointment.attendeePerson.firstname} ${appointment.attendeePerson.surname}`;
  // const executorPhone =
  //   appointment.attendeeCompany?.phone ?? appointment.attendeePerson?.phone;
  // const executorEmail =
  //   appointment.attendeeCompany?.email ?? appointment.attendeePerson?.email;
  const addAppointmentHref = {
    pathname: pathname,
    query: {
      panel: "check",
      organizationId,
      // propertyId: notification.previousCheckResult[0].property?.id,
      notificationId: notification.id,
      notificationVer: notification.ver,
      hidden: ["notification"],
      id: check.id,
      tab: "new-appointment",
    },
  };
  return (
    <Notification
      breadcrumbs={[{ label: "Przegląd" }, { label: "Nadchodzi" }]}
      property={notification.previousCheckResult[0].property}
      title="Umówić przegląd piecy"
      actions={[
        { label: "Umówiono wykonawce", href: addAppointmentHref },
        {
          label: "Wysłano zapytanie",
          tasks: [
            {
              name: "Oczekuję na wykonawce",
              isBlocking: true,
              type: "WAIT_FOR_APPOINTMENT",
            },
          ],
        },
        { label: "Przełóż" },
      ]}
      notifyDate={getNotificationDate(notification)}
      notification={notification}
      setLookup={setLookup}
    >
      <div className="mt-1 flex flex-col">
        <div className="mt-2 flex items-center text-sm text-gray-500">
          <UserIcon
            className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
            aria-hidden="true"
          />
          Ostatni wykonawca TomKotły
        </div>
        <div className="mt-2 flex items-center text-sm text-gray-500">
          <UserIcon
            className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
            aria-hidden="true"
          />
          Tel.: 999233111
        </div>
        <div className="mt-2 flex items-center text-sm text-gray-500">
          <UserIcon
            className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
            aria-hidden="true"
          />
          email.: TomKotly@gmail.com
        </div>
        <div className="mt-2 flex items-center text-sm text-gray-500">
          <UserIcon
            className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
            aria-hidden="true"
          />
          ostatni koszt 123 zł
        </div>
        <div className="mt-2 flex items-center text-sm text-gray-500">
          <CalendarIcon
            className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
            aria-hidden="true"
          />
          Do wykonania przed 9 Stycznia 2020
        </div>
      </div>
    </Notification>
  );
}
