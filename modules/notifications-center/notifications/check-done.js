/* This example requires Tailwind CSS v2.0+ */
import { useRouter } from "next/router";
import { Fragment } from "react";
import {
  BriefcaseIcon,
  CalendarIcon,
  CashIcon,
  ChevronDownIcon,
  PhoneIcon,
  AtSymbolIcon,
  CurrencyDollarIcon,
  UserIcon,
  LocationMarkerIcon,
  PencilIcon,
} from "@heroicons/react/solid";
import { Menu, Transition } from "@headlessui/react";
import { getNotificationDate } from "../domain/notification/contract";

import Notification from "./template";

export default function Example({ notification, setLookup }) {
  const {
    asPath,
    pathname,
    query: { organizationId },
  } = useRouter();

  console.log("notificationcheckdonne", notification);
  const check = notification.previousCheckResult[0].checks[0];
  const appointment = notification.appointments[0];
  const executorName =
    appointment.attendeeCompany?.name ??
    `${appointment.attendeePerson.firstname} ${appointment.attendeePerson.surname}`;
  const executorPhone =
    appointment.attendeeCompany?.phone ?? appointment.attendeePerson?.phone;
  const executorEmail =
    appointment.attendeeCompany?.email ?? appointment.attendeePerson?.email;
  // const successHref = {
  // pathname: `/${organizationId}/check-result/new`,
  // query: {
  //   checkId: check.id,
  //   checkVer: check.ver,
  //   appointmentId: appointment?.id,
  //   appointmentVer: appointment?.ver,
  //   hidden: ["check", "appointment"],
  //   redirect: asPath,
  // },
  // };
  // const modifyHref = {
  //   pathname: `/${organizationId}/appointment/${appointment?.id}/edit`,
  //   query: {
  //     hidden: ["check"],
  //     redirect: asPath,
  //   },
  // };
  const editAppointmentHref = {
    pathname: pathname,
    query: {
      panel: "check",
      organizationId,
      // propertyId: notification.previousCheckResult[0].property?.id,
      notificationId: notification.id,
      notificationVer: notification.ver,
      hidden: ["notification"],
      id: check.id,
      objectId: appointment.id,
      tab: "edit-appointment",
    },
  };
  const addCheckResultHref = {
    pathname: pathname,
    query: {
      panel: "check",
      organizationId,
      // propertyId: notification.previousCheckResult[0].property?.id,
      parentNotificationId: notification.id,
      parentNotificationVer: notification.ver,
      checkId: check.id,
      checkVer: check.ver,
      hidden: ["parentNotification", "check"],
      id: check.id,
      // objectId: appointment.id,
      tab: "add-check-result",
    },
  };

  return (
    <Notification
      breadcrumbs={[{ label: "Przegląd" }, { label: "Wykonany" }]}
      property={check.property}
      title="Termin wykonania przeglądu piecy"
      actions={[
        { label: "Dodaj rezultat wizyty", href: addCheckResultHref },
        { label: "Edytuj wizytę", href: editAppointmentHref },
        { label: "Przełóż" },
      ]}
      notifyDate={getNotificationDate(notification)}
      notification={notification}
      setLookup={setLookup}
    >
      <div className="mt-1 flex flex-col">
        <div className="mt-2 flex items-center text-sm text-gray-500">
          <UserIcon
            className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
            aria-hidden="true"
          />
          Wykonawca {executorName}
        </div>
        {executorPhone && (
          <div className="mt-2 flex items-center text-sm text-gray-500">
            <PhoneIcon
              className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
              aria-hidden="true"
            />
            Tel.: {executorPhone}
          </div>
        )}
        {executorEmail && (
          <div className="mt-2 flex items-center text-sm text-gray-500">
            <AtSymbolIcon
              className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
              aria-hidden="true"
            />
            email.: {executorEmail}
          </div>
        )}
        <div className="mt-2 flex items-center text-sm text-gray-500">
          <CashIcon
            className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
            aria-hidden="true"
          />
          Planowany koszt {appointment.plannedCost} zł
        </div>
        <div className="mt-2 flex items-center text-sm text-gray-500">
          <CalendarIcon
            className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
            aria-hidden="true"
          />
          Do wykonania przed 9 Stycznia 2020 not done
        </div>
      </div>
    </Notification>
  );
}
