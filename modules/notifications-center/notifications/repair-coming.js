/* This example requires Tailwind CSS v2.0+ */
import { Fragment } from "react";
import {
  BriefcaseIcon,
  CalendarIcon,
  CheckIcon,
  ChevronDownIcon,
  ChevronRightIcon,
  CurrencyDollarIcon,
  UserIcon,
  LocationMarkerIcon,
  PencilIcon,
} from "@heroicons/react/solid";
import { Menu, Transition } from "@headlessui/react";
import { getNotificationDate } from "../domain/notification/contract";
import * as moment from "moment";

import Notification from "./template";

export default function Example({ notification, setLookup }) {
  const defect = notification.defects[0];
  const isUrgent = defect.urgency.key === "urgent";
  const { isOwnerAcceptanceNecessary, isForContractor } = defect;
  const hasAppointment = !!notification.appointments.length;

  return (
    <Notification
      breadcrumbs={[{ label: "Usterka" }, { label: "Termin naprawy" }]}
      title={
        isUrgent
          ? "Planowany termin pilnej usterki"
          : "Planowany termin dużej naprawy"
      }
      actions={[
        ...(isOwnerAcceptanceNecessary || isForContractor
          ? []
          : [
              {
                label: "Rozpocznij naprawę",
                tasks: [
                  { name: "Kupno materiałów" },
                  { name: "Wykonanie naprawy" },
                  { name: "Opłata" },
                  { name: "Zwrot kosztów" },
                ],
              },
            ]),
        ...(isOwnerAcceptanceNecessary
          ? [
              {
                label: "Wysłano zapytanie o autoryzację",
                tasks: [{ name: "Oczekuję na autoryzację", isBlocking: true }],
              },
            ]
          : []),
        ...(isForContractor
          ? [
              {
                label: "Wysłano zapytanie do wykonawcy",
                tasks: [{ name: "Oczekuję na wykonawcę", isBlocking: true }],
              },
            ]
          : []),
        { label: "Przełóż" },
      ]}
      notifyDate={getNotificationDate(notification)}
      notification={notification}
      setLookup={setLookup}
    >
      <div className="mt-1 flex flex-col">
        <div className="mt-2 flex items-center text-sm text-gray-500">
          <UserIcon
            className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
            aria-hidden="true"
          />
          {defect.name}
        </div>
        <div className="mt-2 flex items-center text-sm text-gray-500">
          <UserIcon
            className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
            aria-hidden="true"
          />
          Planowany koszt {defect.estimatedCost} zł
        </div>
        <div className="mt-2 flex items-center text-sm text-gray-500">
          <CalendarIcon
            className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
            aria-hidden="true"
          />
          Zgłoszone {moment(defect.createdAt).format("L")}
        </div>
      </div>
    </Notification>
  );
}
