import { useState } from "react";
import { useSubscription, useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";
import {
  ClipboardIcon,
  DocumentTextIcon,
  BriefcaseIcon,
} from "@heroicons/react/outline";

import NotificationsFeed from "./components-smart/notifications-feed";

import PropertiesList from "./components-smart/properties-list";

import Event from "./events";
import Todo from "./todos";
import ContractEndEvent from "./notifications/contract-end";
import CheckComingEvent from "./notifications/check-coming";
import CheckDoneEvent from "./notifications/check-done";
import RepairComingEvent from "./notifications/repair-coming";

import ContractEndTasks from "./tasks/contract-end";
import CheckComingTasks from "./tasks/check-coming";
import CheckDoneTasks from "./tasks/check-done";
import RepairComingTasks from "./tasks/repair-coming";

import ContractEndWaiting from "./waiting/contract-end";
import CheckComingWaiting from "./waiting/check-coming";
import CheckDoneWaiting from "./waiting/check-done";
import RepairComingWaiting from "./waiting/repair-coming";

import { clasifyNotifictions } from "./domain/notification";
import { getEvent, getTodo, getNotificationDate } from "./domain";

// import { getNotificationDate } from "../index";
import * as moment from "moment";

const ADD_TASKS = gql`
  mutation MyMutationTasks($tasks: [versionedTasks_insert_input!]!) {
    insertTasks(objects: $tasks) {
      returning {
        id
      }
      affected_rows
    }
  }
`;
const ADD_APPOINTMENT = gql`
mutation MyMutation($appointment: versionedAppointments_insert_input! ) {
  insertAppointment(object: $appointment) {
    id
  }
}
`;
const ADD_FIX_RESULT = gql`
mutation MyMutation($fixResult: versionedFixResults_insert_input! ) {
  insertFixResult(object: $fixResult) {
    id
  }
}
`;
// ${organizationId ? `{organization_id: {_eq:"${organizationId}"}},` : ''}

export const GET_NOTIFICATIONS_DEFECTS = (method, organizationId) => `
  ${method} DefectsSub($isFinishing: jsonb = {types:["FINISHING"]}) {
    notifications(where: {_and: {
      defects: {id: {_is_null: false}}, 
      _not:{tasks: {_and: {
        isDone: {_eq: true},
         config: {_contains: $isFinishing}
        }}}
      }}
    ) {  
      id
      ver
      reminderDate
      createdAt
      updatedAt
      tasks {
        id
        name
        cost
        decision
        executionDate
        isDone
        config
        createdAt
        updatedAt
        reponsiblePerson {
          id
          surname
          firstname
        }
      }
      decisions{
        key
        name
      }
      appointments {
        attendeePerson {
          id
          ver
          surname
          firstname
        }
        attendeeCompany {
          id
          ver
          name
        }
        date
        plannedCost
        notes
        id
      }
      
      defects {
        id
        ver
        isForContractor
        isOwnerAcceptanceNecessary
        name
        notes
        createdAt
        fixResults {
          id 
          ver
          date
          cost
          serviceman{
            id
            ver
          }
        }
        property {
          id
          name
        }
      
        urgency {
          id
          name
          key
          icon
          color
        }
        estimatedCost
      }
      precedingPeriod {
        id
        ver
        monthsBefore
        name
        weeksBefore
      }
      
    }
  }
  `;
const GET_NOTIFICATIONS_DEFECTS_GQL = gql`
  ${GET_NOTIFICATIONS_DEFECTS("subscription")}
`;
export const GET_NOTIFICATIONS_CHECKS = (method, organizationId) => `
  ${method} ChecksSub {
    notifications(where: 
      {_and: 
        {
          previousCheckResult: {id: {_is_null: false}}, 
          _not: {resultingCheckResult: {id: {_is_null: false}}}}
        }
      ) {
      id
      ver
      reminderDate
      createdAt
      updatedAt
      tasks {
        id
        name
        cost
        decision
        executionDate
        isDone
        config
        createdAt
        updatedAt
        reponsiblePerson {
          id
          surname
          firstname
        }
      }
      decisions{
        key
        name
      }
      appointments {
        attendeePerson {
          id
          ver
          surname
          firstname
        }
        attendeeCompany {
          id
          ver
          name
        }
        date
        plannedCost
        notes
        id
      }
      
      precedingPeriod {
        id
        ver
        monthsBefore
        name
        weeksBefore
      }
      previousCheckResult {
        checks {
          id
          ver
          name
          property {
            id
            ver
            name
          }
          recurrenceInterval {
            id
            name
            monthsInterval
          }
        }
        check {
          id
          ver
          name
          property {
            id
            ver
            name
          }
          recurrenceInterval {
            id
            name
            monthsInterval
          }
        }
        checks {
          id
          ver
          name
          property {
            id
            ver
            name
          }
          recurrenceInterval {
            id
            name
            monthsInterval
          }
        }
        cost
        date
        id
        ver
      }
      resultingCheckResult {
        cost
        date
        id
        ver
      }
    }
  }
  `;
const GET_NOTIFICATIONS_CHECKS_GQL = gql`
  ${GET_NOTIFICATIONS_CHECKS("subscription")}
`;


//notification neeed to have contnract
// contract cannot have terminnation
// contract cannnot have extensions signed
// annexes cannot be to contracts that are terminated
export const GET_NOTIFICATIONS_CONTRACTS = (method, organizationId) => `
  ${method} ContractsSu {
    notifications(where:
      {
        _and: [
          {
            contracts: {id: {_is_null: false}}, 
          },
          {
            _not: {contracts: {contracts: {_or: {
              id: {_is_null: false},
              typeId: {_eq: "54d6733d-d6ff-4ce7-8762-d41f00d8e312"}
            }}}}
          },
          {
            _not: {contracts: {contracts: {_or: {
              id: {_is_null: false},
              typeId: {_eq: "0e02eb25-fd98-473a-ad2b-2e884bbd3615"}
            }}}}
          },
          {
            _not:{
              contracts:{
                refersToContract:{
                  contracts: {_or: {
                    id: {_is_null: false},
                    typeId: {_eq: "54d6733d-d6ff-4ce7-8762-d41f00d8e312"}
                  }}
                }
              }
            }
          }
        ]
      }
      ) {
      id
      ver
      reminderDate
      createdAt
      updatedAt
      tasks {
        id
        name
        cost
        decision
        executionDate
        isDone
        config
        createdAt
        updatedAt
        reponsiblePerson {
          id
          surname
          firstname
        }
      }
      decisions{
        key
        name
      }
      appointments {
        attendeePerson {
          id
          ver
          surname
          firstname
        }
        attendeeCompany {
          id
          ver
          name
        }
        date
        plannedCost
        notes
        id
      }
      contracts {
        endDate
        signer { 
          id
          ver
          firstname
          surname
        }
        contracts {
          id
          type {
            id
            effect
          }
        }
        subjectProperty {
          id
          ver
          name
        }
        id
        ver
        startDate
        type {
          name
          id
          effect
          ver
        }
        category { 
          id
          key
          label
          ver
        }
      }
      
      precedingPeriod {
        id
        ver
        monthsBefore
        name
        weeksBefore
      }
      
    }
  }
  `;
const GET_NOTIFICATIONS_CONTRACTS_GQL = gql`
  ${GET_NOTIFICATIONS_CONTRACTS("subscription")}
`;

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

// 1	Enargia Elektryczna czynna całodobowa
// 2	Opłata handlowa
// 3	Składnik stały stawki sieciowej
// 4	Opłata przejściowa
// 5	Stawka jakościowa Całodobowa
// 6	Składnik zmienny stawki sieciowej
// 7	Opłata OZE pozaszczytowa
// 8	Oplata kogeneracyjna całodobowa
// 9	Oplata abonamentowa
// 10	Opłata Mocowa

export default function Home({ notifications: staticNotifications, properties }) {
  const [hoveredId, setHoveredId] = useState("");
  const [showContracts, setShowContracts] = useState(false);
  const [showChecks, setShowChecks] = useState(false);
  const [showDefects, setShowDefects] = useState(false);
  const [filterPropertyIds, setFilterPropertyIds] = useState([]);
  const { loading: loadigContracts, error: errorContracts, data: dataContracts } = useSubscription(GET_NOTIFICATIONS_CONTRACTS_GQL);
  const { loading: loadigChecks, error: errorChecks, data: dataChecks } = useSubscription(GET_NOTIFICATIONS_CHECKS_GQL);
  const { loading: loadigDefects, error: errorDefects, data: dataDefects } = useSubscription(GET_NOTIFICATIONS_DEFECTS_GQL);
  const [addTasks, { error: errorTasks, loading: loadingTasks }] =
    useMutation(ADD_TASKS);
  const [addAppointment, { error: errorAppointment, loading: loadingAppointment }] =
    useMutation(ADD_APPOINTMENT);
  const [addFixResult, { error: errorFixResult, loading: loadingFixResult }] =
    useMutation(ADD_FIX_RESULT);
  const loading = loadigContracts || loadigChecks || loadigDefects

  console.log("notifications-loading", loading, "loadigContracts", loadigContracts, "loadigChecks", loadigChecks, "loadigDefects", loadigDefects);
  const mergedNotifications = dataContracts && dataChecks && dataDefects && [...dataContracts?.notifications, ...dataChecks?.notifications, ...dataDefects?.notifications]

  const notifications = mergedNotifications ?? staticNotifications ?? [];
  console.log("looookup hoveredId", hoveredId);
  console.log("notifications", notifications);

  const handleAddTasks = (notification) => async (newTasks) => {
    if (loadingTasks || !notification || !newTasks || newTasks.length === 0) {
      return;
    }

    const tasks = newTasks?.map(
      ({ id, name, isDone, config, decision, deleted, ...configRest }) => ({
        id,
        name,
        isDone,
        decision,
        deleted,
        config: { ...config, ...configRest },
        notificationId: notification.id,
        notificationVer: notification.ver,
      })
    );

    await addTasks({ variables: { tasks } });
  };
  const handleClearDefectFix = notification => async () => {
    console.log('handleClearDefectFix', notification)
    if (loadingFixResult || !notification) {
      return;
    }
    const defect = notification.defects[0]
    const fixResult = notification.defects[0].fixResults[0];
    const {
      id,
      date,
      cost,
      serviceman
    } = fixResult

    const newFixResult = {
      servicemanId: serviceman?.id,
      servicemanVer: serviceman?.ver,
      defectId: defect?.id,
      defectVer: defect?.ver,
      id,
      date,
      cost,
      deleted: true,
    }

    await addFixResult({ variables: { fixResult: newFixResult } });

  }
  const handleClearAppointment = (notification) => async () => {
    console.log('handleClearAppointment', notification)
    const appointment = notification.appointments[0];
    if (loadingAppointment || !notification) {
      return;
    }
    const {
      attendeePerson,
      attendeeCompany,
      date,
      plannedCost,
      notes,
      id
    } = appointment

    const newAppointment = {
      attendeeCompanyId: attendeeCompany?.id,
      attendeeCompanyVer: attendeeCompany?.ver,
      attendeePersonId: attendeePerson?.id,
      attendeePersonVer: attendeePerson?.ver,
      notificationId: notification.id,
      notificationVer: notification.ver,
      date,
      id,
      notes,
      plannedCost,
      deleted: true,
    }

    await addAppointment({ variables: { appointment: newAppointment } });
  };

  // if (loading) {
  //   return <span>Wczytuję...</span>;
  // }
  if (errorContracts) {
    console.error(errorContracts);
    return <span>Błąd! {errorContracts?.message ?? JSON.stringify(errorContracts)}</span>;
  }
  if (errorChecks) {
    console.error(errorChecks);
    return <span>Błąd! {errorChecks?.message ?? JSON.stringify(errorChecks)}</span>;
  }
  if (errorDefects) {
    console.error(errorDefects);
    return <span>Błąd! {errorDefects?.message ?? JSON.stringify(errorDefects)}</span>;
  }
  if (!notifications) {
    return <span>Organizacja o tym identyfikatorze nie istnieje</span>;
  }
  // const { events, doing, waiting } = clasifyNotifictions(notifications);

  // console.log("notifications classify ", events, doing, waiting);

  const sortedNotifications = notifications
    .filter(notification => {
      if (!showContracts && !showChecks && !showDefects) return true
      if (notification.contracts?.length && !showContracts) return false
      if (notification.previousCheckResult?.length && !showChecks) return false
      if (notification.defects?.length && !showDefects) return false
      return true
    })
    .map(notification => ({
      ...notification,
      notifyDate: getNotificationDate(notification)//.toISOString()
    }))
    .sort((a, b) => {
      const diff = moment(a.notifyDate).diff(moment(b.notifyDate), 'days');
      // const diff = b.notifyDate < a.notifyDate ? -1 : b.notifyDate > a.notifyDate ? 1 : 0; //moment(b.notifyDate).diff(moment(a.notifyDate), 'days');
      // console.log('SORTTT', diff, a.notifyDate.format('L'), b.notifyDate.format('L'));
      return diff
    })
  // console.log('SORTTT - endnddnnd', sortedNotifications.map(it => it.notifyDate.format('L')))

  return (
    <div className="flex-1 flex items-stretch ">
      {/* overflow-hidden */}
      {loading ? (
        <div>Wczytuje...</div>
      ) :
        (<>
          <main className="flex-1  ">
            {/* overflow-y-auto*/}
            {/* Primary column */}
            <section
              aria-labelledby="primary-heading"
              className="min-w-0 flex-1 h-full flex flex-col  lg:order-last"
            >
              {/* overflow-hidden */}
              <h2
                id="primary-heading"
                className="text-2xl font-extrabold text-gray-300 tracking-tight sm:text-3xl self-center mt-3"
              >
                Wydarzenia
              </h2>
              {errorTasks && errorTasks.message}
              {errorAppointment && errorAppointment.message}
              {errorFixResult && errorFixResult.message}
              <div className='flex mx-auto'>
                <p
                  className="text-base bg-gray-100  text-gray-300 tracking-tight  self-center mr-4"
                >
                  Filtruj:
                </p>
                <span onClick={() => setShowContracts(!showContracts)} className={classNames(showContracts ? 'bg-indigo-600' : 'bg-indigo-200', "cursor-pointer flex px-1 py-1 inline-block rounded-xl mr-3")}>
                  <div className="text-white group flex items-center text-xs font-normal rounded-md">
                    <DocumentTextIcon className="text-white flex-shrink-0 h-4 w-4" />
                  </div>
                  <p
                    className="text-base font-semibold text-white ml-1 tracking-tight  self-center "
                  >
                    Umowy
                  </p>
                </span>
                <span onClick={() => setShowChecks(!showChecks)} className={classNames(showChecks ? 'bg-indigo-600' : 'bg-indigo-200', "cursor-pointer flex px-1 py-1 inline-block rounded-xl mr-3")}>
                  <div className="text-white group flex items-center text-xs font-normal rounded-md">
                    <ClipboardIcon className="text-white flex-shrink-0 h-4 w-4" />
                  </div>
                  <p
                    className="text-base font-semibold text-white ml-1 tracking-tight  self-center "
                  >
                    Przeglądy
                  </p>
                </span>
                <span onClick={() => setShowDefects(!showDefects)} className={classNames(showDefects ? 'bg-indigo-600' : 'bg-indigo-200', "cursor-pointer flex px-1 py-1 inline-block rounded-xl mr-3")}>
                  <div className="text-white group flex items-center text-xs font-normal rounded-md">
                    <BriefcaseIcon className="text-white flex-shrink-0 h-4 w-4" />
                  </div>
                  <p
                    className="text-base font-semibold text-white ml-1 tracking-tight  self-center "
                  >
                    Usterki
                  </p>
                </span>

              </div>
              {/* Your content */}

              {(!staticNotifications && (loadigContracts || loadigChecks || loadigDefects)) ? <div>Wczytuję</div> : <div className="mt-2 px-1 py-5 sm:p-2 space-y-6">
                {/* overflow-y-auto*/}
                <div className="bg-white  sm:rounded-md">
                  {/* overflow-hidden */}
                  <ul role="list" className="divide-y divide-gray-200">
                    {sortedNotifications
                      .reduce((acc, notification, i) => {

                        const event = getEvent(notification)

                        if (filterPropertyIds.length > 0 && !filterPropertyIds.includes(event.propertyId)) {
                          return acc
                        }

                        console.log('KKKKK', notification)
                        const eventComponent = (<Event
                          className='shadow'
                          event={getEvent(notification)}
                          addTasks={handleAddTasks(notification)}
                          onEnterHover={() => setHoveredId(notification.id)}
                          onLeaveHover={() => setHoveredId("")}
                          isHighlighted={hoveredId === notification.id}
                        />)

                        // console.log('isNextMonthThanPrevious00000',
                        //   self.map(a => getEvent(a).date),
                        //   acc.length,
                        //   getEvent(notification).date,
                        //   self.map(a => getEvent(a).date)[acc.length],
                        //   acc.length > 0 && self.map(a => getEvent(a).date)[acc.length - 1],
                        //   // acc.length > 1 && self.map(a => getEvent(a).date)[acc.length - 2],
                        // )
                        // if (acc.length === 0) {
                        //   return [...acc, event]
                        // }
                        // const previousNotificationDateChunks = getEvent(self[acc.length - 1]).date.split('.')
                        // const isTheSameYear = currentNotificationDateChunks[2] === previousNotificationDateChunks[2]
                        // const isTheSameMonth = currentNotificationDateChunks[1] === previousNotificationDateChunks[1]
                        // const isNextMonthThanPrevious = !(isTheSameYear && isTheSameMonth)
                        // console.log('isNextMonthThanPrevious', currentNotificationDateChunks, previousNotificationDateChunks, isNextMonthThanPrevious)
                        // if (isNextMonthThanPrevious) {
                        //   return [...acc, monthHeader, event]

                        // }
                        const currentNotificationDateChunks = event.date.split('.')
                        if (acc.length > 0) {
                          const previousNotificationDateChunks = acc[acc.length - 1].dateChunks
                          const isTheSameYear = currentNotificationDateChunks[2] === previousNotificationDateChunks[2]
                          const isTheSameMonth = currentNotificationDateChunks[1] === previousNotificationDateChunks[1]
                          const isTheSameMonthAndYear = isTheSameYear && isTheSameMonth
                          // console.log('isTheSameMonthAndYear', i, currentNotificationDateChunks, previousNotificationDateChunks, isTheSameMonthAndYear)
                          if (isTheSameMonthAndYear) {
                            return [...acc, { components: [eventComponent], dateChunks: currentNotificationDateChunks }]
                          }
                        }
                        const monthHeader = (<h4
                          className="text-xl bg-gray-100 font-extrabold text-gray-300 tracking-tight sm:text-3xl self-center pl-3"
                        >
                          {currentNotificationDateChunks[1]}.{currentNotificationDateChunks[2]}
                        </h4>)
                        return [...acc, { components: [monthHeader, eventComponent], dateChunks: currentNotificationDateChunks }]
                      }, [])
                      .map(({ components }) => components)}
                  </ul>
                </div>
              </div>}
            </section>
          </main>

          <section className="hidden lg:block w-1/2 opacity-100 hover:opacity-100 h-full flex flex-col mr-20">
            <h3
              id="tasks-heading"
              className="text-2xl font-extrabold text-gray-300 tracking-tight sm:text-3xl text-center self-center mt-3"
            >
              Zadania
            </h3>
            {/* Your content */}
            {(!staticNotifications && (loadigContracts || loadigChecks || loadigDefects)) ? <div>Wczytuję</div> : <div className="flex">
              <div className="w-1/2 mt-2 px-1 py-5 sm:p-2 space-y-6">
                {sortedNotifications
                  .filter(notif => !!getTodo(notif))
                  .map(
                    (notification, i) => {
                      const event = getEvent(notification)

                      if (filterPropertyIds.length > 0 && !filterPropertyIds.includes(event.propertyId)) {
                        return;
                      }

                      return i % 2 === 0 && (
                        <Todo
                          todo={getTodo(notification)}
                          addTasks={handleAddTasks(notification)}
                          onEnterHover={() => setHoveredId(notification.id)}
                          onLeaveHover={() => setHoveredId("")}
                          isHighlighted={hoveredId === notification.id}
                          clearAppointment={handleClearAppointment(notification)}
                          clearDefectFix={handleClearDefectFix(notification)}
                        />
                      )
                    })}

                {/* {[...events, ...doing, ...waiting].map((notification, i) => {
              if (i % 2 === 0) {
                return;
              }
              switch (notification.type) {
                case "CHECK-DONE":
                  return (
                    <CheckDoneTasks
                      notification={notification}
                      setLookup={setLookup}
                    />
                  );
                case "CHECK-PLANNING":
                  return (
                    <CheckComingTasks
                      notification={notification}
                      setLookup={setLookup}
                    />
                  );
                case "CONTRACT":
                  return (
                    <ContractEndTasks
                      notification={notification}
                      setLookup={setLookup}
                    />
                  );
                case "DEFECT":
                  return <RepairComingTasks notification={notification} />;
              }
            })} */}
              </div>
              <div className="w-1/2 mt-2 px-1 py-5 sm:p-2 space-y-6">
                {sortedNotifications
                  .filter(notif => !!getTodo(notif))
                  .map(
                    (notification, i) => {
                      const event = getEvent(notification)

                      if (filterPropertyIds.length > 0 && !filterPropertyIds.includes(event.propertyId)) {
                        return;
                      }

                      return i % 2 === 1 && (
                        <Todo
                          todo={getTodo(notification)}
                          addTasks={handleAddTasks(notification)}
                          onEnterHover={() => setHoveredId(notification.id)}
                          onLeaveHover={() => setHoveredId("")}
                          isHighlighted={hoveredId === notification.id}
                          clearAppointment={handleClearAppointment(notification)}
                          clearDefectFix={handleClearDefectFix(notification)}
                        />
                      )
                    }
                  )}
                {/* {[...events, ...doing, ...waiting].map((notification, i) => {
              if (i % 2 === 1) {
                return;
              }
              switch (notification.type) {
                case "CHECK-DONE":
                  return (
                    <CheckDoneTasks
                      notification={notification}
                      setLookup={setLookup}
                    />
                  );
                case "CHECK-PLANNING":
                  return (
                    <CheckComingTasks
                      notification={notification}
                      setLookup={setLookup}
                    />
                  );
                case "CONTRACT":
                  return (
                    <ContractEndTasks
                      notification={notification}
                      setLookup={setLookup}
                    />
                  );
                case "DEFECT":
                  return <RepairComingTasks notification={notification} />;
              }
            })} */}
              </div>
            </div>}
          </section>
        </>)
      }
      {/* <section className="hidden lg:block w-1/5 overflow-y-auto opacity-100 hover:opacity-100 mr-20 h-full flex flex-col">
        <h3
          id="tasks-heading"
          className="text-2xl font-extrabold text-gray-300 tracking-tight sm:text-3xl text-center self-center mt-3"
        >
          Zadania
        </h3>
        
      </section> */}

      {/* Secondary column (hidden on smaller screens) */}
      <aside className="shadow absolute right-0 hidden w-80 bg-white overflow-y-auto lg:block transform translate-x-3/4 hover:translate-x-0 transition-transform">
        {/* Your content */}
        <div className="px-4 py-5 sm:p-6">
          <PropertiesList properties={properties} setFilterPropertyIds={setFilterPropertyIds} filterPropertyIds={filterPropertyIds} />
        </div>
      </aside>
    </div >
  );
}
