import Notification1 from "../../../components/notification-1";
import Notification2 from "../../../components/notification-2";
import Notification3 from "../../../components/notification-3";
import Notification4 from "../../../components/notification-4";
import FeedDivider from "../../../components/feed-divider";

const notifications = [
  { id: 111111, subject: "Finishing contract", type: "document-end-date" },
  { id: 1, subject: "aaaa", type: "1" },
  { id: 211, subject: "2 may 2021", type: "new-date" },
  { id: 2, subject: "aaaa", type: "2" },
  { id: 3, subject: "aaaa", type: "3" },
  { id: 4, subject: "aaaa", type: "4" },
];

export default function Example() {
  return (
    <div className="flow-root">
      <ul className="-mb-8">
        {notifications.map((notification) => (
          <li key={notification.id} className="mb-8">
            {(() => {
              switch (notification.type) {
                case "1":
                  return <Notification1 />;
                case "2":
                  return <Notification2 />;
                case "3":
                  return <Notification3 />;
                case "4":
                  return <Notification4 />;
                case "new-date":
                  return <FeedDivider text={notification.subject} />;
                case "document-end-date":
                  return;

                default:
                  throw new Error("unknown notification type");
              }
            })()}
          </li>
        ))}
      </ul>
    </div>
  );
}
