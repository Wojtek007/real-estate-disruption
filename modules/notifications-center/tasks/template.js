import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";
import {
  ChevronUpIcon,
  CalendarIcon,
  QuestionMarkCircleIcon,
  FolderIcon,
  HomeIcon,
  InboxIcon,
  UsersIcon,
  DocumentAddIcon,
  CheckIcon,
  ClockIcon,
} from "@heroicons/react/outline";
import {
  LocationMarkerIcon,
  ChevronDoubleLeftIcon,
} from "@heroicons/react/solid";
import * as moment from "moment";

const ADD_TASKS = gql`
  mutation MyMutation($tasks: [versionedTasks_insert_input!]!) {
    insertTasks(objects: $tasks) {
      returning {
        id
      }
      affected_rows
    }
  }
`;

export default function Example({ title, property, notification, setLookup }) {
  const [addTasks, { error: errorTasks, loading: loadingTasks }] =
    useMutation(ADD_TASKS);

  const handleAddTask = async (task) => {
    if (loadingTasks) return;

    const newTask = {
      id: task.id,
      name: task.name,
      isBlocking: task.isBlocking,
      isClosed: task.isClosed,
      isDone: !task.isDone,
      type: task.type,
      notificationId: notification.id,
      notificationVer: notification.ver,
    };

    await addTasks({
      variables: {
        tasks: [newTask],
      },
    });
  };

  const handleUpdateNotification = () => {
    console.log("looookup", notification.id);
    setLookup(notification.id);
  };
  return (
    <div className="hover:bg-gray-50 relative flex flex-col bg-white rounded-2xl  border-white border-2">
      <div className="absolute left-2 top-0 px-1 py-1 inline-block bg-indigo-600 rounded-xl transform -translate-y-1/2">
        <div className="text-white group flex items-center text-xs font-normal rounded-md">
          <DocumentAddIcon className="text-white flex-shrink-0 h-4 w-4" />
        </div>
      </div>
      {notification.appointment && (
        <div className="absolute right-2 top-0 px-2 py-1 inline-block bg-indigo-600 rounded-xl transform -translate-y-1/2">
          <div className="text-white group flex items-center text-xs font-normal rounded-md">
            <span className="truncate">
              {moment(notification.appointment.date).format("L")}
            </span>
            <CalendarIcon className="text-white flex-shrink-0 ml-2 -mr-1 h-4 w-4" />
          </div>
        </div>
      )}
      <div className="mt-3 mb-1 px-1">
        <a
          href="#"
          onClick={handleUpdateNotification}
          className=" hover:underline"
        >
          <p className="flex items-center px-3 py-1 text-xs font-medium rounded-md text-gray-600">
            {/*group hover:bg-gray-50 hover:text-gray-900*/}
            <CheckIcon className="text-gray-400 flex-shrink-0 -ml-1 mr-2 h-5 w-5" />
            {/*group-hover:text-gray-500 */}
            <span className="truncate">{title}</span>
          </p>
        </a>
        <p className="flex items-center px-3 py-1 text-xs font-medium rounded-md text-gray-600">
          {/*group hover:bg-gray-50 hover:text-gray-900*/}
          <HomeIcon className="text-gray-400 flex-shrink-0 -ml-1 mr-2 h-5 w-5" />
          <span className="truncate">{property.name} / Tomasz Szukała</span>
        </p>
        {errorTasks?.message}
        <div className="mt-1 pl-7">
          {notification.tasks
            .sort((a, b) => a.createdAt - b.createdAt)
            .map((task) => (
              <div
                className="relative flex items-center"
                onClick={() => handleAddTask(task)}
              >
                <div className="flex items-center h-5">
                  <input
                    id={task.id}
                    aria-describedby="offers-description"
                    name={task.name}
                    checked={!!(task.isDone || task.isClosed)}
                    type="checkbox"
                    className="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded"
                  />
                </div>
                <div className="ml-2 text-xs">
                  <label htmlFor={task.id} className="text-gray-600">
                    {task.name}
                  </label>
                  {/* <span id="offers-description" className="text-gray-500">
                          <span className="sr-only">Offers </span>when they are
                          accepted or rejected by candidates.
                        </span> */}
                </div>
              </div>
            ))}
        </div>
      </div>
    </div>
  );

  return (
    <div className="bg-white shadow overflow-hidden sm:rounded-md ">
      <ul className="divide-y divide-gray-200">
        <li key={"position.id"}>
          <div className="block hover:bg-gray-50">
            <div className="px-2 py-2 sm:px-6">
              <div className="flex items-center">
                <ChevronDoubleLeftIcon
                  className="flex-shrink-0 h-5 w-5 text-gray-400 mr-2 hover:text-gray-600"
                  aria-hidden="true"
                />
                <p className="text-sm font-medium text-indigo-600">
                  {title}
                  <br />
                  {property?.name}
                </p>
                {/* <div className="ml-2 flex-shrink-0 flex">
                    <p className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                      {position.type}
                    </p>
                  </div> */}
              </div>
              <div className="mt-1">
                {notification.tasks.map((task) => (
                  <div className="relative flex items-start">
                    <div className="flex items-center h-5">
                      <input
                        id={task.id}
                        aria-describedby="offers-description"
                        name={task.name}
                        type="checkbox"
                        className="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded"
                      />
                    </div>
                    <div className="ml-3 text-sm">
                      <label htmlFor={task.id} className="text-gray-600">
                        {task.name}
                      </label>
                      {/* <span id="offers-description" className="text-gray-500">
                          <span className="sr-only">Offers </span>when they are
                          accepted or rejected by candidates.
                        </span> */}
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  );
}
