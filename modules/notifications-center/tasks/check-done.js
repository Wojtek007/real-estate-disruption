/* This example requires Tailwind CSS v2.0+ */
import {
  CalendarIcon,
  LocationMarkerIcon,
  ChevronDoubleLeftIcon,
} from "@heroicons/react/solid";
import Tasks from "./template";

export default function Example({ notification, setLookup }) {
  return (
    <Tasks
      title="Przegląd done"
      notification={notification}
      propertyName={notification.checks[0].property}
      setLookup={setLookup}
    />
  );
}
