/* This example requires Tailwind CSS v2.0+ */
import {
  CalendarIcon,
  LocationMarkerIcon,
  ChevronDoubleLeftIcon,
} from "@heroicons/react/solid";
import Tasks from "./template";

export default function Example({ notification }) {
  return (
    <Tasks
      title="Naprawa"
      property={notification.repairs[0].property}
      tasks={notification.tasks}
    />
  );
}
