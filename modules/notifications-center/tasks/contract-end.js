/* This example requires Tailwind CSS v2.0+ */
import {
  CalendarIcon,
  LocationMarkerIcon,
  ChevronDoubleLeftIcon,
} from "@heroicons/react/solid";
import Tasks from "./template";

export default function Example({ notification, setLookup }) {
  return (
    <Tasks
      title="Przedłużyć umowę"
      property={notification.contracts[0].subjectProperty}
      notification={notification}
      setLookup={setLookup}
    />
  );
}
