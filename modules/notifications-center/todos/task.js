import { useRouter } from "next/router";
import Link from "next/link";

import DecisionMenu from "./decision-menu";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function Example({ task, addTasks, tasks, clearAppointment, clearDefectFix }) {
  const {
    pathname,
    query: { organizationId },
  } = useRouter();
  const hasHrefQuery = task.hrefQuery;
  const isDecision = task.config.types?.includes("DECIDE");
  const handleMarkDone = async () => {
    const { isBlocked, isDone, isDeprecated, config, id, name } = task;
    if (isDone) {
      throw Error('Cannot mark done task tht is already done')
    }
    if (isBlocked || isDeprecated || hasHrefQuery) {
      return;
    }

    const hasOrdinaryBehaviour = config.types?.every((type) =>
      ["BLOCK"].includes(type)
    );
    const isFininshingTask = config.types?.some((type) =>
      ["FINISHING"].includes(type)
    );
    console.log("11111 handleMarkDone : HAS TYPEs config:", config, hasOrdinaryBehaviour, isFininshingTask);

    if (!config.types || hasOrdinaryBehaviour || isFininshingTask) {
      await addTasks([{ id, config, name, isDone: true }]);
    }
    console.log("44444 handleMarkDone : HAS TYPEs config:", config);



  };
  const handleMarkUndone = async () => {
    console.log("999999 handleMarkUnDone : HAS TYPEs config:", task);
    const { isBlocked, isDone, isDeprecated, config, id, name } = task;

    if (config.types?.includes('DECIDE')) {
      const modifiedDecisionTask = {
        id,
        config,
        name,
        isDone: false,
        decision: null,
      };
      const oldTasks = tasks
        .filter(task => task.config.order > config.order)
        .map(task => ({ ...task, deleted: true }))

      await addTasks([modifiedDecisionTask, ...oldTasks]);
      return;
    }
    if (config.types?.includes("SCHEDULE")) {
      await clearAppointment()
      return
    }
    if (config.types?.includes('RESULT')) {
      await clearDefectFix()
      return
    }

    await addTasks([{ id, config, name, isDone: false }]);

  };
  const handleMakeDecision = async (decisionKey) => {
    const { isDone, isBlocked, isDeprecated, config, id, name } = task;

    if (isBlocked || isDone) {
      return;
    }

    const modifiedDecisionTask = {
      id,
      config,
      name,
      isDone: true,
      decision: decisionKey,
    };
    const extraTasks = config.decisions.find(
      (decision) => decision.key === decisionKey
    ).tasks;

    await addTasks([modifiedDecisionTask, ...extraTasks]);
  };
  console.log("99999", hasHrefQuery, task);
  const href = {
    pathname: pathname,
    query: {
      ...task.hrefQuery,
      organizationId,
    },
  };


  return (
    <div className="relative flex items-center" >
      {(() => {
        const inputBox = (
          <div className="flex items-center h-5" onClick={task.isDone ? handleMarkUndone : handleMarkDone}>
            <input
              // id={task.id}
              // name={task.name}
              checked={!!task.isDone}
              type="checkbox"
              className={classNames(
                "h-4 w-4 rounded  cursor-pointer",
                task.isBlocked || task.isDeprecated
                  ? "focus:ring-gray-300 text-gray-600 border-gray-200"
                  : "focus:ring-indigo-500 text-indigo-600 border-gray-300"
              )}
            />
          </div>
        )
        if (task.isDone) {
          return inputBox
        }
        if (hasHrefQuery) {
          return (
            <Link href={href} scroll={false}>
              {inputBox}
            </Link>
          )
        }
        if (isDecision) {
          return (
            <DecisionMenu decisions={task.decisions} makeDecision={handleMakeDecision}>
              {inputBox}
            </DecisionMenu>
          )
        }
        return inputBox
      })()}
      <div className="ml-2 text-xs">
        <label
          // htmlFor={task.id}
          className={classNames(
            task.isDeprecated && "line-through",
            task.isBlocked ? "text-gray-200" : "text-gray-600"
          )}
        >
          {task.name}{" "}
          {!!task.date && <span className="text-gray-500">{task.date}</span>}
          {!!task.info && <span className="text-gray-300">{task.info}</span>}
        </label>
        {/* <span id="offers-description" className="text-gray-500">
              <span className="sr-only">Offers </span>when they are
              accepted or rejected by candidates.
            </span> */}
      </div>
    </div>
  );
}
