import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";
import {
  ChevronUpIcon,
  CalendarIcon,
  QuestionMarkCircleIcon,
  FolderIcon,
  HomeIcon,
  InboxIcon,
  UsersIcon,
  DocumentAddIcon,
  CheckIcon,
  ClockIcon,
  BadgeCheckIcon
} from "@heroicons/react/outline";
import {
  LocationMarkerIcon,
  ChevronDoubleLeftIcon,
} from "@heroicons/react/solid";
import * as moment from "moment";
import Task from "./task";

/**
 * todo:
 * x make possible to mark task as Done
 * x show done tasks properly
 * x show todoList status waiting properly
 * x show event status as waiting proably instead of arrows
 * x show gray out tasks that have blocker not done
 * x mark shceduling task done possible
 * x show todoList as scheduled
 * x show event as scheduled
 * x stirkethrough tasks  which appears in predecesors of somthing else (maybe change to ancestor as it would be more logic of this other task)
 * - show decsion component
 * - put decision inside this task when made
 * - create tasks based on decision 
 * - make possible to undo the decision
 
 * - make marking result tasks possible
 * - show events as done
 * - make possible to edit the result
 * - enable closing of nnotofcaitions when FINISHING task is done
 * - highlingt proper todo list when hover the event
 * - otherway around
 * how to enable reveritng actions?
 */

export default function Example({
  todo,
  addTasks,
  onEnterHover,
  onLeaveHover,
  isHighlighted,
  clearAppointment,
  clearDefectFix
}) {
  if (!todo) {
    return null;
  }

  return (
    <div
      className={`hover:bg-gray-50 relative flex flex-col bg-white rounded-2xl border-${isHighlighted ? "indigo-600" : "white"
        } border-2`}
      onMouseEnter={onEnterHover}
      onMouseLeave={onLeaveHover}
    >
      <div className="absolute left-2 top-0 px-1 py-1 inline-block bg-indigo-600 rounded-xl transform -translate-y-1/2">
        <div className="text-white group flex items-center text-xs font-normal rounded-md">
          {todo.categoryIcon && (
            <todo.categoryIcon className="text-white flex-shrink-0 h-4 w-4" />
          )}
        </div>
      </div>
      {todo.appointmentDate && (
        <div className="absolute right-2 top-0 px-2 py-1 inline-block bg-indigo-600 rounded-xl transform -translate-y-1/2">
          <div className="text-white group flex items-center text-xs font-normal rounded-md">
            <span className="truncate">{todo.appointmentDate}</span>
            <CalendarIcon className="text-white flex-shrink-0 ml-2 -mr-1 h-4 w-4" />
          </div>
        </div>
      )}
      {todo.waitingDays && (
        <div className="absolute right-2 top-0 px-2 py-1 inline-block bg-indigo-600 rounded-xl transform -translate-y-1/2">
          <div className="text-white group flex items-center text-xs font-normal rounded-md">
            <span className="truncate">czekasz {todo.waitingDays}</span>
            <ClockIcon className="text-white flex-shrink-0 ml-2 -mr-1 h-4 w-4" />
          </div>
        </div>
      )}
      {todo.finishedDays && (
        <div className="absolute right-2 top-0 px-2 py-1 inline-block bg-indigo-600 rounded-xl transform -translate-y-1/2">
          <div className="text-white group flex items-center text-xs font-normal rounded-md">
            <span className="truncate">naprawione {todo.finishedDays}</span>
            <BadgeCheckIcon className="text-white flex-shrink-0 ml-2 -mr-1 h-4 w-4" />
          </div>
        </div>
      )}
      <div className="mt-3 mb-1 px-1">
        <a href="#" className=" hover:underline">
          <p className="flex items-center px-3 py-1 text-xs font-medium rounded-md text-gray-600">
            {/*group hover:bg-gray-50 hover:text-gray-900*/}
            <CheckIcon className="text-gray-400 flex-shrink-0 -ml-1 mr-2 h-5 w-5" />
            {/*group-hover:text-gray-500 truncate */}
            <span className="">{todo.title}</span>
          </p>
        </a>
        <p className="flex items-center px-3 py-1 text-xs font-medium rounded-md text-gray-600">
          {/*group hover:bg-gray-50 hover:text-gray-900*/}
          <HomeIcon className="text-gray-400 flex-shrink-0 -ml-1 mr-2 h-5 w-5" />
          <span className="truncate">{todo.propertyName}</span>
        </p>
        <div className="mt-1 pl-7">
          {todo.tasks
            ?.sort((a, b) => a.config.order - b.config.order)
            .map((task) => (
              <Task task={task} addTasks={addTasks} tasks={todo.tasks} clearAppointment={clearAppointment} clearDefectFix={clearDefectFix} />
            ))}
        </div>
      </div>
    </div>
  );
}
