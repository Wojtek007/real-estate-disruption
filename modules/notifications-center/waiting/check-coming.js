import Waiting from "./template";
import {
  ChevronUpIcon,
  CalendarIcon,
  QuestionMarkCircleIcon,
  FolderIcon,
  HomeIcon,
  InboxIcon,
  UsersIcon,
  ClipboardIcon,
  ClockIcon,
} from "@heroicons/react/outline";

export default function Example({ notification, setLookup }) {
  return (
    <Waiting
      property={notification.previousCheckResult[0].checks[0].property}
      notification={notification}
      setLookup={setLookup}
      config={{
        categoryIcon: ClipboardIcon,
        subjectIcon: QuestionMarkCircleIcon,
        subjectText: "Kiedy przegląd",
        kindIcon: ClockIcon,
        kindText: "Czekam 10 dni",
      }}
    />
  );
}
