import Waiting from "./template";
import {
  ChevronUpIcon,
  CalendarIcon,
  QuestionMarkCircleIcon,
  FolderIcon,
  HomeIcon,
  InboxIcon,
  UsersIcon,
  BriefcaseIcon,
  ClockIcon,
} from "@heroicons/react/outline";

export default function Example({ notification, setLookup }) {
  return (
    <Waiting
      property={notification.defects[0].property}
      notification={notification}
      setLookup={setLookup}
      config={{
        categoryIcon: BriefcaseIcon,
        subjectIcon: QuestionMarkCircleIcon,
        subjectText: "Czekaj na naprawe",
        kindIcon: ClockIcon,
        kindText: "Czekam 10 dni",
      }}
    />
  );
}
