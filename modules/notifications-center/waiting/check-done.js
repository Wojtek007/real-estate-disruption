import Waiting from "./template";
import {
  ChevronUpIcon,
  CalendarIcon,
  CheckCircleIcon,
  FolderIcon,
  HomeIcon,
  InboxIcon,
  UsersIcon,
  ClipboardCheckIcon,
  ClockIcon,
} from "@heroicons/react/outline";
import * as moment from "moment";

export default function Example({ notification, setLookup }) {
  return (
    <Waiting
      property={notification.previousCheckResult[0].checks[0].property}
      notification={notification}
      setLookup={setLookup}
      config={{
        categoryIcon: ClipboardCheckIcon,
        subjectIcon: CheckCircleIcon,
        subjectText: "Przegląd zaplanowany",
        kindIcon: CalendarIcon,
        kindText: moment(notification.appointments[0].date).format("L"),
      }}
    />
  );
}
