import { ChevronDoubleLeftIcon } from "@heroicons/react/solid";
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";
import {
  ChevronUpIcon,
  CalendarIcon,
  QuestionMarkCircleIcon,
  FolderIcon,
  HomeIcon,
  InboxIcon,
  UsersIcon,
  DocumentTextIcon,
  ClockIcon,
} from "@heroicons/react/outline";
const INSERT_NOTIFICATION = gql`
  mutation MyMutation($notification: versionedNotifications_insert_input!) {
    insertNotification(object: $notification) {
      id
    }
  }
`;

export default function Example({ notification, property, setLookup, config }) {
  const [updateNotification, { error, loading }] =
    useMutation(INSERT_NOTIFICATION);

  const handleUpdateNotification = () => {
    console.log("looookup", notification.id);
    setLookup(notification.id);
  };
  // !loading &&
  // updateNotification({
  //   variables: {
  //     notification: {
  //       id: notification.id,
  //       appointmentId: notification.appointment.id,
  //       appointmentVer: notification.appointment.ver,
  //       precedingPeriodId: notification.precedingPeriod.id,
  //       precedingPeriodVer: notification.precedingPeriod.id,
  //       reminderDate: notification.reminderDate,
  //     },
  //   },
  // });
  return (
    <div className="hover:bg-gray-50 relative flex flex-col bg-white rounded-2xl  border-white border-2">
      <div className="absolute left-2 top-0 px-1 py-1 inline-block bg-indigo-600 rounded-xl transform -translate-y-1/2">
        <div className="text-white group flex items-center text-xs font-normal rounded-md">
          <config.categoryIcon className="text-white flex-shrink-0 h-4 w-4" />
        </div>
      </div>
      <div className="absolute right-2 top-0 px-2 py-1 inline-block bg-indigo-600 rounded-xl transform -translate-y-1/2">
        <div className="text-white group flex items-center text-xs font-normal rounded-md">
          <span className="truncate">{config.kindText}</span>
          <config.kindIcon className="text-white flex-shrink-0 ml-2 -mr-1 h-4 w-4" />
        </div>
      </div>
      <div className="mt-3 mb-1 px-1">
        <a
          href="#"
          onClick={handleUpdateNotification}
          className=" hover:underline"
        >
          <p className="flex items-center px-3 py-1 text-xs font-medium rounded-md text-gray-600">
            {/*group hover:bg-gray-50 hover:text-gray-900*/}
            <config.subjectIcon className="text-gray-400 flex-shrink-0 -ml-1 mr-2 h-5 w-5" />
            {/*group-hover:text-gray-500 */}
            <span className="truncate">{config.subjectText}</span>
          </p>
        </a>
        <p className="flex items-center px-3 py-1 text-xs font-medium rounded-md text-gray-600">
          {/*group hover:bg-gray-50 hover:text-gray-900*/}
          <HomeIcon className="text-gray-400 flex-shrink-0 -ml-1 mr-2 h-5 w-5" />
          <span className="truncate">{property.name} / Tomasz Szukała</span>
        </p>
        {error?.message}
      </div>
    </div>
  );
}
