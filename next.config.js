const dotenv = require("dotenv");
const { PHASE_DEVELOPMENT_SERVER, PHASE_PRODUCTION_BUILD } = require("next/constants");

dotenv.config();

// module.exports = {
//   env: {
//     AUTH0_DOMAIN: process.env.AUTH0_DOMAIN,
//     AUTH0_AUDIENCE: process.env.AUTH0_AUDIENCE,
//     AUTH0_CLIENT_ID: process.env.AUTH0_CLIENT_ID,
//     AUTH0_CLIENT_SECRET: process.env.AUTH0_CLIENT_SECRET,
//     AUTH0_SCOPE: "openid profile",
//     REDIRECT_URI: process.env.REDIRECT_URI,
//     POST_LOGOUT_REDIRECT_URI: process.env.POST_LOGOUT_REDIRECT_URI,
//     SESSION_COOKIE_SECRET: process.env.SESSION_COOKIE_SECRET,
//     SESSION_COOKIE_LIFETIME: 7200, // 2 hours
//     APP_HOST: process.env.DOMAIN,
//     API_URL: process.env.API_URL,
//     WS_API_URL: process.env.WS_API_URL,
//   },
//   trailingSlash: false,
// };



module.exports = (phase) => {
  console.log('PHASE', phase)
  console.log('process.env.IS_PROD ', process.env.IS_PROD)
  const isDev = phase === PHASE_DEVELOPMENT_SERVER;
  const isProd = phase === PHASE_PRODUCTION_BUILD;

  return {
    env: {
      AUTH0_DOMAIN: process.env.AUTH0_DOMAIN,
      AUTH0_AUDIENCE: process.env.AUTH0_AUDIENCE,
      AUTH0_CLIENT_ID: process.env.AUTH0_CLIENT_ID,
      AUTH0_CLIENT_SECRET: process.env.AUTH0_CLIENT_SECRET,
      SESSION_COOKIE_SECRET: process.env.SESSION_COOKIE_SECRET,
      SESSION_COOKIE_LIFETIME: 7200, // 2 hours
      HASURA_URL: process.env.HASURA_URL,
      HASURA_ADMIN_SECRET: process.env.HASURA_ADMIN_SECRET,
      // DOMAIN: process.env.DOMAIN,

      REDIRECT_URI: isDev
        ? "http://localhost:3000/api/callback"
        : process.env.IS_PROD == "true"
          ? `http://hrigo.com/api/callback`
          : `http://${process.env.VERCEL_URL}/api/callback`,
      POST_LOGOUT_REDIRECT_URI: isDev
        ? "http://localhost:3000/"
        : process.env.IS_PROD == "true"
          ? `http://hrigo.com/`
          : `http://${process.env.VERCEL_URL}/`,
      AUTH0_SCOPE: "openid profile",
      SERVER_URL: isDev
        ? "http://localhost:3000"
        : process.env.IS_PROD == "true"
          ? `http://hrigo.com`
          : `http://${process.env.VERCEL_URL}`,
    },
    trailingSlash: false,
  };
}